/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.test.it;

import de.samply.auth.client.AuthClient;
import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.jwt.KeyLoader;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.ClientBuilder;

import static org.junit.Assert.assertTrue;

/**
 * Does some basic tests with the REST interface
 */
public class REST {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testRestApi() throws InvalidKeyException, InvalidTokenException {
        AuthClient client = new AuthClient(ITSuite.URL,
                KeyLoader.loadKey(ITSuite.getProvider().getPublicKey()), ITSuite.TEST_CLIENT_ID_2, ITSuite.TEST_CLIENT_SECRET_2,
                ClientBuilder.newClient());

        /**
         * The rest interface does not return the internal samply auth client
         */
        assertTrue(client.getClients().getClients().size() == 2);

        assertTrue(client.searchUser("").getUsers().size() == 1);

        assertTrue(client.getDiscovery() != null);

        /**
         * Try again with a code and a new access token
         */
        client = new AuthClient(ITSuite.URL, KeyLoader.loadKey(ITSuite.getProvider().getPublicKey()),
                "test-id-2", "test-secret-2", "code", ClientBuilder.newClient());

        assertTrue(client.getAccessToken() != null);

        assertTrue(client.searchUser("").getUsers().size() == 1);
    }

    @Test
    public void testFail() throws InvalidKeyException, InvalidTokenException {
        AuthClient client = new AuthClient(ITSuite.URL,
                KeyLoader.loadKey(ITSuite.getProvider().getPublicKey()), ITSuite.TEST_CLIENT_ID, ITSuite.TEST_CLIENT_SECRET,
                ClientBuilder.newClient());

        /**
         * should fail, the client is not allowed to use basic authentication
         */
        exception.expect(NotAuthorizedException.class);
        client.searchUser("");
    }

}
