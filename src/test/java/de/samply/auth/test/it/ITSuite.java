/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.test.it;

import de.samply.auth.AuthConfig;
import de.samply.common.config.OAuth2Provider;
import de.samply.common.config.ObjectFactory;
import de.samply.config.util.JAXBUtil;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;

/**
 * The Integration Test Suite
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({REST.class, RegistryRegistration.class})
public class ITSuite {

    static final String URL = "http://localhost:9080/";

    /**
     * This client is not allowed to use basic authentiction
     */
    public static final String TEST_CLIENT_ID = "test-id";
    public static final String TEST_CLIENT_SECRET = "test-secret";

    /**
     * This client is allowed to used basic authentication
     */
    public static final String TEST_CLIENT_ID_2 = "test-id-2";
    public static final String TEST_CLIENT_SECRET_2 = "test-secret-2";

    private static OAuth2Provider provider;

    @BeforeClass
    public static void start() throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException {
        provider = JAXBUtil.findUnmarshall(AuthConfig.OAUTH2_FILE, JAXBContext.newInstance(ObjectFactory.class,
                de.samply.common.mailing.ObjectFactory.class), OAuth2Provider.class);
    }

    public static OAuth2Provider getProvider() {
        return provider;
    }
}
