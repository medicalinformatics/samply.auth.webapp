/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.test.it;

import com.nimbusds.jose.JOSEException;
import de.samply.auth.ClientUtil;
import de.samply.auth.SecurityUtils;
import de.samply.auth.client.*;
import de.samply.auth.client.jwt.JWTAccessToken;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.*;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.InvalidKeyException;
import java.text.ParseException;

import static org.junit.Assert.assertTrue;

public class RegistryRegistration {

    @Test
    public void registerJersey() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, JWTException, JOSEException, ParseException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024);
        KeyPair keyPair = generator.generateKeyPair();

        RegistrationRequestDTO request = new RegistrationRequestDTO();
        request.setBase64EncodedPublicKey(Base64.encodeBase64String(keyPair.getPublic().getEncoded()));
        request.setContactData("Contact: NONE");
        request.setDescription("The OSSE Registery");
        request.setEmail(SecurityUtils.newRandomString(5) + "@mitro.dkfz-heidelberg.de");
        request.setName("OSSE Unit Test Register");

        Client client = ClientUtil.getClient();
        WebTarget target = client.target(ITSuite.URL + "oauth2/register");
        RegistrationDTO post = target.request().accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(request, MediaType.APPLICATION_JSON), RegistrationDTO.class);

        assert(post != null);
        assert(post.getKeyId() != 0);
        assert(post.getUserId() != 0);

        KeyIdentificationDTO keyIdent = new KeyIdentificationDTO();
        keyIdent.setKeyId(post.getKeyId());

        target = client.target(ITSuite.URL + "oauth2/sign_request");
        SignRequestDTO sign = target.request().accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(keyIdent, MediaType.APPLICATION_JSON), SignRequestDTO.class);

        Signature signature = Signature.getInstance(sign.getAlgorithm());
        signature.initSign(keyPair.getPrivate());
        signature.update(sign.getCode().getBytes(StandardCharsets.UTF_8));

        AccessTokenRequestDTO tokenReq = new AccessTokenRequestDTO();
        tokenReq.setSignature(Base64.encodeBase64String(signature.sign()));
        tokenReq.setCode(sign.getCode());

        target = client.target(ITSuite.URL + "oauth2/access_token");
        AccessTokenDTO accessTokenDTO = target.request().accept(MediaType.APPLICATION_JSON)
            .post(Entity.entity(tokenReq, MediaType.APPLICATION_JSON), AccessTokenDTO.class);

        assert(accessTokenDTO != null);

        JWTAccessToken access = new JWTAccessToken(KeyLoader.loadKey(ITSuite.getProvider().getPublicKey()), accessTokenDTO.getAccessToken());
        assert(access.isValid());

        LoginRequestDTO req = new LoginRequestDTO();
        req.setClientId(ITSuite.TEST_CLIENT_ID);

        target = client.target(ITSuite.URL + "oauth2/login");
        LoginDTO code = target.request(MediaType.APPLICATION_JSON).header("Authorization", access.getHeader())
            .post(Entity.entity(req, MediaType.APPLICATION_JSON), LoginDTO.class);

        PublicKey loadKey = KeyLoader.loadKey((String) access.getClaimsSet().getClaim(JWTVocabulary.KEY));

        assertTrue(loadKey.equals(keyPair.getPublic()));
        assertTrue(code != null);
    }

    @Test
    public void registerWithClient() throws NoSuchAlgorithmException, de.samply.auth.client.InvalidKeyException, InvalidTokenException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024);
        KeyPair keyPair = generator.generateKeyPair();

        RegistrationRequestDTO request = new RegistrationRequestDTO();
        request.setBase64EncodedPublicKey(Base64.encodeBase64String(keyPair.getPublic().getEncoded()));
        request.setContactData("Contact: NONE");
        request.setDescription("The OSSE Registery");
        request.setEmail(SecurityUtils.newRandomString(5) + "@mitro.dkfz-heidelberg.de");
        request.setName("OSSE Unit Test Register");

        AuthClient client = new AuthClient(ITSuite.URL,
                KeyLoader.loadKey(ITSuite.getProvider().getPublicKey()), keyPair.getPrivate(),
                ClientBuilder.newClient());

        RegistrationDTO register = client.register(request);

        assert(register != null);
        assert(register.getKeyId() != 0);
        assert(register.getUserId() != 0);

        JWTAccessToken accessToken = client.getAccessToken();
        assertTrue(accessToken != null);
    }

}
