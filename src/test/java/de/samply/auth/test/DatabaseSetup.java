/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.test;

import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Calendar;

import org.junit.Test;

import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.*;
import de.samply.auth.rest.Scope;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

/**
 * A simple database setup with one additional client, that is allowed to do basic authentication
 */
public class DatabaseSetup {

    /**
     * Creates two clients, one with disabled basicAuthentication and one with enabled basicAuthentication, and
     * one user. Also stores a request in the database.
     * @throws DAOException
     */
    @Test
    public void test() throws DAOException {
        try (AuthConnection auth = ConnectionFactory.get()) {
            Client testClient = new Client();
            testClient.setBasicAuthentication(false);
            testClient.setActive(true);
            testClient.setClientId("test-id");
            testClient.setClientSecret("test-secret");
            testClient.setDescription("NONE");
            testClient.setName("TestClient");
            testClient.setRedirectUrl("http://localhost:8080/");
            testClient.setTrusted(true);
            testClient.setType(ClientType.UNDEFINED);
            testClient.setWhitelist(false);

            auth.get(ClientDAO.class).saveClient(testClient);


            testClient = new Client();
            testClient.setBasicAuthentication(true);
            testClient.setActive(true);
            testClient.setClientId("test-id-2");
            testClient.setClientSecret("test-secret-2");
            testClient.setDescription("NONE");
            testClient.setName("TestClient2");
            testClient.setRedirectUrl("http://localhost:8080/");
            testClient.setTrusted(true);
            testClient.setType(ClientType.UNDEFINED);
            testClient.setWhitelist(false);

            auth.get(ClientDAO.class).saveClient(testClient);


            User user = new User();
            user.setEmailVerified(true);
            user.setEmail("mitro-unittest@mitro.dkfz-heidelberg.de");
            user.setName("MITRO Unittest");
            user.setActive(true);
            user.setContactData("NONE");
            user.setHashedPassword("!disabled");
            user.setSearchable(true);
            user.setRounds(0);
            user.setLanguage("de");
            user.setSalt("!disabled");
            user.setUsertype(Usertype.NORMAL);
            user.setEulaAccepted(false);
            user.setClientCertificate(false);

            auth.get(UserDAO.class).saveUser(user);



            Request request = new Request();
            request.setClientId(testClient.getId());
            request.setCode("code");

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, 5);

            request.setExpirationDate(new Timestamp(calendar.getTime().getTime()));
            request.setUserId(user.getId());

            JSONResource json = new JSONResource();
            json.setProperty(JWTVocabulary.SCOPE, Scope.OPENID.getIdentifier());

            request.setData(json);

            auth.get(RequestDAO.class).saveRequest(request);

            auth.commit();

            /**
             * There must be two clients: the samply auth internal client and the one
             * that has been added just now
             */
            assertTrue(auth.get(ClientDAO.class).getClients().size() == 3);
        } catch(DAOException e) {
            e.printStackTrace();
        }
    }

}
