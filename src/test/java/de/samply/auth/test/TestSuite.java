/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.test;


import static de.samply.auth.AuthConfig.POSTGRES_FILE;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.xml.sax.SAXException;

import de.samply.auth.AuthConfig;
import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.JAXBUtil;
import de.samply.sdao.DAOException;

/**
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(DatabaseSetup.class)
public class TestSuite {

    private static Postgresql database;

    @BeforeClass
    public static void start() throws FileNotFoundException, ParserConfigurationException, JAXBException, SAXException, SQLException, DAOException {

        try {
            database = JAXBUtil.findUnmarshall(POSTGRES_FILE, JAXBContext.newInstance(ObjectFactory.class,
                    de.samply.common.mailing.ObjectFactory.class), Postgresql.class);

            /**
             * Manually drop all tables in the database.
             */
            Connection connection = getConnection();
            connection.createStatement().execute("DROP OWNED BY \"" + database.getUsername() + "\"");
            connection.commit();

            AuthConfig.initialize(AuthConfig.getProjectName(), "");
        } catch(DAOException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://" + database.getHost() + "/" +
                database.getDatabase() +
                "?user=" + database.getUsername() +
                "&password=" + database.getPassword();

        Connection connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
        return connection;
    }

}
