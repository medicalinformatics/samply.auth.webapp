# Samply.Auth

## General information

Samply.Auth is the application for central authentication and authorization. It
stores the credentials of each user or delegates them to another external
identity provider, e.g. LDAP, ADFS, or Shibboleth. Samply.Auth combines those
external identity providers so that other applications can support Samply.Auth
without supporting all identity providers.

Samply.Auth implements parts of the [OpenID-Connect specificiation](http://openid.net/specs/openid-connect-core-1_0.html),
using signed JWT as Access tokens, ID tokens and refresh tokens.

## Features

- implements parts of OpenID Connect
- Supports local and external users (LDAP, ADFS, Shibboleth)
- configure the required password complexity for local users
- password recovery via email
- configurable clients, roles and permissions
- discovery service


## Build

Use maven to build the jar:

```
mvn clean install
```
