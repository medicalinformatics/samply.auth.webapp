# Basic Configuration

See the instructions for the installation to see the basic configuration via files.

# Advanced Configuration

Samply.Auth allows you to create clients, scopes, roles, permissions, external identity providers, mappings and locations.
You can create each of those in the admin interface, simply by accessing `/admin/`.


## Clients

The following attributes are required to create a new client:

- `Client Name`: The name of the client. Not relevant for the API.
- `Client Description`: The description of the client. Not relevant for the API.
- `Redirect URLs`: The valid redirect URLs. Samply.Auth will check, if the provided redirect URL via parameters, does start
  with one of those redirect URLs provided in this field.
- `Client ID`: A client ID. By default a random string, but you can modify it to your needs.
- `Client Secret`: The client secret. By default a random 64 characters long string. It is not recommended to change this value
- `Client Type`: The client type. Reserved. Used in OSSE EDC only.
- `Trusted`: If checked, the client is trusted and therefore the user does not need to approve the requested scopes.
  If unchecked, the user must approve the client when using the authentication mechanism.
- `Use whitelist`: If checked, only authorized users will be able to use this client. They must be member of at least one
  role with the `use` permission for this client.
- `Allow basic authentication`: If checked, allows the client to use basic authentication using its client ID and client secret
  for the API of Samply.Auth. If unchecked, an access token is necessary for all API calls in Samply.Auth.

The overview allows you to see the details of each client by clicking on `Details` in the menu. Furthermore the menu allows
you to disable clients. Disabled clients can not exchange codes for access tokens and generally can not use the authentication service.


It is not recommended to delete clients. Instead, they should be disabled. If you really want to delete a client,
disable it first, wait until the access tokens are not valid anymore (2 hours) and make sure the client is not part
of a mapping or role.

The colors of rows in the table represent the following state:

- `red`: Client is disabled
- `green`: Client is enabled



## Scopes

Scopes are artibrarily defined strings.



## Roles

The following attributes are required to create a new roles:

- `identifier`: Case sensitive identifier for this role
- `name`: Human readable name for this role
- `description`: Human readable description for this role
- `admin`: The admnistrator of the role

Roles can have members and administrators. An administrator is not automatically a member of the role, but he is able
to modify the role by adding new members or assigning new permissions. Permissions are always bound to a specific client, e.g.
`admin in client xyz`. Usually permissions are not included in the access token, except the checkbox `Export` is checked.
In any case, the permissions are included in the ID token.


There are two different types of administrators for roles: `admin` and `assign members`. The `admin` type allows the administrator
to assign new members *and* modify the permissions and admin of this role. The `assign members` type allows the administrator
to assign new members only.



## Permissions

Permissions are artbitrarily defined strings. The are two special permissions: `use` and `admin`. The `use` permission is
required for clients that use a whitelist. If a user wants to access a client that uses a whitelist, he must have the `use`
permission for this client. With the `admin` permission you can give a regular Samply.Auth user access to the admin interface
in Samply.Auth. Keep in mind, that regular users can not upgrade the database in case of an application update.


## External Identity providers

External identitiy providers give you the option to use external authentication methods, e.g. an LDAP or Active Directory.
Current Samply.Auth supports three different external identity providers: LDAP, ADFS and Shibboleth. You can enter a
Shibboleth federation like DFN-AAI and let Samply.Auth act as a middleman.


## Common configuration

The common configuration gives you several options:

- `Enable local accounts (Login)`: Enables the login for local accounts
- `Enable Registration (via form)`: Enables the form based registration in the web browser
- `Enable Registration (via REST)`: Enables the REST based registration
- `Activate new users by default`: New users will be activated by default and therefore are able to login right away
- `Enable Email Verification`: After the registration process an email is sent to verify the email address
- `Require Email Verification`: If checked, users without a verified email address are not able to login
- `Password Policy`: The settings allow you to defined the minimum security requirements for __new__ passwords
- `Banner`: Allows you to customize the banner and contact information
- `Enable EULA`: If checked, all users must agree to the EULA once they login or register.
- `Your EULA`: Your EULA. Use HTML.
- `Use external authorization for the admin interface`: If checked, it disables the interal authentication
  and authorization filter. This is meant to be used with a reverse proxy in front of the servlet container. The
  reverse proxy must authenticate and authorize the user via basic authentication, client certificates, kerberos,
  or other mechanisms.
