# Installation

## Requirements

Before you deploy this application, install and configure the required components:

* Postgresql (>=9.3)
    * With a user that has write-access to an empty database
* Tomcat (>=7.0)
    * the role `auth-admin` and a user with this role is required to access the admin interface (`/admin/`), use the `tomcat-users.xml` to configure the role and user
* JRE (>=7)
    * the application has not been tested with the JRE 8, it may work just as well

Here is a sample `tomcat-users.xml`

```xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
    <role rolename="auth-admin"/>
    <user username="admin" password="kdkjweijofwe12390yxc" roles="auth-admin"/>
</tomcat-users>
```

Configure the password to your needs.

### Distribution

The distribution file contains all necessary files for the deployment:

1. The actual `war` for the servlet container
2. example configuration files, that must be renamed (remove the `.example` part at the end of each file) and edited properly


### Configuration files location

The configuration files must be moved to a specific folder, depending on the operating system you are using.

#### Linux

Under Linux the application tries to find the configuration files in the `/etc/samply/` folder, so that the configuration files must be moved to:

* `/etc/samply/auth.postgres.xml`: the database configuration
* `/etc/samply/auth.oauth2.xml`: the central authentication configuration
* `/etc/samply/auth.mail.xml`: the mail SMTP configuration
* `/etc/samply/auth.templates/*.soy`: the email templates

#### Windows

Set the Registry Key `HKEY_LOCAL_MACHINE\SOFTWARE\SAMPLY\ConfDir` to a path on your hard drive, e.g. `C:\Samply\` and
put the following configuration files in there:

* `auth.postgres.xml`: the database configuration
* `auth.oauth2.xml`: the central authentication configuration
* `auth.mail.xml`: the mail SMTP configuration
* `auth.templates/*.soy`: the email templates

### Database (`auth.postgres.xml`)

Here is a sample database configuration file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<postgresql
    xmlns="http://schema.samply.de/config/PostgreSQL"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://schema.samply.de/config/PostgreSQL http://schema.samply.de/config/PostgreSQL.xsd">

    <host>localhost</host>

    <database>the database</database>

    <username>the database user</username>

    <password>the password for the database user</password>

</postgresql>
```

If your PostgreSQL server listens on a different port than the default port, you
can append the port in the `host` tag, e.g. `localhost:9586`. Please make sure,
that the database exists and the user has write access in this database (the
user must be able to create new tables).

### Central authentication (`auth.oauth2.xml`)

Your Samply.Auth instance needs an RSA private/public keypair in a format that Samply.Auth understands.
Do not use a passphrase to protect the keys, otherwise Samply.Auth can not read them. Instead you should protect the configuration
files itself, so that only the tomcat user can read the files. You are also strongly encouraged to not deploy other applications
on the same Tomcat server as Samply.Auth.

Generate a key pair using the following commands:

```
openssl genrsa -out samply.auth.key 4096

echo "Private key:"
openssl pkcs8 -topk8 -nocrypt -in samply.auth.key -outform DER | base64

echo "Public key:"
openssl rsa -in samply.auth.key -outform DER -pubout | base64

```

The first base64 encoded output is your private key, the second one is your public key.

Here is a sample authentication configuration file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<oAuth2Provider xmlns="http://schema.samply.de/config/OAuth2Provider"
    xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://schema.samply.de/config/OAuth2Provider http://schema.samply.de/config/OAuth2Provider.xsd ">
    <!-- THIS IS JUST A TEST PRIVATE KEY, YOU MUST GENERATE A NEW KEY! -->
    <privateKey>
MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCgEOoBwIpAYRvGewmuMp26aEwI
+Gk6S3FUvYj8/M2MLouNeie2exkqkROT1+VwnXTkW5/e1/a2BWUnwDFXYxq9NV5qrWJduJazZcyd
cyKx8Owl/vK60yqCuJoT3tfoYdgnTmhDFU/f/2tbEzO2KaybURljy8fmVjBwIlY0o0DgSe9Y/bsq
+G70MLIM0KXDgMw6nbyPLhjIaJU7NJJ1nuR3/ve3bUojWaD9N416fJDpoUAr5SaAQr1ONIHU5XW+
YHNtdqtDBu/FIRvKzal9O7YFDTyLhNAr++KllgsPHUEyMmpB8H7a5OuOBK1znVaLDcYwzx2yEWvp
lnvqTK/FvDi4gj8DOGx1zkR+0bej23HhQefJSuupPaP2w0pCfvNMzpduPwLThhppSi9mPty6AJp2
bXpYStjplup0SEKq8Lnzd7hxRF/eCtBKUxSqjsgD9wnrCiEBZQajScaux4s/CV1HSUHu688cXdzY
52YTfAV2JopmVTUvqMLiHNmlwXORUADkOqMVk/uSxfdDsT8G+gR7VGMxiqPk4s6XdBluLKw5o1ep
LHNF7yNqoOH/DWkMf940AQv2fzX9j98ezVUJjhGXAdgzMh4/aFAgMyhH+YEfNwJyAOJqvpwhcdFz
u0vafgEBphRIPYzAi73xdFCv7obzwA8L0ntDQPTss8sOleGk8wIDAQABAoICAHSpn0TDpoAhxvK0
vmt+bMNuPOzU0S0xy1ouAmgRgWbIjeIBxGwV8FO8BX7d/lZEDOxbc3wh/6jO6dk9FTlR/C0ndrdd
w7H3Va9PZQDk58a1iAhxd0x/mB5KmdIdu1Od7MNJPGiYYe0q5n4s0qXUXf+d7ll0LzBMZWV1QUuA
KUrP2GkOFZrOE2BbFYqNkCOb/j5CtSnOOTXoM8xBFvYvZummpbnFKCH3s4SbNrytiZkec/0KOTOG
cR9M2cvnqheliLuhoFVr+tm8tdrsZrnmCVZJPXWayXclUR3K7CiJ8/i4EgYZWPwFmUGbWyhG5srW
sTrl2gjs9a/8hDhSyFHgS53X+x9WFE49sLrjyxVRLPE2JzJwrDapmG4mGwNDXDz8HYzwTyPHZj/Z
IX+U4cbXcOo5nIpFYc5l/7XyFzyxIVZjDtY6coOnM2Mi6lOHDYGzx86W5ZpyeRv1Ow/4hGWv7wna
F98K8/7DoiX4GINL09IhTx0Zb1zDSYvXkA5HGh8MSYNnysWXLmkHBL+sFi+lTs2GENhYeldVJRqW
cWlP3pqGNfc0Y4MyCIF6xy9RQwFBpOp8yUVToafr9+2JOaPDpPcBrMpnPEsiT8Wy/mqYQN7f9H67
aYkESphk7oTTN9BC1Alx/px1lzvPyhuNZOxUztoHezFgu2Ogll+txbQ6Eg0RAoIBAQDOwA+Vupzu
xgXzOC+XGF2ri6N9zkVsdIaFxcCpkHzdTM8B50yGd6vlggn471oXkhzSMQNeI5cJmdUFikBYNEl5
IsUlFKnpmRAkqszxABU076DXc4dSWxd4f3V3tdi0wfbPxmJzMNF0Pt3mpplj9MTM5d+ptDC6rl0v
JGau2B0BnszQWe0QCTEGeJd4jHHEersQ+L8EO+ApksDJbKkoNqumOmvqxA0SixaRz5nutgTY9dCr
6BPrLcco/ML7aG2WnZqv2I5EEs+unRz9Fn15WlSkK027lhR2cdesHQBQiJbPfAYkpKQkGDAl7FXb
6LNxlKDDam4MoSDd4J1xZG7jO5elAoIBAQDGMfuX3tqsf7kgUqog0KcanlgzybB9l+KOAq7XHUmZ
tE4Qp96k8Vh/16oJcZ2PGifr67lZwx5wCn0Ov6s9WMlYXtlRKjiRpBN7//4WV4TvIrEFUymov4Nh
P9JF6qd9MgNIEeLfOZnAkcjTiy68+EZ5+ZmV/RD3aihKKMPaO/rmyCMiepCpJDaFjK29NKXOAyjo
Uw508QdaOsdfi8RxRHW6Obu6LAkURU1dBuoeLt+fwd/NYuaoRlz7NqTquLaxVjG6sP/Fx9oFjV4d
QSu9MtrRrZdp8ds4CQjOtDHkyJVxNKSFehFubQVpJF9C3d0df7wCVAbfTi+oPOEZqVwdwua3AoIB
AAOG5z0op9Cy9BsIFFfBhKeOhEV7JVgKTNkvXHAIFPk5fDaOGZIZrIcHxt/GRYUtauxzBZmvhb4W
L7oed6aJjc9RNofBaHyhrdLSTxi1Zac3h0jloaNYBnh5xCk8ouvCe1FkEv3gEVKoV+S9ZtR7snfE
XCJq/oHQw6owNsg4y5XhfaD4R8EERx/TItJdovs1FMffVnLeCp7q7/r/rYsQE7GMQZcJrSdp1AK8
xeV6mW6SEvM7zEswxrr+RN1BFM2C9saVLSggIgbai0oN/in5zoPWcUcH8mVq38EB5aV6DwiPFxO8
80fiWb/r3jMYLyd635s1z5bLkOH4GPd8PSAHPjkCggEAK9XcPp/Un7h0GpkN9coQW4vQGEwwk+am
/h/Lydo5cBx386kdTj/vAf+SJRqS7ZasY7q0OQ01yzQWqs9rsVrU1Y3M5O3Aqz2t7YwWB5z90uJK
YtIc8azCAx1ZhYs71i3cmfESy6pyvivATkGHadZDyDCTbqAVQ5OgK57OipMfkDGNy89t9WsEAA+4
UZO2ZFVAk/nixayVorhDZneEUEUDANhKFAgvTeNyE/Q2jcxyO2Rm+oc4WtmNMSbJ3PxTs+9Lem4x
c+4a44V73l6OrP71dNC/H3iVstQUtZsXOfG/U9V/2eHAvzqvqqyXwTNO93pL+/vXtUYWtlAYvVM/
A3obCwKCAQACywn4n5fhrsbXTVFH3a5aFkflWcYphYb2Alx7bnbxOYIRLkvTqB3UcRzbmsJeslh+
fJd48YOPm1bKBxaCJB348WXYhCCClTUXZqcve3k+g3MQSoKVv9n+E3IK0reM0fYr8O0AiXBAMAH8
6GaT0Bqm0hrmI1GYGt2t8qV0QbxV6v74qQwlTg13dcqpPxUwPm64XM4TlS1gS3f64p5PigNo+8iS
5E+WUiSiL7YmQRrOshLfOBZfzCMIM83x0OJwU166vvRzAECUCGV9Sfoi6Qg3GkR61QI+Be2nv3+d
mF7PEQ/2fiiCs0H7G1/1+8phr2vqVDNtgRtZcgoQhy1AWKvj
    </privateKey>
    <publicKey>
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAoBDqAcCKQGEbxnsJrjKdumhMCPhpOktx
VL2I/PzNjC6LjXontnsZKpETk9flcJ105Fuf3tf2tgVlJ8AxV2MavTVeaq1iXbiWs2XMnXMisfDs
Jf7yutMqgriaE97X6GHYJ05oQxVP3/9rWxMztimsm1EZY8vH5lYwcCJWNKNA4EnvWP27Kvhu9DCy
DNClw4DMOp28jy4YyGiVOzSSdZ7kd/73t21KI1mg/TeNenyQ6aFAK+UmgEK9TjSB1OV1vmBzbXar
QwbvxSEbys2pfTu2BQ08i4TQK/vipZYLDx1BMjJqQfB+2uTrjgStc51Wiw3GMM8dshFr6ZZ76kyv
xbw4uII/Azhsdc5EftG3o9tx4UHnyUrrqT2j9sNKQn7zTM6Xbj8C04YaaUovZj7cugCadm16WErY
6ZbqdEhCqvC583e4cURf3grQSlMUqo7IA/cJ6wohAWUGo0nGrseLPwldR0lB7uvPHF3c2OdmE3wF
diaKZlU1L6jC4hzZpcFzkVAA5DqjFZP7ksX3Q7E/BvoEe1RjMYqj5OLOl3QZbiysOaNXqSxzRe8j
aqDh/w1pDH/eNAEL9n81/Y/fHs1VCY4RlwHYMzIeP2hQIDMoR/mBHzcCcgDiar6cIXHRc7tL2n4B
AaYUSD2MwIu98XRQr+6G88APC9J7Q0D07LPLDpXhpPMCAwEAAQ==
    </publicKey>
    <issuer>https://your-server.your-domain.tld/</issuer>
</oAuth2Provider>
```

The issuer value should be the base URL of your application. In case your private key got compromised, you can
replace the private and public key, but you must also give your clients the new public key. In any case, the issuer value
must not be changed.

#### Email (`auth.mail.xml)

Samply.Auth uses emails to send invite links, requests for email verification, requests for password resets, and other things. Those settings are
configured via the `auth.mail.xml` file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ns1:mailSending
        xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
        xmlns:ns1="http://schema.samply.de/config/MailSending"
        xsi:schemaLocation="http://schema.samply.de/config/MailSending">
    <ns1:host>mailhost.dkfz-heidelberg.de</ns1:host>
    <ns1:protocol>smtp</ns1:protocol>
    <ns1:fromAddress>noreply@www12.inet.dkfz-heidelberg.de</ns1:fromAddress>
    <ns1:fromName>Samply.Authentication</ns1:fromName>
    <ns1:port>25</ns1:port>
    <ns1:user/>
    <ns1:password/>
    <ns1:templateFolder>auth.templates/</ns1:templateFolder>
</ns1:mailSending>
```

The `host`, `protocol`, `port`, `user` (optional, leave empty if not in use), `password` (optional, leave empty if not in use) values are used to create
the connection to the SMTP server. The `fromAddress` is the email address that will be used to send emails.

The final `templateFolder` value points to a relative or absolute path on the hard drive, where all the email templates are stored. The email template
files are SOY files, that you can customize to your needs. Use the default SOY files, if you do not want to customize the emails send by your Samply.Auth
instance.


## Deployment

Copy the `war` file into the `webapps` folder of your Tomcat installation and rename it according to your needs, e.g. `ROOT.war`.

See [this website](https://tomcat.apache.org/tomcat-8.0-doc/config/context.html) for more informations about context containers in tomcat.

Start the server.

## Usage

Open your web browser and navigate to the application, e.g. `$HOST/`. The admin interface is available under the `/admin/` path,
e.g. `$HOST/admin` or `http://localhost:8080/admin/`.