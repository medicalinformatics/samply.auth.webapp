# Changelog

## Version 1.12.0

- Split name into first name / last name for flexible representation of display name
- Only NORMAL users are now visible in User View
- minor changes

## Version 1.11.0

- added the possibility to use client specific terms of use
- added the administration view for editing terms of view
- replaced the existing EULA with terms of view for Auth Client
- added case insensivity to role identifier for correct role comparison in SharePoint
- extended role mapping by adding the possibility to use Regex
- minor changes in GUI

## Version 1.10.0

- added email templates:
  use the `auth.mail.xml` configuration file to configure the mailing settings. Use the Soy-Templates to configure
  the emails send for verification, password recovery and invitation
- added invitations, that can be send using the interface for roles. The invited user is automatically trusted, meaning
  his account is enabled by default, regardless of external identity provider settings or default settings. An
  invitation can be used only once for registration purposes, afterwards the invitation only redirects authenticated
  users to the target URL.
- added a new attribute for external identity providers: `defaultUserVerified`. If false, the user has to verify his email
  if required. If true, the email is assumed to be verified by the external identity provider.
- added the option the enable an EULA, an End-User-License-Agreement. The EULA itself is configurable through the
  admin interface. Users must agree to the EULA, if they want to use a client
- added the option to enable external authorization for the admin interface. This option also disables the internal
  authentication in the servlet container. This is meant to be used with a reverse proxy in front of the
  servlet container, so the reverse proxy can decide which authentication mechanism is used, e.g. client certificates, Kerberos,
  Basic Authentication, LDAP, etc.
- added support for shibboleth authentication, even if the name and email address are not supplied by the identity provider
  by letting the user verify his email address
- added test page for shibboleth authentification which provides the information about received attributes,
  the debug information is also sent per email to admin
- added optional authentication cookies. The user can now decide for how long he will be logged in by using a cookie, that is
  stored in the database.
- added a debug view which lists all currently open requests, the application version as well as the build branch and timestamp
- only trusted clients can now request an access token with the login scope
- the user list in the admin interface now contains a form for new local users
- added login via client certification
- added new external Identity Provider (SQL Identity Provider), which allows integrating the existing user databases
- added invitation overview to edit role page
- fixed the openid connect compatibility
- fixed redirect bug
- fixed bug with multiple persistent id in DFN-AAI login accounts
- several bug fixes in login with LDAP identity provider

## Version 1.9.4

- fixed a bug in LDAP client by adding the check for empty passwords

## Version 1.9.2

- fixed a minor bug in the admin interface, which ignored the new client secret value

## Version 1.9.0

- added locations with identifiers, name, description and contactfield
- added '/locations' resource for a list of all locations
- when getting an access token via private key, the ID token is returned as well
- minor changes

## Version 1.8.0

- Renamed the configuration file to `auth.oauth2.xml`
- the demo ribbon now depends on the system property `de.samply.development`,
  set the property to `true` to show the ribbon
- added the first name attribute for external identity providers
- added better support for external identity providers
- added an identifier for external identity providers
- added password policies
- added datatables in the admin interface
- added the internal Samply.Auth Client. This client is the application itself.
  It allows defining admins in the application without tomcat.
- added support for Shibboleth
- added the state parameter in the REST interface

## Version 1.7.1

- Added the `/oauth2/userinfo` resource

## Version 1.7.0

- Added a new attribute for users: usertype
- Added permissions and roles
- Added role mapping between external roles and internal roles
- Added first site documentation

## Version 1.6.0

- Added a new column in `samplyUser` table: `rounds` represents the number of
  rounds used in the hashing algorithm
- Uses the `type` attribute in JWTs
- issuer configuration string in the oauth2provider config file
- switched to Apache Commons Hex
- The stylesheets and javascript files are now loaded by script and link tags,
  with an additional version parameter to prevent caching across different
  versions
- added the base64 (NOT base64URL) encoded public key in the /oauth2/certs
  REST-resource
- Added support for external identity providers (LDAP/AD and ADFS)

