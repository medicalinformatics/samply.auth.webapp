function enablePasswordCheck() {
    $("#registerform\\:password").keyup(function(event) {
        var errors = $(".password-requirement.text-danger").length;

        if(errors == 0) {
            $("#registerform\\:password").closest(".form-group").removeClass().addClass("form-group");
        } else {
            $("#registerform\\:password").closest(".form-group").removeClass().addClass("form-group has-error");
        }

        checkPasswordRepeat();
    });

    $("#registerform\\:passwordRepeat").keyup(function(event) {
        checkPasswordRepeat();
    });
}

function checkPasswordRepeat() {
    var password = $("#registerform\\:password").val();
    var passwordRepeat = $("#registerform\\:passwordRepeat").val();

    if(password == passwordRepeat) {
        $("#registerform\\:passwordRepeat").closest(".form-group").removeClass().addClass("form-group");
    } else {
        $("#registerform\\:passwordRepeat").closest(".form-group").removeClass().addClass("form-group has-error");
    }
}

function enablePasswordRegex(selector, regex, count) {
    $("#registerform\\:password").keyup(function(event) {
        updatePasswordClasses($(this).val(), selector, regex, count);
    });

    updatePasswordClasses($("#registerform\\:password").val(), selector, regex, count);
}

function updatePasswordClasses(input, selector, regex, count) {
    if(getCount(input, regex) < count) {
        $(selector).removeClass().addClass("password-requirement text-danger");
    } else {
        $(selector).removeClass().addClass("password-requirement text-success");
    }
}

function getCount(input, regex) {
    var count = 0;
    var regx = XRegExp(regex);

    XRegExp.forEach(input, regx, function(match, i) {
        ++count;
    });
    return count;
}

/**
 * Enables select2 for all select fields with the select2-class
 */
function enableSelect2() {
    $("select.select2").select2({
        minimumResultsForSearch : 10
    });
}

/**
 * opens the details of a row in a datatable.
 * @param button
 * @param id
 */
function showTableRow(button, id) {
    var tr = $(button).closest("tr");
    var row = table.row(tr);
    var details = $("#details #details-" + id).clone();
    row.child(details).show();
}

/**
 * Closes the details of a row in a datatable.
 * @param element
 */
function closeTableRow(element) {
    var tr = $(element).closest("tr").prev();
    var row = table.row(tr);
    row.child.hide();
}
