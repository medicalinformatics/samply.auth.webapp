--
-- Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
-- Deutsches Krebsforschungszentrum in Heidelberg
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see http://www.gnu.org/licenses.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

CREATE TABLE "eula" (id SERIAL PRIMARY KEY,
  "eulaType" TEXT NOT NULL,
  "version" INTEGER NOT NULL,
  "title" JSON NOT NULL,
  "htmlText" JSON,
  "checkBoxLabel" JSON NOT NULL);

CREATE TABLE "userEula" ("userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "eulaId" INTEGER NOT NULL REFERENCES "eula" ("id"),
    "date" TIMESTAMP NOT NULL,

    PRIMARY KEY ("userId", "eulaId"));

CREATE INDEX ON "userEula" ("userId");
CREATE INDEX ON "userEula" ("eulaId");

CREATE TABLE "clientEula" ("clientId" INTEGER NOT NULL REFERENCES "client" ("id"),
    "eulaId" INTEGER NOT NULL REFERENCES "eula" ("id"),

    PRIMARY KEY ("clientId", "eulaId"));

CREATE INDEX ON "clientEula" ("clientId");
CREATE INDEX ON "clientEula" ("eulaId");

INSERT INTO "eula" ("eulaType", version, title, "htmlText", "checkBoxLabel") VALUES ('samply.Auth', 1, '{}', '{}', '{}');

INSERT INTO "userEula" ("userId", "eulaId", "date")
  SELECT id, 1, '1970-01-01 00:00:00'::timestamp from "samplyUser" WHERE "eulaAccepted" = TRUE;
