--
-- Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
-- Deutsches Krebsforschungszentrum in Heidelberg
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see http://www.gnu.org/licenses.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--


CREATE TYPE "administrationMode" AS ENUM ('MEMBERS', 'ADMIN');

CREATE TYPE "providerType" AS ENUM ('LDAP', 'ADFS');


CREATE TABLE "identityProvider" (id SERIAL PRIMARY KEY NOT NULL,
    "label" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "type" "providerType" NOT NULL,
    "emailAttribute" TEXT NOT NULL,
    "nameAttribute" TEXT NOT NULL,
    "idAttribute" TEXT NOT NULL,
    "memberAttribute" TEXT NOT NULL,

    /* ADFS */
    "publicKey" TEXT,
    "resource" TEXT,
    "clientId" TEXT,

    /* LDAP */
    "searchBase" TEXT,
    "baseDN" TEXT,
    "anonymous" BOOLEAN,
    "searchFilter" TEXT,
    "bindDN" TEXT,
    "bindPassword" TEXT);


CREATE TABLE "role" (id SERIAL PRIMARY KEY NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL);


CREATE TABLE "permission" (id SERIAL PRIMARY KEY NOT NULL,
    "name" TEXT NOT NULL UNIQUE);


CREATE TABLE "rolePermission" ("id" SERIAL PRIMARY KEY NOT NULL,
    "roleId" INTEGER NOT NULL REFERENCES "role" ("id"),
    "permissionId" INTEGER NOT NULL REFERENCES "permission" ("id"),
    "clientId" INTEGER NOT NULL REFERENCES "client" ("id"));


CREATE INDEX ON "rolePermission" ("id");


CREATE TABLE "roleMapping" (id SERIAL PRIMARY KEY NOT NULL,
    "identityProviderId" INTEGER NOT NULL REFERENCES "identityProvider" ("id"),
    "roleId" INTEGER NOT NULL REFERENCES "role" ("id"),
    "roleName" TEXT NOT NULL);

CREATE INDEX ON "roleMapping" ("identityProviderId");
CREATE INDEX ON "roleMapping" ("roleId");


CREATE TABLE "userRoles" ("userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "roleId" INTEGER NOT NULL REFERENCES "role" ("id"),
    "derived" BOOLEAN NOT NULL DEFAULT FALSE,

    PRIMARY KEY ("userId", "roleId", "derived"));

CREATE INDEX ON "userRoles" ("userId");
CREATE INDEX ON "userRoles" ("roleId");


CREATE TABLE "userAdminRoles" ("userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "roleId" INTEGER NOT NULL REFERENCES "role" ("id"),
    "administrationMode" "administrationMode" NOT NULL,

    PRIMARY KEY ("userId", "roleId"));



ALTER TABLE "client" ADD COLUMN "whitelist" BOOLEAN;

UPDATE "client" SET "whitelist" = false;

ALTER TABLE "client" ALTER COLUMN "whitelist" SET NOT NULL;



INSERT INTO "permission" ("name") VALUES ('use');

ALTER TABLE "samplyUser" RENAME COLUMN "config" TO "data";



ALTER TABLE "samplyUser" ADD COLUMN "externalId" TEXT;

UPDATE "samplyUser" SET "externalId" = NULL;


ALTER TABLE "samplyUser" ADD COLUMN "providerId" INTEGER REFERENCES "identityProvider" ("id");

UPDATE "samplyUser" SET "providerId" = NULL;


