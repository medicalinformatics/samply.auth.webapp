--
-- Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
-- Deutsches Krebsforschungszentrum in Heidelberg
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see http://www.gnu.org/licenses.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--


INSERT INTO client (name, description, "clientId", "clientSecret", "redirectUrl", active, trusted, type) VALUES (
    'MDR', 'Local MDR',
    '81johack6k6op',
    'ent650sq00magel754oq3ar3idormcvou3v5o26j4j9oh82493p1jerl40lm7iee8rjpn89ia1av5uarekivaenadl4l07irjf3n4v',
    'http://localhost:8080/mdr-gui,https://localhost/,https://apache/,https://tomcat/', true, true, 'MDR');
