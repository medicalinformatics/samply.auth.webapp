--
-- Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
-- Deutsches Krebsforschungszentrum in Heidelberg
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see http://www.gnu.org/licenses.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--


CREATE TYPE "clientType" AS ENUM ('MDR', 'FORMREPOSITORY', 'UNDEFINED');

/* We can not name this table "user", because
   it conflicts with the PSQL "user" table */
CREATE TABLE "samplyUser" (id SERIAL PRIMARY KEY,
    "email" TEXT NOT NULL UNIQUE,
    "hashedPassword" TEXT NOT NULL,
    "salt" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL DEFAULT FALSE,
    "config" JSON NOT NULL,
    "contactData" TEXT NOT NULL,
    "verified" BOOLEAN NOT NULL);

CREATE TABLE "recoverCode" ("userId" INTEGER PRIMARY KEY REFERENCES "samplyUser" ("id"),
    "code" TEXT NOT NULL UNIQUE,
    "expirationDate" TIMESTAMP NOT NULL);

CREATE TABLE "activationCode" ("userId" INTEGER PRIMARY KEY REFERENCES "samplyUser" ("id"),
    "code" TEXT NOT NULL UNIQUE,
    "expirationDate" TIMESTAMP NOT NULL);

CREATE TABLE "client" (id SERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE,
    "description" TEXT NOT NULL,
    "clientId" TEXT NOT NULL UNIQUE,
    "clientSecret" TEXT NOT NULL,
    "redirectUrl" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL,
    "trusted" BOOLEAN NOT NULL DEFAULT FALSE,
    "type" "clientType" NOT NULL);

CREATE TABLE "scope" (id SERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE,
    "title_en" TEXT NOT NULL,
    "description_en" TEXT NOT NULL,
    "title_de" TEXT NOT NULL,
    "description_de" TEXT NOT NULL,
    "icon" TEXT NOT NULL);

CREATE TABLE "request" (id SERIAL PRIMARY KEY,
    "clientId" INTEGER NOT NULL REFERENCES "client" ("id"),
    "userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "data" JSON NOT NULL,
    "code" TEXT NOT NULL UNIQUE,
    "expirationDate" TIMESTAMP NOT NULL);

CREATE TABLE "accessToken" (id SERIAL PRIMARY KEY,
    "userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "token" TEXT NOT NULL UNIQUE,
    "expirationDate" TIMESTAMP NOT NULL);

CREATE TABLE "remember" (id SERIAL PRIMARY KEY,
    "userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "clientId" INTEGER NOT NULL REFERENCES "client" ("id"));

CREATE TABLE "key" (id SERIAL PRIMARY KEY,
    "base64EncodedKey" TEXT NOT NULL UNIQUE,
    "description" TEXT NOT NULL,
    "userId" INTEGER NOT NULL REFERENCES "samplyUser" ("id"),
    "sha512Hash" TEXT NOT NULL UNIQUE);

CREATE TABLE "signRequest" (id SERIAL PRIMARY KEY,
    "keyId" INTEGER NOT NULL REFERENCES "key" ("id"),
    "code" TEXT NOT NULL,
    "expirationDate" TIMESTAMP NOT NULL,
    "algorithm" TEXT NOT NULL);

CREATE TABLE "refreshToken" (id SERIAL PRIMARY KEY,
    "clientId" INTEGER REFERENCES "client" ("id"),
    "userId" INTEGER REFERENCES "samplyUser" ("id"),
    "token" TEXT NOT NULL UNIQUE,
    "uuid" TEXT NOT NULL UNIQUE);

CREATE TABLE "config" (id SERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE,
    "value" JSON NOT NULL);

CREATE INDEX ON "recoverCode" ("userId");
CREATE INDEX ON "activationCode" ("userId");
CREATE INDEX ON "request" ("clientId");
CREATE INDEX ON "request" ("userId");
CREATE INDEX ON "accessToken" ("userId");
CREATE INDEX ON "remember" ("userId");
CREATE INDEX ON "remember" ("clientId");
CREATE INDEX ON "key" ("userId");
CREATE INDEX ON "signRequest" ("keyId");
CREATE INDEX ON "refreshToken" ("clientId");
CREATE INDEX ON "refreshToken" ("userId");

INSERT INTO "config" ("name", "value") VALUES ('defaultConfig',
    '{
        "dbVersion":8,
        "registration":true,
        "restRegistration":true,
        "defaultUser":false,
        "verifyEmail":true,
        "verificationRequired":true,
        "mtaServer":"mail.imbei.uni-mainz.de",
        "emailFrom":"Samply.Auth <auth.noreply@osse-register.de>"
    }');
INSERT INTO "config" ("name", "value") VALUES ('config', '{}');


INSERT INTO scope (name, title_en, description_en, title_de, description_de, icon) VALUES ('openid',
    'Identity', 'Allows the application to identity you',
    'Identität', 'Erlaubt der Applikation sie zu identifizieren', '<i class="fa fa-eye fa-fw"></i>');

INSERT INTO scope (name, title_en, description_en, title_de, description_de, icon) VALUES ('mdr',
    'MDR', 'Allows the application to access your data on the MDR',
    'MDR', 'Erlaubt der Applikation den Zugriff auf ihre Daten im MDR', '<i class="fa fa-database fa-fw"></i>');

INSERT INTO scope (name, title_en, description_en, title_de, description_de, icon) VALUES ('formrepository',
    'Form Repository', 'Allows the application to access your data on the Form Repository',
    'Form Repository', 'Erlaubt der Applikation den Zugriff auf ihre Daten im Form Repository', '<i class="fa fa-clipboard fa-fw"></i>');

INSERT INTO scope (name, title_en, description_en, title_de, description_de, icon) VALUES ('login',
    'Login', 'Allows the application to request a login for you in other applications',
    'Login', 'Erlaubt der Applikation sie auf anderen Applikationen einzuloggen', '<i class="fa fa-sign-in fa-fw"></i>');
