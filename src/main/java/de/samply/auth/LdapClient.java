/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;

/**
 * Creates a new LDAP Client.
 */
public class LdapClient implements AutoCloseable {

    /**
     * The host URL, e.g. "ldap://my-organization.com"
     */
    private final String url;

    /**
     * The Base DN, e.g. "dc=my-organization,dc=com"
     */
    private final String baseDn;

    /**
     * The bind DN and bind password.
     */
    private final String bindDn, bindPassword;

    private DirContext context = null;

    /**
     * Initializes a new LDAP client that will use the given url, base DN, bind DN and bind password.
     * Authentication method will be 'simple'.
     * @param url
     * @param baseDn
     * @param bindDn
     * @param bindPassword
     */
    public LdapClient(String url, String baseDn, String bindDn, String bindPassword) {
        this.url = url;
        this.baseDn = baseDn;
        this.bindDn = bindDn;
        this.bindPassword = bindPassword;
    }

    /**
     * Initializes the LdapClient for an anonymous bind.
     * @param url
     * @param baseDn
     */
    public LdapClient(String url, String baseDn) {
        this.url = url;
        this.baseDn = baseDn;
        this.bindDn = null;
        this.bindPassword = null;
    }

    /**
     * Tries to bind with the current credentials. Returns true, if the bind was successful, false otherwise.
     * @return
     */
    public boolean checkBind() {
        StringBuilder builder = new StringBuilder(url);

        if(!url.endsWith("/")) {
            builder.append("/");
        }
        builder.append(baseDn);

        Hashtable<String, String> env = new Hashtable<>();

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, builder.toString());

        // To get rid of the PartialResultException when using Active Directory
        env.put(Context.REFERRAL, "follow");

        if(bindDn != null && bindPassword != null) {
            // Needed for the Bind (User Authorized to Query the LDAP server)
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, bindDn);
            env.put(Context.SECURITY_CREDENTIALS, bindPassword);
        } else {
            env.put(Context.SECURITY_AUTHENTICATION, "none");
        }

        try {
            context = new InitialDirContext(env);
            return true;
        } catch(NamingException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Searches the LDAP with the given filter and on the given search base. Returns the first result only.
     * @param searchBase
     * @param filter
     * @return
     */
    public SearchResult search(String searchBase, String filter) {
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search Entire Subtree
        controls.setCountLimit(1);   //Sets the maximum number of entries to be returned as a result of the search
        controls.setTimeLimit(5000); // Sets the time limit of these SearchControls in milliseconds

        try {
            NamingEnumeration<SearchResult> results = context.search(searchBase, filter, controls);
            if(results.hasMore()) {
                SearchResult result = results.next();
                results.close();
                return result;
            } else {
                results.close();
                return null;
            }
        } catch (NamingException e) {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see java.lang.AutoCloseable#close()
     */
    @Override
    public void close() throws NamingException {
        if(this.context != null) {
            this.context.close();
        }
    }

}
