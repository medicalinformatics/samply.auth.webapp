/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.regex.*;

/**
 * A connection to the external SQL database (postgreqsl, mysql)
 */
public class SqlConnection {

    /**
     * The host URL, e.g. "postgresql://localhost:5432/mainzelliste"
     */
    private final String url;

    /**
     * The driver name, e.g. "org.postgresql.Driver" for PostgreSQL
     */
    private final String sqlDriver;

    /**
     * The bind DN, bind password and query
     */
    private final String username, password, query;

    private Connection conn;
    private ResultSet rs;

    /**
     * Constructor for SqlConnection
     *
     * @param url a {@link String} object.
     * @param sqlDriver a {@link String} object.
     * @param username a {@link String} object.
     * @param password a {@link String} object.
     * @param query a {@link String} object.
     */
    public SqlConnection(String url, String sqlDriver, String username, String password, String query) {
        this.url = url;
        this.sqlDriver = sqlDriver;
        this.username = username;
        this.password = password;
        this.query = query;
    }

    /**
     * Connects to SQL DB
     * @return
     */
    public boolean connectToSql() throws SQLException {
        String loginUrl = "jdbc:" + url;

        try {
            Class.forName(sqlDriver);

            if (loginUrl.endsWith("/")) {
                loginUrl = loginUrl.substring(0, loginUrl.length()-1);
            }

            conn = DriverManager.getConnection(loginUrl, username, password);
            if(conn != null) {
                return true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Searches SQL DB for a user given email and password.
     * Depending on query (user with no/one group or user with multiple group), one or more results possible
     * @param email
     * @param password
     * @return ResultSet
     */
    public boolean searchUser(String email, String password) throws SQLException {
        // search for all placeholders in the query
        String regex = "(\\{email\\}|\\{password\\})";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(query);

        // add parameters to query
        String queryText = query.replace("{email}", "?");
        queryText = queryText.replace("{password}", "?");
        PreparedStatement stmt = conn.prepareStatement(queryText, rs.TYPE_SCROLL_SENSITIVE, rs.CONCUR_READ_ONLY);

        // pass in parameters depending on placeholder type
        int i = 1;
        while(matcher.find()) {
            if(matcher.group().equals("{email}")){
                stmt.setString(i, email);
            } else {
                stmt.setString(i, password);
            }
            i++;
        }

        rs = stmt.executeQuery();
        return rs.next();
    }

    /**
     * Returns the name (or lastname if there are separated fields for firstname/lastname)
     */
    public String getName() throws SQLException{
        return NameUtils.getDisplayName(getFirstName(), getLastName());
    }

    /**
     * Returns the firstname (empty string if there is no first name field)
     */
    public String getFirstName() throws SQLException{
        try {
            return rs.getString("firstname");
        } catch (SQLException e) {
            return "";
        }
    }

    /**
     * Returns the lastname
     */
    public String getLastName() throws SQLException{
        try {
            return rs.getString("lastname");
        } catch (SQLException e) {
            return "";
        }
    }

    /**
     * Returns the email
     */
    public String getEmail() throws SQLException{
        try {
            return rs.getString("email");
        } catch (SQLException e) {
            return "";
        }
    }

    /**
     * Returns the username
     */
    public String getUsername() throws SQLException{
        try {
            return rs.getString("username");
        } catch (SQLException e) {
            return "";
        }
    }

    /**
     * Returns the list of groups for a user (if no groups, returns empty list)
     */
    public List<String> getGroups() throws SQLException{
        List<String> groups = new ArrayList<String>();
        String groupName;
        try {
            rs.beforeFirst();
            while (rs.next()) {
                groupName = rs.getString("member");
                // No group found for a user
                if(groupName == null) {
                    return Collections.emptyList();
                }
                if (!groups.contains(groupName)) {
                    groups.add(groupName);
                }
            }
            return groups;
        } catch (SQLException e) {
            return Collections.emptyList();
        }
    }

    /**
     * Closes SQL DB connection
     * @return
     */
    public void close() throws SQLException{
        conn.close();
    }
}
