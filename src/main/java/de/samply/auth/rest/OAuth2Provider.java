/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nimbusds.jose.JOSEException;

import de.samply.auth.AuthConfig;
import de.samply.auth.SecurityUtils;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.*;
import de.samply.auth.dao.dto.Usertype;
import de.samply.auth.jwt.AbstractJWT;
import de.samply.auth.jwt.JWTAccessToken;
import de.samply.auth.jwt.JWTIDToken;
import de.samply.auth.jwt.JWTRefreshToken;
import de.samply.auth.utils.HashUtils;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;
import de.samply.sdao.json.Value;

/**
 * The REST interface for the OAuth2 workflow.
 *
 */
@Path("/")
public class OAuth2Provider {

    private static final Logger logger = LoggerFactory.getLogger(OAuth2Provider.class);

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{path:((token)|(access_token))}")
    public AccessTokenDTO getAccessToken(MultivaluedMap<String,String> multivaluedMap) {
        if(!multivaluedMap.containsKey("grant_type") ||
                (!multivaluedMap.getFirst("grant_type").equals("code") && !multivaluedMap.getFirst("grant_type").equals("authorization_code")) ||
                !(
                    (multivaluedMap.containsKey("client_secret") && multivaluedMap.containsKey("client_id") && multivaluedMap.containsKey("code")) ||
                    (multivaluedMap.containsKey("signature")) ||
                    (multivaluedMap.containsKey("refresh_token"))
                )) {
            throw new BadRequestException();
        } else {
            AccessTokenRequestDTO dto = new AccessTokenRequestDTO();
            dto.setClientId(multivaluedMap.getFirst("client_id"));
            dto.setClientSecret(multivaluedMap.getFirst("client_secret"));
            dto.setCode(multivaluedMap.getFirst("code"));
            dto.setRefreshToken(multivaluedMap.getFirst("refresh_token"));
            dto.setSignature(multivaluedMap.getFirst("signature"));
            return getAccessToken(dto);
        }
    }

    /**
     * Exchange a code or a signed code for an OAuth2 access token and possibly an OpenID token
     *
     * @param request a {@link de.samply.auth.rest.AccessTokenRequestDTO} object.
     * @return a {@link de.samply.auth.rest.AccessTokenDTO} object.
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{path:((token)|(access_token))}")
    public AccessTokenDTO getAccessToken(AccessTokenRequestDTO request) {
        if(request == null) {
            logger.error("Request is empty, bad request!");
            throw new BadRequestException();
        }

        if(request.getSignature() != null) {
            return getAccessTokenBySignature(request);
        } else if(request.getCode() != null) {
            return getAccessTokenByCode(request);
        } else {
            return getAccessTokenByRefreshToken(request);
        }
    }

    /**
     * Returns an access token, if the refresh token is valid (= signature) and has not been revoked by the user.
     * Does not generate a new refresh token.
     * @param request
     * @return the access token.
     */
    private AccessTokenDTO getAccessTokenByRefreshToken(
            AccessTokenRequestDTO request) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            de.samply.auth.client.jwt.JWTRefreshToken refreshToken =
                    new de.samply.auth.client.jwt.JWTRefreshToken(AuthConfig.getPublicKey(), request.getRefreshToken());

            if(!refreshToken.isValid()) {
                throw new ClientErrorException(Status.FORBIDDEN);
            } else {
                RefreshToken rToken = auth.get(RefreshTokenDAO.class).getRefreshToken(refreshToken.getClaimsSet().getJWTID(), request.getClientId(),
                        request.getClientSecret());

                if(rToken == null) {
                    throw new NotFoundException();
                }

                Client client = auth.get(ClientDAO.class).getClient(rToken.getClientId());
                User user = auth.get(UserDAO.class).getUser(rToken.getUserId());

                if(user.getActive()) {
                    List<RolePermission> permissions = auth.get(RoleDAO.class).getRolePermissionsForUser(user.getId());
                    List<Key> userKeys = auth.get(KeyDAO.class).getUserKeys(user.getId());
                    List<Role> roles = auth.get(RoleDAO.class).getRoles(user.getId());
                    List<Location> locations = auth.get(LocationDAO.class).getLocations(user.getId());

                    JWTAccessToken accessTokenJWT = new JWTAccessToken(rToken.getUserId(), refreshToken.getScopes(), permissions, null);
                    JWTIDToken idToken = new JWTIDToken(user, userKeys, roles, locations, permissions, client.getClientId(), null);

                    AccessToken token = new AccessToken();
                    token.setUserId(rToken.getUserId());
                    token.setAccessToken(accessTokenJWT.getSerialized());
                    token.setExpirationDate(new Timestamp(accessTokenJWT.getExpirationTime().getTime()));
                    auth.get(AccessTokenDAO.class).saveAccessToken(token);

                    AccessTokenDTO dto = new AccessTokenDTO();
                    dto.setAccessToken(token.getAccessToken());
                    dto.setIdToken(idToken.getSerialized());
                    dto.setRefreshToken(rToken.getToken());
                    dto.setExpiresIn(AbstractJWT.VALIDITY_HOURS * 3600);

                    auth.commit();
                    return dto;
                } else {
                    throw new ClientErrorException(Status.UNAUTHORIZED);
                }
            }
        } catch (DAOException | JOSEException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        } catch (JWTException e) {
            e.printStackTrace();
            logger.error("Refresh token not valid!");
            throw new BadRequestException();
        }
    }

    /**
     * Returns an access token if the code has been signed properly.
     * Currently this returns an access token with four scopes:
     *
     * "mdr", "formrepository", "login", "openid"
     *
     * @param request
     * @return the access token
     * @throws DAOException
     */
    private AccessTokenDTO getAccessTokenBySignature(AccessTokenRequestDTO request) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            SignRequestDAO signRequestDao = auth.get(SignRequestDAO.class);
            SignRequest signRequest = signRequestDao.getSignRequest(request.getCode());

            if(signRequest == null) {
                throw new NotFoundException();
            } else {
                Key key = auth.get(KeyDAO.class).getKey(signRequest.getKeyId());
                User user = auth.get(UserDAO.class).getUser(key.getUserId());

                boolean valid = SecurityUtils.isSignatureValid(signRequest.getCode(),
                        key.getBase64EncodedKey(), request.getSignature(), signRequest.getAlgorithm());

                /**
                 * If the user which owns this key, is disabled or the the signature is invalid, return 401
                 */
                if(valid && user.getActive()) {
                    List<Key> userKeys = auth.get(KeyDAO.class).getUserKeys(user.getId());
                    List<RolePermission> permissions = auth.get(RoleDAO.class).getRolePermissionsForUser(user.getId());
                    List<Role> roles = auth.get(RoleDAO.class).getRoles(user.getId());
                    List<Location> locations = auth.get(LocationDAO.class).getLocations(user.getId());

                    signRequestDao.deleteSignRequest(signRequest.getId());

                    JWTAccessToken accessTokenJWT = new JWTAccessToken(key.getUserId(),
                            Arrays.asList("mdr", "formrepository", "login", "openid"), permissions,
                            KeyLoader.loadKey(key.getBase64EncodedKey()));

                    JWTIDToken idToken = new JWTIDToken(user, userKeys, roles, locations, permissions, "", null);

                    AccessToken token = new AccessToken();
                    token.setUserId(key.getUserId());
                    token.setAccessToken(accessTokenJWT.getSerialized());
                    token.setExpirationDate(new Timestamp(accessTokenJWT.getExpirationTime().getTime()));

                    auth.get(AccessTokenDAO.class).saveAccessToken(token);

                    AccessTokenDTO dto = new AccessTokenDTO();
                    dto.setAccessToken(token.getAccessToken());
                    dto.setIdToken(idToken.getSerialized());
                    dto.setIdToken(null);
                    dto.setExpiresIn(AbstractJWT.VALIDITY_HOURS * 3600);

                    auth.commit();
                    return dto;
                } else {
                    throw new ClientErrorException(Status.UNAUTHORIZED);
                }
            }
        } catch (DAOException | JOSEException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * A client (in OAuth2 terminology) requests to exchange a code for an OAuth2 access token, using
     * his client secret.
     * @param request
     * @return the access token.
     */
    private AccessTokenDTO getAccessTokenByCode(AccessTokenRequestDTO request) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            RequestDAO requestDao = auth.get(RequestDAO.class);
            Request req = requestDao.getRequest(request.getCode(), request.getClientId(), request.getClientSecret());

            if(req == null) {
                logger.error("Request not found, code unknown!");
                throw new NotFoundException();
            } else {
                Client client = auth.get(ClientDAO.class).getClient(req.getClientId());
                User user = auth.get(UserDAO.class).getUser(req.getUserId());

                requestDao.deleteRequest(req);

                /**
                 * If the client is *not* trusted, check the scoped for the login scope.
                 * Only trusted clients can request the login scope! This is already checked in the grant bean, but better
                 * safe than sorry, in the end the access token is generated here.
                 */
                if(!client.getTrusted()) {
                    for(Value scope : req.getData().getProperties(JWTVocabulary.SCOPE)) {
                        if(scope.getValue().equals(Scope.LOGIN.getIdentifier())) {
                            logger.error("A non-trusted client has requests the login scope, aborting");
                            throw new ForbiddenException();
                        }
                    }
                }

                if(user.getActive()) {
                    List<Key> userKeys = auth.get(KeyDAO.class).getUserKeys(user.getId());
                    List<RolePermission> permissions = auth.get(RoleDAO.class).getRolePermissionsForUser(user.getId());
                    List<Role> roles = auth.get(RoleDAO.class).getRoles(user.getId());
                    List<Location> locations = auth.get(LocationDAO.class).getLocations(user.getId());
                    Value nonce = req.getData().getProperty(Vocabulary.NONCE);

                    JWTAccessToken accessTokenJWT = new JWTAccessToken(req, permissions);
                    JWTIDToken idToken = new JWTIDToken(user, userKeys, roles, locations, permissions, client.getClientId(),
                            (nonce == null ? null : nonce.getValue()));
                    JWTRefreshToken refreshToken = new JWTRefreshToken(req);

                    AccessToken token = new AccessToken();
                    token.setUserId(req.getUserId());
                    token.setAccessToken(accessTokenJWT.getSerialized());
                    token.setExpirationDate(new Timestamp(accessTokenJWT.getExpirationTime().getTime()));
                    auth.get(AccessTokenDAO.class).saveAccessToken(token);

                    RefreshToken rToken = new RefreshToken();
                    rToken.setClientId(client.getId());
                    rToken.setUserId(user.getId());
                    rToken.setToken(refreshToken.getSerialized());
                    rToken.setUuid(refreshToken.getJwtid());
                    auth.get(RefreshTokenDAO.class).saveRefreshToken(rToken);

                    AccessTokenDTO dto = new AccessTokenDTO();
                    dto.setAccessToken(token.getAccessToken());
                    dto.setIdToken(idToken.getSerialized());
                    dto.setRefreshToken(rToken.getToken());
                    dto.setExpiresIn(AbstractJWT.VALIDITY_HOURS * 3600);

                    auth.commit();
                    return dto;
                } else {
                    throw new ClientErrorException(Status.UNAUTHORIZED);
                }
            }
        } catch (DAOException | JOSEException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * <p>Requests a code that the client can sign using his private key in order to exchange the signed
     * code for an access token.</p>
     *
     * @param keyIdent a {@link de.samply.auth.rest.KeyIdentificationDTO} object.
     * @return a {@link de.samply.auth.rest.SignRequestDTO} object.
     * @param path a {@link java.lang.String} object.
     */
    @POST
    @Path("/{path:((signRequest)|(sign_request))}")
    @Produces(MediaType.APPLICATION_JSON)
    public SignRequestDTO getSignRequest(KeyIdentificationDTO keyIdent, @PathParam("path") String path) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            if(keyIdent == null) {
                throw new NotFoundException();
            }

            KeyDAO keyDao = auth.get(KeyDAO.class);
            Key key;

            if(keyIdent.getKeyId() != 0) {
                key = keyDao.getKey(keyIdent.getKeyId());
            } else {
                key = keyDao.getKey(keyIdent.getSha512Hash());
            }

            if(key == null) {
                throw new NotFoundException();
            }

            SignRequest request = new SignRequest();
            request.setKeyId(key.getId());
            request.setCode(SecurityUtils.newRandomString());
            request.setExpirationDate(new Timestamp(new Date().getTime() + 1000 * 60*2));
            request.setAlgorithm("SHA512withRSA");

            auth.get(SignRequestDAO.class).saveSignRequest(request);
            auth.commit();

            SignRequestDTO dto = new SignRequestDTO();
            dto.setCode(request.getCode());
            dto.setExpirationDate(request.getExpirationDate().getTime());
            dto.setAlgorithm(request.getAlgorithm());

            return dto;
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Registers the registry in this application.
     *
     * @param request a {@link de.samply.auth.rest.RegistrationRequestDTO} object.
     * @return a {@link de.samply.auth.rest.RegistrationDTO} object.
     */
    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public RegistrationDTO register(RegistrationRequestDTO request) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            if(request == null) {
                throw new BadRequestException();
            }

            if(!AuthConfig.getRestRegistrationEnabled()) {
                throw new ServerErrorException(501);
            }

            UserDAO userDao = auth.get(UserDAO.class);

            User user = userDao.getInternalUser(request.getEmail());

            if(user != null) {
                List<Key> userKeys = auth.get(KeyDAO.class).getUserKeys(user.getId());

                RegistrationDTO conflict = new RegistrationDTO();
                conflict.setUserId(user.getId());

                if(userKeys.size() > 0 && userKeys.get(0).getBase64EncodedKey().equals(request.getBase64EncodedPublicKey())) {
                    conflict.setKeyId(userKeys.get(0).getId());
                    return conflict;
                } else {
                    throw new ClientErrorException(Status.CONFLICT);
                }
            }

            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decodeBase64(request.getBase64EncodedPublicKey()));

            KeyFactory kf;
            PublicKey pubKey;
            try {
                kf = KeyFactory.getInstance("RSA");
                pubKey = kf.generatePublic(spec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                throw new BadRequestException();
            }

            JSONResource config = new JSONResource();
            config.setProperty("name", request.getName());
            config.setProperty("lang", "en");

            user = new User();
            user.setActive(AuthConfig.getDefaultUserEnabled());
            user.setEmail(request.getEmail());
            user.setData(config);
            user.setHashedPassword("!disabled");
            user.setSalt("!disabled");
            user.setContactData(request.getContactData());
            user.setEmailVerified(false);
            user.setClientCertificate(false);
            user.setEulaAccepted(true);

            if(request.getUsertype() == null) {
                user.setUsertype(Usertype.OSSE_REGISTRY);
            } else {
                user.setUsertype(Usertype.valueOf(request.getUsertype()));
            }

            userDao.saveUser(user);

            Key key = new Key();
            key.setBase64EncodedKey(request.getBase64EncodedPublicKey());
            key.setUserId(user.getId());
            key.setSha512Hash(HashUtils.SHA512(pubKey.getEncoded()));
            key.setDescription(request.getDescription());

            auth.get(KeyDAO.class).saveKey(key);

            /**
             * Create a location for this OSSE user... but only for OSSE users and bridgehead users.
             */
            if(user.getUsertype() == Usertype.BRIDGEHEAD || user.getUsertype() == Usertype.OSSE_REGISTRY) {
                Location location = new Location();
                location.setContact(user.getEmail());
                location.setDescription(request.getDescription());
                location.setIdentifier("OSSE-" + user.getId());
                location.setName(request.getName());

                auth.get(LocationDAO.class).saveLocation(location);

                List<User> members = new ArrayList<>();
                members.add(user);
                auth.get(LocationDAO.class).updateMembers(location, members);

                List<LocationAdministration> admins = new ArrayList<>();
                LocationAdministration admin = new LocationAdministration();
                admin.setLocation(location);
                admin.setUser(user);
                admin.setMode(AdministrationMode.ADMIN);
                admins.add(admin);

                auth.get(LocationDAO.class).updateAdmins(location, admins);
            }

            auth.commit();

            RegistrationDTO dto = new RegistrationDTO();
            dto.setKeyId(key.getId());
            dto.setUserId(user.getId());

            return dto;
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }
}
