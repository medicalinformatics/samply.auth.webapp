/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import java.util.ArrayList;
import java.util.List;

import de.samply.auth.AuthConfig;
import de.samply.auth.dao.dto.Location;
import de.samply.auth.dao.dto.Role;
import de.samply.auth.dao.dto.User;

/**
 * A Helper class that maps between POJOs from the database and data transfer
 * objects for the REST interface.
 *
 *
 */
public final class Mapper {

    /**
     * Maps a list of user POJOs to a list of user DTOs.
     * @param users
     * @return
     */
    public static List<UserDTO> convert(List<User> users) {
        List<UserDTO> target = new ArrayList<>();
        for(User user : users) {
            target.add(convert(user));
        }
        return target;
    }

    /**
     * Maps a user POJO to a user DTO.
     * @param user
     * @return
     */
    public static UserDTO convert(User user) {
        UserDTO dto = new UserDTO();
        dto.setEmail(user.getEmail());
        dto.setName(user.getName());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmailVerified(user.getEmailVerified());
        dto.setId(AuthConfig.getSubjectPrefix() + user.getId());
        dto.setContactInformation(user.getContactData());

        dto.setExternalLabel(AuthConfig.getProviderLabel(user));

        return dto;
    }

    /**
     * Maps the given locations to location DTOs
     * @param locations
     * @return
     */
    public static LocationListDTO convertLocations(List<Location> locations) {
        LocationListDTO dto = new LocationListDTO();
        List<LocationDTO> target = new ArrayList<>();

        for(Location location : locations) {
            LocationDTO locTarget = new LocationDTO();
            locTarget.setId(location.getIdentifier());
            locTarget.setContact(location.getContact());
            locTarget.setDescription(location.getDescription());
            locTarget.setName(location.getName());
            target.add(locTarget);
        }

        dto.setLocations(target);
        return dto;
    }

    /**
     * Maps the given role to role DTO
     * @param role
     * @return
     */
    public static RoleDTO convertRole(Role role) {
        RoleDTO roleTarget = new RoleDTO();
        roleTarget.setName(role.getName());
        roleTarget.setDescription(role.getDescription());
        roleTarget.setIdentifier(role.getIdentifier());
        return roleTarget;
    }

    /**
     * Maps the given roles to role DTOs
     * @param roles
     * @return
     */
    public static RoleListDTO convertRoles(List<Role> roles) {
        RoleListDTO dto = new RoleListDTO();
        List<RoleDTO> target = new ArrayList<>();

        for(Role role : roles) {
            target.add(convertRole(role));
        }

        dto.setRoles(target);
        return dto;
    }
}
