/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.Client;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.glassfish.hk2.api.Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ext.Provider;

/**
 * The Client factory checks if basic authentication has been used and if the client
 * is allowed to do so. If so, the factory gets the client from the database and returns it.
 */
@Provider
class ClientFactory implements Factory<Client> {

    private Client client = null;

    private final static Logger logger = LoggerFactory.getLogger(ClientFactory.class);

    /**
     * Checks if the Authorization header contains basic authentication. If so, it tries to find the
     * client in the database and checks, if everything is alright.
     *
     * @param request the HTTP request object
     */
    @Inject
    public ClientFactory(HttpServletRequest request) {
        String basicAuthentication = request.getHeader("Authorization");

        try {
            if(basicAuthentication == null) {
                client = null;
            } else {
                String[] inp = basicAuthentication.split(" ");

                if(inp[0].equals("Basic")) {
                    basicAuthentication = basicAuthentication.replaceAll("^Basic\\s+", "").replaceAll("\\s+", "");
                } else {
                    client = null;
                    basicAuthentication = null;
                }
            }

            if(basicAuthentication != null) {
                String data = new String(Base64.decodeBase64(basicAuthentication), Charsets.UTF_8);

                String[] fields = data.split(":");

                if(fields.length != 2) {
                    throw new BadRequestException();
                } else {
                    logger.debug("Basic Authentication is used, checking");
                    try(AuthConnection auth = ConnectionFactory.get()) {
                        Client client = auth.get(ClientDAO.class).getClient(fields[0]);
                        if(client == null || !client.getActive() || !client.getClientSecret().equals(fields[1]) || !client.getBasicAuthentication()) {
                            throw new BadRequestException();
                        } else {
                            logger.debug("Basic Authentication looks good");
                            this.client = client;
                        }
                    }
                }
            }
        } catch(Exception | Error e) {
            client = null;
        }
    }

    @Override
    public Client provide() {
        return client;
    }

    @Override
    public void dispose(Client instance) {

    }
}
