/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;

import de.samply.auth.AuthConfig;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ScopeDAO;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Scope;
import de.samply.sdao.DAOException;

/**
 * The OpenID Connect discovery service implementation
 *
 */
@Path("/")
public class DiscoveryHandler {

    /**
     * Returns the public OAuth2 configuration.
     * That includes all known clients, all known scopes, the URL to
     * get the public key, etc.
     *
     * @param context the servlet context
     * @param request the HTTP request
     * @return the current OAuth2 configuration
     */
    @GET
    @Path("/.well-known/openid-configuration")
    @Produces(MediaType.APPLICATION_JSON)
    public OAuth2Discovery getOAuth2Config(@Context ServletContext context, @Context HttpServletRequest request) {
        String host = request.getServerName();
        String base = request.getScheme() + "://" + host + ":" + request.getServerPort() + context.getContextPath() + "/oauth2/";
        OAuth2Discovery config = new OAuth2Discovery();
        config.setIssuer(AuthConfig.getIssuer());
        config.setAuthorizationEndpoint(base + "authorize");
        config.setTokenEndpoint(base + "access_token");
        config.setScopesSupported(new ArrayList<String>());
        config.setJwksUri(base + "certs");
        config.setResponseTypesSupported(Collections.singletonList("code"));
        config.setApplication("Samply.Auth");
        config.setVersion(ServletUtil.getVersion(context));
        config.setTokenEndpointAuthMethodsSupported(Arrays.asList("client_secret_post"));

        try(AuthConnection auth = ConnectionFactory.get()) {
            for(Scope s : auth.get(ScopeDAO.class).getAllScopes()) {
                config.getScopesSupported().add(s.getName());
            }
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }

        return config;
    }

    /**
     * Returns the public key used in this OAuth2 application.
     *
     * @return the list of public keys used in this Samply Auth instance
     */
    @GET
    @Path("/certs")
    @Produces(MediaType.APPLICATION_JSON)
    public OAuth2Keys getOAuth2Keys() {
        OAuth2Keys keys = new OAuth2Keys();
        OAuth2Key key = new OAuth2Key();

        key.setKeyId("1");
        key.setUse("sig");
        key.setKeyType("RSA");
        PublicKey pub = AuthConfig.getPublicKey();
        RSAPublicKey rsa = (RSAPublicKey) pub;

        key.setN(Base64.encodeBase64URLSafeString(rsa.getModulus().toByteArray()));
        key.setE(Base64.encodeBase64URLSafeString(rsa.getPublicExponent().toByteArray()));
        key.setDerFormat(Base64.encodeBase64URLSafeString(rsa.getEncoded()));
        key.setBase64DerFormat(Base64.encodeBase64String(rsa.getEncoded()));

        keys.setKeys(Collections.singletonList(key));

        return keys;
    }

    /**
     * Returns all active clients.
     *
     * @param request the HTTP request
     * @return the list of clients currently active in this Samply Auth instance
     */
    @GET
    @Path("/clients")
    @Produces(MediaType.APPLICATION_JSON)
    public ClientListDTO getActiveClients(@Context HttpServletRequest request) {
        String domain = ServletUtil.getDomain(request.getServerName());

        try(AuthConnection auth = ConnectionFactory.get()) {
            List<ClientDescriptionDTO> target = new ArrayList<>();
            for(Client c : auth.get(ClientDAO.class).getClients()) {
                if(c.getActive()) {
                    ClientDescriptionDTO dto = new ClientDescriptionDTO();
                    dto.setClientId(c.getClientId());
                    dto.setName(c.getName());
                    dto.setRedirectUrl(c.getRedirectUrl(domain));
                    dto.setDescription(c.getDescription());
                    dto.setType(ClientType.valueOf(c.getType().toString()));
                    target.add(dto);
                }
            }
            return new ClientListDTO(target);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }

}
