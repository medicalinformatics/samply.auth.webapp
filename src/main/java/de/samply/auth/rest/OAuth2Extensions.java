/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nimbusds.jwt.JWTClaimsSet;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWTAccessToken;
import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.*;
import de.samply.sdao.DAOException;

/**
 * Those OAuth2 Extensions include:
 *
 * - Obtain a code used in the OAuth2 flow using an already obtained access_token (with "login" scope)
 * - validate an access token and return all fields in this access token
 *
 */
@Path("/")
public class OAuth2Extensions {

    @Inject
    private JWTAccessToken accessToken;

    @Inject
    private Client client;

    private final static Logger logger = LoggerFactory.getLogger(OAuth2Extensions.class);

    /**
     * Request a code for a specific client
     *
     * Requires a valid access token with a "login" scope
     *
     * @param request the login request that identified the client where the user wants to login
     * @param servletRequest the HTTP servlet request
     * @throws de.samply.sdao.DAOException when any database error occurs
     * @return the login DTO with the code and redirect URL.
     */
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public LoginDTO getLoginCode(LoginRequestDTO request, @Context HttpServletRequest servletRequest) throws DAOException {
        if(accessToken == null) {
            throw new NotAuthorizedException("Not authorized");
        }

        if(!accessToken.getScopes().contains(Scope.LOGIN.getIdentifier())) {
            throw new ForbiddenException();
        }

        if(request == null) {
            throw new BadRequestException();
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            Client client = auth.get(ClientDAO.class).getClient(request.getClientId());
            String domain = ServletUtil.getDomain(servletRequest.getServerName());

            if(client == null) {
                logger.error("Client for login not found, aborting");
                throw new NotFoundException();
            }

            if(!client.getActive()) {
                logger.error("Client for login has been disabled, aborting");
                throw new NotFoundException();
            }

            if(!client.getTrusted()) {
                logger.error("Client for login is not trusted, aborting");
                throw new ForbiddenException();
            }

            AccessToken token = auth.get(AccessTokenDAO.class).getAccessToken(accessToken.getSerialized());

            User user = auth.get(UserDAO.class).getUser(token.getUserId());

            if(user == null || !user.getActive()) {
                throw new NotFoundException();
            }

            Request req = new Request();
            req.setClientId(client.getId());
            req.setData(Request.getDataResourceWithString(accessToken.getScopes()));
            req.setUserId(token.getUserId());

            auth.get(RequestDAO.class).saveRequest(req);
            auth.commit();

            LoginDTO dto = new LoginDTO();
            dto.setCode(req.getCode());
            dto.setRedirectUrl(client.getRedirectUrl(domain));
            return dto;
        }
    }


    /**
     * Returns the informations contained in the access token used
     * for this request.
     *
     * @return a {@link de.samply.auth.rest.TokenInfoDTO} object.
     */
    @GET
    @Path("/tokeninfo")
    @Produces(MediaType.APPLICATION_JSON)
    public TokenInfoDTO getTokenInfo() {
        if(accessToken == null) {
            throw new NotAuthorizedException("Not authorized");
        }

        TokenInfoDTO dto = new TokenInfoDTO();
        JWTClaimsSet claims = accessToken.getClaimsSet();
        dto.setExpirationDate(claims.getExpirationTime().getTime() / 1000);
        dto.setIssuedAt(claims.getIssueTime().getTime() / 1000);
        dto.setIssuer(claims.getIssuer());
        dto.setJti(claims.getJWTID());
        dto.setNotBefore(claims.getNotBeforeTime().getTime() / 1000);
        dto.setScope(accessToken.getScopes());
        dto.setSubject(claims.getSubject());
        return dto;
    }

    /**
     * Returns informations about the user for the given access token.
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/userinfo")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO getUserInfo() throws DAOException {
        if(accessToken == null) {
            throw new NotAuthorizedException("Not authorized");
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            String subId = accessToken.getSubject().replace(AuthConfig.getSubjectPrefix(), "");
            int id = 0;
            try {
                id = Integer.parseInt(subId);
            } catch(NumberFormatException e) {
                throw new BadRequestException();
            }

            User user = auth.get(UserDAO.class).getUser(id);

            List<Location> locations = auth.get(LocationDAO.class).getLocations(id);
            List<Role> roles = auth.get(RoleDAO.class).getRoles(id);

            UserDTO userDto = Mapper.convert(user);

            userDto.setLocations(Mapper.convertLocations(locations).getLocations());
            userDto.setRoles(Mapper.convertRoles(roles).getRoles());

            return userDto;
        }
    }

    /**
     * Returns a list of users that either have a similar username or a similar email as query.
     * @param query the search string
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/users/search")
    @Produces(MediaType.APPLICATION_JSON)
    public UserListDTO searchUsers(@QueryParam("query") String query) throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Basic");
        }

        UserListDTO dto = new UserListDTO();
        try (AuthConnection auth = ConnectionFactory.get()) {
            dto.setUsers(Mapper.convert(auth.get(UserDAO.class).searchUser(query)));
        }

        return dto;
    }

    /**
     * Returns a list of currently available locations.
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/locations")
    @Produces(MediaType.APPLICATION_JSON)
    public LocationListDTO getLocations() throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Not authorized");
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            return Mapper.convertLocations(auth.get(LocationDAO.class).getLocations());
        }
    }

    /**
     * Returns a list of currently available roles (without the users)
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/roles")
    @Produces(MediaType.APPLICATION_JSON)
    public RoleListDTO getRoles() throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Not authorized!", "Basic", "OpenID-Connect");
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            return Mapper.convertRoles(auth.get(RoleDAO.class).getRoles());
        }
    }

    /**
     * Returns the details for the user with the given id
     * @param userId
     * @return
     */
    @GET
    @Path("/users/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO getUser(@PathParam("userId") String userId) throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Basic");
        }

        try {
            Integer id = Integer.parseInt(userId);
            try(AuthConnection auth = ConnectionFactory.get()) {
                User user = auth.get(UserDAO.class).getUser(id);

                if(user == null) {
                    throw new NotFoundException();
                }

                UserDTO dto = Mapper.convert(user);
                dto.setRoles(Mapper.convertRoles(auth.get(RoleDAO.class).getRoles(id)).getRoles());
                dto.setLocations(Mapper.convertLocations(auth.get(LocationDAO.class).getLocations(id)).getLocations());
                return dto;
            }
        } catch(NumberFormatException e) {
            throw new BadRequestException();
        }
    }


    /**
     * Returns the user identified by its subject.
     * @param subject
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO getUserBySubject(@QueryParam("sub") String subject) throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Basic");
        }

        Pattern idPattern = Pattern.compile("^" + Pattern.quote(AuthConfig.getSubjectPrefix()) + "(\\d+)$");

        Matcher matcher = idPattern.matcher(subject);
        if(matcher.find()) {
            return getUser(matcher.group(1));
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Returns the role with the given identifier.
     * @param roleIdentifier
     * @return
     * @throws DAOException
     */
    @GET
    @Path("/roles/{roleIdentifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public RoleDTO getRoleDetails(@PathParam("roleIdentifier") String roleIdentifier) throws DAOException {
        if(accessToken == null && client == null) {
            throw new NotAuthorizedException("Basic");
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            Role role = auth.get(RoleDAO.class).getRole(roleIdentifier);

            if(role == null) {
                throw new NotFoundException();
            }

            RoleDTO roleDTO = Mapper.convertRole(role);
            roleDTO.setMembers(Mapper.convert(auth.get(UserDAO.class).getAllMembers(role)));
            return roleDTO;
        }
    }
}
