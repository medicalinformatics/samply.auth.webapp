/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.glassfish.hk2.api.Factory;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWTAccessToken;

/**
 * The AccessTokenFactory uses the client side JWT tokens and checks their validity.
 *
 * This class should be used in REST resources with the \@Inject annotation.
 *
 */
@Provider
public class AccessTokenFactory implements Factory<JWTAccessToken> {

    private JWTAccessToken accessToken = null;

    /**
     * Checks if the Authorization header contains the access token and
     * if so, it checks if it is valid and returns the JWTAccessToken.
     *
     * @param request the HTTP request object
     */
    @Inject
    public AccessTokenFactory(HttpServletRequest request) {
        String auth = request.getHeader("Authorization");

        try {
            if(auth == null) {
                auth = request.getParameter("access_token");
            } else {
                String[] inp = auth.split(" ");

                if(inp[0].equals("Bearer")) {
                    auth = auth.replaceAll("^Bearer\\s+", "").replaceAll("\\s+", "");
                } else {
                    throw new BadRequestException();
                }
            }

            if(auth != null) {
                accessToken = new JWTAccessToken(AuthConfig.getPublicKey(), auth);

                if(!accessToken.isValid()) {
                    throw new ClientErrorException(Status.UNAUTHORIZED);
                }
            }
        } catch(Exception | Error e) {
            accessToken = null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public JWTAccessToken provide() {
        return accessToken;
    }

    /** {@inheritDoc} */
    @Override
    public void dispose(JWTAccessToken instance) {
    }

}
