/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Some helper methods for security
 *
 */
public class SecurityUtils {

    private static final Logger logger = LoggerFactory.getLogger(SecurityUtils.class);

    public enum PasswordStatus {
        OK, MIN_LENGTH, MIN_UPPER, MIN_LOWER, MIN_NUMBER, MIN_SPECIAL
    }

    private static Pattern lowerPattern = Pattern.compile("\\p{Ll}", Pattern.UNICODE_CHARACTER_CLASS);

    private static Pattern upperPattern = Pattern.compile("\\p{Lu}", Pattern.UNICODE_CHARACTER_CLASS);

    private static Pattern numberPattern = Pattern.compile("\\d", Pattern.UNICODE_CHARACTER_CLASS);

    private static Pattern specialPattern = Pattern.compile("[^\\p{L}\\d]", Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * Generates a new 512 big strong random string
     *
     * @return a {@link java.lang.String} object.
     */
    public synchronized static String newRandomString() {
        return newRandomString(64);
    }

    /**
     * Generates a new random string with the given length.
     *
     * @return a {@link java.lang.String} object.
     */
    public synchronized static String newRandomString(int length) {
        return new BigInteger(length * 8, new SecureRandom()).toString(32);
    }

    /**
     * Hashes the password and the salt with the PBKDF2WithHmacSHA1 algorithm
     *
     * @param password a {@link java.lang.String} object.
     * @param salt a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String hashPassword(String password, String salt, int rounds) {
        char[] chars = password.toCharArray();

        try {
            PBEKeySpec spec = new PBEKeySpec(chars, salt.getBytes(StandardCharsets.UTF_8), rounds, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory
                    .getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return Hex.encodeHexString(hash);
        } catch(Exception e) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Checks if the base64 encoded signature is valid for the input and base64 encoded public key
     *
     * @param input a {@link java.lang.String} object.
     * @param base64EncodedPublicKey a {@link java.lang.String} object.
     * @param base64EncodedSignature a {@link java.lang.String} object.
     * @param algorithm a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean isSignatureValid(String input, String base64EncodedPublicKey, String base64EncodedSignature,
            String algorithm) {
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decodeBase64(base64EncodedPublicKey));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = factory.generatePublic(spec);
            Signature signature = Signature.getInstance(algorithm);
            signature.initVerify(publicKey);
            signature.update(input.getBytes(StandardCharsets.UTF_8));
            return signature.verify(Base64.decodeBase64(base64EncodedSignature));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns true, if the password meets the security requirements.
     *
     * @param input a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static PasswordStatus checkPasswordSecurity(String input, PasswordPolicy policy) {
        if(input.length() < policy.getMinLength()) {
            logger.error("Password is not long enough!");
            return PasswordStatus.MIN_LENGTH;
        }

        if(getCount(input, lowerPattern) < policy.getMinLowerCase()) {
            logger.error("Password does not contain enough lower case characters");
            return PasswordStatus.MIN_LOWER;
        }

        if(getCount(input, upperPattern) < policy.getMinUpperCase()) {
            logger.error("Password does not contain enough upper case characters!");
            return PasswordStatus.MIN_UPPER;
        }

        if(getCount(input, specialPattern) < policy.getMinSpecial()) {
            logger.error("Password does not contain enough special characters!");
            return PasswordStatus.MIN_SPECIAL;
        }

        if(getCount(input, numberPattern) < policy.getMinNumber()) {
            logger.error("Password does not contain enough numbers!");
            return PasswordStatus.MIN_NUMBER;
        }

        return PasswordStatus.OK;
    }

    private static int getCount(String input, Pattern pattern) {
        int count = 0;
        Matcher matcher = pattern.matcher(input);
        while(matcher.find()) {
            ++count;
        }
        return count;
    }
}
