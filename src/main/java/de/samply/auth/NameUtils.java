package de.samply.auth;

import de.samply.string.util.StringUtil;

/**
 * Simple utility class for name transformations to get display name
 */
public class NameUtils {
    /**
     * Returns the display name using first and last name
     * @param firstName, lastName
     * @return String
     */
    public static String getDisplayName(String firstName, String lastName) {
        if (!StringUtil.isEmpty(firstName)) {
            return firstName + " " + lastName;
        } else {
            return lastName;
        }
    }

    /**
     * Returns the shibboleth style display name using first and last name
     * @param firstName, lastName
     * @return String
     */
    public static String getShibbolethDisplayName(String firstName, String lastName) {
        if (StringUtil.isEmpty(firstName)) {
            return lastName;
        } else {
            if (!StringUtil.isEmpty(lastName)) {
                return lastName + ", " + firstName;
            }
        }
        return "";
    }

    /**
     * Extracts the first name from full name string
     * @param displayName
     * @return String
     */
    public static String getFirstName(String displayName) {
        if (displayName.contains(", ")) {
            // 1. lastname, firstname (DFN-AAI Style)
            String[] parts = displayName.split(", ");
            return parts[1];
        } else {
            // 2. firstname lastname (default Samply.Auth Format)
            String[] parts = displayName.split(" ");
            if (parts.length > 1) {
                String givenName = parts[parts.length - 2];
                for (int i = parts.length - 3; i >= 0; i--) {
                    givenName = parts[i] + ' ' + givenName;
                }
                return givenName;
            }
        }
        return "";
    }

    /**
     * Extracts the last name from full name string
     * @param displayName
     * @return String
     */
    public static String getLastName(String displayName) {
        if (displayName.contains(", ")) {
            // 1. lastname, firstname (DFN-AAI Style)
            String[] parts = displayName.split(", ");
            return parts[0];
        } else {
            // 2. firstname lastname (default Samply.Auth Format)
            String[] parts = displayName.split(" ");
            return parts[parts.length - 1];
        }
    }
}
