/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.jwt;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nimbusds.jose.JOSEException;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWT;
import de.samply.auth.dao.dto.RolePermission;

/**
 * The server side AbstractJWT, that signs a constructed JWT.
 *
 */
public abstract class AbstractJWT extends JWT {

    /**
     * Define the default validity of JWTs in hours as 2 hours.
     */
    public static final int VALIDITY_HOURS = 2;

    /**
     * The serialized representation of this JWT. Changing this string
     * does not change the JWTClaimsSet in any way.
     */
    private String serialized;

    /**
     * The expiration date of this JWT
     */
    private Date expirationTime;

    /**
     * Creates a new JWT, that is valid from "now - 15 minutes" to "now + 2 hours".
     * Contains a random UUID.
     */
    protected AbstractJWT(String tokenType) {
        this(tokenType, VALIDITY_HOURS);
    }

    protected AbstractJWT(String tokenType, int hoursValidity) {
        super(tokenType, hoursValidity);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -15);

        builder.notBeforeTime(calendar.getTime());
        builder.issueTime(new Date());

        calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, hoursValidity);
        builder.expirationTime(calendar.getTime());

        expirationTime = calendar.getTime();
    }

    /**
     * Returns a permission hashmap for the given role permissions.
     * @param permissions
     * @param all
     * @return
     */
    protected Map<String, List<String>> getPermissionMap(List<RolePermission> permissions, boolean all) {
        HashMap<String, List<String>> map = new HashMap<>();
        for(RolePermission perm : permissions) {
            if(perm.isExport() || all) {
                if(!map.containsKey(perm.getClient().getClientId())) {
                    map.put(perm.getClient().getClientId(), new ArrayList<String>());
                }
                map.get(perm.getClient().getClientId()).add(perm.getPermission().getName());
            }
        }
        return map;
    }


    /**
     * Returns the serialized String (JWT)
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSerialized() {
        return serialized;
    }

    /**
     * Signs this JWT with the private key in the AuthConfig
     *
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    protected void sign() throws JOSEException {
        PrivateKey key = AuthConfig.getPrivateKey();
        serialized = sign(key);
    }

    /**
     * Returns the subject for the given userId.
     * @param userId
     * @return
     */
    protected String getSubject(int userId) {
        return AuthConfig.getSubjectPrefix() + userId;
    }

    /**
     * @return the expirationTime
     */
    public Date getExpirationTime() {
        return expirationTime;
    }

    /**
     * @param expirationTime the expirationTime to set
     */
    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

}
