/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.jwt;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.nimbusds.jose.JOSEException;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.dao.Vocabulary;
import de.samply.auth.dao.dto.Request;
import de.samply.auth.dao.dto.RolePermission;
import de.samply.sdao.json.Value;

/**
 * The server side JWT access token that is created for a specific user and request
 *
 */
public class JWTAccessToken extends AbstractJWT {

    /**
     * <p>Constructor for JWTAccessToken.</p>
     *
     * @param request a {@link de.samply.auth.dao.dto.Request} object.
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    public JWTAccessToken(Request request, List<RolePermission> permissions) throws JOSEException {
        super(JWTVocabulary.TokenType.ACCESS_TOKEN);

        List<String> scopes = new ArrayList<>();
        for(Value scope : request.getData().getProperties(JWTVocabulary.SCOPE)) {
            scopes.add(scope.getValue());
        }

        if(request.getData().getProperty(Vocabulary.STATE) != null) {
            builder.claim(JWTVocabulary.STATE,
                    request.getData().getProperty(Vocabulary.STATE).getValue());
        }

        init(request.getUserId(), scopes, permissions, null);
    }

    /**
     * <p>Constructor for JWTAccessToken.</p>
     *
     * @param userId a int.
     * @param scopes a {@link java.util.List} object.
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    public JWTAccessToken(int userId, List<String> scopes, List<RolePermission> permissions,
            PublicKey publicKey) throws JOSEException {
        super(JWTVocabulary.TokenType.ACCESS_TOKEN);
        init(userId, scopes, permissions, publicKey);
    }

    /**
     * Sets all access token claims.
     * @param userId
     * @param scopes
     * @param permissions
     * @throws JOSEException
     */
    private void init(int userId, List<String> scopes, List<RolePermission> permissions, PublicKey publicKey) throws JOSEException {
        builder.issuer(AuthConfig.getIssuer());
        builder.subject(getSubject(userId));
        builder.claim(JWTVocabulary.SCOPE, scopes);

        if(publicKey != null) {
            builder.claim(JWTVocabulary.KEY, Base64.encodeBase64String(publicKey.getEncoded()));
        }

        builder.claim(JWTVocabulary.PERMISSIONS, getPermissionMap(permissions, false));

        sign();
    }

}
