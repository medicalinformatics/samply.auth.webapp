/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.jwt;

import java.util.ArrayList;
import java.util.List;

import com.nimbusds.jose.JOSEException;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.dao.dto.Request;
import de.samply.sdao.json.Value;

/**
 * The server side JWT refresh token.
 *
 */
public class JWTRefreshToken extends AbstractJWT {

    /**
     * Define the validity of a refresh token in hours as 48.
     */
    public static final int VALIDITY_HOURS = 48;

    /**
     * Creates a new JWT Refresh token for the given request.
     *
     * @param request the request that contains the user and the requested scopes
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    public JWTRefreshToken(Request request) throws JOSEException {
        super(JWTVocabulary.TokenType.REFRESH_TOKEN, VALIDITY_HOURS);

        List<String> scopes = new ArrayList<>();
        for(Value scope : request.getData().getProperties("scope")) {
            scopes.add(scope.getValue());
        }

        init(request.getUserId(), scopes);
    }

    /**
     * <p>Constructor for JWTRefreshToken.</p>
     *
     * @param userId the user ID from the database.
     * @param scopes a {@link java.util.List} object.
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    public JWTRefreshToken(int userId, List<String> scopes) throws JOSEException {
        super(JWTVocabulary.TokenType.REFRESH_TOKEN, VALIDITY_HOURS);
        init(userId, scopes);
    }

    /**
     * Initializes this refresh token with all necessary claims.
     *
     * @param userId
     * @param scopes
     * @throws JOSEException
     */
    private void init(int userId, List<String> scopes) throws JOSEException {
        builder.issuer(AuthConfig.getIssuer());
        builder.subject(getSubject(userId));
        builder.claim(JWTVocabulary.SCOPE, scopes);
        sign();
    }

}
