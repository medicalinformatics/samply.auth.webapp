/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.jwt;

import java.util.ArrayList;
import java.util.List;

import com.nimbusds.jose.JOSEException;

import de.samply.auth.AuthConfig;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.dao.dto.*;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

/**
 * The server side JWT ID token that is created for a specific user. May contain sensitive data like
 * the users name.
 *
 */
public class JWTIDToken extends AbstractJWT {

    /**
     * <p>Constructor for JWTIDToken.</p>
     *
     * @param user the user that is used in the subject claim of this ID token
     * @param keys the users public keys
     * @param roles the users roles
     * @param clientId the client ID that is used in the audience claim in this ID token
     * @throws com.nimbusds.jose.JOSEException if any.
     */
    public JWTIDToken(User user, List<Key> keys, List<Role> roles, List<Location> locations, List<RolePermission> permissions, String clientId, String nonce) throws JOSEException {
        super(JWTVocabulary.TokenType.ID_TOKEN);
        init(user, clientId);

        List<String> keyDescriptions = new ArrayList<>();
        for(Key k : keys) {
            keyDescriptions.add(k.getDescription());
        }

        JSONArray roleArray = new JSONArray();
        for(Role role : roles) {
            JSONObject object = new JSONObject();
            object.put(JWTVocabulary.ROLE_NAME, role.getName());
            object.put(JWTVocabulary.ROLE_DESCRIPTION, role.getDescription());
            object.put(JWTVocabulary.ROLE_IDENTIFIER, role.getIdentifier());
            roleArray.add(object);
        }

        JSONArray locationList = new JSONArray();
        for(Location location : locations) {
            JSONObject object = new JSONObject();
            object.put(JWTVocabulary.LOCATION_IDENTIFIER, location.getIdentifier());
            object.put(JWTVocabulary.LOCATION_NAME, location.getName());
            object.put(JWTVocabulary.LOCATION_DESCRIPTION, location.getDescription());
            object.put(JWTVocabulary.LOCATION_CONTACT, location.getContact());
            locationList.add(object);
        }

        builder.claim(JWTVocabulary.DESCRIPTION, keyDescriptions);
        builder.claim(JWTVocabulary.LOCATIONS, locationList);
        builder.claim(JWTVocabulary.USERTYPE, user.getUsertype().toString());
        builder.claim(JWTVocabulary.PERMISSIONS, getPermissionMap(permissions, true));
        builder.claim(JWTVocabulary.ROLES, roleArray);

        if(nonce != null) {
            builder.claim(JWTVocabulary.NONCE, nonce);
        }

        sign();
    }

    /**
     * Sets all ID token claims.
     * @param user
     * @param clientId
     */
    private void init(User user, String clientId) {
        builder.audience(clientId);
        builder.issuer(AuthConfig.getIssuer());
        builder.subject(getSubject(user.getId()));
        builder.claim(JWTVocabulary.EMAIL, user.getEmail());
        builder.claim(JWTVocabulary.LANG, user.getLanguage());
        builder.claim(JWTVocabulary.NAME, user.getName());
        builder.claim(JWTVocabulary.GIVEN_NAME, user.getFirstName());
        builder.claim(JWTVocabulary.FAMILY_NAME, user.getLastName());

        if(user.isExternal()) {
            builder.claim(JWTVocabulary.EXTERNAL_LABEL,
                    AuthConfig.getProviderLabel(user));
        }
    }

}
