/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.admin;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.RequestDAO;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Request;
import de.samply.sdao.DAOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages the debug view in the admin interface
 */
@ViewScoped
@ManagedBean
public class DebugBean {

    private ArrayList<RequestWrapper> requests;

    /**
     * initializes the bean with all currently available requests
     */
    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            List<Client> clients = auth.get(ClientDAO.class).getClients();
            List<Request> requests = auth.get(RequestDAO.class).getRequests();

            this.requests = new ArrayList<>();

            Map<Integer, Client> clientMap = new HashMap<>();

            for(Client client : clients) {
                clientMap.put(client.getId(), client);
            }

            for(Request request : requests) {
                RequestWrapper wrapper = new RequestWrapper();
                wrapper.setRequest(request);
                wrapper.setClient(clientMap.get(request.getClientId()));
                this.requests.add(wrapper);
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<RequestWrapper> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<RequestWrapper> requests) {
        this.requests = requests;
    }

    /**
     * A wrapper for requests and the client that made the request
     */
    public static class RequestWrapper {

        /**
         * The request itself
         */
        private Request request;

        /**
         * The client that made the request
         */
        private Client client;


        public Request getRequest() {
            return request;
        }

        public void setRequest(Request request) {
            this.request = request;
        }

        public Client getClient() {
            return client;
        }

        public void setClient(Client client) {
            this.client = client;
        }
    }

}
