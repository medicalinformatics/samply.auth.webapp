/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.AuthConfig;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.LocationDAO;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.AdministrationMode;
import de.samply.auth.dao.dto.Location;
import de.samply.auth.dao.dto.LocationAdministration;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

@ManagedBean
@ViewScoped
public class LocationListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of all currently available locations.
     */
    private List<Location> locations;

    /**
     * The name of the new location
     */
    private String name;

    /**
     * The description of the new location.
     */
    private String description;

    /**
     * The location identifier (unique).
     */
    private String identifier;

    /**
     * The contact informations for the new location.
     */
    private String contact;

    /**
     * The ID from a user selected as admin.
     */
    private String identity;

    /**
     * The location that is currently edited.
     */
    private Location location;

    /**
     * The fields necessary for the ajax user search.
     */
    private String searchText, suggestions, searchEmail;

    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            locations = auth.get(LocationDAO.class).getLocations();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepares the edit mode for the given location.
     * @param location
     * @return
     */
    public String edit(Location location) {
        this.location = location;
        name = location.getName();
        description = location.getDescription();
        identifier = location.getIdentifier();
        contact = location.getContact();
        return null;
    }

    /**
     * Deletes the given location.
     * @param location
     * @return
     */
    public String delete(Location location) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(LocationDAO.class).deleteLocation(location);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "locations";
    }

    /**
     * Creates or updates the location.
     * @return
     */
    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            Location location = new Location();
            location.setName(name);
            location.setDescription(description);
            location.setIdentifier(identifier);
            location.setContact(contact);

            if(this.location == null) {
                auth.get(LocationDAO.class).saveLocation(location);

                User user = auth.get(UserDAO.class).getUser(Integer.parseInt(identity));
                LocationAdministration admin = new LocationAdministration();
                admin.setUser(user);
                admin.setMode(AdministrationMode.ADMIN);
                admin.setLocation(location);
                auth.get(LocationDAO.class).updateAdmins(location, Arrays.asList(admin));
            } else {
                location.setId(this.location.getId());
                auth.get(LocationDAO.class).updateLocation(location);
            }

            auth.commit();

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "locations";
    }

    /**
     * Searches for users using the searchText.
     * @return
     */
    public String searchEmail() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            List<User> users = auth.get(UserDAO.class).findUsers(searchText);

            suggestions = StringUtil.join(users, ";|;", new Builder<User>() {
                @Override
                public String build(User o) {
                    return o.getName() + " <" + o.getEmail() + "> [" + o.getId() + "]" + " ("
                            + AuthConfig.getProviderLabel(o) + ")";
                }
            });
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String cancel() {
        reset();
        return "locations";
    }

    private void reset() {
        name = description = identifier = "";
        location = null;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param identity the identity to set
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * @return the searchEmail
     */
    public String getSearchEmail() {
        return searchEmail;
    }

    /**
     * @param searchEmail the searchEmail to set
     */
    public void setSearchEmail(String searchEmail) {
        this.searchEmail = searchEmail;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the locations
     */
    public List<Location> getLocations() {
        return locations;
    }

    /**
     * @param locations the locations to set
     */
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

}
