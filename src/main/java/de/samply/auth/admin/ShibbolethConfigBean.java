/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.Shibboleth;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ViewScoped
@ManagedBean
public class ShibbolethConfigBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /**
     * The attributes for the shibboleth configuration
     */
    private boolean shibActive, shibDefaultActive, shibDefaultVerified, shibUseHttpHeaders;

    private String shibNameAttribute, shibFirstNameAttribute, shibLastNameAttribute, shibIdentityAttribute, shibMailAttribute;

    private String shibName;

    @PostConstruct
    public void init() {
        Shibboleth shib = AuthConfig.getShibboleth();
        shibActive = shib.isActive();
        shibDefaultVerified = shib.isDefaultVerified();
        shibDefaultActive = shib.isDefaultActive();
        shibIdentityAttribute = shib.getIdentityAttribute();
        shibMailAttribute = shib.getMailAttribute();
        shibNameAttribute = shib.getNameAttribute();
        shibFirstNameAttribute = shib.getFirstNameAttribute();
        shibLastNameAttribute = shib.getLastNameAttribute();
        shibName = shib.getName();
        shibUseHttpHeaders = shib.isUseHttpHeaders();
    }

    public String saveConfig() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            JSONResource config = auth.getConfig();

            JSONResource shib = new JSONResource();
            shib.setProperty(Vocabulary.Shibboleth.ACTIVE, shibActive);
            shib.setProperty(Vocabulary.Shibboleth.DEFAULT_ACTIVE, shibDefaultActive);
            shib.setProperty(Vocabulary.Shibboleth.DEFAULT_VERIFIED, shibDefaultVerified);
            shib.setProperty(Vocabulary.Shibboleth.IDENTITY_ATTRIBUTE, shibIdentityAttribute);
            shib.setProperty(Vocabulary.Shibboleth.MAIL_ATTRIBUTE, shibMailAttribute);
            shib.setProperty(Vocabulary.Shibboleth.NAME_ATTRIBUTE, shibNameAttribute);
            shib.setProperty(Vocabulary.Shibboleth.FIRST_NAME_ATTRIBUTE, shibFirstNameAttribute);
            shib.setProperty(Vocabulary.Shibboleth.LAST_NAME_ATTRIBUTE, shibLastNameAttribute);
            shib.setProperty(Vocabulary.Shibboleth.NAME, shibName);
            shib.setProperty(Vocabulary.Shibboleth.USE_HTTP_HEADERS, isShibUseHttpHeaders());

            config.setProperty(Vocabulary.SHIBBOLETH, shib);
            auth.saveConfig(config);
            auth.commit();

            AuthConfig.reloadConfig(auth);
            Message.addInfo("configSaved");
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "shibboleth";
    }

    /**
     * @return the shibActive
     */
    public boolean isShibActive() {
        return shibActive;
    }

    /**
     * @param shibActive the shibActive to set
     */
    public void setShibActive(boolean shibActive) {
        this.shibActive = shibActive;
    }

    /**
     * @return the shibDefaultActive
     */
    public boolean isShibDefaultActive() {
        return shibDefaultActive;
    }

    /**
     * @param shibDefaultActive the shibDefaultActive to set
     */
    public void setShibDefaultActive(boolean shibDefaultActive) {
        this.shibDefaultActive = shibDefaultActive;
    }

    /**
     * @return the shibNameAttribute
     */
    public String getShibNameAttribute() {
        return shibNameAttribute;
    }

    /**
     * @return the shibFirstNameAttribute
     */
    public String getShibFirstNameAttribute() {
        return shibFirstNameAttribute;
    }

    /**
     * @return the shibLastNameAttribute
     */
    public String getShibLastNameAttribute() {
        return shibLastNameAttribute;
    }

    /**
     * @param shibNameAttribute the shibNameAttribute to set
     */
    public void setShibNameAttribute(String shibNameAttribute) {
        this.shibNameAttribute = shibNameAttribute;
    }

    /**
     * @param shibFirstNameAttribute the shibFirstNameAttribute to set
     */
    public void setShibFirstNameAttribute(String shibFirstNameAttribute) {
        this.shibFirstNameAttribute = shibFirstNameAttribute;
    }

    /**
     * @param shibLastNameAttribute the shibLastNameAttribute to set
     */
    public void setShibLastNameAttribute(String shibLastNameAttribute) {
        this.shibLastNameAttribute = shibLastNameAttribute;
    }

    /**
     * @return the shibIdentityAttribute
     */
    public String getShibIdentityAttribute() {
        return shibIdentityAttribute;
    }

    /**
     * @param shibIdentityAttribute the shibIdentityAttribute to set
     */
    public void setShibIdentityAttribute(String shibIdentityAttribute) {
        this.shibIdentityAttribute = shibIdentityAttribute;
    }

    /**
     * @return the shibMailAttribute
     */
    public String getShibMailAttribute() {
        return shibMailAttribute;
    }

    /**
     * @param shibMailAttribute the shibMailAttribute to set
     */
    public void setShibMailAttribute(String shibMailAttribute) {
        this.shibMailAttribute = shibMailAttribute;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the shibName
     */
    public String getShibName() {
        return shibName;
    }

    /**
     * @param shibName the shibName to set
     */
    public void setShibName(String shibName) {
        this.shibName = shibName;
    }

    /**
     * @return the shibUseHttpHeaders
     */
    public boolean isShibUseHttpHeaders() {
        return shibUseHttpHeaders;
    }

    /**
     * @param shibUseHttpHeaders the shibUseHttpHeaders to set
     */
    public void setShibUseHttpHeaders(boolean shibUseHttpHeaders) {
        this.shibUseHttpHeaders = shibUseHttpHeaders;
    }

    public boolean isShibDefaultVerified() {
        return shibDefaultVerified;
    }

    public void setShibDefaultVerified(boolean shibDefaultVerified) {
        this.shibDefaultVerified = shibDefaultVerified;
    }
}
