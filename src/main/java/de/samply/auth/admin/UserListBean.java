/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.NameUtils;
import de.samply.auth.SecurityUtils;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.User;
import de.samply.auth.dao.dto.Usertype;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * The user list bean in the admin interface.
 *
 */
@ManagedBean
@ViewScoped
public class UserListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The available users.
     */
    private List<User> users;

    /**
     * The currently selected user.
     */
    private User user;

    /**
     * The first name for the new user
     */
    private String firstName;

    /**
     * The last name for the new user
     */
    private String lastName;

    /**
     * The new users email address
     */
    private String email;

    /**
     * The contact information
     */
    private String contact;

    /**
     * The new password, 1st input
     */
    private String newPassword;

    /**
     * The new password, 2nd input
     */
    private String newPasswordRepeat;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            users = auth.get(UserDAO.class).getUsers(Usertype.NORMAL);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Activates the edit mode for the given user.
     * @param user
     */
    public void selectUser(User user) {
        this.user = user;
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.contact = user.getContactData();
        this.newPassword = "";
        this.newPasswordRepeat = "";
    }

    /**
     * Commits the new user or the current changes to the selected user.
     * @return
     */
    public String commit() {
        User user = null;

        if(this.user != null) {
            user = this.user;
        } else {
            /**
             * New users must have a password
             */
            if(StringUtil.isEmpty(newPassword)) {
                Message.add("userForm", "admin.passwordMissing", false, FacesMessage.SEVERITY_ERROR);
                return null;
            }

            user = new User();

            /**
             * Those values can still be changed through the table, so just set everything to false.
             */
            user.setActive(false);
            user.setEulaAccepted(false);
            user.setEmailVerified(false);
            user.setClientCertificate(false);
            user.setUsertype(Usertype.NORMAL);
        }

        if(!StringUtil.isEmpty(newPassword)) {
            if (!newPassword.equals(newPasswordRepeat)) {
                Message.add("userForm", "passwordsNotEqual", false, FacesMessage.SEVERITY_ERROR);
                return null;
            } else {
                user.setRounds(AuthConfig.DEFAULT_ROUNDS);
                user.setSalt(SecurityUtils.newRandomString());
                user.setHashedPassword(SecurityUtils.hashPassword(newPassword, user.getSalt(), AuthConfig.DEFAULT_ROUNDS));
            }
        }

        user.setLanguage("en");
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setName(NameUtils.getDisplayName(firstName, lastName));

        user.setContactData(contact);

        try(AuthConnection auth = ConnectionFactory.get()) {
            if(this.user != null) {
                auth.get(UserDAO.class).updateEmail(user);
                auth.get(UserDAO.class).updateData(user);
                auth.get(UserDAO.class).updateContact(user);
                auth.get(UserDAO.class).updatePasswordByAdmin(user.getId(), user.getHashedPassword(), user.getSalt(), AuthConfig.DEFAULT_ROUNDS);
            } else {
                auth.get(UserDAO.class).saveUser(user);
            }
            auth.commit();

            return "/admin/users?faces-redirect=true";
        } catch (DAOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Cancels the edit mode for the currently selected user.
     */
    public void cancel() {
        this.user = null;
        this.email = null;
        this.firstName = null;
        this.lastName = null;
        this.contact = null;
        this.newPassword = null;
        this.newPasswordRepeat = null;
    }

    /**
     * Disables or enables the given user
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @return a {@link java.lang.String} object.
     */
    public String toggle(User user) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            if(user.getActive()) {
                auth.get(UserDAO.class).disableUser(user);
            } else {
                auth.get(UserDAO.class).enableUser(user);
            }
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/users";
    }

    /**
     * Toggles the searchable flag for the given user.
     * @param user
     * @return
     */
    public String toggleSearchable(User user) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            user.setSearchable(! user.getSearchable());
            auth.get(UserDAO.class).updateData(user);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/users";
    }

    /**
     * Toggles the permission to authenticate via client certificates for the given user.
     * @param user
     * @return
     */
    public String toggleClientCertificate(User user) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            user.setClientCertificate(! user.getClientCertificate());
            auth.get(UserDAO.class).updateClientCertificate(user);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "/admin/users";
    }

    /**
     * <p>verify.</p>
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @return a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public String verify(User user) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(UserDAO.class).verifyUser(user);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/users";
    }

    /**
     * Deletes the given user from the database, but only, if the user has been disabled
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @return a {@link java.lang.String} object.
     */
    public String delete(User user) {
        if(user.getActive()) {
            Message.add("tableForm", "userActive", false, FacesMessage.SEVERITY_ERROR);
            return null;
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(UserDAO.class).deleteUser(user);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/users";
    }

    /**
     * <p>Getter for the field <code>users</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * <p>Setter for the field <code>users</code>.</p>
     *
     * @param users a {@link java.util.List} object.
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeat() {
        return newPasswordRepeat;
    }

    public void setNewPasswordRepeat(String newPasswordRepeat) {
        this.newPasswordRepeat = newPasswordRepeat;
    }
}
