/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ProviderDAO;
import de.samply.auth.dao.RoleDAO;
import de.samply.auth.dao.RoleMappingDAO;
import de.samply.auth.dao.dto.Role;
import de.samply.auth.dao.dto.RoleMapping;
import de.samply.sdao.DAOException;

/**
 * The bean for the external role mappings.
 */
@ManagedBean
@ViewScoped
public class MappingListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of all currently available mappings.
     */
    private List<RoleMapping> mappings;

    /**
     * The list of all available internal roles.
     */
    private List<Role> roles;

    /**
     * The name of the *external* role name (or a DN for LDAP groups).
     */
    private String roleName;

    /**
     * The selected role ID and provider ID.
     */
    private int roleId, providerId;

    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            mappings = auth.get(RoleMappingDAO.class).getRoleMappings();
            roles = auth.get(RoleDAO.class).getRoles();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new role mapping.
     * @return
     */
    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            RoleMapping mapping = new RoleMapping();
            mapping.setProvider(auth.get(ProviderDAO.class).getProvider(providerId));
            mapping.setRole(auth.get(RoleDAO.class).getRole(roleId));
            mapping.setRoleName(roleName);

            auth.get(RoleMappingDAO.class).saveRoleMapping(mapping);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "mappings";
    }

    /**
     * Deletes the given role mapping.
     * @param mapping
     * @return
     */
    public String delete(RoleMapping mapping) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(RoleMappingDAO.class).deleteRoleMapping(mapping);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "mappings";
    }

    /**
     * @return the mappings
     */
    public List<RoleMapping> getMappings() {
        return mappings;
    }

    /**
     * @param mappings the mappings to set
     */
    public void setMappings(List<RoleMapping> mappings) {
        this.mappings = mappings;
    }

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return the roleId
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the providerId
     */
    public int getProviderId() {
        return providerId;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

}
