/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ScopeDAO;
import de.samply.auth.dao.dto.Scope;
import de.samply.sdao.DAOException;

/**
 * The bean for listing and adding new scopes in the admin interface.
 *
 */
@ManagedBean
@ViewScoped
public class ScopeListBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of all currently available scopes.
     */
    private List<Scope> scopes;

    /**
     * The name for the new scope
     */
    private String scopeName;

    /**
     * The german title for the new scope
     */
    private String titleDe;

    /**
     * The german description for the new scope
     */
    private String descriptionDe;

    /**
     * The english title for the new scope
     */
    private String titleEn;

    /**
     * The english description for the new scope
     */
    private String descriptionEn;

    /**
     * The icon for the new scope, e.g. "&lt;i class='...'/&gt;"
     */
    private String icon;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            scopes = auth.get(ScopeDAO.class).getAllScopes();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>commit.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String commit() {
        Scope scope = new Scope();
        scope.setName(scopeName);
        scope.setIcon(icon);
        scope.setDescriptionDe(descriptionDe);
        scope.setDescriptionEn(descriptionEn);
        scope.setTitleDe(titleDe);
        scope.setTitleEn(titleEn);

        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(ScopeDAO.class).saveScope(scope);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/scopes";
    }

    /**
     * <p>delete.</p>
     *
     * @param scope a {@link de.samply.auth.dao.dto.Scope} object.
     * @return a {@link java.lang.String} object.
     */
    public String delete(Scope scope) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(ScopeDAO.class).deleteScope(scope);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/scopes";
    }

    /**
     * <p>Getter for the field <code>scopes</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Scope> getScopes() {
        return scopes;
    }

    /**
     * <p>Setter for the field <code>scopes</code>.</p>
     *
     * @param scopes a {@link java.util.List} object.
     */
    public void setScopes(List<Scope> scopes) {
        this.scopes = scopes;
    }

    /**
     * <p>Getter for the field <code>scopeName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getScopeName() {
        return scopeName;
    }

    /**
     * <p>Setter for the field <code>scopeName</code>.</p>
     *
     * @param scopeName a {@link java.lang.String} object.
     */
    public void setScopeName(String scopeName) {
        this.scopeName = scopeName;
    }

    /**
     * <p>Getter for the field <code>titleDe</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitleDe() {
        return titleDe;
    }

    /**
     * <p>Setter for the field <code>titleDe</code>.</p>
     *
     * @param titleDe a {@link java.lang.String} object.
     */
    public void setTitleDe(String titleDe) {
        this.titleDe = titleDe;
    }

    /**
     * <p>Getter for the field <code>descriptionDe</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescriptionDe() {
        return descriptionDe;
    }

    /**
     * <p>Setter for the field <code>descriptionDe</code>.</p>
     *
     * @param descriptionDe a {@link java.lang.String} object.
     */
    public void setDescriptionDe(String descriptionDe) {
        this.descriptionDe = descriptionDe;
    }

    /**
     * <p>Getter for the field <code>titleEn</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitleEn() {
        return titleEn;
    }

    /**
     * <p>Setter for the field <code>titleEn</code>.</p>
     *
     * @param titleEn a {@link java.lang.String} object.
     */
    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    /**
     * <p>Getter for the field <code>descriptionEn</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * <p>Setter for the field <code>descriptionEn</code>.</p>
     *
     * @param descriptionEn a {@link java.lang.String} object.
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * <p>Getter for the field <code>icon</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIcon() {
        return icon;
    }

    /**
     * <p>Setter for the field <code>icon</code>.</p>
     *
     * @param icon a {@link java.lang.String} object.
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

}
