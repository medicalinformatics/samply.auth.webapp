/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import de.samply.auth.Constants;
import de.samply.auth.SecurityUtils;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.ClientType;
import de.samply.sdao.DAOException;
import de.samply.auth.dao.EulaDAO;
import de.samply.auth.dao.dto.Eula;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * The client list view in the admin interface.
 *
 */
@ManagedBean
@ViewScoped
public class ClientListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<Client> clients;

    /**
     * The name for the new client
     */
    private String name;

    /**
     * The description for the new client
     */
    private String description;

    /**
     * The Client ID for the new client. This value is usually generated.
     */
    private String clientId;

    /**
     * The Client Secret for the new client. This value is usually generated.
     */
    private String clientSecret;

    /**
     * At least one redirect URL, separated by ',', e.g. "https://mdr.samply.de,https://mdr.ccp-it.dktk.dkfz.de"
     */
    private String redirectUrl;

    /**
     * The Client type for the new client
     */
    private ClientType type = ClientType.UNDEFINED;

    /**
     * The Trust flag for the new client. If true, the user will not see the permissions the client requests, instead
     * Samply Auth accepts the request instantly.
     */
    private Boolean trust = true;

    /**
     * The whitelist flag for the new client. If true, users need a permission to access this application.
     */
    private Boolean whitelist = false;

    /**
     * The basicAuthentication flag for the new client. If true, the client can use Basic Authentication with its
     * client id and secret.
     */
    private Boolean basicAuthentication = false;

    /**
     * List of all available eula types
     */
    private List<Eula> eulaList;

    /**
     * Selected eulas which need to be accepted to use the client
     */
    private Map<Integer, Boolean> eulaSelected;

    /**
     * The selected client.
     */
    private Client client;

    /**
     * Initializes this bean. Loads all clients from the database and generates a
     * new client ID and secret.
     */
    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            clients = auth.get(ClientDAO.class).getClients();
            clientId = SecurityUtils.newRandomString(8);
            clientSecret = SecurityUtils.newRandomString();

            /**
             * For security reasons the local samply auth client will not be displayed in the overview.
             */
            for(Iterator<Client> it = clients.iterator(); it.hasNext(); ) {
                if(it.next().getClientId().equals(Constants.AUTH_CLIENT_ID)) {
                    it.remove();
                }
            }

            eulaList = auth.get(EulaDAO.class).getAllEulaTypes();
            eulaSelected = new HashMap<>();

            for(Eula eula : eulaList) {
                eulaSelected.put(eula.getId(), false);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Disables or enables the client
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @return a {@link java.lang.String} object.
     */
    public String toggle(Client client) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            ClientDAO dao = auth.get(ClientDAO.class);

            if(client.getActive()) {
                dao.disable(client);
            } else {
                dao.enable(client);
            }

            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/index";
    }

    /**
     * Trusts or untrusts the client
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @return a {@link java.lang.String} object.
     */
    public String toggleTrust(Client client) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            if(client.getTrusted()) {
                auth.get(ClientDAO.class).distrust(client);
            } else {
                auth.get(ClientDAO.class).trust(client);
            }

            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/admin/index";
    }

    /**
     * Deletes the client
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @return a {@link java.lang.String} object.
     */
    public String delete(Client client) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(ClientDAO.class).delete(client);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "index";
    }

    /**
     * Creates the new client
     *
     * @return a {@link java.lang.String} object.
     */
    public String commit() {
        Client client = new Client();
        client.setName(name);
        client.setDescription(description);
        client.setClientId(clientId);
        client.setClientSecret(clientSecret);
        client.setRedirectUrl(redirectUrl);
        client.setActive(true);
        client.setTrusted(trust);
        client.setType(type);
        client.setWhitelist(whitelist);
        client.setBasicAuthentication(basicAuthentication);

        try(AuthConnection auth = ConnectionFactory.get()) {
            if(this.client == null) {
                auth.get(ClientDAO.class).saveClient(client);
            } else {
                client.setId(this.client.getId());
                auth.get(ClientDAO.class).updateClient(client);
                this.client = null;
            }

            // Edit the list of selected eulas for a client (if needed)
            List<Eula> eulas = auth.get(EulaDAO.class).getEulaForClient(client.getId());
            List<Integer> eulaIds = new ArrayList<>();
            for (Eula dbEula : eulas) {
                eulaIds.add(dbEula.getId());
            }

            for(Eula eula : eulaList) {
                if (eulaSelected.get(eula.getId())){
                    if (!eulaIds.contains(eula.getId())){
                        auth.get(EulaDAO.class).addEulaToClient(eula, client);
                    }
                } else {
                    if (eulaIds.contains(eula.getId())) {
                        auth.get(EulaDAO.class).deleteEulaForClient(eula, client);
                    }
                }
            }

            auth.commit();

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "index";
    }

    /**
     * <p>Starts the edit mode for the specified client.</p>
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @return a {@link java.lang.String} object.
     * @since 1.4.RC4
     */
    public String edit(Client client) {
        this.client = client;
        this.clientId = client.getClientId();
        this.clientSecret = client.getClientSecret();
        this.description = client.getDescription();
        this.name = client.getName();
        this.redirectUrl = client.getRedirectUrl();
        this.trust = client.getTrusted();
        this.whitelist = client.getWhitelist();
        this.type = client.getType();
        this.basicAuthentication = client.getBasicAuthentication();

        for(Eula eula : eulaList) {
            eulaSelected.put(eula.getId(), false);
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            List<Eula> eulaForClientList = auth.get(EulaDAO.class).getEulaForClient(client.getId());
            for(Eula eula : eulaForClientList) {
                if (eulaSelected.containsKey(eula.getId())) {
                    eulaSelected.put(eula.getId(), true);
                }
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * <p>cancel.</p>
     *
     * @return a {@link java.lang.String} object.
     * @since 1.4.RC4
     */
    public String cancel() {
        reset();
        return null;
    }

    private void reset() {
        this.client = null;
        this.clientId = SecurityUtils.newRandomString(8);
        this.clientSecret = SecurityUtils.newRandomString();
        this.trust = true;
        this.whitelist = false;
        this.type = ClientType.UNDEFINED;
        this.redirectUrl = "";
        this.description = "";
        this.name = "";
        this.basicAuthentication = false;
        for(Eula eula : eulaList) {
            eulaSelected.put(eula.getId(), false);
        }
    }

    /**
     * <p>Getter for the field <code>clients</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Client> getClients() {
        return clients;
    }

    /**
     * <p>Setter for the field <code>clients</code>.</p>
     *
     * @param clients a {@link java.util.List} object.
     */
    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>clientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * <p>Setter for the field <code>clientId</code>.</p>
     *
     * @param clientId a {@link java.lang.String} object.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * <p>Getter for the field <code>clientSecret</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * <p>Setter for the field <code>clientSecret</code>.</p>
     *
     * @param clientSecret a {@link java.lang.String} object.
     */
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    /**
     * <p>Getter for the field <code>redirectUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * <p>Setter for the field <code>redirectUrl</code>.</p>
     *
     * @param redirectUrl a {@link java.lang.String} object.
     */
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * <p>Getter for the field <code>trust</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getTrust() {
        return trust;
    }

    /**
     * <p>Setter for the field <code>trust</code>.</p>
     *
     * @param trust a {@link java.lang.Boolean} object.
     */
    public void setTrust(Boolean trust) {
        this.trust = trust;
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return the type
     * @since 1.4.RC4
     */
    public ClientType getType() {
        return type;
    }

    /**
     * <p>Setter for the field <code>type</code>.</p>
     *
     * @param type the type to set
     * @since 1.4.RC4
     */
    public void setType(ClientType type) {
        this.type = type;
    }

    /**
     * <p>Getter for the field <code>client</code>.</p>
     *
     * @return the client
     * @since 1.4.RC4
     */
    public Client getClient() {
        return client;
    }

    /**
     * <p>Setter for the field <code>client</code>.</p>
     *
     * @param client the client to set
     * @since 1.4.RC4
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the whitelist
     */
    public Boolean getWhitelist() {
        return whitelist;
    }

    /**
     * @param whitelist the whitelist to set
     */
    public void setWhitelist(Boolean whitelist) {
        this.whitelist = whitelist;
    }

    public Boolean getBasicAuthentication() {
        return basicAuthentication;
    }

    public void setBasicAuthentication(Boolean basicAuthentication) {
        this.basicAuthentication = basicAuthentication;
    }

    /**
     * @return the list of available eulas
     */
    public List<Eula> getEulaList() { return eulaList; }

    /**
     * @param eulaList eula list to set
     */
    public void setEulaList(List<Eula> eulaList) {this.eulaList = eulaList; }

    /**
     * @return selected eulas for the client
     */
    public Map<Integer,Boolean> getEulaSelected() { return eulaSelected; }

    /**
     * @param eulaSelected selected eulas to set
     */
    public void setEulaSelected(Map<Integer,Boolean> eulaSelected) {this.eulaSelected = eulaSelected; }

    /**
     * @return list of eula types as string (needed for client details)
     */
    public String getEulaForClient(int clientId) {
        String eulaForClientAsString = "";
        try(AuthConnection auth = ConnectionFactory.get()) {
            List<Eula> eulaForClientList = auth.get(EulaDAO.class).getEulaForClient(clientId);
            for(Eula eula : eulaForClientList) {
                if (eulaForClientAsString.length()>0) {
                    eulaForClientAsString = eulaForClientAsString+";"+eula.getEulaType();
                } else {
                    eulaForClientAsString = eula.getEulaType();
                }
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return eulaForClientAsString;
    }
}
