/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.Constants;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.PermissionDAO;
import de.samply.auth.dao.dto.Permission;
import de.samply.sdao.DAOException;

@ManagedBean
@ViewScoped
public class PermissionListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<Permission> permissions;

    private String name;

    private Permission permission = null;

    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            permissions = auth.get(PermissionDAO.class).getPermissions();

            /**
             * Remove the internal permissions.
             */
            for(Iterator<Permission> it = permissions.iterator(); it.hasNext(); ) {
                Permission next = it.next();
                if(next.getName().equals(Constants.PERMISSION_ADMIN) || next.getName().equals(Constants.PERMISSION_USE)) {
                    it.remove();
                }
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            Permission p = new Permission();
            p.setName(name);

            if(permission == null) {
                auth.get(PermissionDAO.class).savePermission(p);
            } else {
                p.setId(permission.getId());
                auth.get(PermissionDAO.class).updatePermission(p);
            }
            auth.commit();

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "permissions";
    }

    public String cancel() {
        reset();
        return "permissions";
    }

    private void reset() {
        name = "";
        permission = null;
    }

    public String edit(Permission permission) {
        this.permission = permission;
        this.name = permission.getName();
        return null;
    }

    public String delete(Permission permission) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(PermissionDAO.class).deletePermission(permission);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "permissions";
    }

    /**
     * @return the permissions
     */
    public List<Permission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }


}
