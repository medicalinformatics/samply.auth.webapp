/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.sdao.Upgrader;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Manages the upgrade process initiated by the admin in the admin interface.
 *
 * @since 1.4.0
 */
@ManagedBean
@ViewScoped
public class UpgradeBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private boolean dryRunSuccess = false;

    /**
     * Starts the database upgrade. The first run is a dry run. Only if this dry run completes
     * without exceptions, the real upgrade can be executed.
     *
     * @param dry a boolean.
     * @return a {@link java.lang.String} object.
     */
    public String run(boolean dry) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            Upgrader upgrader = new Upgrader(auth);

            upgrader.upgrade(dry);

            dryRunSuccess = true;

            /**
             * If the user wants a real upgrade run, commit the transaction,
             * reload the config and redirect him back to the index web page.
             */
            if(dry == false) {
                auth.commit();
                AuthConfig.reloadConfig(auth);
                return "/admin/index?faces-redirect=true";
            }

            Message.addInfo("admin.dryRunSuccessful");
        } catch (Exception e) {
            e.printStackTrace();
            Message.addError("admin.contactServerAdmin");
        }
        return null;
    }

    /**
     * <p>isDryRunSuccess.</p>
     *
     * @return the dryRunSuccess
     */
    public boolean isDryRunSuccess() {
        return dryRunSuccess;
    }

    /**
     * <p>Setter for the field <code>dryRunSuccess</code>.</p>
     *
     * @param dryRunSuccess the dryRunSuccess to set
     */
    public void setDryRunSuccess(boolean dryRunSuccess) {
        this.dryRunSuccess = dryRunSuccess;
    }

}
