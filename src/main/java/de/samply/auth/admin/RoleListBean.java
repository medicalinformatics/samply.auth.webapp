/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.auth.AuthConfig;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.RoleDAO;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.*;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

@ManagedBean
@ViewScoped
public class RoleListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of all currently available roles.
     */
    private List<Role> roles;

    /**
     * The name of the new role
     */
    private String name;

    /**
     * The description of the new role.
     */
    private String description;

    /**
     * The role identifier (unique).
     */
    private String identifier;

    /**
     * The ID from a user selected as admin.
     */
    private String identity;

    /**
     * The role that is currently edited.
     */
    private Role role;

    /**
     * The fields necessary for the ajax user search.
     */
    private String searchText, suggestions, searchEmail;

    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            roles = auth.get(RoleDAO.class).getRoles();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepares the edit mode for the given role.
     * @param role
     * @return
     */
    public String edit(Role role) {
        this.role = role;
        name = role.getName();
        description = role.getDescription();
        identifier = role.getIdentifier();
        return null;
    }

    /**
     * Deletes the given role.
     * @param role
     * @return
     */
    public String delete(Role role) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(RoleDAO.class).deleteRole(role);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "roles";
    }

    /**
     * Creates or updates the role.
     * @return
     */
    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            Role role = new Role();
            role.setName(name);
            role.setDescription(description);
            role.setIdentifier(identifier);

            if(this.role == null) {
                auth.get(RoleDAO.class).saveRole(role);

                User user = auth.get(UserDAO.class).getUser(Integer.parseInt(identity));
                RoleAdministration admin = new RoleAdministration();
                admin.setUser(user);
                admin.setMode(AdministrationMode.ADMIN);
                admin.setRole(role);
                auth.get(RoleDAO.class).updateAdmins(role, Arrays.asList(admin));
            } else {
                role.setId(this.role.getId());
                auth.get(RoleDAO.class).updateRole(role);
            }

            auth.commit();

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "roles";
    }

    /**
     * Searches for users using the searchText.
     * @return
     */
    public String searchEmail() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            List<User> users = auth.get(UserDAO.class).findUsers(searchText, Usertype.NORMAL);

            suggestions = StringUtil.join(users, ";|;", new Builder<User>() {
                @Override
                public String build(User o) {
                    return o.getName() + " <" + o.getEmail() + "> [" + o.getId() + "]" + " ("
                            + AuthConfig.getProviderLabel(o) + ")";
                }
            });
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String cancel() {
        reset();
        return "roles";
    }

    private void reset() {
        name = description = identifier = "";
        role = null;
    }

    /**
     * @return the roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param identity the identity to set
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * @return the searchEmail
     */
    public String getSearchEmail() {
        return searchEmail;
    }

    /**
     * @param searchEmail the searchEmail to set
     */
    public void setSearchEmail(String searchEmail) {
        this.searchEmail = searchEmail;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
