/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import de.samply.auth.AuthConfig;
import de.samply.auth.ImageServlet;
import de.samply.auth.Message;
import de.samply.auth.PasswordPolicy;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.ConfigDAO;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;

/**
 * The configuration options in the admin interface.
 *
 */
@ManagedBean
@ViewScoped
public class ConfigBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * If true, the registration is enabled
     */
    private Boolean registrationEnabled;

    /**
     * If true, new users are enabled by default. Does not apply to external identity providers.
     */
    private Boolean defaultUserEnabled;

    /**
     * If true, the registration of new users via rest is enabled.
     */
    private Boolean restRegistrationEnabled;

    /**
     * If true, new users will receive an email for verification.
     */
    private Boolean verifyEmailEnabled;

    /**
     * If true, users must verify their email first before they can use the account.
     */
    private Boolean verifyRequiredEnabled;

    /**
     * The contact information field at the bottom of the page.
     */
    private String contactInformation;

    /**
     * The file logo upload
     */
    private Part fileLogo;

    /**
     * The password policy settings
     */

    private int minLength;

    private int minNumbers;

    private int minSpecial;

    private int minUpper;

    private int minLower;

    /**
     * Enable EULA
     */
    private Boolean eulaEnabled;

    /**
     * The EULA itself.
     */
    private String eula;

    /**
     * Use external authorization for the admin interface
     */
    private Boolean externalAuthorization;

    /**
     * If false, the login for local accouts is disabled.
     */
    private Boolean localAccountsEnabled;

    /**
     * If true, the authentication via client certificates is enabled. The user must bind a client certificate to an
     * account first.
     */
    private Boolean clientCertificateEnabled;

    /**
     * If true, the login screen should hide the login via client certificate button.
     */
    private Boolean hideClientCertificateButton;

    /**
     * The admin email to send debug information
     */
    private String adminEmail;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {
        registrationEnabled = AuthConfig.getRegistrationEnabled();
        defaultUserEnabled = AuthConfig.getDefaultUserEnabled();
        restRegistrationEnabled = AuthConfig.getRestRegistrationEnabled();
        verifyEmailEnabled = AuthConfig.getVerifyEmailEnabled();
        verifyRequiredEnabled = AuthConfig.getVerificationRequired();
        contactInformation = AuthConfig.getContactInformation();
        eulaEnabled = AuthConfig.isEulaEnabled();
        externalAuthorization = AuthConfig.isExternalAuthorization();

        PasswordPolicy policy = AuthConfig.getPolicy();

        minLength = policy.getMinLength();
        minLower = policy.getMinLowerCase();
        minNumbers = policy.getMinNumber();
        minSpecial = policy.getMinSpecial();
        minUpper = policy.getMinUpperCase();

        eula = AuthConfig.getEula();

        localAccountsEnabled = AuthConfig.getLocalAccountsEnabled();
        clientCertificateEnabled = AuthConfig.isClientCertificatesEnabled();
        hideClientCertificateButton = AuthConfig.isHideClientCertificateButton();

        adminEmail = AuthConfig.getAdminEmail();
    }

    /**
     * Saves the configuration in the database
     *
     * @return a {@link java.lang.String} object.
     */
    public String saveConfig() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            JSONResource config = auth.getConfig();
            config.setProperty(Vocabulary.REGISTRATION_ENABLED, registrationEnabled);
            config.setProperty(Vocabulary.DEFAULT_USER_ENABLED, defaultUserEnabled);
            config.setProperty(Vocabulary.REST_REGISTRATION_ENABLED, restRegistrationEnabled);
            config.setProperty(Vocabulary.VERIFY_EMAIL_ENABLED, verifyEmailEnabled);
            config.setProperty(Vocabulary.VERIFICATION_REQUIRED, verifyRequiredEnabled);
            config.setProperty(Vocabulary.CONTACT_INFO, contactInformation);
            config.setProperty(Vocabulary.LOCAL_ACCOUNTS_ENABLED, localAccountsEnabled);
            config.setProperty(Vocabulary.CLIENT_CERTIFICATE_ENABLED, clientCertificateEnabled);
            config.setProperty(Vocabulary.HIDE_CLIENT_CERTIFICATE_BUTTON, hideClientCertificateButton);

            config.setProperty(Vocabulary.EXTERNAL_AUTHORIZATION, externalAuthorization);
            config.setProperty(Vocabulary.EULA_ENABLED, eulaEnabled);

            JSONResource policy = new JSONResource();
            policy.setProperty(Vocabulary.PasswordPolicy.MIN_LENGTH, minLength);
            policy.setProperty(Vocabulary.PasswordPolicy.MIN_LOWER_CASE, minLower);
            policy.setProperty(Vocabulary.PasswordPolicy.MIN_NUMBERS, minNumbers);
            policy.setProperty(Vocabulary.PasswordPolicy.MIN_SPECIAL_CHARACTERS, minSpecial);
            policy.setProperty(Vocabulary.PasswordPolicy.MIN_UPPER_CASE, minUpper);

            config.setProperty(Vocabulary.PASSWORD_POLICY, policy);

            if(fileLogo != null) {
                byte[] target = IOUtils.toByteArray(fileLogo.getInputStream());
                JSONResource data = new JSONResource();
                data.setProperty(Vocabulary.IMAGE_DATA, Base64.encodeBase64String(target));
                auth.get(ConfigDAO.class).updateConfig(Vocabulary.LOGO, data);

                ImageServlet.setLogo(target);
            }

            auth.saveConfig(config);

            JSONResource eulaConf = new JSONResource();
            eulaConf.setProperty(Vocabulary.EULA, eula);
            auth.get(ConfigDAO.class).updateConfig(Vocabulary.EULA, eulaConf);

            JSONResource adminConf = new JSONResource();
            adminConf.setProperty(Vocabulary.EMAIL, adminEmail);
            auth.get(ConfigDAO.class).updateConfig(Vocabulary.ADMIN, adminConf);

            auth.commit();

            AuthConfig.reloadConfig(auth);
            Message.addInfo("configSaved");
        } catch (DAOException | IOException e) {
            e.printStackTrace();
            Message.addError("admin.saveConfigFailed");
        }

        return null;
    }

    /**
     * Validates the Logo file.
     * @param ctx
     * @param comp
     * @param value
     */
    public void validateLogoFile(FacesContext ctx, UIComponent comp, Object value) {
        if(value == null) {
            return;
        }
        List<FacesMessage> msgs = new ArrayList<>();
        Part file = (Part) value;

        if (file.getSize() > 1024 * 1024) {
            msgs.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, "File too big", "File too big, reduce size!"));
        }

        if (!"image/svg+xml".equals(file.getContentType())) {
            msgs.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Not an SVG: " + file.getContentType(), "Not an SVG!"));
        }

        if (!msgs.isEmpty()) {
            throw new ValidatorException(msgs);
        }
    }

    /**
     * <p>Getter for the field <code>defaultUserEnabled</code>.</p>
     *
     * @return the defaultUserEnabled
     */
    public Boolean getDefaultUserEnabled() {
        return defaultUserEnabled;
    }

    /**
     * <p>Setter for the field <code>defaultUserEnabled</code>.</p>
     *
     * @param defaultUserEnabled the defaultUserEnabled to set
     */
    public void setDefaultUserEnabled(Boolean defaultUserEnabled) {
        this.defaultUserEnabled = defaultUserEnabled;
    }

    /**
     * <p>Getter for the field <code>registrationEnabled</code>.</p>
     *
     * @return the registrationEnabled
     */
    public Boolean getRegistrationEnabled() {
        return registrationEnabled;
    }

    /**
     * <p>Setter for the field <code>registrationEnabled</code>.</p>
     *
     * @param registrationEnabled the registrationEnabled to set
     */
    public void setRegistrationEnabled(Boolean registrationEnabled) {
        this.registrationEnabled = registrationEnabled;
    }

    /**
     * <p>Getter for the field <code>restRegistrationEnabled</code>.</p>
     *
     * @return the restRegistrationEnabled
     */
    public Boolean getRestRegistrationEnabled() {
        return restRegistrationEnabled;
    }

    /**
     * <p>Setter for the field <code>restRegistrationEnabled</code>.</p>
     *
     * @param restRegistrationEnabled the restRegistrationEnabled to set
     */
    public void setRestRegistrationEnabled(Boolean restRegistrationEnabled) {
        this.restRegistrationEnabled = restRegistrationEnabled;
    }

    /**
     * <p>Getter for the field <code>verifyEmailEnabled</code>.</p>
     *
     * @return the verifyEmailEnabled
     * @since 1.4.0
     */
    public Boolean getVerifyEmailEnabled() {
        return verifyEmailEnabled;
    }

    /**
     * <p>Setter for the field <code>verifyEmailEnabled</code>.</p>
     *
     * @param verifyEmailEnabled the verifyEmailEnabled to set
     * @since 1.4.0
     */
    public void setVerifyEmailEnabled(Boolean verifyEmailEnabled) {
        this.verifyEmailEnabled = verifyEmailEnabled;
    }

    /**
     * <p>Getter for the field <code>verifyRequiredEnabled</code>.</p>
     *
     * @return the verifyRequiredEnabled
     * @since 1.4.0
     */
    public Boolean getVerifyRequiredEnabled() {
        return verifyRequiredEnabled;
    }

    /**
     * <p>Setter for the field <code>verifyRequiredEnabled</code>.</p>
     *
     * @param verifyRequiredEnabled the verifyRequiredEnabled to set
     * @since 1.4.0
     */
    public void setVerifyRequiredEnabled(Boolean verifyRequiredEnabled) {
        this.verifyRequiredEnabled = verifyRequiredEnabled;
    }

    /**
     * @return the minLength
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * @param minLength the minLength to set
     */
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    /**
     * @return the minNumbers
     */
    public int getMinNumbers() {
        return minNumbers;
    }

    /**
     * @param minNumbers the minNumbers to set
     */
    public void setMinNumbers(int minNumbers) {
        this.minNumbers = minNumbers;
    }

    /**
     * @return the minSpecial
     */
    public int getMinSpecial() {
        return minSpecial;
    }

    /**
     * @param minSpecial the minSpecial to set
     */
    public void setMinSpecial(int minSpecial) {
        this.minSpecial = minSpecial;
    }

    /**
     * @return the minUpper
     */
    public int getMinUpper() {
        return minUpper;
    }

    /**
     * @param minUpper the minUpper to set
     */
    public void setMinUpper(int minUpper) {
        this.minUpper = minUpper;
    }

    /**
     * @return the minLower
     */
    public int getMinLower() {
        return minLower;
    }

    /**
     * @param minLower the minLower to set
     */
    public void setMinLower(int minLower) {
        this.minLower = minLower;
    }

    /**
     * @return the contactInformation
     */
    public String getContactInformation() {
        return contactInformation;
    }

    /**
     * @param contactInformation the contactInformation to set
     */
    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

    /**
     * @return the file
     */
    public Part getFileLogo() {
        return fileLogo;
    }

    /**
     * @param file the file to set
     */
    public void setFileLogo(Part file) {
        this.fileLogo = file;
    }

    public Boolean getExternalAuthorization() {
        return externalAuthorization;
    }

    public void setExternalAuthorization(Boolean externalAuthorization) {
        this.externalAuthorization = externalAuthorization;
    }

    public Boolean getEulaEnabled() {
        return eulaEnabled;
    }

    public void setEulaEnabled(Boolean eulaEnabled) {
        this.eulaEnabled = eulaEnabled;
    }

    public String getEula() {
        return eula;
    }

    public void setEula(String eula) {
        this.eula = eula;
    }

    public Boolean getLocalAccountsEnabled() {
        return localAccountsEnabled;
    }

    public void setLocalAccountsEnabled(Boolean localAccountsEnabled) {
        this.localAccountsEnabled = localAccountsEnabled;
    }

    public Boolean getClientCertificateEnabled() {
        return clientCertificateEnabled;
    }

    public void setClientCertificateEnabled(Boolean clientCertificateEnabled) {
        this.clientCertificateEnabled = clientCertificateEnabled;
    }

    public Boolean getHideClientCertificateButton() {
        return hideClientCertificateButton;
    }

    public void setHideClientCertificateButton(Boolean hideClientCertificateButton) {
        this.hideClientCertificateButton = hideClientCertificateButton;
    }

    /**
     * @return the admin email
     */
    public String getAdminEmail() {
        return adminEmail;
    }

    /**
     * @param adminEmail the admin email to set
     */
    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }
}
