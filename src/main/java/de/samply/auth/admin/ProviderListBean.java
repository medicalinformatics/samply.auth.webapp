/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import de.samply.auth.AuthConfig;
import de.samply.auth.LdapClient;
import de.samply.auth.Message;
import de.samply.auth.SqlConnection;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ProviderDAO;
import de.samply.auth.dao.dto.*;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;
import org.apache.commons.codec.binary.Base64;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.NamingException;
import java.io.Serializable;
import java.security.PublicKey;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;

/**
 * Manages the list of external identity providers in the admin interface.
 */
@ManagedBean
@ViewScoped
public class ProviderListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of currently available external identity providers
     */
    private List<IdentityProvider> providers;

    /**
     * The selected identity provider in edit mode. Otherwise null.
     */
    private IdentityProvider provider = null;

    /**
     * The identifier for this external identity provider
     */
    private String identifier;

    /**
     * The label for the new identity provider.
     */
    private String label;

    /**
     * The URL for the new identity provider (e.g. "ldap://uni-mainz.de" or "https://dktk.dkfz.de/adfs/oauth2").
     */
    private String url;

    /**
     * The provider type for the new identity provider.
     */
    private ProviderType type = ProviderType.LDAP;

    /**
     * The attributes for the new ADFS identity provider.
     */
    private String publicKey, resource, clientId;

    /**
     * The attribute names for the various attributes., e.g. nameAttribute = "displayName" in AD.
     */
    private String emailAttribute, nameAttribute, firstNameAttribute, lastNameAttribute, idAttribute, memberAttribute;

    /**
     * The attributes for the new LDAP identity provider.
     */
    private String searchBase, baseDN, searchFilter, bindDN, bindPassword;

    /**
     * The additional attribute for the new SQL identity provider.
     */
    private String sqlDriver, searchQuery;

    /**
     * The link for the password recovery function of this identity provider
     */
    private String passwordRecoveryLink;

    /**
     * Input test field used to test LDAP.
     */
    private String testLdapInput;

    /**
     * If true, the LDAP bind should be anonymous
     */
    private boolean anonymous;

    /**
     * If true, new users from this external identity provider are activated by default.
     */
    private Boolean defaultActive;

    /**
     * If true, the email address of new users will be flagged as verified by default.
     */
    private Boolean defaultVerified;

    /**
     * The active attribute of the identity provider.
     */
    private Boolean active;

    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            providers = auth.get(ProviderDAO.class).getAllProviders();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds or edits the currently selected provider.
     * @return
     */
    public String commit() {
        IdentityProvider provider = null;

        /**
         * Append a trailing slash.
         */
        if(!url.endsWith("/")) {
            url = url + "/";
        }

        if(type == ProviderType.LDAP) {
            LdapProvider ldap = new LdapProvider();
            ldap.setBaseDN(baseDN);
            ldap.setAnonymous(anonymous);
            ldap.setBindDN(bindDN);
            ldap.setBindPassword(getPassword());
            ldap.setSearchBase(searchBase);
            ldap.setSearchFilter(searchFilter);
            provider = ldap;
        } else if(type == ProviderType.ADFS) {
            AdfsProvider adfs = new AdfsProvider();

            String input = publicKey.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
            PublicKey rsaKey = KeyLoader.loadKey(input);

            adfs.setPublicKey(Base64.encodeBase64String(rsaKey.getEncoded()));
            adfs.setClientId(clientId);
            adfs.setResource(resource);
            provider = adfs;
        } else if(type == ProviderType.SQL) {
            SqlProvider sqlP = new SqlProvider();
            sqlP.setSqlDriver(sqlDriver);
            sqlP.setBindDN(bindDN);
            sqlP.setBindPassword(bindPassword);
            sqlP.setSearchQuery(searchQuery);

            // The attributes values are not needed, because the data is extracted from SQL query
            if(memberAttribute==null || memberAttribute.trim().isEmpty()) {
                memberAttribute = "!disabled";
            }
            if(emailAttribute==null || emailAttribute.trim().isEmpty()) {
                emailAttribute = "!disabled";
            }
            if(idAttribute==null || idAttribute.trim().isEmpty()) {
                idAttribute = "!disabled";
            }
            if(nameAttribute==null || nameAttribute.trim().isEmpty()) {
                nameAttribute = "!disabled";
            }
            if(firstNameAttribute==null || firstNameAttribute.trim().isEmpty()) {
                firstNameAttribute = "!disabled";
            }
            if(lastNameAttribute==null || lastNameAttribute.trim().isEmpty()) {
                lastNameAttribute = "!disabled";
            }

            provider = sqlP;
        } else {
            throw new UnsupportedOperationException();
        }

        provider.setMemberAttribute(memberAttribute);
        provider.setEmailAttribute(emailAttribute);
        provider.setIdAttribute(idAttribute);
        provider.setNameAttribute(nameAttribute);
        provider.setFirstNameAttribute(firstNameAttribute);
        provider.setLastNameAttribute(lastNameAttribute);
        provider.setLabel(label);
        provider.setUrl(url);
        provider.setIdentifier(identifier);
        provider.setPasswordRecoveryLink(passwordRecoveryLink);
        provider.setDefaultUserEnabled(defaultActive);
        provider.setDefaultUserVerified(defaultVerified);
        provider.setActive(active);

        try (AuthConnection auth = ConnectionFactory.get()) {
            if(this.provider == null) {
                auth.get(ProviderDAO.class).saveProvider(provider);
            } else {
                provider.setId(this.provider.getId());
                auth.get(ProviderDAO.class).updateProvider(provider);
            }
            auth.commit();

            AuthConfig.reloadConfig(auth);

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "external";
    }

    /**
     * Cancels the edit process.
     * @return
     */
    public String cancel() {
        reset();
        return null;
    }

    /**
     * Deletes an identity provider. This may have unpredictable consequences, like users using the external provider losing access, even after
     * readding the identity provider (because it has a new ID).
     * @param provider
     * @return
     */
    public String delete(IdentityProvider provider) {
        try (AuthConnection auth = ConnectionFactory.get()) {
            auth.get(ProviderDAO.class).deleteProvider(provider);
            auth.commit();

            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "external";
    }

    /**
     * Resets this bean.
     */
    private void reset() {
        this.provider = null;
        this.type = ProviderType.LDAP;
        this.anonymous = false;
        this.baseDN = "";
        this.bindDN = "";
        this.clientId = "";
        this.emailAttribute = "";
        this.idAttribute = "";
        this.nameAttribute = "";
        this.publicKey = "";
        this.sqlDriver = "";
        this.searchQuery = "";
        this.label = "";
        this.resource = "";
        this.searchBase = "";
        this.searchFilter = "";
        this.memberAttribute = "";
        this.firstNameAttribute = "";
        this.lastNameAttribute = "";
        this.url = "";
        this.identifier = "";
        this.passwordRecoveryLink = "";
        this.defaultActive = false;
        this.defaultVerified = false;
        this.active = true;
    }

    /**
     * Starts to edit the given identity provider.
     * @param provider
     * @return
     */
    public String edit(IdentityProvider provider) {
        this.provider = provider;

        if(provider instanceof LdapProvider) {
            LdapProvider ldap = (LdapProvider) provider;
            searchBase = ldap.getSearchBase();
            searchFilter = ldap.getSearchFilter();
            baseDN = ldap.getBaseDN();
            anonymous = ldap.isAnonymous();
            bindPassword = ldap.getBindPassword();
            bindDN = ldap.getBindDN();
            type = ProviderType.LDAP;
        } else if(provider instanceof AdfsProvider) {
            AdfsProvider adfs = (AdfsProvider) provider;
            publicKey = adfs.getPublicKey();
            resource = adfs.getResource();
            clientId = adfs.getClientId();
            type = ProviderType.ADFS;
        } else if(provider instanceof SqlProvider) {
            SqlProvider sqlP = (SqlProvider) provider;
            sqlDriver = sqlP.getSqlDriver();
            bindDN = sqlP.getBindDN();
            bindPassword = sqlP.getBindPassword();
            searchQuery = sqlP.getSearchQuery();
            type = ProviderType.SQL;
        } else {
            throw new UnsupportedOperationException();
        }

        memberAttribute = provider.getMemberAttribute();
        emailAttribute = provider.getEmailAttribute();
        idAttribute = provider.getIdAttribute();
        nameAttribute = provider.getNameAttribute();
        firstNameAttribute = provider.getFirstNameAttribute();
        lastNameAttribute = provider.getLastNameAttribute();
        label = provider.getLabel();
        url = provider.getUrl();
        identifier = provider.getIdentifier();
        passwordRecoveryLink = provider.getPasswordRecoveryLink();
        defaultActive  = provider.isDefaultUserEnabled();
        defaultVerified = provider.isDefaultUserVerified();
        active = provider.isActive();
        return null;
    }

    /**
     * Tests the LDAP connection.
     */
    public void testLdap() {
        String password = null;
        String bindDN = null;
        if(! anonymous) {
            password = getPassword();
            bindDN = this.bindDN;
        }

        try(LdapClient client = new LdapClient(url, baseDN, bindDN, password)) {
            if(! client.checkBind()) {
                Message.addError("ldapTestBindFailed");
            } else {
                if(client.search(searchBase, MessageFormat.format(searchFilter, testLdapInput)) != null) {
                    Message.addInfo("ldapTestSuccessful");
                } else {
                    Message.addError("ldapTestNoResult");
                }
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the SQL connection (only connectivity)
     */
    public void testSQL() {

        SqlConnection sqlConn = new SqlConnection(url, sqlDriver, bindDN, bindPassword, null);

        try {
            if (sqlConn.connectToSql()) {
                Message.addInfo("sqlTestBindSuccessful");
                sqlConn.close();
            }
        } catch (SQLException e) {
            Message.addInfo("sqlTestBindFailed");
        }
    }

    /**
     * Returns the current LDAP bind password
     * @return
     */
    public String getPassword() {
        if(StringUtil.isEmpty(bindPassword) && provider instanceof LdapProvider) {
            return ((LdapProvider) provider).getBindPassword();
        } else {
            return bindPassword;
        }
    }

    /**
     * @return the providers
     */
    public List<IdentityProvider> getProviders() {
        return providers;
    }

    /**
     * @param providers the providers to set
     */
    public void setProviders(List<IdentityProvider> providers) {
        this.providers = providers;
    }

    /**
     * @return the selectedProvider
     */
    public IdentityProvider getProvider() {
        return provider;
    }

    /**
     * @param selectedProvider the selectedProvider to set
     */
    public void setProvider(IdentityProvider selectedProvider) {
        this.provider = selectedProvider;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the type
     */
    public ProviderType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ProviderType type) {
        this.type = type;
    }

    /**
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the searchBase
     */
    public String getSearchBase() {
        return searchBase;
    }

    /**
     * @param searchBase the searchBase to set
     */
    public void setSearchBase(String searchBase) {
        this.searchBase = searchBase;
    }

    /**
     * @return the baseDN
     */
    public String getBaseDN() {
        return baseDN;
    }

    /**
     * @param baseDN the baseDN to set
     */
    public void setBaseDN(String baseDN) {
        this.baseDN = baseDN;
    }

    /**
     * @return the searchFilter
     */
    public String getSearchFilter() {
        return searchFilter;
    }

    /**
     * @param searchFilter the searchFilter to set
     */
    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }

    /**
     * @return the emailAttribute
     */
    public String getEmailAttribute() {
        return emailAttribute;
    }

    /**
     * @param emailAttribute the emailAttribute to set
     */
    public void setEmailAttribute(String emailAttribute) {
        this.emailAttribute = emailAttribute;
    }

    /**
     * @return the nameAttribute
     */
    public String getNameAttribute() {
        return nameAttribute;
    }

    /**
     * @param nameAttribute the nameAttribute to set
     */
    public void setNameAttribute(String nameAttribute) {
        this.nameAttribute = nameAttribute;
    }

    /**
     * @return the idAttribute
     */
    public String getIdAttribute() {
        return idAttribute;
    }

    /**
     * @param idAttribute the idAttribute to set
     */
    public void setIdAttribute(String idAttribute) {
        this.idAttribute = idAttribute;
    }

    /**
     * @return the bindDN
     */
    public String getBindDN() {
        return bindDN;
    }

    /**
     * @param bindDN the bindDN to set
     */
    public void setBindDN(String bindDN) {
        this.bindDN = bindDN;
    }

    /**
     * @return the bindPassword
     */
    public String getBindPassword() {
        return bindPassword;
    }

    /**
     * @param sqlDriver the sqlDriver to set
     */
    public void setSqlDriver(String sqlDriver) {
        this.sqlDriver = sqlDriver;
    }

    public String getSqlDriver() {
        return sqlDriver;
    }

    /**
     * @param searchQuery the searchQuery to set
     */
    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    /**
     * @param bindPassword the bindPassword to set
     */
    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

    /**
     * @return the anonymous
     */
    public boolean isAnonymous() {
        return anonymous;
    }

    /**
     * @param anonymous the anonymous to set
     */
    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the memberAttribute
     */
    public String getMemberAttribute() {
        return memberAttribute;
    }

    /**
     * @param memberAttribute the memberAttribute to set
     */
    public void setMemberAttribute(String memberAttribute) {
        this.memberAttribute = memberAttribute;
    }

    /**
     * @return the firstNameAttribute
     */
    public String getFirstNameAttribute() {
        return firstNameAttribute;
    }

    /**
     * @param firstNameAttribute the firstNameAttribute to set
     */
    public void setFirstNameAttribute(String firstNameAttribute) {
        this.firstNameAttribute = firstNameAttribute;
    }

    /**
     * @return the lastNameAttribute
     */
    public String getLastNameAttribute() {
        return lastNameAttribute;
    }

    /**
     * @param lastNameAttribute the firstNameAttribute to set
     */
    public void setLastNameAttribute(String lastNameAttribute) {
        this.lastNameAttribute = lastNameAttribute;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the passwordRecoveryLink
     */
    public String getPasswordRecoveryLink() {
        return passwordRecoveryLink;
    }

    /**
     * @param passwordRecoveryLink the passwordRecoveryLink to set
     */
    public void setPasswordRecoveryLink(String passwordRecoveryLink) {
        this.passwordRecoveryLink = passwordRecoveryLink;
    }

    /**
     * @return the testLdapInput
     */
    public String getTestLdapInput() {
        return testLdapInput;
    }

    /**
     * @param testLdapInput the testLdapInput to set
     */
    public void setTestLdapInput(String testLdapInput) {
        this.testLdapInput = testLdapInput;
    }

    /**
     * @return the defaultActive
     */
    public Boolean getDefaultActive() {
        return defaultActive;
    }

    /**
     * @param defaultActive the defaultActive to set
     */
    public void setDefaultActive(Boolean defaultActive) {
        this.defaultActive = defaultActive;
    }

    public Boolean getDefaultVerified() {
        return defaultVerified;
    }

    public void setDefaultVerified(Boolean defaultVerified) {
        this.defaultVerified = defaultVerified;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
