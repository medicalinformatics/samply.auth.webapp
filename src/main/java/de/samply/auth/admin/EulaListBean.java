/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.admin;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.EulaDAO;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Eula;
import de.samply.sdao.DAOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.*;

import java.util.Locale;

/**
 * The eula list view in the admin interface.
 *
 */
@ManagedBean
@ViewScoped
public class EulaListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of all currently available eulas.
     */
    private List<Eula> eulas;

    /**
     * The eula type
     */
    private String eulaType;

    /**
     * The eula version
     */
    private int version;

    /**
     * The current eula language
     */
    private String language;

    /**
     * If true, new additional language is needed,
     * otherwise only the available languages are used for eula
     */
    private Boolean newLang;

    /**
     * Eula title
     */
    public String title;

    /**
     * Eula main text (in Html or Text format)
     */
    public String htmlText;

    /**
     * Text for eula checkbox
     */
    public String checkBoxLabel;

    /**
     * The eula that is currently edited.
     */
    private Eula eula;

    /**
     * True, if preview for eula is on, otherwise false
     */
    private boolean preview = false;

    /**
     * Initializes this bean. Loads all eulas from the database,
     * set version 1 and language to default environment language for a new eula
     */
    @PostConstruct
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            eulas = auth.get(EulaDAO.class).getAllEulaTypes();
            version = 1;
            Locale locale = Locale.getDefault();
            language = locale.getLanguage();
            newLang = false;
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates the new eula or edit the existent eula
     *
     * @return a {@link String} object.
     */
    public String commit() {
        Eula eulaToSave;
        Eula eulaOldVersion = null;
        if (this.eula == null) {
            eulaToSave = new Eula();
        } else {
            eulaToSave = this.eula;
        }
        eulaToSave.setEulaType(eulaType);
        eulaToSave.setVersion(version);
        eulaToSave.setTitleForLang(language, title);
        eulaToSave.setHtmlTextForLang(language, htmlText);
        eulaToSave.setCheckBoxLabelForLang(language, checkBoxLabel);

        try(AuthConnection auth = ConnectionFactory.get()) {
            if(this.eula == null) {
                // If new version added, extract the old eula version
                if (eulaToSave.getVersion() > 1) {
                    eulaOldVersion = auth.get(EulaDAO.class).getEulaLastVersion(eulaToSave.getEulaType());
                }
                auth.get(EulaDAO.class).saveEula(eulaToSave);
                if (eulaOldVersion != null) {
                    // Update the eula version needed to be accepted for clients
                    auth.get(ClientDAO.class).updateEulaVersion(eulaOldVersion, eulaToSave);
                }
            } else {
                eulaToSave.setId(eulaToSave.getId());
                auth.get(EulaDAO.class).updateEula(eulaToSave);
                this.eula = null;
            }

            auth.commit();
            reset();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "eulas";
    }

    /**
     * <p>Starts the edit mode for the specified eula.</p>
     *
     * @param eula a {@link Client} object.
     * @return a {@link String} object.
     * @since 1.4.RC4
     */
    public String edit(Eula eula) {
        this.eula = eula;
        this.eulaType = eula.getEulaType();
        this.version = eula.getVersion();
        if (language == null) {
            this.language = eula.getLanguage();
        }
        this.title = eula.getTitleForLang(this.language);
        this.htmlText = eula.getHtmlTextForLang(this.language);
        this.checkBoxLabel = eula.getCheckBoxLabelForLang(this.language);
        this.newLang = false;
        this.preview = false;
        return null;
    }

    /**
     * <p>cancel.</p>
     *
     * @return a {@link String} object.
     * @since 1.4.RC4
     */
    public String cancel() {
        reset();
        return null;
    }

    /**
     * Resets the defaults value for a new eula
     */
    private void reset() {
        this.eula = null;
        this.eulaType = null;
        this.version = 1;
        this.title = "";
        this.htmlText = "";
        this.checkBoxLabel = "";
        this.newLang = false;
        this.preview = false;
    }

    /**
     * <p>Getter for the field <code>eulas</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<Eula> getEulas() {
        return eulas;
    }

    /**
     * <p>Setter for the field <code>eulas</code>.</p>
     *
     * @param eulas a {@link List} object.
     */
    public void setEulas(List<Eula> eulas) {
        this.eulas = eulas;
    }

    /**
     * <p>Getter for the field <code>eulaType</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getEulaType() {
        return eulaType;
    }

    /**
     * <p>Setter for the field <code>eulaType</code>.</p>
     *
     * @param eulaType a {@link String} object.
     */
    public void setEulaType(String eulaType) {
        this.eulaType = eulaType;
    }

    /**
     * <p>Getter for the field <code>version</code>.</p>
     *
     * @return a {@link String} object.
     */
    public int getVersion() {
        return version;
    }

    /**
     * <p>Setter for the field <code>version</code>.</p>
     *
     * @param version a {@link String} object.
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the current edit language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the eula
     */
    public Eula getEula() {
        return eula;
    }

    /**
     * @param eula the eula to set
     */
    public void setEula(Eula eula) {
        this.eula = eula;
    }

    /**
     * @return the eula title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * @param title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the eula main html text
     */
    public String getHtmlText() {
        return this.htmlText;
    }

    /**
     * @param htmlText to set
     */
    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
    /**
     * @return the text for check box in eula
     */
    public String getCheckBoxLabel() {
        return this.checkBoxLabel;
    }

    /**
     * @param checkBoxLabel to set
     */
    public void setCheckBoxLabel(String checkBoxLabel) {
        this.checkBoxLabel = checkBoxLabel;
    }

    /**
     * Returns the list of all supported languages if new language to be added,
     * otherwise the list of available languages in eula
     *
     * @return a {@link Set<String>} object.
     */
    public Set<String> getLanguages() {
        // List of all supported languages (except those which already are in eula)
        if (newLang || this.eula == null) {
            Set<String> languages = new HashSet<String>();
            Locale[] locale = Locale.getAvailableLocales();
            for (int i=0; i<locale.length; i++) {
                if (eula == null || !eula.getLanguages().contains(locale[i].getLanguage())) {
                    languages.add(locale[i].getLanguage());
                }
            }
            return languages;
        }

        return eula.getLanguages();
    }

    /**
     * Changes the current language after select event
     */
    public void changeLanguage() {
        if (eula != null) {
            this.title = eula.getTitleForLang(this.language);
            this.htmlText = eula.getHtmlTextForLang(this.language);
            this.checkBoxLabel = eula.getCheckBoxLabelForLang(this.language);
        }
    }

    /**
     * Starts the edit mode for the new version of given eula type
     */
    public String addVersion(Eula eula) {
        this.eula = null;
        this.eulaType = eula.getEulaType();
        this.language = eula.getLanguage();
        this.version = eula.getVersion() + 1;
        this.title = eula.getTitleForLang(this.language);
        this.newLang = false;
        this.htmlText = eula.getHtmlTextForLang(this.language);
        this.checkBoxLabel = eula.getCheckBoxLabelForLang(this.language);
        this.preview = false;
        return null;
    }

    /**
     * Starts the edit mode for the new language of given eula
     */
    public String addLanguage(Eula eula) {
        this.eula = eula;
        this.eulaType = eula.getEulaType();
        this.version = eula.getVersion();
        this.title = "";
        this.htmlText = "";
        this.checkBoxLabel = "";
        this.newLang = true;
        this.preview = false;
        return null;
    }

    public void startPreview() {
        this.preview = true;
    }

    public void stopPreview() {
        this.preview = false;
    }

    public boolean getPreview() {
        return preview;
    }
}
