/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.beans;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import de.samply.auth.AuthConfig;
import de.samply.auth.Constants;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientCertificateDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.ClientCertificate;
import de.samply.auth.exception.LoginException;
import de.samply.auth.utils.HashUtils;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * The bean that manages the login via client certificate. Only certain users are allowed to do that, and also
 */
@ManagedBean
@ViewScoped
public class LoginCertificateBean implements Serializable {

    private static final long serialVersionUID = 4228170073480670380L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value =  "#{grantBean}")
    private GrantBean grantBean;

    public String init() throws LoginException, CertificateException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String sslClientCert = request.getHeader(Constants.HEADER_SSL_CLIENT_CERT);
        String sslClientVerify = request.getHeader(Constants.HEADER_SSL_CLIENT_VERIFY);

        if(AuthConfig.isClientCertificatesEnabled() && sslClientCert != null && Constants.SSL_CLIENT_VERIFY_SUCCESS.equals(sslClientVerify)) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            String cert = sslClientCert
                    .replace("-----BEGIN CERTIFICATE-----", "")
                    .replace("-----END CERTIFICATE-----", "")
                    .replace(" ", "");

            try (AuthConnection auth = ConnectionFactory.get()) {
                if (!StringUtil.isEmpty(cert)) {
                    Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(Base64.decodeBase64(cert)));

                    if(certificate instanceof X509Certificate) {
                        X509Certificate x509 = (X509Certificate) certificate;
                        String hash = HashUtils.SHA512(Base64.decodeBase64(cert));
                        ClientCertificate clientCertifiate = auth.get(ClientCertificateDAO.class).getClientCertificate(hash);

                        if(clientCertifiate != null && x509.getSubjectDN().toString().equals(clientCertifiate.getSubjectDN())
                                && x509.getSerialNumber().toString().equals(clientCertifiate.getSerialNumber())) {
                            return userBean.login(clientCertifiate, !StringUtil.isEmpty(grantBean.getRedirectUrl()));
                        }
                    }
                }
            } catch (CertificateException ignored) {
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Something went wrong... return to the login page.
         */
        return "/login?faces-redirect=true&includeViewParams=true";
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public GrantBean getGrantBean() {
        return grantBean;
    }

    public void setGrantBean(GrantBean grantBean) {
        this.grantBean = grantBean;
    }
}
