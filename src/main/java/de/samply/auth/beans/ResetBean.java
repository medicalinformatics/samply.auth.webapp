/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.PasswordPolicy;
import de.samply.auth.SecurityUtils;
import de.samply.auth.SecurityUtils.PasswordStatus;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.User;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * This bean handles the reset password method
 *
 * @since 1.4.0
 */
@ManagedBean
@ViewScoped
public class ResetBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger logger = LoggerFactory.getLogger(ResetBean.class);

    /**
     * The users email
     */
    private String email;

    /**
     * The code used for that reset request
     */
    private String code;

    /**
     * The users new password in clear text
     */
    private String password;

    /**
     * The users new password in clear text, must be equal to password
     */
    private String passwordRepeat;

    /**
     * The user from the database.
     */
    private User user;

    /**
     * This method sends an email to the users email address with a code that is valid for one hour.
     *
     * @return a {@link java.lang.String} object.
     */
    public String recover() {

        try(AuthConnection auth = ConnectionFactory.get()) {
            UserDAO userDAO = auth.get(UserDAO.class);
            User user = userDAO.getInternalUser(email);

            if(user != null) {
                if(user.isExternal()) {
                    Message.addError("passwordExternalUser");
                    return null;
                }

                MailUtils.sendRecoveryMail(user);

                logger.debug("User " + email + " is requesting to reset his password. Email has been sent.");
                auth.commit();
            }
        } catch (DAOException e) {
            e.printStackTrace();
            return "/error?error=errorOccured";
        }

        /**
         * In any case, show the user that the email has been sent.
         */
        Message.addInfo("recoveryMailSent");
        return null;
    }

    /**
     * This is called when the user clicks on the link in the email. Just load the user
     * and check if the code is not valid, return to the login page.
     *
     * @return a {@link java.lang.String} object.
     */
    public String initialize() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            UserDAO userDAO = auth.get(UserDAO.class);

            user = userDAO.getUserRecoverCode(code);

            /**
             * Just check if this code is valid. getUserCode does that for us.
             */
            if(user == null) {
                Message.addError("codeInvalid", true);
                return "/login?faces-redirect=true";
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Updates the new users password in the database.
     *
     * @return a {@link java.lang.String} object.
     */
    public String reset() {
        if(SecurityUtils.checkPasswordSecurity(password, AuthConfig.getPolicy()) != PasswordStatus.OK) {
            PasswordPolicy policy = AuthConfig.getPolicy();
            Message.add("passwordPolicy", false, FacesMessage.SEVERITY_ERROR,
                    policy.getMinLength(), policy.getMinLowerCase(), policy.getMinUpperCase(), policy.getMinNumber(),
                    policy.getMinSpecial());
            return null;
        }

        if(!password.equals(passwordRepeat)) {
            Message.addError("passwordsNotEqual");
            return null;
        }

        /**
         * If the code is valid, user will not be null, just do the password
         * reset.
         */
        if(user != null) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                UserDAO userDAO = auth.get(UserDAO.class);

                String salt = SecurityUtils.newRandomString();
                String hashedPassword = SecurityUtils.hashPassword(password, salt, AuthConfig.DEFAULT_ROUNDS);

                userDAO.resetPassword(code, user, hashedPassword, salt, AuthConfig.DEFAULT_ROUNDS);
                auth.commit();

                Message.addInfo("passwordReset", true);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        return "/login?faces-redirect=true";
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>password</code>.</p>
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * <p>Setter for the field <code>password</code>.</p>
     *
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * <p>Getter for the field <code>passwordRepeat</code>.</p>
     *
     * @return the passwordRepeat
     */
    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    /**
     * <p>Setter for the field <code>passwordRepeat</code>.</p>
     *
     * @param passwordRepeat the passwordRepeat to set
     */
    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    /**
     * <p>Getter for the field <code>user</code>.</p>
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * <p>Setter for the field <code>user</code>.</p>
     *
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

}
