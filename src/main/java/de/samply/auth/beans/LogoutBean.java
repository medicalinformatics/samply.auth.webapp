/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.rest.ServletUtil;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * Executes the logout and redirects the user if necessary.
 *
 */
@ManagedBean
@ViewScoped
public class LogoutBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The redirect URL that is used *after* the user has been logged
     * out
     */
    private String redirectUrl;

    /**
     * The client ID used to check if the redirect URL is valid
     */
    private String clientId;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * Executes the logout. If a redirect url has been provided, check the client Id and
     * redirect the user if everything looks good.
     */
    public void logout() {
        boolean shibboleth = false;

        if(userBean.getLoginValid() && userBean.getUser().isShibbolethUser()) {
            shibboleth = true;
        }

        userBean.logout(false);

        /**
         * If this request contains a redirect url, use it.
         */
        if(!StringUtil.isEmpty(clientId) && !StringUtil.isEmpty(redirectUrl)) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                ClientDAO dao = auth.get(ClientDAO.class);
                Client client = dao.getClient(clientId);

                if(client != null && client.getActive() && client.redirectUrlValid(redirectUrl)) {
                    if(shibboleth) {
                        ServletUtil.logoutShibboleth(redirectUrl);
                    } else {
                        FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
                    }
                }
            } catch (DAOException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <p>Getter for the field <code>clientId</code>.</p>
     *
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * <p>Setter for the field <code>clientId</code>.</p>
     *
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * <p>Getter for the field <code>redirectUrl</code>.</p>
     *
     * @return the redirectUrl
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * <p>Setter for the field <code>redirectUrl</code>.</p>
     *
     * @param redirectUrl the redirectUrl to set
     */
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * <p>Getter for the field <code>userBean</code>.</p>
     *
     * @return the userBean
     * @since 1.4.0
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * <p>Setter for the field <code>userBean</code>.</p>
     *
     * @param userBean the userBean to set
     * @since 1.4.0
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
