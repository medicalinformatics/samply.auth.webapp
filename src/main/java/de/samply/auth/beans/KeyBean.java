/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;
import java.security.PublicKey;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.auth.Message;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.KeyDAO;
import de.samply.auth.dao.dto.Key;
import de.samply.auth.utils.HashUtils;
import de.samply.sdao.DAOException;

/**
 * <p>Used to managed public keys in the user interface.</p>
 *
 * @since 1.4.RC4
 */
@ManagedBean
@ViewScoped
public class KeyBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * The Base64 encoded public RSA key
     */
    private String publicKey;

    /**
     * The key description
     */
    private String description;

    /**
     * The list of keys that belong to the user.
     */
    private List<Key> keys;

    /**
     * <p>initialize.</p>
     */
    @PostConstruct
    public void initialize() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            keys = auth.get(KeyDAO.class).getUserKeys(userBean.getUser().getId());
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the specified key into the database, if it is a valid RSA key.
     *
     * @return a {@link java.lang.String} object.
     */
    public String commit() {
        String input = publicKey.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "")
                .replaceAll("[\r\n]", "");
        PublicKey publicKey2 = KeyLoader.loadKey(input);

        if(publicKey2 != null) {
            try (AuthConnection auth = ConnectionFactory.get()) {
                KeyDAO keyDAO = auth.get(KeyDAO.class);
                Key key = new Key();
                key.setBase64EncodedKey(input);
                key.setDescription(description);
                key.setUserId(getUserBean().getUser().getId());
                key.setSha512Hash(HashUtils.SHA512(publicKey2.getEncoded()));

                keyDAO.saveKey(key);
                auth.commit();

                return "/keys.xhtml?faces-redirect=true";
            } catch (DAOException e) {
                e.printStackTrace();
            }
        } else {
            Message.addError("keyInvalid");
        }

        return null;
    }

    /**
     * Deletes the specified key. Called when the user pressed the delete button.
     *
     * @param key a {@link de.samply.auth.dao.dto.Key} object.
     * @return a {@link java.lang.String} object.
     */
    public String delete(Key key) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(KeyDAO.class).deleteKey(key);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return "/keys?faces-redirect=true";
    }

    /**
     * <p>Getter for the field <code>publicKey</code>.</p>
     *
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * <p>Setter for the field <code>publicKey</code>.</p>
     *
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>userBean</code>.</p>
     *
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * <p>Setter for the field <code>userBean</code>.</p>
     *
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * <p>Getter for the field <code>keys</code>.</p>
     *
     * @return the keys
     */
    public List<Key> getKeys() {
        return keys;
    }

    /**
     * <p>Setter for the field <code>keys</code>.</p>
     *
     * @param keys the keys to set
     */
    public void setKeys(List<Key> keys) {
        this.keys = keys;
    }

    /**
     * <p>getFingerPrint.</p>
     *
     * @param key a {@link de.samply.auth.dao.dto.Key} object.
     * @return a {@link java.lang.String} object.
     */
    public String getFingerPrint(Key key) {
        return HashUtils.getFingerPrint(key.getBase64EncodedKey());
    }

}
