/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.beans;

import de.samply.auth.AuthConfig;
import de.samply.auth.Shibboleth;
import de.samply.auth.ShibbolethUser;
import de.samply.string.util.StringUtil;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.*;

/**
 * Used to test the current shibboleth authentication.
 * Does not redirect the user to actually authenticate himself via shibboleth.
 */
@ManagedBean
@ViewScoped
public class ShibbolethTestBean implements Serializable {

    private static final long serialVersionUID = -7746787914263178570L;

    /**
     * If true, the user is indeed authenticated via shibboleth.
     */
    private Boolean authenticationDone = false;

    /**
     * If true, the user is authenticated via shibboleth, and the IdP sends a persistent ID.
     */
    private Boolean authenticationOk = false;

    /**
     * If true, the IdP sends all requested attributes
     */
    private Boolean attributesOk = false;

    private List<String> headerNames;
    private Map headerValues;

    public void init() {

        Shibboleth shibConfig = AuthConfig.getShibboleth();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        ShibbolethUser shibUser = ShibbolethUser.fromRequest(request, shibConfig);

        if(!StringUtil.isEmpty(shibUser.getIdentityProvider())) {
            authenticationDone = true;

            if (!StringUtil.isEmpty(shibUser.getIdentity())) {
                authenticationOk = true;

                if (!StringUtil.isEmpty(shibUser.getEmail()) && !StringUtil.isEmpty(shibUser.getName())) {
                    attributesOk = true;
                }
            }
        }

        // extract all the attributes from HTTP headers
        headerValues = ShibbolethUser.getHeadersInfoFromRequest(request);
        headerNames = new ArrayList<String> (headerValues.keySet());

        String email = AuthConfig.getAdminEmail();
        MailUtils.sendShibbolethTestMail(email, headerValues);
    }

    public List getHeaderNames() {
        return headerNames;
    }

    public void setHeaderNames(List<String> headerNames) {
        this.headerNames = headerNames;
    }

    public Map getHeaderValues() {
        return headerValues;
    }

    public void setHeaderValues(Map headerValues) {
        this.headerValues = headerValues;
    }

    public Boolean getAttributesOk() {
        return attributesOk;
    }

    public void setAttributesOk(Boolean attributesOk) {
        this.attributesOk = attributesOk;
    }

    public Boolean getAuthenticationOk() {
        return authenticationOk;
    }

    public void setAuthenticationOk(Boolean authenticationOk) {
        this.authenticationOk = authenticationOk;
    }

    public Boolean getAuthenticationDone() {
        return authenticationDone;
    }

    public void setAuthenticationDone(Boolean authenticationDone) {
        this.authenticationDone = authenticationDone;
    }
}
