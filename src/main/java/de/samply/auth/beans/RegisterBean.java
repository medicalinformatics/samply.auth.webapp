/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.NameUtils;
import de.samply.auth.PasswordPolicy;
import de.samply.auth.SecurityUtils;
import de.samply.auth.SecurityUtils.PasswordStatus;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.InvitationDAO;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.User;
import de.samply.auth.dao.dto.Usertype;
import de.samply.auth.exception.LoginException;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * Handles user registration.
 *
 */
@ManagedBean
@ViewScoped
public class RegisterBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Use a regular expression to check for invalid characters in the name.
     */
    public static final Pattern namePattern = Pattern.compile("^[- ,'\\.\\p{L}]+$", Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * The users email. Unique in the database.
     */
    private String email;

    /**
     * The users first name.
     */
    private String firstName;

    /**
     * The users last name.
     */
    private String lastName;

    /**
     * Some contact information. This field is not checked
     */
    private String contactData;

    /**
     * The users clear text password.
     */
    private String password, passwordRepeat;

    /**
     * If true, he accepts the eula.
     */
    private Boolean acceptEula = false;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{grantBean}")
    private GrantBean grantBean;

    /**
     * Called by register.xhtml. If the user has been invited, fill out the name and email fields.
     * @return
     */
    public String init() {
        if(userBean.isInvited()) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                Invitation invitation = auth.get(InvitationDAO.class).getInvitation(userBean.getInvitationUuid());

                if(invitation != null && !invitation.isUsed()) {
                    email = invitation.getEmail();
                    firstName = invitation.getFirstName();
                    lastName = invitation.getLastName();
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Creates a new user, if the registration process is enabled.
     *
     * @return a {@link java.lang.String} object.
     */
    public String register() {
        User user = registerUser();
        if(user != null) {
            String redirectURL = userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
            if(redirectURL == null) {
                /**
                 * In this case an error happened.
                 */
                return "/login.xhtml?faces-redirect=true&includeViewParams=true";
            } else {
                return redirectURL;
            }
        } else {
            return null;
        }
    }

    /**
     * Registers the current user.
     * @return
     */
    private User registerUser() {
        Pattern emailRegex = Pattern.compile(".*@.*\\..*");

        email = email.trim();

        if(!namePattern.matcher(firstName).matches() || !namePattern.matcher(lastName).matches()) {
            Message.addError("invalidName");
            return null;
        }

        if(!emailRegex.matcher(email).find()) {
            Message.addError("invalidEmailAddress");
            return null;
        }

        if(!AuthConfig.getRegistrationEnabled() && !userBean.isInvited()) {
            Message.addError("registrationDisabled");
            return null;
        }

        if(SecurityUtils.checkPasswordSecurity(password, AuthConfig.getPolicy()) != PasswordStatus.OK) {
            PasswordPolicy policy = AuthConfig.getPolicy();
            Message.add("passwordPolicy", false, FacesMessage.SEVERITY_ERROR,
                    policy.getMinLength(), policy.getMinLowerCase(), policy.getMinUpperCase(), policy.getMinNumber(),
                    policy.getMinSpecial());
            return null;
        }

        if(!password.equals(passwordRepeat)) {
            Message.addError("passwordsNotEqual");
            return null;
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            if(userBean.isInvited() && !userBean.isInvitationValid(auth)) {
                /**
                 * in this case the invitation link has been used already, throw an error
                 */
                Message.add("invitationLinkUsed", false, FacesMessage.SEVERITY_ERROR);
                return null;
            }

            UserDAO userDao = auth.get(UserDAO.class);
            User existing = userDao.getInternalUser(email);

            if(existing != null) {
                Message.addError("emailInUse");
                return null;
            }

            User user = new User();
            user.setEmail(email);

            /**
             * The contact field is disabled during the registration (for now)
             */
//            user.setContactData(contactData);

            user.setContactData("");
            user.setRounds(AuthConfig.DEFAULT_ROUNDS);

            user.setSalt(SecurityUtils.newRandomString());
            user.setHashedPassword(SecurityUtils.hashPassword(password, user.getSalt(), AuthConfig.DEFAULT_ROUNDS));

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setName(NameUtils.getDisplayName(firstName, lastName));

            user.setLanguage("en");

            /**
             * If the EULA is enabled, he *must* accept it, otherwise he did not accept it yet.
             */
            user.setEulaAccepted(acceptEula);
            user.setSearchable(acceptEula);

            /**
             * Only if the users uses the same email address, as the one specified in the invitation link,
             * he can be enabled and verified automatically.
             */
            if(userBean.isInvitationValid(auth) && userBean.getInvitation().getEmail().equalsIgnoreCase(email)) {
                user.setActive(true);
                user.setEmailVerified(true);
            } else {
                user.setActive(AuthConfig.getDefaultUserEnabled());
                user.setEmailVerified(false);
            }

            user.setUsertype(Usertype.NORMAL);
            user.setClientCertificate(false);

            userDao.saveUser(user);

            /**
             * Send an email, if verification is enabled and the user isn't verified already by using an
             * invitation link
             */
            if(AuthConfig.getVerifyEmailEnabled() && !user.getEmailVerified()) {
                MailUtils.resendActivationMail(auth, user);

                if(AuthConfig.getVerificationRequired()) {
                    Message.addInfo("registrationSuccessfulVerify");
                } else {
                    Message.addInfo("registrationSuccessful");
                }
            } else {
                Message.addInfo("registrationSuccessful");
            }

            auth.commit();

            /**
             * Automatic login, if verification is not required, and if the registration hasnt been internal.
             */
            if(user.getEmailVerified() || !AuthConfig.getVerificationRequired()) {
                userBean.login(0, email, password, !StringUtil.isEmpty(grantBean.getRedirectUrl()));
            }

            Message.keepMessages();
            return user;
        } catch (DAOException | LoginException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Getter for the field <code>password</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPassword() {
        return password;
    }

    /**
     * <p>Setter for the field <code>password</code>.</p>
     *
     * @param password a {@link java.lang.String} object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * <p>Getter for the field <code>passwordRepeat</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    /**
     * <p>Setter for the field <code>passwordRepeat</code>.</p>
     *
     * @param passwordRepeat a {@link java.lang.String} object.
     */
    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    /**
     * <p>Getter for the field <code>contactData</code>.</p>
     *
     * @return the contactData
     */
    public String getContactData() {
        return contactData;
    }

    /**
     * <p>Setter for the field <code>contactData</code>.</p>
     *
     * @param contactData the contactData to set
     */
    public void setContactData(String contactData) {
        this.contactData = contactData;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return the name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return the name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param lastName the last name to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public GrantBean getGrantBean() {
        return grantBean;
    }

    public void setGrantBean(GrantBean grantBean) {
        this.grantBean = grantBean;
    }

    public Boolean getAcceptEula() {
        return acceptEula;
    }

    public void setAcceptEula(Boolean acceptEula) {
        this.acceptEula = acceptEula;
    }
}
