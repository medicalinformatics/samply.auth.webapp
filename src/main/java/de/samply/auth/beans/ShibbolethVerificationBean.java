/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.beans;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.Shibboleth;
import de.samply.auth.ShibbolethUser;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ShibbolethVerificationDAO;
import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.ShibbolethVerification;
import de.samply.auth.exception.LoginException;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;

/**
 * Verifies the user by letting him enter his email address and name. Then it sends an email, so the verification
 * is complete. The ShibbolethVerificationFilter guarantees that there is a shibboleth identity when this bean
 * is called.
 */
@ManagedBean
@ViewScoped
public class ShibbolethVerificationBean implements Serializable {

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{grantBean}")
    private GrantBean grantBean;

    /**
     * The email address that the user entered.
     */
    private String email;

    /**
     * The first name of the user.
     */
    private String firstName;

    /**
     * The last name of the user.
     */
    private String lastName;

    /**
     * Whether the user accepted the eula.
     */
    private Boolean acceptEula = false;

    /**
     * The verification UUID when
     */
    private String verificationUuid;

    public String init() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Shibboleth shibConfig = AuthConfig.getShibboleth();
        ShibbolethUser shibUser = ShibbolethUser.fromRequest(request, shibConfig);

        if(!StringUtil.isEmpty(shibUser.getIdentity())) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                if (userBean.isInvited() && userBean.isInvitationValid(auth)) {
                    /**
                     * Create the user, because he is invited therefore his details are in the invitation
                     */
                    Invitation invitation = userBean.getInvitation();

                    if (userBean.shibbolethLogin(shibUser.getIdentity(), invitation.getName(), invitation.getFirstName(), invitation.getLastName(), invitation.getEmail())) {
                        return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
                    }
                }

                if (!StringUtil.isEmpty(verificationUuid)) {
                    ShibbolethVerification verification = auth.get(ShibbolethVerificationDAO.class)
                            .get(verificationUuid, shibUser.getIdentity());

                    /**
                     * If the verification link is valid, use the data from the verification object to verify the
                     * current user.
                     */
                    if(verification == null) {
                        Message.addError("shibVerificationLinkInvalid");
                    } else if (userBean.shibbolethLogin(shibUser.getIdentity(),
                            verification.getName(), verification.getFirstName(), verification.getLastName(), verification.getEmail(), shibConfig.isDefaultActive(),
                            true, true)) {

                        auth.get(ShibbolethVerificationDAO.class).delete(verification);
                        auth.commit();

                        return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
                    }
                }
            } catch (DAOException | LoginException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Sends the email and creates the shibboleth verification object.
     * @return
     */
    public String verify() {
        if(AuthConfig.isEulaEnabled() && !acceptEula) {
            Message.addError("acceptEulaRequired");
            return null;
        }

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Shibboleth shibConfig = AuthConfig.getShibboleth();
        ShibbolethUser shibUser = ShibbolethUser.fromRequest(request, shibConfig);

        try (AuthConnection auth = ConnectionFactory.get()) {
            ShibbolethVerification verification = new ShibbolethVerification();
            verification.setUuid(UUID.randomUUID());
            verification.setFirstName(firstName);
            verification.setLastName(lastName);
            verification.setEmail(email);
            verification.setShibbolethIdentity(shibUser.getIdentity());

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR, 2);

            verification.setExpirationDate(new Timestamp(calendar.getTime().getTime()));

            auth.get(ShibbolethVerificationDAO.class).save(verification);
            auth.commit();

            MailUtils.sendShibbolethVerification(verification);
            Message.addInfo("shibbolethVerificationSend");
        } catch (DAOException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public GrantBean getGrantBean() {
        return grantBean;
    }

    public void setGrantBean(GrantBean grantBean) {
        this.grantBean = grantBean;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getAcceptEula() {
        return acceptEula;
    }

    public void setAcceptEula(Boolean acceptEula) {
        this.acceptEula = acceptEula;
    }

    public String getVerificationUuid() {
        return verificationUuid;
    }

    public void setVerificationUuid(String verificationUuid) {
        this.verificationUuid = verificationUuid;
    }
}
