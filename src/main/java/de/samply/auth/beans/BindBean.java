/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.beans;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import de.samply.auth.AuthConfig;
import de.samply.auth.Constants;
import de.samply.auth.Message;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientCertificateDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.ClientCertificate;
import de.samply.auth.utils.HashUtils;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * Handles the bound between a client certificate and a user.
 */
@ManagedBean
@ViewScoped
public class BindBean implements Serializable {

    private static final long serialVersionUID = -5329880066357931428L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * Called by the bind.xhtml page in the clientCert directory.
     * @return
     * @throws CertificateException
     */
    public String init() throws CertificateException {
        if(AuthConfig.isClientCertificatesEnabled() && userBean.getLoginValid() && userBean.getUser().getClientCertificate()) {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            String sslClientCert = request.getHeader(Constants.HEADER_SSL_CLIENT_CERT);
            String sslClientVerify = request.getHeader(Constants.HEADER_SSL_CLIENT_VERIFY);

            String cert = sslClientCert
                    .replace("-----BEGIN CERTIFICATE-----", "")
                    .replace("-----END CERTIFICATE-----", "")
                    .replace(" ", "");

            try {
                if (!StringUtil.isEmpty(cert) && Constants.SSL_CLIENT_VERIFY_SUCCESS.equals(sslClientVerify)) {
                    /**
                     * Try to parse the certificate
                     */
                    Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(Base64.decodeBase64(cert)));

                    if(certificate instanceof X509Certificate) {
                        X509Certificate x509 = (X509Certificate) certificate;

                        boolean equalsEmailAddress = false;

                        /**
                         * Check for the email address
                         */
                        for (List<?> sub : x509.getSubjectAlternativeNames()) {
                            for (Object obj : sub) {
                                if(obj instanceof  String) {
                                    if (userBean.getUser().getEmail().equalsIgnoreCase((String) obj)) {
                                        equalsEmailAddress = true;
                                    }
                                }
                            }
                        }

                        /**
                         * The client certificate must contain the email address as subject alternative name.
                         */
                        if(!equalsEmailAddress) {
                            Message.add("invalidClientCertificate", true, FacesMessage.SEVERITY_ERROR);
                            return "/profile?faces-redirect";
                        }

                        /**
                         * At this point the certificate is valid. Create the has and store it in the DB.
                         */
                        ClientCertificate cc = new ClientCertificate();
                        String hash = HashUtils.SHA512(Base64.decodeBase64(cert));
                        cc.setCertificateHash(hash);
                        cc.setUserId(userBean.getUser().getId());
                        cc.setSubjectDN(x509.getSubjectDN().toString());
                        cc.setSerialNumber(x509.getSerialNumber().toString());

                        try (AuthConnection auth = ConnectionFactory.get()) {
                            if(auth.get(ClientCertificateDAO.class).getClientCertificate(hash) == null) {
                                auth.get(ClientCertificateDAO.class).saveClientCertificate(cc);
                                auth.commit();
                                Message.add("certificateBound", true, FacesMessage.SEVERITY_INFO);
                            }
                        } catch (DAOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch(CertificateException e) {
                System.out.println("Certificate not valid...");
            }
        }

        return "/profile?faces-redirect=true";
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }
}
