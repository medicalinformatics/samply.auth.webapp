/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.samply.adfs.client.AdfsClient;
import de.samply.auth.*;
import de.samply.auth.client.jwt.JWTAdfsAccessToken;
import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.*;
import de.samply.auth.exception.*;
import de.samply.auth.mail.MailUtils;
import de.samply.auth.rest.ServletUtil;
import de.samply.auth.utils.HashUtils;
import de.samply.auth.utils.OAuth2ClientConfig;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * <p>UserBean class.</p>
 *
 * @since 1.4.0
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(UserBean.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The current user, null if the user hasnt logged in yet.
     */
    private User user;

    /**
     * If true, the user has a valid login.
     */
    private Boolean loginValid = false;

    /**
     * The list of roles that are editable by this user.
     */
    private List<Role> roles = Collections.emptyList(), editableRoles = Collections.emptyList();

    /**
     * The list of locations that are editable by this user.
     */
    private List<Location> locations = Collections.emptyList(), editableLocations = Collections.emptyList();

    /**
     * A list of *active* role permissions for the current user.
     */
    private List<RolePermission> permissions = Collections.emptyList();

    /**
     * A list of *external* groups when using an external identity provider.
     */
    private List<String> externalGroups = Collections.emptyList();

    /**
     * Because some external identity providers need a redirect, those three values need to be stored
     * in a session scoped bean.
     */
    private String clientId, redirectUri, scope, state, nonce;

    /**
     * The ADFS provider Id that has been selected.
     */
    private int adfsProviderId;

    /**
     * The invitation UUID, that has been passed as argument, e.g. login.xhtml=invitation=abcde
     */
    private String invitationUuid;

    /**
     * The invitation itself.
     */
    private Invitation invitation;

    /**
     * If true, the user has accepted the EULA on the eula.xhtml page
     */
    private Boolean eulaAccepted;

    /**
     * A list of eulas need to be accepted by the current user.
     */
    private List<Eula> eulaToAccept = Collections.emptyList();

    /**
     * The selected remember me mode.
     */
    private RememberMeMode rememberMeMode;

    /**
     * The cookie that has been used for the authentication. Null if no cookie has been used.
     */
    private AuthenticationCookie cookie = null;

    /**
     * Checks if the password is correct for the given email
     *
     * @param email a {@link java.lang.String} object.
     * @param password a {@link java.lang.String} object.
     * @param grant a boolean.
     * @return a {@link java.lang.String} object.
     * @throws WrongPasswordException
     */
    public String login(int providerId, String email, String password, boolean grant) throws LoginException {
        /**
         * Usually the buttons at the login page are not shown if the identity provider is disabled, but just to be sure, check
         * again.
         */
        IdentityProvider identityProvider = AuthConfig.getIdentityProvider(providerId);
        if(identityProvider != null && ! identityProvider.isActive()) {
            Message.addError("idpDisabled");
            return null;
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            User user = auth.get(UserDAO.class).getInternalUser(email);

            if(providerId == 0) {
                if(user == null) {
                    throw new WrongPasswordException();
                }

                if(!user.getEmailVerified() && AuthConfig.getVerificationRequired()) {
                    throw new VerificationRequiredException(user.getId());
                }

                if(!user.getActive()) {
                    throw new DisabledAccountException();
                }

                String hashed = SecurityUtils.hashPassword(password, user.getSalt(), user.getRounds());

                if(hashed != null && hashed.equals(user.getHashedPassword())) {
                    this.user = user;
                    this.user.setHashedPassword("");

                    updateUserData(auth, AuthConfig.getLocalIdentityProvider(), new ArrayList<String>());

                    return getRedirectURL(grant);
                } else {
                    Message.addError("wrongPasswordUsername");
                }
            } else if(AuthConfig.getIdentityProvider(providerId) instanceof LdapProvider) {
                return ldapLogin(auth, providerId, email, password, grant);
            } else if(AuthConfig.getIdentityProvider(providerId) instanceof SqlProvider) {
                return sqlLogin(auth, providerId, email, password, grant);
            } else {
                Message.addError("loginNotAvailable");
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Makes a login with the given client certificate object from the database.
     * @param clientCertificate
     * @param grant
     * @return
     * @throws LoginException
     */
    public String login(ClientCertificate clientCertificate, boolean grant) throws LoginException {
        if(! AuthConfig.isClientCertificatesEnabled()) {
            return null;
        }

        if(clientCertificate == null) {
            return null;
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            User user = auth.get(UserDAO.class).getUser(clientCertificate.getUserId());

            if(!user.getEmailVerified() && AuthConfig.getVerificationRequired()) {
                throw new VerificationRequiredException(user.getId());
            }

            if(!user.getActive()) {
                throw new DisabledAccountException();
            }

            if(!user.getClientCertificate()) {
                throw new InvalidAuthenticationException();
            }

            this.user = user;
            this.user.setHashedPassword("");

            updateUserData(auth);

            return getRedirectURL(grant);
        } catch (DAOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Removes the current invitation. Sadly, this is necessary, otherwise the user does not have
     * the option to login after clicking on an unused invitation link with a preselected register mode.
     * @return
     */
    public String cancelInvitation() {
        invitation = null;
        invitationUuid = null;
        return null;
    }

    /**
     * Returns the current redirect URL, either the invitation target URL, the profile page, or the grant.xhtml page
     * @param grant
     * @return
     */
    public String getRedirectURL(boolean grant) {
        if(!getLoginValid()) {
            return null;
        }

        // Get clientId from Url to determine if all EULAs for a client were accepted by user
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String clientId = params.get("client_id");
        if (clientId == null) {
            clientId = this.clientId;
        }

        // If not all EULAs were accepted, redirect to EULA view
        if(!eulaAccepted(clientId) && AuthConfig.isEulaEnabled()) {
            return "/eula.xhtml?includeViewParams=true&faces-redirect=true";
        }

        if(isInvited()) {
            return "/invitation.xhtml?includeViewParams=true&faces-redirect=true";
        }

        try {
            /**
             * Check if an ADFS client has been used.
             * Because this ADFS client has only one redirect URI that *must* match the given one, it is impossible
             * to redirect the user using "includeViewParams". We have to construct the String by hand.
             */
            if (adfsProviderId != 0 && !StringUtil.isEmpty(clientId) &&
                        !StringUtil.isEmpty(scope) && !StringUtil.isEmpty(redirectUri)) {
                /**
                 * In case the user comes from another client, redirect him back to "grant.xhtml".
                 */

                return "/grant?faces-redirect=true&client_id=" + clientId + "&scope=" + scope + "&redirect_uri=" +
                        URLEncoder.encode(redirectUri, StandardCharsets.UTF_8.displayName());
            } else if (grant) {
                return "/grant.xhtml?faces-redirect=true&includeViewParams=true";
            } else {
                /**
                 * Otherwise just redirect to the profile page
                 */
                return "/profile.xhtml?faces-redirect=true";
            }
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Tries a login for the given LDAP identity provider with the given email and password.
     * @param auth
     * @param providerId
     * @param email
     * @param password
     * @param grant
     * @return
     * @throws DAOException
     * @throws LoginException
     */
    private String ldapLogin(AuthConnection auth, int providerId, String email, String password, boolean grant) throws DAOException, LoginException {
        LdapProvider ldap = AuthConfig.getLdapProvider(providerId);

        String bindDn, bindPassword;

        if(ldap.isAnonymous()) {
            bindDn = email;
            bindPassword = password;
        } else {
            bindDn = ldap.getBindDN();
            bindPassword = ldap.getBindPassword();
        }

        /**
         * This is a two step process:
         * 1. search the DN for the given email
         * 2. try to auth the found DN with the given Password
         */
        try(LdapClient client = new LdapClient(ldap.getUrl(), ldap.getBaseDN(), bindDn, bindPassword)) {
            if(client.checkBind()) {
                SearchResult result = client.search(ldap.getSearchBase(), MessageFormat.format(ldap.getSearchFilter(), email));

                if(result != null) {
                    logger.debug("Found user with DN " + result.getNameInNamespace() + ", attempting to bind with this DN");

                    /**
                     * Check the password. If the bind is anonymous, this bind is just a repetition of the first one, but
                     * it does not matter.
                     */
                    try(LdapClient client2 = new LdapClient(ldap.getUrl(), ldap.getBaseDN(), result.getNameInNamespace(), password)) {
                        if(client2.checkBind()) {
                            /**
                             * Everything checks out.
                             */
                            logger.debug("Bind was successful, user is valid!");
                            Attributes attrs = result.getAttributes();

                            String identification = getLDAPIdentification(ldap, attrs);

                            User user = auth.get(UserDAO.class).getExternalUser(ldap, identification);

                            /**
                             * If there already is a user with the given email, check if the external properties are
                             * the same as the current ones. If the user does not exist, create him in the database.
                             */
                            List<String> groups = Collections.emptyList();

                            if(attrs.get(ldap.getMemberAttribute()) != null) {
                                @SuppressWarnings("unchecked")
                                NamingEnumeration<String> all = (NamingEnumeration<String>) attrs.get(ldap.getMemberAttribute()).getAll();

                                groups = Collections.list(all);
                            }

                            createOrUpdateExternalUser(auth, user, identification, ldap,
                                    StringUtil.isEmpty(ldap.getNameAttribute()) ? null : attrs.get(ldap.getNameAttribute()).get().toString(),
                                    StringUtil.isEmpty(ldap.getFirstNameAttribute()) ? null : attrs.get(ldap.getFirstNameAttribute()).get().toString(),
                                    StringUtil.isEmpty(ldap.getLastNameAttribute()) ? null : attrs.get(ldap.getLastNameAttribute()).get().toString(),
                                    attrs.get(ldap.getEmailAttribute()).get().toString(), groups);

                            if(!loginValid) {
                                return null;
                            }

                            return getRedirectURL(grant);
                        } else {
                            logger.debug("User found, but bind was not successful!");
                        }
                    }
                } else {
                    logger.debug("User " + email + " in " + ldap.getLabel() + " not found!");
                }
            }

        } catch (NamingException e) {
            e.printStackTrace();
        }

        Message.addError("wrongPasswordUsername");
        return null;
    }

    /**
     * Tries a login for the given SQL identity provider with the given email and password.
     * @param auth
     * @param providerId
     * @param email
     * @param password
     * @param grant
     * @return
     * @throws DAOException
     * @throws LoginException
     */
    private String sqlLogin(AuthConnection auth, int providerId, String email, String password, boolean grant) throws DAOException, LoginException {
        SqlProvider sqlP = AuthConfig.getSqlProvider(providerId);

        String bindDn, bindPassword, url, searchQuery, sqlDriver;

        url = sqlP.getUrl();
        bindDn = sqlP.getBindDN();
        bindPassword = sqlP.getBindPassword();
        sqlDriver = sqlP.getSqlDriver();
        searchQuery = sqlP.getSearchQuery();

        // 1. Login to SQL DB
        SqlConnection sqlConn = new SqlConnection(url, sqlDriver, bindDn, bindPassword, searchQuery);
        try {
            if(sqlConn.connectToSql()) {
                // 2. Search for user
                if(sqlConn.searchUser(email, password)) {
                    User user = auth.get(UserDAO.class).getExternalUserByEmail(sqlP, email);
                    createOrUpdateExternalUser(auth, user, email, sqlP,
                            sqlConn.getName(),
                            StringUtil.isEmpty(sqlConn.getFirstName()) ? null : sqlConn.getFirstName(),
                            StringUtil.isEmpty(sqlConn.getLastName()) ? null : sqlConn.getLastName(),
                            sqlConn.getEmail(), sqlConn.getGroups());
                    if(!loginValid) {
                        return null;
                    }

                    return getRedirectURL(grant);
                } else {
                    logger.debug("Unknown user (not in DB)!");
                }
                sqlConn.close();
            } else {
                logger.debug("Cannot connect to DB (false url, username or password)!");
            }
        }
        catch (SQLException e){
            logger.debug("Cannot connect to DB!");
        }

        return null;
    }

    /**
     * This method checks, if the given user informations already exists in the database, but may be not linked to an external identity
     * provider.
     * @param auth
     * @param user
     * @param identification
     * @param provider
     * @param name
     * @param email
     * @param groups
     * @throws DAOException
     * @throws LoginException
     */
    private void createOrUpdateExternalUser(AuthConnection auth, User user, String identification,
            IdentityProvider provider, String name, String firstName, String lastName, String email, List<String> groups) throws DAOException, LoginException {

        if(user == null) {
            this.user = createExternalUser(auth, email, name, provider.isDefaultUserEnabled(), provider.isDefaultUserVerified(), firstName, lastName);
            this.user.setProviderId(provider.getId());
            this.user.setExternalId(identification);
            auth.get(UserDAO.class).saveUser(this.user);
            auth.commit();

            /**
             * If email verification is enabled and the user hasn't been verified yet, send an email.
             */
            if(!this.user.getEmailVerified() && AuthConfig.getVerifyEmailEnabled()) {
                MailUtils.resendActivationMail(auth, this.user);
            }

        } else if(identification.equals(user.getExternalId())) {
            this.user = user;

            user.setEmail(email);

            user.setFirstName(firstName);
            user.setLastName(lastName);

            if(!StringUtil.isEmpty(name)) {
                user.setName(name);
            } else {
                user.setName(NameUtils.getDisplayName(firstName, lastName));
            }

            auth.get(UserDAO.class).updateEmail(user);
            auth.get(UserDAO.class).updateData(user);
            auth.commit();
        } else {
            Message.addError("accountAlreadyExists");
        }

        updateUserData(auth, provider, groups);
    }

    /**
     * Updates the user data for the current user.
     * @param auth
     * @throws LoginException
     * @throws DAOException
     */
    public void updateUserData(AuthConnection auth) throws LoginException, DAOException {
        updateUserData(auth, AuthConfig.getLocalIdentityProvider(), Collections.EMPTY_LIST);
    }

    /**
     * Updated the editable roles and permissions. Should be called right after a successful login.
     * @param auth
     * @param provider
     * @throws DAOException
     * @throws LoginException
     */
    private void updateUserData(AuthConnection auth, IdentityProvider provider, List<String> groups) throws DAOException, LoginException {
        /**
         * The activation status of the user may have already been checked in some cases,
         * just do it again before we really let the user login.
         */
        if(user != null) {
            if(!user.getActive()) {
                invalidateLogin();
                throw new DisabledAccountException();
            }

            if(!user.getEmailVerified() && AuthConfig.getVerificationRequired()) {
                throw new VerificationRequiredException(invalidateLogin());
            }

            loginValid = true;

            /**
             * Only if its not the local provider. Delete all "old" derived rules from the database
             * and reiterate the mappings for the identity provider.
             */
            if(provider.getId() != 0) {
                auth.get(RoleDAO.class).deleteDerivedMembership(user);
                List<RoleMapping> mappings = auth.get(RoleMappingDAO.class).getRoleMappingsForProvider(provider.getId());

                for(RoleMapping map : mappings) {
                    // Use role name or regex for mapping
                    // Example for LDAP Group: CN=G230,.*
                    // Example for ADFS Group: AD\\G230
                    Pattern mapPattern = Pattern.compile(map.getRoleName(), Pattern.CASE_INSENSITIVE);
                    for(String group : groups) {
                        Matcher m = mapPattern.matcher(group);
                        if(m.matches()) {
                            auth.get(RoleDAO.class).addDerivedMember(map.getRole(), user);
                        }
                    }
                }
                auth.commit();
            }

            editableRoles = auth.get(RoleDAO.class).getEditableRoles(user.getId());
            permissions = auth.get(RoleDAO.class).getRolePermissionsForUser(user.getId());
            roles = auth.get(RoleDAO.class).getRoles(user.getId());
            locations = auth.get(LocationDAO.class).getLocations(user.getId());
            editableLocations = auth.get(LocationDAO.class).getEditableLocations(user.getId());

            /**
             * Create the cookie if necessary and store it in the database. Also send the cookie back to
             * the browser.
             */
            if(rememberMeMode != null && rememberMeMode != RememberMeMode.THIS_LOGIN) {
                logger.debug("Setting cookie");
                Map<String, Object> cookieProperties = new HashMap<>();
                cookieProperties.put("maxAge", rememberMeMode.age);

                String random = SecurityUtils.newRandomString();

                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, rememberMeMode.age);

                AuthenticationCookie cookie = new AuthenticationCookie();
                cookie.setUserId(user.getId());
                cookie.setExpirationDate(new Timestamp(calendar.getTime().getTime()));
                cookie.setUuid(UUID.randomUUID());
                cookie.setHash(HashUtils.SHA512(random.getBytes()));

                auth.get(AuthenticationCookieDAO.class).save(cookie);
                auth.commit();

                this.cookie = cookie;

                FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(Constants.AUTH_COOKIE,
                        cookie.getUuid().toString() + ":" + random, cookieProperties);
            }
        } else {
            invalidateLogin();
        }
    }

    /**
     * Invalidates the current login.
     */
    private int invalidateLogin() {
        int userId = 0;

        if(user != null) {
            userId = user.getId();
        }

        loginValid = false;
        user = null;

        permissions = Collections.emptyList();
        editableRoles = Collections.emptyList();
        externalGroups = Collections.emptyList();
        roles = Collections.emptyList();
        locations = Collections.emptyList();

        return userId;
    }

    /**
     * Initializes the invitation and uses it, if possible.
     * @return
     */
    public String initInvitation() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            Invitation invitation = auth.get(InvitationDAO.class).getInvitation(invitationUuid);

            if(invitation != null) {
                /**
                 * At this point we should mark the invitiation as used
                 */
                if(!invitation.isUsed()) {
                    invitation.setUsedByUserId(user.getId());
                    auth.get(InvitationDAO.class).markAsUsed(invitation);

                    if(invitation.getRoleId() != 0) {
                        Role role = auth.get(RoleDAO.class).getRole(invitation.getRoleId());
                        logger.debug("Finishing invitation by adding current user as a member of role " + role.getIdentifier());

                        List<User> members = auth.get(UserDAO.class).getMembers(role);
                        boolean found = false;

                        /**
                         * Check, if the user is already a member of the role.
                         */
                        for(User user : members) {
                            if(user.getId() == this.user.getId()) {
                                found = true;
                                logger.debug("User is already a member of this role...");
                            }
                        }

                        if(! found) {
                            members.add(user);
                            auth.get(RoleDAO.class).updateMembers(role, members);
                        }
                    }

                    auth.commit();
                }

                if(StringUtil.isEmpty(invitation.getTarget())) {
                    return "/profile.xhtml=faces-redirect=true";
                } else {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(invitation.getTarget());
                }

                /**
                 * Remove the invitation uuid.
                 */
                invitationUuid = null;
            }
        } catch (DAOException | IOException e) {
            e.printStackTrace();
        }

        /**
         * Something bad happend, just show the default invitation page. It shows an error.
         */
        return null;
    }

    /**
     * If true, the user has used a proper invitation link and the invitation hasn't been used already.
     * @param auth
     * @return
     * @throws DAOException
     */
    public boolean isInvitationValid(AuthConnection auth) throws DAOException {
        invitation = auth.get(InvitationDAO.class).getInvitation(invitationUuid);
        return invitation != null && !invitation.isUsed();
    }

    /**
     * If true, the user has used a proper invitation link
     * @return
     */
    public boolean isInvited() {
        return invitationUuid != null;
    }

    /**
     * Returns true, if the current user is allowed to use the given client.
     * @param client
     * @return
     */
    public boolean mayUse(Client client) {
        return checkPermission(client.getClientId(), Constants.PERMISSION_USE);
    }

    /**
     * Checks if the user has the permission with the given permission identifier for the given clientId.
     * @param clientId
     * @param permissionString
     * @return
     */
    public boolean checkPermission(String clientId, String permissionString) {
        for(RolePermission permission : permissions) {
            if(permission.getClient().getClientId().equals(clientId) && permission.getPermission().getName().equals(permissionString)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Resend the activation email for the current user.
     * @return
     */
    public String resendActivation() {
        try {
            MailUtils.resendActivationMail(user);
            Message.addInfo("emailSent");
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method should be called by the AdfsBean. Returns the new value of "loginValid".
     * @param provider
     * @param accessToken
     * @throws LoginException
     */
    @SuppressWarnings("unchecked")
    public boolean adfsLogin(AdfsProvider provider, JWTAdfsAccessToken accessToken) throws LoginException {
        try (AuthConnection auth = ConnectionFactory.get()) {
            String identification = getADFSIdentification(provider, accessToken);
            User user = auth.get(UserDAO.class).getExternalUser(provider, identification);

            /**
             * Keep in mind that if the user is just in one group, json handles it as a String and not
             * a list of stirngs, thus a simple cast to List<String> is not possible.
             */
            List<String> groups = Collections.emptyList();

            Object jsonGroup = accessToken.getClaimsSet().getClaim(provider.getMemberAttribute());

            if(jsonGroup instanceof List<?>) {
                groups = (List<String>) jsonGroup;
            } else if(jsonGroup instanceof String) {
                groups = Arrays.asList((String) jsonGroup);
            }

            createOrUpdateExternalUser(auth, user, identification, provider,
                    accessToken.getClaimsSet().getStringClaim(provider.getNameAttribute()),
                    accessToken.getClaimsSet().getStringClaim(provider.getFirstNameAttribute()),
                    accessToken.getClaimsSet().getStringClaim(provider.getLastNameAttribute()),
                    accessToken.getClaimsSet().getStringClaim(provider.getEmailAttribute()),
                    groups);
        } catch (DAOException | ParseException e) {
            e.printStackTrace();
        }

        return loginValid;
    }

    /**
     * Tries to login with the given values, uses the default values for activation, verification, and the EULA agreement.
     * @param identity
     * @param name
     * @param email
     * @return
     * @throws LoginException
     */
    public boolean shibbolethLogin(String identity, String name, String firstName, String lastName, String email) throws LoginException {
        Shibboleth shib = AuthConfig.getShibboleth();
        return shibbolethLogin(identity, name, firstName, lastName, email, shib.isDefaultActive(), shib.isDefaultVerified(), false);
    }

    /**
     * @param identity
     * @param name
     * @param email
     * @throws LoginException
     */
    public boolean shibbolethLogin(String identity, String name, String firstName, String lastName, String email, boolean active, boolean verified, boolean eulaAccepted) throws LoginException {
        try(AuthConnection auth = ConnectionFactory.get()) {
            User user = auth.get(UserDAO.class).getShibbolethUser(identity);

            if(user == null) {
                // If the email and name from invitation are different to email and name from shibboleth identity,
                // use the invitation data
                if(this.isInvited()) {
                    Invitation invitation = auth.get(InvitationDAO.class).getInvitation(invitationUuid);
                    if(!StringUtil.isEmpty(email)&&!email.equals(invitation.getEmail())) {
                        email = invitation.getEmail();
                    }
                    if(!StringUtil.isEmpty(firstName)&&!firstName.equals(invitation.getFirstName())) {
                        firstName = invitation.getFirstName();
                    }
                    if(!StringUtil.isEmpty(lastName)&&!lastName.equals(invitation.getLastName())) {
                        lastName = invitation.getLastName();
                    }
                    if(!StringUtil.isEmpty(name)) {
                        name = invitation.getName();
                    }
                }
                user = createExternalUser(auth, email, name, active, verified, firstName, lastName);
                user.setEulaAccepted(eulaAccepted);
                user.setSearchable(eulaAccepted);
                user.setShibbolethIdentity(identity);
                auth.get(UserDAO.class).saveUser(user);
                auth.commit();

                /**
                 * If email verification is enabled and the user hasn't been verified yet, send an email.
                 */
                if(!user.getEmailVerified() && AuthConfig.getVerifyEmailEnabled()) {
                    MailUtils.resendActivationMail(auth, user);
                }
            }
            // Don't overwrite the existing email and name with the data from DFN-AAI
            // TODO: Admin should know about the difference between DFN-AAI and existing data and edit it if necessary
/**            else {

                 * Update the fields if necessary.
                 */
/*                if(!StringUtil.isEmpty(email)) {
                    user.setEmail(email);
                    auth.get(UserDAO.class).updateEmail(user);
                }

                if(!StringUtil.isEmpty(name)) {
                    user.setName(name);
                    auth.get(UserDAO.class).updateData(user);
                }
            } **/

            this.user = user;
            updateUserData(auth, AuthConfig.getLocalIdentityProvider(), new ArrayList<String>());

            auth.commit();

            return user.getActive();
        } catch (DAOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Creates a user with a disabled password.
     * @param auth
     * @param email
     * @param name
     * @param defaultActive
     * @param defaultVerified
     * @return
     * @throws DAOException
     */
    private User createExternalUser(AuthConnection auth, String email, String name, boolean defaultActive, boolean defaultVerified) throws DAOException {
        return createExternalUser(auth, email, name, defaultActive, defaultVerified, null, null);
    }

    /**
     * Creates a user with a disabled password.
     * @param email
     * @param name
     * @return
     */
    private User createExternalUser(AuthConnection auth, String email, String name, boolean defaultActive, boolean defaultVerified,
                                    String firstName, String lastName) throws DAOException {
        User user = new User();
        user.setEmail(email);
        user.setContactData("");
        user.setSalt("!disabled");
        user.setRounds(AuthConfig.DEFAULT_ROUNDS);
        user.setHashedPassword("!disabled");

        if(!StringUtil.isEmpty(firstName)) {
            user.setFirstName(firstName);
        }

        if(!StringUtil.isEmpty(lastName)) {
            user.setLastName(lastName);
        }

        // If display name is not provided, display first name and last name
        if(!StringUtil.isEmpty(name)) {
            user.setName(name);
        } else {
            user.setName(NameUtils.getDisplayName(firstName, lastName));
        }

        /**
         * If an invitation has been used, the user has been actively invited to this service, therefore
         * enable him and set the emailVerified flag to true, because he obviously received the invitation email
         */
        if(isInvitationValid(auth)) {
            user.setActive(true);
            user.setEmailVerified(true);
        } else {
            user.setActive(defaultActive);
            user.setEmailVerified(defaultVerified);
        }

        user.setLanguage("en");
        user.setUsertype(Usertype.NORMAL);
        user.setEulaAccepted(false);
        user.setClientCertificate(false);
        return user;
    }

    private String getADFSIdentification(AdfsProvider provider, JWTAdfsAccessToken accessToken) throws ParseException {
        return accessToken.getClaimsSet().getStringClaim(provider.getIdAttribute());
    }

    private String getLDAPIdentification(LdapProvider ldap, Attributes attrs) throws NamingException {
        return attrs.get(ldap.getIdAttribute()).get().toString();
    }

    /**
     * Executes a logout.
     *
     * @return a {@link java.lang.String} object.
     */
    public String logout() {
        return logout(true);
    }

    /**
     * Executes a logout. If shibbolethRedirect is true, it will redirect the user to the shibboleth
     * Logout page and back to this application.
     * @param shibbolethRedirect
     * @return
     */
    public String logout(boolean shibbolethRedirect) {
        boolean shibboleth = user != null && !StringUtil.isEmpty(user.getShibbolethIdentity());

        invalidateLogin();

        /**
         * Remove the cookie from the DB, just to be sure.
         */
        if(cookie != null) {
            try (AuthConnection auth = ConnectionFactory.get()) {
                auth.get(AuthenticationCookieDAO.class).remove(cookie);
                auth.commit();
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Remove existing cookie, if necessary
         */
        Map<String, Object> cookieProperties = new HashMap<>();
        cookieProperties.put("maxAge", 0);
        FacesContext.getCurrentInstance().getExternalContext().addResponseCookie(Constants.AUTH_COOKIE, "NULL", cookieProperties);

        if(shibboleth && shibbolethRedirect) {
            String applicationContextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + "/";
            ServletUtil.logoutShibboleth(applicationContextPath);
            return null;
        } else {
            return "/login.xhtml?faces-redirect=true";
        }
    }

    /**
     * Redirects the user to the ADFS provider
     * @param provider
     * @param clientId
     * @param redirectUri
     * @param scope
     * @return
     */
    public String redirect(AdfsProvider provider, String clientId, String redirectUri, String scope, String state, String nonce) {
        this.clientId = clientId;
        this.redirectUri = redirectUri;
        this.scope = scope;
        this.state = state;
        this.nonce = nonce;
        this.adfsProviderId = provider.getId();

        if(!provider.isActive()) {
            Message.addError("idpDisabled");
            return null;
        }

        try {
            AdfsClient client = new AdfsClient(provider.getUrl(), provider.getPublicKey(), provider.getResource(),
                    provider.getClientId(), ClientUtil.getClient());
            FacesContext.getCurrentInstance().getExternalContext().redirect(client.getRedirectUrl(getLocalADFSRedirectUrl()));
        } catch (IOException e) {
        }
        return null;
    }

    public String getLocalADFSRedirectUrl() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return OAuth2ClientConfig.getLocalRedirectUrl(context.getRequestScheme(),
                context.getRequestServerName(), context.getRequestServerPort(),
                context.getRequestContextPath(), "/adfsLogin.xhtml");
    }

    /**
     * Returns true if the admin is an administrator in Samply Auth.
     * @return
     */
    public Boolean isAdmin() {
        return checkPermission(Constants.AUTH_CLIENT_ID, Constants.PERMISSION_ADMIN) ||
                FacesContext.getCurrentInstance().getExternalContext().isUserInRole(Constants.EXTERNAL_ADMIN_ROLE);
    }

    /**
     * @param string
     * @return
     */
    public boolean isInRole(String string) {
        if(roles == null ) {
            return false;
        }

        for(Role role : roles) {
            if(role.getIdentifier().equals(string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the editableRoles
     */
    public List<Role> getEditableRoles() {
        return editableRoles;
    }

    /**
     * @param editableRoles the editableRoles to set
     */
    public void setEditableRoles(List<Role> editableRoles) {
        this.editableRoles = editableRoles;
    }

    /**
     * @return the permissions
     */
    public List<RolePermission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(List<RolePermission> permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the externalGroups
     */
    public List<String> getExternalGroups() {
        return externalGroups;
    }

    /**
     * @param externalGroups the externalGroups to set
     */
    public void setExternalGroups(List<String> externalGroups) {
        this.externalGroups = externalGroups;
    }

    /**
     * <p>Getter for the field <code>user</code>.</p>
     *
     * @return a {@link de.samply.auth.dao.dto.User} object.
     */
    public User getUser() {
        return user;
    }

    /**
     * <p>Setter for the field <code>user</code>.</p>
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * <p>Getter for the field <code>loginValid</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getLoginValid() {
        return loginValid;
    }

    /**
     * <p>Setter for the field <code>loginValid</code>.</p>
     *
     * @param loginValid a {@link java.lang.Boolean} object.
     */
    public void setLoginValid(Boolean loginValid) {
        this.loginValid = loginValid;
    }

    /**
     * @return the adfsProviderId
     */
    public int getAdfsProviderId() {
        return adfsProviderId;
    }

    /**
     * @param adfsProviderId the adfsProviderId to set
     */
    public void setAdfsProviderId(int adfsProviderId) {
        this.adfsProviderId = adfsProviderId;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the redirectUri
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * @param redirectUri the redirectUri to set
     */
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    /**
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * @param scope the scope to set
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * @return the roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return the locations
     */
    public List<Location> getLocations() {
        return locations;
    }

    /**
     * @param locations the locations to set
     */
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    /**
     * @return the editableLocations
     */
    public List<Location> getEditableLocations() {
        return editableLocations;
    }

    /**
     * @param editableLocations the editableLocations to set
     */
    public void setEditableLocations(List<Location> editableLocations) {
        this.editableLocations = editableLocations;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public String getInvitationUuid() {
        return invitationUuid;
    }

    public void setInvitationUuid(String invitationUuid) {
        this.invitationUuid = invitationUuid;
    }

    /**
     * True if all EULAs for a client were accepted, false otherwise
     * @param clientId
     * @return
     */
    public Boolean eulaAccepted(String clientId) {
        return getNotAcceptedEula(clientId).isEmpty();
    }

    /**
     * Gets the list of Eula which were not accepted by the current user (for a client)
     * @param clientId
     * @return
     */
    public List<Eula> getNotAcceptedEula(String clientId) {
        List<Eula> eulas = new ArrayList<>();
        try (AuthConnection auth = ConnectionFactory.get()) {
            Client client = auth.get(ClientDAO.class).getClient(clientId);
            if(client != null) {
                eulas = auth.get(EulaDAO.class).getEulaToAccept(client.getId(), this.getUser().getId());
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        setClientId(clientId);
        setEulaToAccept(eulas);
        return eulas;
    }

    public Boolean getEulaAccepted() {
        return eulaAccepted;
    }

    public void setEulaAccepted(Boolean eulaAccepted) {
        this.eulaAccepted = eulaAccepted;
    }

    public RememberMeMode getRememberMeMode() {
        return rememberMeMode;
    }

    public void setRememberMeMode(RememberMeMode rememberMeMode) {
        this.rememberMeMode = rememberMeMode;
    }

    public AuthenticationCookie getCookie() {
        return cookie;
    }

    public void setCookie(AuthenticationCookie cookie) {
        this.cookie = cookie;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public List<Eula> getEulaToAccept() {
        return eulaToAccept;
    }

    public void setEulaToAccept(List<Eula> eulaToAccept) {
        this.eulaToAccept = eulaToAccept;
    }
}
