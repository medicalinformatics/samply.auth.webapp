/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.adfs.client.AdfsClient;
import de.samply.auth.AuthConfig;
import de.samply.auth.ClientUtil;
import de.samply.auth.Message;
import de.samply.auth.client.jwt.JWTAdfsAccessToken;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.dao.dto.AdfsProvider;
import de.samply.auth.exception.LoginException;
import de.samply.string.util.StringUtil;

/**
 * This bean remembers the redirect url when the user click on an ADFS identity
 * provider, and handles the ADFS callback by getting an access token from ADFS.
 */
@ManagedBean
@ViewScoped
public class AdfsBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The selected ADFS provider.
     */
    private transient AdfsProvider provider;

    /**
     * The error code.
     */
    private String error;

    /**
     * The error description.
     */
    private String errorDescription;

    /**
     * The code from the ADFS
     */
    private String code;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * Called by adfsLogin.xhtml, gets the access token using the code.
     * @return
     */
    public String login() {
        if(provider == null) {
            int adfsId = getUserBean().getAdfsProviderId();
            if(adfsId == 0) {
                return "/error.xhtml?faces-redirect=true";
            } else {
                provider = AuthConfig.getAdfsProvider(adfsId);
            }
        }

        if(!StringUtil.isEmpty(error)) {
            Message.addMessage(error, errorDescription, false, FacesMessage.SEVERITY_ERROR);
            return "/login.xhtml";
        }

        AdfsClient client = new AdfsClient(provider.getUrl(), provider.getPublicKey(), provider.getResource(),
                provider.getClientId(), ClientUtil.getClient());
        try {
            JWTAdfsAccessToken accessToken = client.getAccessToken(code, getUserBean().getLocalADFSRedirectUrl());

            if(userBean.adfsLogin(provider, accessToken)) {
                return userBean.getRedirectURL(! StringUtil.isEmpty(userBean.getRedirectUri()));
            } else {
                return "/login.xhtml";
            }
        } catch (JWTException e) {
            e.printStackTrace();
        } catch (LoginException e) {
            Message.addError(e.getErrorMessageID());
            return "/login.xhtml";
        }

        return null;
    }

    /**
     * @return the provider
     */
    public AdfsProvider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(AdfsProvider provider) {
        this.provider = provider;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * @param errorDescription the errorDescription to set
     */
    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
