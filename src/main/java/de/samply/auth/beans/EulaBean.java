/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.beans;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.EulaDAO;
import de.samply.auth.dao.dto.Eula;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * Manages the EULA view and lets the user accept them.
 */
@ManagedBean
@ViewScoped
public class EulaBean {
    // Multiple EULA are allowed in a view
    private Map<Integer, Boolean> acceptedEula;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{grantBean}")
    private GrantBean grantBean;

    /**
     * Eula needed to be accepted for client by a user
     */
    private List<Eula> eulaToAccept;

    /**
     * <p>init.</p>
     */
    public void init() {
        acceptedEula = new HashMap<Integer, Boolean>();
        this.eulaToAccept = userBean.getEulaToAccept();
        for (Eula eula : eulaToAccept) {
            acceptedEula.put(eula.getId(), false);
        }
    }

    /**
     * Accepts multiple EULA for the given user.
     * @return
     */
    public String accept() {
        if(userBean.getLoginValid()) {
            // check if all EULAs were accepted
            for (Eula eula : eulaToAccept) {
                if (!acceptedEula.get(eula.getId())) {
                    return null;
                }
            }

            // store in DB the information about accepted EULAs
            try (AuthConnection auth = ConnectionFactory.get()) {
                userBean.getUser().setSearchable(true);
                auth.get(UserDAO.class).updateData(userBean.getUser());

                for (Eula eula : eulaToAccept) {
                    auth.get(EulaDAO.class).userAcceptedEula(eula, userBean.getUser());
                }

                auth.commit();
            } catch (DAOException e) {
                e.printStackTrace();
            }

            return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
        } else {
            return null;
        }
    }

    public Map<Integer, Boolean> getAcceptedEula() { return acceptedEula; }

    public void setAcceptedEula(Map<Integer, Boolean> acceptedEula) {
        this.acceptedEula = acceptedEula;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public GrantBean getGrantBean() {
        return grantBean;
    }

    public void setGrantBean(GrantBean grantBean) {
        this.grantBean = grantBean;
    }

    public List<Eula> getEulaToAccept() {
        return eulaToAccept;
    }

    public void setEulaToAccept(List<Eula> eulaToAccept) {
        this.eulaToAccept = eulaToAccept;
    }
}
