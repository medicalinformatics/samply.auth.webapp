/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import de.samply.auth.AuthUtil;
import de.samply.auth.Message;
import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.AdministrationMode;
import de.samply.auth.dao.dto.Location;
import de.samply.auth.dao.dto.LocationAdministration;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.DAOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class LocationEditBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int locationId;

    /**
     * The role which is edited.
     */
    private Location location = null;

    /**
     * The list of members
     */
    private List<User> members;

    /**
     * The list of admins in this group.
     */
    private List<LocationAdministration> admins;

    /**
     * The users ID from the select fields (select2 ajax search request)
     */
    private String identity;

    /**
     * The search text and the resulting suggestions.
     */
    private String searchText, suggestions;

    /**
     * The mode selected in the interface
     */
    private AdministrationMode mode;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * The *current* mode (it depends on the user).
     */
    private AdministrationMode currentMode;

    /**
     * Gets the initial list.
     */
    public void init() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            location = auth.get(LocationDAO.class).getLocation(locationId);

            members = auth.get(UserDAO.class).getMembers(location);
            admins = auth.get(UserDAO.class).getAdmins(location);

            currentMode = null;

            if(userBean.isAdmin()) {
                currentMode = AdministrationMode.ADMIN;
            } else {
                for(LocationAdministration admin : admins) {
                    if(admin.getUser().getId() == userBean.getUser().getId()) {
                        currentMode = admin.getMode();
                    }
                }
            }

            if(currentMode == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect(
                        FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/index.xhtml");
            }
        } catch (DAOException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds the given user (by the identity string) to the members list.
     * @return
     */
    public String add() {
        Integer userId = Integer.parseInt(identity);

        /**
         * Abort if the user is already a member.
         */
        for(User user : members) {
            if(user.getId() == userId) {
                return null;
            }
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            members.add(auth.get(UserDAO.class).getUser(userId));
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds the given user (by the identity string) to the admin list with the AdministrationMode mode.
     * @return
     */
    public String addAdmin() {
        Integer userId = Integer.parseInt(identity);

        /**
         * Abort if the user is already an admin.
         */
        for(LocationAdministration admin : admins) {
            if(admin.getUser().getId() == userId) {
                return null;
            }
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            LocationAdministration admin = new LocationAdministration();
            admin.setUser(auth.get(UserDAO.class).getUser(userId));
            admin.setMode(getMode());
            admin.setLocation(location);
            admins.add(admin);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(LocationDAO.class).updateMembers(location, members);

            if(currentMode == AdministrationMode.ADMIN) {
                auth.get(LocationDAO.class).updateAdmins(location, admins);
            }
            Message.addInfo("locationSubmitted");
            auth.commit();
        } catch(NoAdminsException e) {
            Message.addError("noAdminsException");
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Removes the given member from the members list.
     * @param member
     * @return
     */
    public String removeMember(User member) {
        members.remove(member);
        return null;
    }

    public String removeAdmin(LocationAdministration admin) {
        admins.remove(admin);
        return null;
    }

    /**
     * Searches for users using the searchText.
     * @return
     */
    public String searchEmail() {
        suggestions = AuthUtil.getSuggestions(searchText);
        return null;
    }

    /**
     * @return the role
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * @param role the role to set
     */
    public void setLocationId(int role) {
        this.locationId = role;
    }

    /**
     * @return the members
     */
    public List<User> getMembers() {
        return members;
    }

    /**
     * @param members the members to set
     */
    public void setMembers(List<User> members) {
        this.members = members;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * @return the admins
     */
    public List<LocationAdministration> getAdmins() {
        return admins;
    }

    /**
     * @param admins the admins to set
     */
    public void setAdmins(List<LocationAdministration> admins) {
        this.admins = admins;
    }

    /**
     * @return the identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param identity the identity to set
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * @return the mode
     */
    public AdministrationMode getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(AdministrationMode mode) {
        this.mode = mode;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the currentMode
     */
    public AdministrationMode getCurrentMode() {
        return currentMode;
    }

    /**
     * @param currentMode the currentMode to set
     */
    public void setCurrentMode(AdministrationMode currentMode) {
        this.currentMode = currentMode;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

}
