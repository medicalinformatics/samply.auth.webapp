/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import de.samply.auth.Message;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * This bean handles the reset password method
 *
 * @since 1.4.0
 */
@ManagedBean
@ViewScoped
public class ActivationBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger logger = LoggerFactory.getLogger(ActivationBean.class);

    /**
     * The code parameter.
     */
    private String code;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * This is called when the user clicks on the link in the email. Just load the user
     * and check if the code is not valid, return to the login page.
     *
     * @return a {@link java.lang.String} object.
     */
    public String initialize() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            UserDAO userDAO = auth.get(UserDAO.class);

            User user = userDAO.getUserActivationCode(code);

            /**
             * Just check if this code is valid. getUserActivationCode does that for us.
             */
            if(user == null) {
                Message.addError("codeInvalid", true);
                return "/login?faces-redirect=true";
            } else {
                logger.debug("User " + user.getEmail() + " has been verified");
                userDAO.verifyUser(user);
                auth.commit();

                Message.addInfo("emailVerified", true);

                if(userBean.getUser() != null && userBean.getUser().getId() == user.getId()) {
                    userBean.getUser().setEmailVerified(true);
                    return "/profile?faces-redirect=true";
                }

                return "/login?faces-redirect=true";
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
