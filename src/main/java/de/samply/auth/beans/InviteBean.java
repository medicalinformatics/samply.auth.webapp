/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.InvitationDAO;
import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.PreselectMode;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * This bean invites new members via email. It does *not* handle invitation links on
 * invitation.xhtml ! See UserBean for the invitation handling.
 * Created by paul on 4/25/16.
 */
@ManagedBean
@ViewScoped
public class InviteBean implements Serializable {

    /**
     * The first name of the new user.
     */
    private String firstName;

    /**
     * The last name of the new user.
     */
    private String lastName;

    /**
     * The invitees email address
     */
    private String email;

    /**
     * The target URL when the user uses the invitation link
     */
    private String targetUrl;

    /**
     * The PreselectMode for the invitation
     */
    private PreselectMode preselectMode = PreselectMode.NONE;

    /**
     * The selected provider ID, if the preselect mode equals "EXTERNAL_IDP".
     */
    private int providerId;

    public void sendInvitationLink(Invitation invitation) {
        try(AuthConnection auth = ConnectionFactory.get()){
            invitation.setTarget(targetUrl);
            invitation.setFirstName(firstName);
            invitation.setLastName(lastName);
            invitation.setEmail(email);
            invitation.setPreselectMode(preselectMode);

            if(preselectMode == PreselectMode.EXTERNAL_IDP) {
                invitation.setProviderId(providerId);
            }

            MailUtils.sendInviteLink(invitation);

            auth.get(InvitationDAO.class).saveInvite(invitation);
            auth.commit();

            targetUrl = "";
            email = "";
            firstName = "";
            lastName = "";
        } catch(DAOException e) {
            e.printStackTrace();
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public PreselectMode getPreselectMode() {
        return preselectMode;
    }

    public void setPreselectMode(PreselectMode preselectMode) {
        this.preselectMode = preselectMode;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }
}
