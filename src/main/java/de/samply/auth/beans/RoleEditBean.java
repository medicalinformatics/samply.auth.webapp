/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;

import de.samply.auth.AuthConfig;
import de.samply.auth.AuthUtil;
import de.samply.auth.Constants;
import de.samply.auth.Message;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.InvitationDAO;
import de.samply.auth.dao.NoAdminsException;
import de.samply.auth.dao.PermissionDAO;
import de.samply.auth.dao.RoleDAO;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.*;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

@ManagedBean
@ViewScoped
public class RoleEditBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int roleId;

    /**
     * The role which is edited.
     */
    private Role role = null;

    /**
     * The list of members
     */
    private List<User> members;

    /**
     * The list of admins in this group.
     */
    private List<RoleAdministration> admins;

    /**
     * The list of invitations
     */
    private List<Invitation> invitations;

    /**
     * The users ID from the select fields (select2 ajax search request)
     */
    private String identity;

    /**
     * The search text and the resulting suggestions.
     */
    private String searchText, suggestions;

    /**
     * The list of active role permissions for this role.
     */
    private List<RolePermission> permissions;

    /**
     * The mode selected in the interface
     */
    private AdministrationMode mode;

    /**
     * A list of all available permissions
     */
    private List<Permission> permissionList;

    /**
     * A list of all available clients
     */
    private List<Client> clients;

    /**
     * The selected permission name and clientID in the interface.
     */
    private String permissionName, clientId;

    /**
     * The permission export value.
     */
    private boolean permissionExport = false;

    @ManagedProperty(value = "#{registerBean}")
    private RegisterBean registerBean;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{inviteBean}")
    private InviteBean inviteBean;

    /**
     * The *current* mode (it depends on the user).
     */
    private AdministrationMode currentMode;

    /**
     * Gets the initial list.
     * @param adminInterface if true, this method is called from the /admin/ subdirectory
     */
    public void init(boolean adminInterface) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            role = auth.get(RoleDAO.class).getRole(roleId);

            members = auth.get(UserDAO.class).getMembers(role);
            admins = auth.get(UserDAO.class).getAdmins(role);
            invitations = auth.get(InvitationDAO.class).getInvitedMembers(role);
            permissions = auth.get(RoleDAO.class).getRolePermissions(role);

            permissionList = auth.get(PermissionDAO.class).getPermissions();
            clients = auth.get(ClientDAO.class).getClients();

            currentMode = null;

            if(userBean.isAdmin() || (adminInterface && AuthConfig.isExternalAuthorization())) {
                currentMode = AdministrationMode.ADMIN;
            } else {
                if(userBean.getUser() != null) {
                    for (RoleAdministration admin : admins) {
                        if (admin.getUser().getId() == userBean.getUser().getId()) {
                            currentMode = admin.getMode();
                        }
                    }
                }
            }

            if(currentMode == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect(
                        FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/index.xhtml");
            }
        } catch (DAOException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds the given user (by the identity string) to the members list.
     * @return
     */
    public String add() {
        Integer userId = Integer.parseInt(identity);

        if(userId == 0) {
            // New user will be invited
            inviteBean.setEmail(searchText);
            return null;
        }

        /**
         * Abort if the user is already a member.
         */
        for(User user : members) {
            if(user.getId() == userId) {
                return null;
            }
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            members.add(auth.get(UserDAO.class).getUser(userId));
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds the given user (by the identity string) to the admin list with the AdministrationMode mode.
     * @return
     */
    public String addAdmin() {
        Integer userId = Integer.parseInt(identity);

        /**
         * Abort if the user is already an admin.
         */
        for(RoleAdministration admin : admins) {
            if(admin.getUser().getId() == userId) {
                return null;
            }
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            RoleAdministration admin = new RoleAdministration();
            admin.setUser(auth.get(UserDAO.class).getUser(userId));
            admin.setMode(getMode());
            admin.setRole(role);
            admins.add(admin);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds the permission to the list of permissions.
     * @return
     */
    public String addPermission() {
        RolePermission perm = new RolePermission();

        /**
         * Abort if the clientId or permission name is null.
         */
        if(StringUtil.isEmpty(clientId) || StringUtil.isEmpty(permissionName)) {
            return null;
        }

        for(Client c : clients) {
            if(c.getClientId().equals(clientId)) {
                perm.setClient(c);
            }
        }

        for(Permission p : permissionList) {
            if(p.getName().equals(permissionName)) {
                perm.setPermission(p);
            }
        }

        perm.setExport(permissionExport);

        /**
         * Allow only admins to add the admin permission to samply auth itself.
         */
        if(perm.getPermission().getName().equals(Constants.PERMISSION_ADMIN) &&
                perm.getClient().getClientId().equals(Constants.AUTH_CLIENT_ID)) {
            if(userBean.isAdmin()) {
                permissions.add(perm);
            }
        } else {
            permissions.add(perm);
        }

        return null;
    }

    /**
     * Saves the role with all its members and administrators.
     * @return
     */
    public String commit() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(RoleDAO.class).updateMembers(role, members);
            auth.get(InvitationDAO.class).updateInvitations(role, invitations);
            if(currentMode == AdministrationMode.ADMIN) {
                auth.get(RoleDAO.class).updateAdmins(role, admins);
                auth.get(RoleDAO.class).updatePermissions(role, permissions);
            }
            Message.addInfo("roleSubmitted");
            auth.commit();
        } catch(NoAdminsException e) {
            Message.addError("noAdminsException");
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Removes the given member from the members list.
     * @param member
     * @return
     */
    public String removeMember(User member) {
        members.remove(member);
        return null;
    }

    public String removeAdmin(RoleAdministration admin) {
        admins.remove(admin);
        return null;
    }

    public String removePermission(RolePermission permission) {
        permissions.remove(permission);
        return null;
    }

    public String removeInvitation(Invitation invitation) {
        invitations.remove(invitation);
        return null;
    }

    /**
     * Creates a new user and adds it to the members list.
     * @return
     */
    public String sendInviteLink() throws MessagingException {
        Invitation invitation = new Invitation();
        invitation.setUuid(UUID.randomUUID());
        invitation.setRoleId(roleId);
        inviteBean.sendInvitationLink(invitation);
        invitations.add(invitation);

        Message.addInfo("editRoleForm:targetUrl", "emailInviteSent");

        return null;
    }

    public Boolean showMemberInvite() {
        if(!"0".equalsIgnoreCase(identity))
            return false;
        else
            return true;
    }

    /**
     * Searches for users using the searchText.
     * If not found, show invite member text
     * @return
     */
    public String searchEmail() {
        suggestions = AuthUtil.getSuggestions(searchText, Usertype.NORMAL);
        if("".equalsIgnoreCase(suggestions) || suggestions == null)
            suggestions = "Invite Member with [0]"+ getSearchText();
        return null;
    }

    /**
     * @return the role
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * @param role the role to set
     */
    public void setRoleId(int role) {
        this.roleId = role;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the members
     */
    public List<User> getMembers() {
        return members;
    }

    /**
     * @param members the members to set
     */
    public void setMembers(List<User> members) {
        this.members = members;
    }

    /**
     * @return the invited members
     */
    public List<Invitation> getInvitations() {
        return invitations;
    }

    /**
     * @param invitations the members to set
     */
    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * @return the admins
     */
    public List<RoleAdministration> getAdmins() {
        return admins;
    }

    /**
     * @param admins the admins to set
     */
    public void setAdmins(List<RoleAdministration> admins) {
        this.admins = admins;
    }

    /**
     * @return the identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param identity the identity to set
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * @return the registerBean
     */
    public RegisterBean getRegisterBean() {
        return registerBean;
    }

    /**
     * @param registerBean the registerBean to set
     */
    public void setRegisterBean(RegisterBean registerBean) {
        this.registerBean = registerBean;
    }

    /**
     * @return the mode
     */
    public AdministrationMode getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(AdministrationMode mode) {
        this.mode = mode;
    }

    /**
     * @return the permissions
     */
    public List<RolePermission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(List<RolePermission> permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the permissionList
     */
    public List<Permission> getPermissionList() {
        return permissionList;
    }

    /**
     * @param permissionList the permissionList to set
     */
    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }

    /**
     * @return the clients
     */
    public List<Client> getClients() {
        return clients;
    }

    /**
     * @param clients the clients to set
     */
    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    /**
     * @return the permissionName
     */
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * @param permissionName the permissionName to set
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the currentMode
     */
    public AdministrationMode getCurrentMode() {
        return currentMode;
    }

    /**
     * @param currentMode the currentMode to set
     */
    public void setCurrentMode(AdministrationMode currentMode) {
        this.currentMode = currentMode;
    }

    /**
     * @return the permissionExport
     */
    public boolean isPermissionExport() {
        return permissionExport;
    }

    /**
     * @param permissionExport the permissionExport to set
     */
    public void setPermissionExport(boolean permissionExport) {
        this.permissionExport = permissionExport;
    }

    public InviteBean getInviteBean() {
        return inviteBean;
    }

    public void setInviteBean(InviteBean inviteBean) {
        this.inviteBean = inviteBean;
    }
}
