/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import de.samply.auth.rest.ServletUtil;

/**
 * The request scoped bean that checks if the server
 * name contains the text "osse".
 *
 */
@ManagedBean
@RequestScoped
public class RequestBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String OSSE_TITLE = "OSSE";
    private static final String SAMPLY_TITLE = "Samply";

    public static final String CSS_DEFAULT = "default";
    public static final String CSS_SIMPLE = "simple";

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{param.css}")
    private String css;


    /**
     * Returns the server name from the request.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerName() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
    }

    /**
     * Returns the Shibboleth Login URL on this server (usually /Shibboleth.sso/Login) with
     * the *current* request URL as return parameter.
     *
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getShibbolethUrl() throws UnsupportedEncodingException {
        return getShibbolethLoginUrl();
    }

    /**
     * Returns the title the web page should show. If the server contains the string "osse",
     * it shows "OSSE", otherwise "Samply" is shown.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitle() {
        if(isOsse()) {
            return OSSE_TITLE;
        } else {
            return SAMPLY_TITLE;
        }
    }

    /**
     * If true, this request was made for an OSSE domain.
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean isOsse() {
        return getServerName().toLowerCase().contains("osse-register") || System.getProperty("de.samply.osseDevelopment") != null;
    }

    /**
     * If true, this server is running in a productive environment.
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean isProduction() {
        return System.getProperty("de.samply.development", "false").equalsIgnoreCase("false");
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Boolean isCssSimple() {
        return CSS_SIMPLE.equalsIgnoreCase(css);
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    /**
     * Returns the URL for a login in Shibboleth, usually /Shibboleth.sso/Login?xyz. The redirect must be made
     * with the external context redirect method, not with JSF.
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getShibbolethLoginUrl() throws UnsupportedEncodingException {
        return getShibbolethLoginUrl((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
    }

    /**
     * Returns the URL for a login in Shibboleth, usually /Shibboleth.sso/Login?xyz. The redirect must be made
     * with the external context redirect method, not with JSF.
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getShibbolethLoginUrl(HttpServletRequest request) throws UnsupportedEncodingException {
        return "/Shibboleth.sso/Login?target=" +
                URLEncoder.encode(ServletUtil.getRequestUrl(request),
                        StandardCharsets.UTF_8.displayName());
    }
}
