/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.NameUtils;
import de.samply.auth.PasswordPolicy;
import de.samply.auth.SecurityUtils;
import de.samply.auth.SecurityUtils.PasswordStatus;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientCertificateDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.ClientCertificate;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.DAOException;

/**
 * The profile bean for profile.xhtml. Used to change passwords or store contact information.
 *
 */
@ManagedBean
@ViewScoped
public class ProfileBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The users first name
     */
    private String firstName;

    /**
     * The users last name
     */
    private String lastName;

    /**
     * The users contact information, e.g. a phone number. This value is not
     * validated, it might not even make sense.
     */
    private String contact;

    /**
     * The current password (clear text)
     */
    private String password;

    /**
     * The new password (clear text)
     */
    private String newPassword;

    /**
     * The new password (repeated, clear text)
     */
    private String newPasswordRepeat;

    /**
     * If true, the user appears in the search REST API, otherwise he does not.
     */
    private Boolean searchable;

    /**
     * The list of client certificates for this account.
     */
    private List<ClientCertificate> clientCertificates;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {
        firstName = userBean.getUser().getFirstName();
        lastName = userBean.getUser().getLastName();
        contact = userBean.getUser().getContactData();
        searchable = userBean.getUser().getSearchable();

        try(AuthConnection auth = ConnectionFactory.get()) {
            clientCertificates = auth.get(ClientCertificateDAO.class).getClientCertificates(userBean.getUser().getId());
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the contact information into the json data column
     *
     * @return a {@link java.lang.String} object.
     */
    public String saveContactInformation() {
        Pattern namePattern = RegisterBean.namePattern;

        User user = userBean.getUser();

        if(!namePattern.matcher(firstName).matches() || !namePattern.matcher(lastName).matches()) {
            Message.addError("invalidName");
            return null;
        }

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setName(NameUtils.getDisplayName(firstName, lastName));
        user.setContactData(contact);
        user.setSearchable(searchable);

        try(AuthConnection auth = ConnectionFactory.get()) {
            UserDAO dao = auth.get(UserDAO.class);

            dao.updateData(user);
            dao.updateContact(user);
            auth.commit();

            Message.addInfo("profileUpdated");
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Changes the password and updates the database.
     *
     * @return a {@link java.lang.String} object.
     */
    public String changePassword() {
        if(SecurityUtils.checkPasswordSecurity(newPassword, AuthConfig.getPolicy()) != PasswordStatus.OK) {
            PasswordPolicy policy = AuthConfig.getPolicy();
            Message.add("passwordPolicy", false, FacesMessage.SEVERITY_ERROR,
                    policy.getMinLength(), policy.getMinLowerCase(), policy.getMinUpperCase(), policy.getMinNumber(),
                    policy.getMinSpecial());
            return null;
        }

        if(!newPassword.equals(newPasswordRepeat)) {
            Message.addError("passwordsNotEqual");
            return null;
        }

        try(AuthConnection auth = ConnectionFactory.get()) {
            UserDAO userDao = auth.get(UserDAO.class);
            User user = userDao.getUser(userBean.getUser().getId());

            String hashed = SecurityUtils.hashPassword(password, user.getSalt(), user.getRounds());
            String newHashed = SecurityUtils.hashPassword(newPassword, user.getSalt(), AuthConfig.DEFAULT_ROUNDS);

            if(hashed.equals(user.getHashedPassword())) {
                userDao.updatePassword(user.getId(), hashed, newHashed, AuthConfig.DEFAULT_ROUNDS);
                Message.addInfo("passwordChanged");
                auth.commit();
            } else {
                Message.addError("wrongPasswordUsername");
            }

            return null;
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String removeClientCertificate(ClientCertificate cc) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            auth.get(ClientCertificateDAO.class).removeClientCertificate(cc);
            auth.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "/profile";
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * <p>Getter for the field <code>newPassword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * <p>Setter for the field <code>newPassword</code>.</p>
     *
     * @param newPassword a {@link java.lang.String} object.
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * <p>Getter for the field <code>newPasswordRepeat</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNewPasswordRepeat() {
        return newPasswordRepeat;
    }

    /**
     * <p>Setter for the field <code>newPasswordRepeat</code>.</p>
     *
     * @param newPasswordRepeat a {@link java.lang.String} object.
     */
    public void setNewPasswordRepeat(String newPasswordRepeat) {
        this.newPasswordRepeat = newPasswordRepeat;
    }

    /**
     * <p>Getter for the field <code>password</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPassword() {
        return password;
    }

    /**
     * <p>Setter for the field <code>password</code>.</p>
     *
     * @param password a {@link java.lang.String} object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * <p>Getter for the field <code>userBean</code>.</p>
     *
     * @return a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * <p>Setter for the field <code>userBean</code>.</p>
     *
     * @param userBean a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * <p>Getter for the field <code>contact</code>.</p>
     *
     * @return the contact
     * @since 1.4.0
     */
    public String getContact() {
        return contact;
    }

    /**
     * <p>Setter for the field <code>contact</code>.</p>
     *
     * @param contact the contact to set
     * @since 1.4.0
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the searchable
     */
    public Boolean getSearchable() {
        return searchable;
    }

    /**
     * @param searchable the searchable to set
     */
    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    public List<ClientCertificate> getClientCertificates() {
        return clientCertificates;
    }

    public void setClientCertificates(List<ClientCertificate> clientCertificates) {
        this.clientCertificates = clientCertificates;
    }
}
