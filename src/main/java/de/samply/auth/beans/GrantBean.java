/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import de.samply.auth.dao.*;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Request;
import de.samply.auth.dao.dto.Scope;
import de.samply.auth.exception.*;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * This bean grants the requesting application access. The user must agree if the application
 * is not trusted.
 *
 */
@ManagedBean
@ViewScoped
public class GrantBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{userBean}")
    private UserBean userBean;

    /**
     * The client ID that requests a new access token
     */
    private String clientId;

    /**
     * The clients redirect URL. This URL must be one of the
     * redirect URLs in the database associated with the client.
     */
    private String redirectUrl;

    /**
     * The requested scopes
     */
    private String scope;

    /**
     * The requested external identity provider, optional.
     */
    private String ip;

    /**
     * The client from the database.
     */
    private Client client;

    /**
     * The list of requested scopes from the databsae.
     */
    private List<Scope> scopes;

    /**
     * The state from the request.
     */
    private String state;

    /**
     * The nonce from the request.
     */
    private String nonce;

    /**
     * If true, the grant will be remembered.
     */
    private Boolean remember = true;

    /**
     * Initialized the grant.xhtml site:
     *
     * - check if the client is known to this auth server
     * - check if the client is active
     * - check if the "openid" scope is requested, if not redirect with "error=no_openid_scope"
     *
     * @throws java.io.IOException if any.
     * @return a {@link java.lang.String} object.
     */
    public String init() throws IOException {
        if(clientId != null && scope != null) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                client = auth.get(ClientDAO.class).getClient(clientId);

                if(client == null) {
                    throw new ClientUnknownException();
                }

                if(!client.getActive()) {
                    throw new ClientDisabledException();
                }

                if(!client.redirectUrlValid(redirectUrl)) {
                    throw new InvalidRedirectURLException();
                }

                /**
                 * If the client needs special permission, check the permissions of the user.
                 */
                if(client.getWhitelist() && !userBean.mayUse(client)) {
                    throw new PermissionDeniedException();
                }

                scopes = new ArrayList<>();
                boolean openIdScope = false;
                boolean loginScope = false;

                for(String s : scope.split(" ")) {
                    if(s.trim().length() > 0) {
                        Scope sc = auth.get(ScopeDAO.class).getScope(s.trim());
                        if(sc != null) {
                            scopes.add(sc);

                            if(s.equals(de.samply.auth.rest.Scope.OPENID.getIdentifier())) {
                                openIdScope = true;
                            }

                            if(s.equals(de.samply.auth.rest.Scope.LOGIN.getIdentifier())) {
                                loginScope = true;
                            }
                        }
                    }
                }

                /**
                 * If the client is not trusted and requests the login scope, throw an exception.
                 */
                if(!client.getTrusted() && loginScope) {
                    throw new NotTrustedException();
                }

                /**
                 * Throw an exception if the client does not request the openid scope!
                 */
                if(!openIdScope) {
                    throw new NoOpenIDScopeException();
                }

                /**
                 * Grant the client access, if the client is trusted, or the user agreed on trusting the client
                 * before.
                 */
                if(auth.get(RememberDAO.class).remembers(userBean.getUser().getId(), client.getId()) ||
                        client.getTrusted()) {
                    grantAccess();
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        /**
         * No error occured: The app is known, the user data is ok,
         * and this app is not trusted, so show all requested permissions
         * and let the user decide.
         */
        return null;
    }

    /**
     * Called when the user click on the "Grant Access" button:
     *
     * - checks if the redirect url is valid for the specific client
     * - generate a code for the client and store it in the database
     * - redirect back to the client
     *
     * @throws java.io.IOException if any.
     */
    public void grantAccess() throws IOException {
        /**
         * This has been already checked, but we want to make sure the host is valid before we
         * send a redirect.
         */
        if(client.redirectUrlValid(redirectUrl)) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                RememberDAO rememberDao = auth.get(RememberDAO.class);

                Request request = new Request();
                request.setClientId(client.getId());
                request.setUserId(userBean.getUser().getId());
                request.setData(Request.getDataResourceWithScopes(scopes));

                /**
                 * The state is optional
                 */
                if(!StringUtil.isEmpty(state)) {
                    request.getData().setProperty(Vocabulary.STATE, state);
                }

                /**
                 * The nonce is optional
                 */
                if(!StringUtil.isEmpty(nonce)) {
                    request.getData().setProperty(Vocabulary.NONCE, nonce);
                }
                auth.get(RequestDAO.class).saveRequest(request);

                /**
                 * Remember the users decision, if he checked the checkbox and the client is not already trusted
                 */
                if(!client.getTrusted() && remember && !rememberDao.remembers(userBean.getUser().getId(), client.getId())) {
                    rememberDao.remember(userBean.getUser().getId(), client.getId());
                }

                /**
                 * Commit the request.
                 */
                auth.commit();

                StringBuilder url = new StringBuilder(redirectUrl);

                if(redirectUrl.contains("?")) {
                    url.append("&");
                } else {
                    url.append("?");
                }
                url.append("code=").append(request.getCode());

                /**
                 * If a state has been given as a parameter, add it to the redirect URL, so that the client can check it.
                 */
                if(! StringUtil.isEmpty(state)) {
                    url.append("&state=").append(state);
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect(url.toString());
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Redirect back to the client with "error=denied"
     *
     * @throws java.io.IOException if any.
     * @return a {@link java.lang.String} object.
     */
    public String denyAccess() throws IOException {
        return "/error?error=denied&faces-redirect=true";
    }

    /**
     * <p>Getter for the field <code>redirectUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * <p>Setter for the field <code>redirectUrl</code>.</p>
     *
     * @param redirectUrl a {@link java.lang.String} object.
     */
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * <p>Getter for the field <code>scope</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getScope() {
        return scope;
    }

    /**
     * <p>Setter for the field <code>scope</code>.</p>
     *
     * @param scope a {@link java.lang.String} object.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * <p>getApp.</p>
     *
     * @return a {@link de.samply.auth.dao.dto.Client} object.
     * @since 1.4.0
     */
    public Client getClient() {
        return client;
    }

    /**
     * <p>setApp.</p>
     *
     * @param app a {@link de.samply.auth.dao.dto.Client} object.
     * @since 1.4.0
     */
    public void setClient(Client app) {
        this.client = app;
    }

    /**
     * <p>Getter for the field <code>scopes</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Scope> getScopes() {
        return scopes;
    }

    /**
     * <p>Setter for the field <code>scopes</code>.</p>
     *
     * @param scopes a {@link java.util.List} object.
     */
    public void setScopes(List<Scope> scopes) {
        this.scopes = scopes;
    }

    /**
     * <p>Getter for the field <code>userBean</code>.</p>
     *
     * @return a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * <p>Setter for the field <code>userBean</code>.</p>
     *
     * @param userBean a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * <p>Getter for the field <code>remember</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getRemember() {
        return remember;
    }

    /**
     * <p>Setter for the field <code>remember</code>.</p>
     *
     * @param remember a {@link java.lang.Boolean} object.
     */
    public void setRemember(Boolean remember) {
        this.remember = remember;
    }

    /**
     * <p>Getter for the field <code>clientId</code>.</p>
     *
     * @return the clientId
     * @since 1.4.RC4
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * <p>Setter for the field <code>clientId</code>.</p>
     *
     * @param clientId the clientId to set
     * @since 1.4.RC4
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
