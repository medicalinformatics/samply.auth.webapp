/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import de.samply.auth.AuthConfig;
import de.samply.auth.PasswordPolicy;
import de.samply.auth.RememberMeMode;
import de.samply.auth.Shibboleth;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ClientDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.dto.*;
import de.samply.auth.rest.ServletUtil;
import de.samply.sdao.DAOException;
import jersey.repackaged.com.google.common.collect.Lists;

/**
 * The application wide bean that is used to get the maven version of this
 * application.
 *
 */
@ManagedBean
@ApplicationScoped
public class ApplicationBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ServletContext getFacesContext() {
        return (ServletContext)
                FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    /**
     * Returns the maven version of this application.
     *
     * @return the maven version
     */
    public String getVersion() {
        return ServletUtil.getVersion(getFacesContext());
    }

    /**
     * Returns the SCM commit ID.
     * @return
     */
    public String getCommitBuildId() {
        return ServletUtil.getBuildCommitId(getFacesContext());
    }

    /**
     * Returns the SCM commit branch.
     * @return
     */
    public String getCommitBranch() {
        return ServletUtil.getBuildCommitBranch(getFacesContext());
    }

    /**
     * Returns the Timestamp of the commit.
     * @return
     */
    public String getCommitTimestamp() {
        return ServletUtil.getBuildTimestamp(getFacesContext());
    }

    /**
     * Returns the JSF version of the currently used JSF library
     * @return
     */
    public String getJSFVersion() {
        return FacesContext.class.getPackage().getImplementationVersion();
    }

    /**
     * Returns the JSF vendor of the currently used JSF library
     * @return
     */
    public String getJSFTitle() {
        return FacesContext.class.getPackage().getImplementationTitle();
    }

    /**
     * Returns the database version.
     *
     * @return the database version
     * @since 1.4.0
     */
    public String getDBVersion() {
        return "" + AuthConfig.getDbVersion();
    }

    /**
     * Returns the list of all available "remember me modes".
     * @return
     */
    public List<RememberMeMode> getRememberMeModes() {
        return Arrays.asList(RememberMeMode.values());
    }

    /**
     * <p>getClients.</p>
     *
     * @return a {@link java.util.List} object.
     * @since 1.4.0
     */
    public List<Client> getClients() {
        try(AuthConnection auth = ConnectionFactory.get()) {
            return auth.get(ClientDAO.class).getClients();
        } catch (DAOException e) {
            return Collections.emptyList();
        }
    }

    /**
     * Returns a list of active Ldap Providers.
     * @return
     */
    public List<LdapProvider> getLdapProviders() {
        return Lists.newArrayList(AuthConfig.getLdapProviders());
    }

    /**
     * Returns a list of active ADFS providers.
     * @return
     */
    public List<AdfsProvider> getAdfsProviders() {
        return Lists.newArrayList(AuthConfig.getAdfsProviders());
    }

    /**
     * Returns a list of active ADFS providers.
     * @return
     */
    public List<SqlProvider> getSqlProviders() {
        return Lists.newArrayList(AuthConfig.getSqlProviders());
    }

    /**
     * Returns a list of all active Identity Providers, including the local identity
     * provider.
     * @return
     */
    public List<IdentityProvider> getIdentityProviders() {
        return AuthConfig.getIdentityProviders();
    }

    /**
     * Returns the identity provider with the given ID.
     * @param id
     * @return
     */
    public IdentityProvider getIdentityProvider(int id) {
        return AuthConfig.getIdentityProvider(id);
    }

    /**
     * Returns a human readable string of the identity provider for the given user.
     * @param user
     * @return
     */
    public String getProviderLabel(User user) {
        return AuthConfig.getProviderLabel(user);
    }

    /**
     * Returns the URL for the given client. Uses the server name from the request
     * to check if one URL should be propritized.
     *
     * @param client the client
     * @return the clients URL
     * @since 1.4.0
     */
    public String getClientUrl(Client client) {
        String serverName = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
        String domain = ServletUtil.getDomain(serverName);
        return client.getRedirectUrl(domain);
    }

    /**
     * Returns all client types.
     *
     * @return all client types
     */
    public ClientType[] getClientTypes() {
        return ClientType.values();
    }

    /**
     * Returns all provider types
     * @return
     */
    public ProviderType[] getProviderTypes() {
        return ProviderType.values();
    }

    /**
     * Returns all administration modes.
     * @return
     */
    public AdministrationMode[] getAdministrationModes() {
        return AdministrationMode.values();
    }

    /**
     * Returns the current password policy.
     * @return
     */
    public PasswordPolicy getPasswordPolicy() {
        return AuthConfig.getPolicy();
    }

    /**
     * Returns the current shibboleth configuration.
     * @return
     */
    public Shibboleth getShibboleth() {
        return AuthConfig.getShibboleth();
    }

    /**
     * Returns the current contact information text.
     * @return
     */
    public String getContactInformation() {
        return AuthConfig.getContactInformation();
    }

    /**
     * If true, the EULA has been enabled.
     * @return
     */
    public Boolean getEulaEnabled() {
        return AuthConfig.isEulaEnabled();
    }

    /**
     * If true, local accounts are enable, meaning local accounts can login, and if the registration is
     * enabled, new users can create a local account.
     * @return
     */
    public Boolean getLocalAccountsActive() {
        return AuthConfig.getLocalAccountsEnabled();
    }

    /**
     * If true, the authentication can be done via client certificates
     * @return
     */
    public Boolean getClientCertificateAuthentication() {
        return AuthConfig.isClientCertificatesEnabled();
    }

    /**
     * If true, the UI should hide the "login via client certificate" button.
     * @return
     */
    public Boolean getHideClientCertificateButton() {
        return AuthConfig.isHideClientCertificateButton();
    }

    /**
     * Build the invitation link for an invitation
     * @return url
     */
    public String getInvitationLink(Invitation invitation) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        String url = ServletUtil.getLocalRedirectUrl(context.getRequestScheme(), context.getRequestServerName(),
                context.getRequestServerPort(), context.getRequestContextPath(), "/invitation.xhtml?invitation=" + invitation.getUuid());

        return url;
    }

    /**
     * Returns the available preselect modes
     * @return
     */
    public PreselectMode[] getPreselectModes() {
        List<PreselectMode> list = new ArrayList<>();
        list.add(PreselectMode.NONE);
        list.add(PreselectMode.LOCAL);

        if(AuthConfig.getShibboleth().isActive()) {
            list.add(PreselectMode.SHIBBOLETH);
        }

        if(AuthConfig.getIdentityProviders().size() > 0) {
            list.add(PreselectMode.EXTERNAL_IDP);
        }
        return list.toArray(new PreselectMode[list.size()]);
    }

}
