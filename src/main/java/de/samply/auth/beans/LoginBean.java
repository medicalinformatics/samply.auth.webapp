/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.beans;

import java.io.IOException;
import java.io.Serializable;
import java.security.cert.CertificateException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import de.samply.auth.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.samply.auth.AuthConfig;
import de.samply.auth.Message;
import de.samply.auth.NameUtils;
import de.samply.auth.Shibboleth;
import de.samply.auth.ShibbolethUser;
import de.samply.auth.dao.dto.*;
import de.samply.auth.exception.LoginException;
import de.samply.auth.exception.VerificationRequiredException;
import de.samply.auth.mail.MailUtils;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * Manages the login website
 *
 */
@ManagedBean
@ViewScoped
public class LoginBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(LoginBean.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The email address that the user has entered
     */
    private String email;

    /**
     * The password that the user has entered
     */
    private String password;

    /**
     * The provider ID,
     */
    private Integer providerId = 0;
    private String providerTypeAsString = "";

    @ManagedProperty(value = "#{grantBean}")
    private GrantBean grantBean;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    @ManagedProperty(value = "#{loginCertificateBean}")
    private LoginCertificateBean loginCertificateBean;

    /**
     * If this value is true, the button to resend the activation email is send again.
     */
    private Boolean showResendButton = false;

    /**
     * The userId to whom a new verification email will be sent.
     */
    private int verifyUserId = 0;

    /**
     * The selected identity provider. Initially this is null.
     */
    private IdentityProvider provider = null;

    /**
     * Is called by the login.xhtml
     */
    public String init() {
        /**
         * Try login via client certificate
         */
        try {
            loginCertificateBean.init();

            if(userBean.getLoginValid()) {
                return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
            }
        } catch (LoginException | CertificateException e) {
            /**
             * If that one fails, ignore it.
             */
            e.printStackTrace();
        }


        if(!StringUtil.isEmpty(grantBean.getIp())) {
            try(AuthConnection auth = ConnectionFactory.get()) {
                IdentityProvider provider = auth.get(ProviderDAO.class).getProvider(grantBean.getIp());

                if(provider instanceof AdfsProvider) {
                    userBean.redirect((AdfsProvider) provider, grantBean.getClientId(), grantBean.getRedirectUrl(),
                            grantBean.getScope(), grantBean.getState(), grantBean.getNonce());
                    return null;
                }

                if (provider.getType() != null) {
                    providerTypeAsString = provider.getType().toString();
                }

                if(provider instanceof LdapProvider) {
                    providerId = provider.getId();
                }

                if(provider instanceof SqlProvider) {
                    providerId = provider.getId();
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        /**
         * If the user is a shibboleth user, try to authenticate him.
         */
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Shibboleth shibConfig = AuthConfig.getShibboleth();
        ShibbolethUser shibUser = ShibbolethUser.fromRequest(request, shibConfig);
        if(shibConfig.isActive() && !StringUtil.isEmpty(shibUser.getIdentity())) {
        	String identity = shibUser.getIdentity();
            String name = shibUser.getName();
            String firstName = shibUser.getFirstName();
            String lastName = shibUser.getLastName();
            if(StringUtil.isEmpty(name)) {
                name = NameUtils.getShibbolethDisplayName(firstName, lastName);
            }
            String email = shibUser.getEmail();

            try(AuthConnection auth = ConnectionFactory.get()) {
                if(StringUtil.isEmpty(identity) && StringUtil.isEmpty(shibUser.getIdentityProvider())) {
                    /**
                     * If the identity is empty and we *know* that shibboleth authentication was successful, show an
                     * error message.
                     */
                    logger.error("Shibboleth authentication failed because of empty attributes!");
                    logger.error("Shibboleth identity: " + identity);
                    Message.addError("shibIdentityProviderNotSupported");
                } else if(StringUtil.isEmpty(name) || StringUtil.isEmpty(email)) {
                    User shibbolethUser = auth.get(UserDAO.class).getShibbolethUser(identity);
                    if(shibbolethUser != null && userBean.shibbolethLogin(identity, null, null, null, null)) {
                        return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
                    } else {
                        logger.warn("Shibboleth authentication needs more values because of empty attributes!");
                        logger.warn("Shibboleth name: " + name);
                        logger.warn("Shibboleth email: " + email);
                        logger.warn("Redirecting the user to the shibboleth user page");
                        return "/shibbolethVerification.xhtml?faces-redirect=true&includeViewParams=true";
                    }
                } else {
                    if(userBean.shibbolethLogin(identity, name, firstName, lastName, email)) {
                        return userBean.getRedirectURL(!StringUtil.isEmpty(grantBean.getRedirectUrl()));
                    }
                }
            } catch (VerificationRequiredException e) {
                showResendButton = true;
                verifyUserId = e.getUserId();
            } catch (LoginException e) {
                Message.addError(e.getErrorMessageID());
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Handle the preselection in the invitation, if required
         */
        if(userBean.isInvited()) {
            try (AuthConnection auth = ConnectionFactory.get()) {
                if (!userBean.getLoginValid() && userBean.isInvitationValid(auth) && userBean.getInvitation().getPreselectMode() != PreselectMode.NONE) {
                    /**
                     * In case the invitation hasn't been used, use the registration form, redirect to shibboleth, or select external IdP
                     */

                    Invitation invitation = userBean.getInvitation();

                    switch (invitation.getPreselectMode()) {
                        case LOCAL:
                            return "/register.xhtml?includeViewParams=true&faces-redirect=true";

                        case SHIBBOLETH:
                            FacesContext.getCurrentInstance().getExternalContext().redirect(RequestBean.getShibbolethLoginUrl());
                            return null;

                        case EXTERNAL_IDP:
                            selectIdentityProvider(invitation.getProviderId());
                    }
                } else if(userBean.getInvitation().isUsed() && userBean.getInvitation().getUsedByUserId() != 0) {
                    /**
                     * Fill out the known fields, if the invitation has been used by a user, if possible
                     */

                    Invitation invitation = userBean.getInvitation();

                    User user = auth.get(UserDAO.class).getUser(invitation.getUsedByUserId());

                    if(user.isExternal()) {
                        if(user.isShibbolethUser()) {
                            FacesContext.getCurrentInstance().getExternalContext().redirect(RequestBean.getShibbolethLoginUrl());
                            return null;
                        } else if(AuthConfig.getIdentityProvider(user.getProviderId()) instanceof LdapProvider) {
                            this.email = user.getExternalId();
                            selectIdentityProvider(user.getProviderId());
                        }
                    } else {
                        this.email = user.getEmail();
                    }
                }
            } catch (DAOException | IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * If there is only *one* way to authenticate the user, show the form right away. This is only the case, if
         * shibboleth authentication has been disabled and there is only one identity provider
         */
        if(! AuthConfig.getShibboleth().isActive() && AuthConfig.getActiveIdentityProviders().size() == 1) {
            selectIdentityProvider(AuthConfig.getActiveIdentityProviders().get(0).getId());
        }

        return null;
    }

    /**
     * Deselects the currently selected identity provider. Called by the cancel button.
     */
    public void deselectIdentityProvider() {
        providerId = 0;
        provider = null;
        providerTypeAsString = "";
    }

    /**
     * Selects the given identity provider. Which should be an LDAP identity provider.
     * @param id
     */
    public void selectIdentityProvider(int id) {
        providerId = id;
        provider = AuthConfig.getIdentityProvider(id);
        if (provider.getType() != null) {
            providerTypeAsString = provider.getType().toString();
        }
    }

    /**
     * If the user clicks on login, let the session bean check the entered username and password.
     *
     * @return a {@link java.lang.String} object.
     */
    public String login() {
        try {
            if(email == null || "".equalsIgnoreCase(email) || password == null || "".equals(password)) {
                Message.addError("login.emptyfields");
                return null;
            }

            return userBean.login(providerId, email, password, !StringUtil.isEmpty(grantBean.getRedirectUrl()));
        } catch (VerificationRequiredException e) {
            showResendButton = true;
            verifyUserId = e.getUserId();
            return null;
        } catch (LoginException e) {
            Message.addError(e.getErrorMessageID());
            return null;
        }
    }

    /**
     * Called when the user wants a new activation code sent to his email address.
     *
     * @return a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public String resend() {
        try (AuthConnection auth = ConnectionFactory.get()){
            showResendButton = MailUtils.resendActivationMail(auth.get(UserDAO.class).getUser(verifyUserId));
            Message.addInfo("emailSent");
        } catch (DAOException e) {
            e.printStackTrace();
            return "/error?error=errorOccured";
        }

        return null;
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Getter for the field <code>password</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPassword() {
        return password;
    }

    /**
     * <p>Setter for the field <code>password</code>.</p>
     *
     * @param password a {@link java.lang.String} object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * <p>Getter for the field <code>userBean</code>.</p>
     *
     * @return a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * <p>Setter for the field <code>userBean</code>.</p>
     *
     * @param userBean a {@link de.samply.auth.beans.UserBean} object.
     * @since 1.4.0
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * <p>Getter for the field <code>grantBean</code>.</p>
     *
     * @return a {@link de.samply.auth.beans.GrantBean} object.
     */
    public GrantBean getGrantBean() {
        return grantBean;
    }

    /**
     * <p>Setter for the field <code>grantBean</code>.</p>
     *
     * @param grantBean a {@link de.samply.auth.beans.GrantBean} object.
     */
    public void setGrantBean(GrantBean grantBean) {
        this.grantBean = grantBean;
    }

    /**
     * <p>Getter for the field <code>showResendButton</code>.</p>
     *
     * @return the showResendButton
     * @since 1.4.0
     */
    public Boolean getShowResendButton() {
        return showResendButton;
    }

    /**
     * <p>Setter for the field <code>showResendButton</code>.</p>
     *
     * @param showResendButton the showResendButton to set
     * @since 1.4.0
     */
    public void setShowResendButton(Boolean showResendButton) {
        this.showResendButton = showResendButton;
    }

    /**
     * @return the providerId
     */
    public Integer getProviderId() {
        return providerId;
    }

    /**
     * @return the providerType as String
     */
    public String getProviderTypeAsString() {
        return providerTypeAsString;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    /**
     * @return the resendUserId
     */

    public int getVerifyUserId() {
        return verifyUserId;
    }

    /**
     * @param resendUserId the resendUserId to set
     */
    public void setVerifyUserId(int resendUserId) {
        this.verifyUserId = resendUserId;
    }

    public IdentityProvider getProvider() {
        return provider;
    }

    public void setProvider(IdentityProvider provider) {
        this.provider = provider;
    }

    public LoginCertificateBean getLoginCertificateBean() {
        return loginCertificateBean;
    }

    public void setLoginCertificateBean(LoginCertificateBean loginCertificateBean) {
        this.loginCertificateBean = loginCertificateBean;
    }
}
