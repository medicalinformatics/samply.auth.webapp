/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

/**
 * A user that has been authenticated using Shibboleth
 */
public class ShibbolethUser {

    /**
     * The users identity
     */
    private String identity;

    /**
     * The users name
     */
    private String name;

    /**
     * The users first name
     */
    private String firstName;

    /**
     * The users last name
     */
    private String lastName;

    /**
     * The users email address.
     */
    private String email;

    /**
     * The Shibboleth Identity Provider String that has authenticated the user.
     */
    private String identityProvider;

    /**
     * @return the identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param identity the identity to set
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the last name to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public static ShibbolethUser fromRequest(HttpServletRequest request, Shibboleth config) {
        ShibbolethUser user = new ShibbolethUser();
        if(config.isUseHttpHeaders()) {
            /**
             * Apparently the HTTP headers must be encoded in ISO-8859-1 (RFC), which is not the default for Java.
             * Just convert them and be done with it.
             */
            if(request.getHeader(config.getMailAttribute()) != null) {
                user.setEmail(new String(request.getHeader(config.getMailAttribute()).getBytes(StandardCharsets.ISO_8859_1)));
            }

            if(request.getHeader(config.getNameAttribute()) != null) {
                user.setName(new String(request.getHeader(config.getNameAttribute()).getBytes(StandardCharsets.ISO_8859_1)));
            }

            if(request.getHeader(config.getFirstNameAttribute()) != null) {
                user.setFirstName(new String(request.getHeader(config.getFirstNameAttribute()).getBytes(StandardCharsets.ISO_8859_1)));
            }

            if(request.getHeader(config.getLastNameAttribute()) != null) {
                user.setLastName(new String(request.getHeader(config.getLastNameAttribute()).getBytes(StandardCharsets.ISO_8859_1)));
            }

            if(request.getHeader(config.getIdentityAttribute()) != null) {
                user.setIdentity(new String(request.getHeader(config.getIdentityAttribute()).getBytes(StandardCharsets.ISO_8859_1)));
            }

            if(request.getHeader(Constants.SHIB_IDENTITY_PROVIDER) != null) {
                user.setIdentityProvider(new String(request.getHeader(Constants.SHIB_IDENTITY_PROVIDER).getBytes(StandardCharsets.ISO_8859_1)));
            }
        } else {
            user.setEmail((String) request.getAttribute(config.getMailAttribute()));
            user.setName((String) request.getAttribute(config.getNameAttribute()));
            user.setFirstName((String) request.getAttribute(config.getFirstNameAttribute()));
            user.setLastName((String) request.getAttribute(config.getLastNameAttribute()));
            user.setIdentity((String) request.getAttribute(config.getIdentityAttribute()));
            user.setIdentityProvider((String) request.getAttribute(Constants.SHIB_IDENTITY_PROVIDER));
        }
        return user;
    }

    /**
     * Get all HTTP headers (needed to check if we use correctly the header names)
     */
    public static Map<String, String> getHeadersInfoFromRequest(HttpServletRequest request) {

        Map<String, String> map = new HashMap<String, String>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }

    public String getIdentityProvider() {
        return identityProvider;
    }

    public void setIdentityProvider(String identityProvider) {
        this.identityProvider = identityProvider;
    }
}
