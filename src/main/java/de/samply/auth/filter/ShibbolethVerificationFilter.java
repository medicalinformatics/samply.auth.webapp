/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.filter;

import de.samply.auth.AuthConfig;
import de.samply.auth.Shibboleth;
import de.samply.auth.ShibbolethUser;
import de.samply.auth.beans.RequestBean;
import de.samply.string.util.StringUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter for websites, that need a user that is logged in properly.
 *
 */
@WebFilter(filterName = "ShibbolethVerificationFilter")
public class ShibbolethVerificationFilter implements Filter {

    /** {@inheritDoc} */
    @Override
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     *
     * If there is no session or the user is not logged in, redirect to "/login.xhtml"
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        Shibboleth shibConfig = AuthConfig.getShibboleth();
        ShibbolethUser shibUser = ShibbolethUser.fromRequest(req, shibConfig);

        if(StringUtil.isEmpty(shibUser.getIdentity())) {
            HttpServletResponse resp = (HttpServletResponse) response;
            resp.sendRedirect(RequestBean.getShibbolethLoginUrl(req));
            return;
        }

        chain.doFilter(request, response);
    }

    /** {@inheritDoc} */
    @Override
    public void init(FilterConfig config) throws ServletException {
    }

}
