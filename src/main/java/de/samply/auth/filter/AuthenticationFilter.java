/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import de.samply.auth.beans.UserBean;

/**
 * Filter for websites, that need a user that is logged in properly.
 *
 */
@WebFilter(filterName = "AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    /** {@inheritDoc} */
    @Override
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     *
     * If there is no session or the user is not logged in, redirect to "/login.xhtml"
     * If eula for a client was not accepted, redirect to "/eula.xhtml"
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        String clientId = ((HttpServletRequest) request).getParameter("client_id");

        if(session == null) {
            FilterUtil.redirect(request, response, "/login.xhtml");
            return;
        }

        UserBean userBean = (UserBean) session.getAttribute("userBean");

        if(userBean == null || !userBean.getLoginValid()) {
            FilterUtil.redirect(request, response, "/login.xhtml");
            return;
        }

        if(clientId!= null && !userBean.getNotAcceptedEula(clientId).isEmpty()) {
            FilterUtil.redirect(request, response, "/eula.xhtml");
            return;
        }

        chain.doFilter(request, response);
    }

    /** {@inheritDoc} */
    @Override
    public void init(FilterConfig config) throws ServletException {
    }

}
