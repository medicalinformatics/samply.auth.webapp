/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This filter rewrites all misconfigured URLs, e.g. "https://auth.samply.de/users/23" to
 * "https://auth.samply.de/oauth2/users/23".
 */
@WebFilter(filterName = "RewriteFilter")
public class RewriteFilter implements Filter {

    /**
     * The pattern used to identify requests to "/users/" instead of "/oauth2/users/".
     */
    private static final Pattern usersPattern = Pattern.compile("^/users/(\\d+)$");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;

            String url = httpRequest.getServletPath();

            Matcher userMatcher = usersPattern.matcher(url);
            if(userMatcher.find()) {
                /**
                 * Use the request dispatcher to forward the request to the real servlet
                 */
                request.getRequestDispatcher("/oauth2/users/" + userMatcher.group(1)).forward(request, response);
                return;
            }

            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
