/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.filter;

import de.samply.auth.Constants;
import de.samply.auth.beans.UserBean;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.AuthenticationCookieDAO;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.AuthenticationCookie;
import de.samply.auth.dao.dto.User;
import de.samply.auth.utils.HashUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Handles the authentication cookie.
 */
@WebFilter(filterName = "CookieFilter")
public class CookieFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(CookieFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        /**
         * This is crucial, because the default encoding in tomcat is ISO-8859-1 and the HttpRequest trys to parse
         * the parameters using this default encoding.
         */
        httpServletRequest.setCharacterEncoding(StandardCharsets.UTF_8.displayName());

        HttpSession session = ((HttpServletRequest) request).getSession(true);

        if(session != null) {
            UserBean userBean = (UserBean) session.getAttribute("userBean");
            Cookie[] cookies = httpServletRequest.getCookies();

            if(userBean == null) {
                userBean = new UserBean();
                session.setAttribute("userBean", userBean);
            }

            /**
             * Check for cookies only, if the user is not logged in in the current instance and
             * if there are any cookies anyway.
             */
            if(!userBean.getLoginValid() && cookies != null && cookies.length > 0) {
                for(Cookie cookie : cookies) {
                    if(Constants.AUTH_COOKIE.equals(cookie.getName())) {
                        /**
                         * So we found a cookie. Lets try to find it in the database.
                         */
                        logger.trace("Authentication cookie found");
                        try (AuthConnection auth = ConnectionFactory.get()) {
                            String[] parts = cookie.getValue().split(":");
                            UUID uuid = UUID.fromString(parts[0]);
                            String hash = HashUtils.SHA512(parts[1]);

                            AuthenticationCookie authzCookie = auth.get(AuthenticationCookieDAO.class).get(uuid.toString(), hash);

                            /**
                             * If we found the cookie, set the user and assume the login is valid.
                             */
                            if(authzCookie != null) {
                                logger.debug("Valid authentication cookie found, no need to login.");
                                User user = auth.get(UserDAO.class).getUser(authzCookie.getUserId());

                                userBean.setCookie(authzCookie);
                                userBean.setUser(user);
                                userBean.updateUserData(auth);
                            }
                        } catch(Exception e) {
                            logger.error("Authentication cookie has an invalid format, ignoring");
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
