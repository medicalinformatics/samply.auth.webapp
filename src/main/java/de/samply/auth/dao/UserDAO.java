/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import de.samply.auth.SecurityUtils;
import de.samply.auth.dao.dto.*;
import de.samply.auth.dao.dto.Misc.*;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.ObjectMapper;

/**
 * <p>UserDAO class.</p>
 *
 */
public class UserDAO extends AbstractDAO<User> {

    UserDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Returns all members for the given role.
     * @param location
     * @return
     * @throws DAOException
     */
    public List<User> getMembers(Location location) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " IN "
                + "(SELECT " + UserLocations.USER_ID.access() + " FROM " + UserLocations.TABLE.from() + " WHERE " + UserLocations.LOCATION_ID.access()
                + " = ?)", location.getId());
    }

    /**
     * Returns all administrators for the given role.
     * @param location
     * @return
     * @throws DAOException
     */
    public List<LocationAdministration> getAdmins(Location location) throws DAOException {
        return executeSelect("SELECT " + getSelectFields(User.class, LocationAdministration.class, Location.class) + " FROM " + SQL_TABLE
                + " LEFT JOIN " + LocationAdministration.TABLE.from()
                + " ON " + LocationAdministration.USER_ID.access() + " = " + User.ID.access()
                + " LEFT JOIN " + Location.TABLE.from()
                + " ON " + LocationAdministration.LOCATION_ID.access() + " = " + Location.ID.access()
                + " WHERE "
                + LocationAdministration.LOCATION_ID.access() + " = ?", locationAdminMapper, location.getId());
    }

    /**
     * Returns all members for the given role.
     * @param role
     * @return
     * @throws DAOException
     */
    public List<User> getAllMembers(Role role) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " IN "
                + "(SELECT " + UserRoles.USER_ID.access() + " FROM " + UserRoles.TABLE.from() + " WHERE " + UserRoles.ROLE_ID.access()
                + " = ?)", role.getId());
    }

    /**
     * Returns all non-derived members for the given role.
     * @param role
     * @return
     * @throws DAOException
     */
    public List<User> getMembers(Role role) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " IN "
                + "(SELECT " + UserRoles.USER_ID.access() + " FROM " + UserRoles.TABLE.from() + " WHERE " + UserRoles.ROLE_ID.access()
                + " = ? AND " + UserRoles.DERIVED.access() + " = false)", role.getId());
    }

    /**
     * Returns all administrators for the given role.
     * @param role
     * @return
     * @throws DAOException
     */
    public List<RoleAdministration> getAdmins(Role role) throws DAOException {
        return executeSelect("SELECT " + getSelectFields(User.class, RoleAdministration.class, Role.class) + " FROM " + SQL_TABLE
                + " LEFT JOIN " + RoleAdministration.TABLE.from()
                + " ON " + RoleAdministration.USER_ID.access() + " = " + User.ID.access()
                + " LEFT JOIN " + Role.TABLE.from()
                + " ON " + RoleAdministration.ROLE_ID.access() + " = " + Role.ID.access()
                + " WHERE "
                + RoleAdministration.ROLE_ID.access() + " = ?", roleAdminMapper, role.getId());
    }

    /**
     * Returns the internal user with the given email address.
     *
     * @param email a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public User getInternalUser(String email) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + User.EMAIL.access() + " = ? AND "
                + User.PROVIDER_ID.access() + " IS NULL AND "
                + User.SHIBBOLETH_IDENTITY.access() + " IS NULL", email.toLowerCase());
    }

    /**
     * Returns the user with the given user id.
     *
     * @param userId a int.
     * @return a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public User getUser(int userId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " = ?", userId);
    }

    /**
     * Returns the user for the given external identity provider and the given external id.
     * @param provider
     * @param externalId
     * @return
     * @throws DAOException
     */
    public User getExternalUser(IdentityProvider provider, String externalId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + User.PROVIDER_ID.access() + " = ? AND "
                + User.EXTERNAL_ID.access() + " = ?",
                provider.getId(), externalId);
    }

    /**
     * Returns the user for the given external identity provider and email.
     * @param provider
     * @param email
     * @return
     * @throws DAOException
     */
    public User getExternalUserByEmail(IdentityProvider provider, String email) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                        + User.PROVIDER_ID.access() + " = ? AND "
                        + User.EMAIL.access() + " = ?",
                provider.getId(), email);
    }

    /**
     * Returns the user with the given shibboleth identity.
     *
     * @param identity
     * @return
     * @throws DAOException
     */
    public User getShibbolethUser(String identity) throws DAOException {
        //FIXME Find out how to use array in prepared statements in de.samply.sdao
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + User.SHIBBOLETH_IDENTITY.access() + " LIKE ANY (ARRAY[ "+ splitIdentity(identity) +" ])");
    }

    /**
     * Returns all users.
     *
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<User> getUsers() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " ORDER BY " + User.ID.access());
    }

    /**
     * Returns all users of a certain type
     *
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<User> getUsers(Usertype userType) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + User.USERTYPE.access() + " = ?::usertype", userType);
    }

    /**
     * Saves the given user in the database.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveUser(User user) throws DAOException {
        user.setId(insert(
                User.TABLE,
                User.EMAIL.val(user.getEmail().toLowerCase()),
                User.HASHED_PASSWORD.val(user.getHashedPassword()),
                User.SALT.val(user.getSalt()),
                User.DATA.val(user.getData()),
                User.ACTIVE.val(user.getActive()),
                User.CONTACT_DATA.val(user.getContactData()),
                User.VERIFIED.val(user.getEmailVerified()),
                User.ROUNDS.val(user.getRounds()),
                User.PROVIDER_ID.val(user.getProviderId() == 0 ? null : user.getProviderId()),
                User.EXTERNAL_ID.val(user.getExternalId()),
                User.USERTYPE.val(user.getUsertype()),
                User.SHIBBOLETH_IDENTITY.val(user.getShibbolethIdentity()),
                User.EULA_ACCEPTED.val(user.getEulaAccepted()),
                User.CLIENT_CERTIFICATE.val(user.getClientCertificate())));
    }

    /**
     * Updates the "clientCertificate" value for the given user.
     * @param user
     * @throws DAOException
     */
    public void updateClientCertificate(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.CLIENT_CERTIFICATE.val(user.getClientCertificate()));
    }

    /**
     * Updates the user data attributes, like his real name.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void updateData(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.DATA.val(user.getData()));
    }

    /**
     * Updates the user contact information.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public void updateContact(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.CONTACT_DATA.val(user.getContactData()));
    }

    /**
     * Enables the given user account.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void enableUser(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.ACTIVE.val(true));
    }

    /**
     * Disables the given user account.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void disableUser(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.ACTIVE.val(false));
    }

    /**
     * Returns a new Recover code.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @return a {@link java.lang.String} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public String getNewRecoverCode(User user) throws DAOException {
        String code = SecurityUtils.newRandomString();

        delete(RecoverCode.TABLE, RecoverCode.USER_ID.val(user.getId()));

        insert(RecoverCode.TABLE,
                RecoverCode.USER_ID.val(user.getId()),
                RecoverCode.CODE.val(code),
                RecoverCode.EXPIRATION_DATE.val(new Timestamp(new Date().getTime() + 1000 * 60*60)));

        return code;
    }

    /**
     * Returns a new activation code.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @return a {@link java.lang.String} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public String getActivationCode(User user) throws DAOException {
        String code = SecurityUtils.newRandomString();

        delete(ActivationCode.TABLE, ActivationCode.USER_ID.val(user.getId()));

        insert(ActivationCode.TABLE,
                ActivationCode.USER_ID.val(user.getId()),
                ActivationCode.CODE.val(code),
                ActivationCode.EXPIRATION_DATE.val(new Timestamp(new Date().getTime() + 1000 * 60*60 * 24 * 7)));

        return code;
    }

    /**
     * Deletes the given user from the database.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void deleteUser(User user) throws DAOException {
        User dbUser = getUser(user.getId());

        if(dbUser.getActive()) {
            return;
        }

        delete(RecoverCode.TABLE, RecoverCode.USER_ID.val(user.getId()));
        delete(ActivationCode.TABLE, ActivationCode.USER_ID.val(user.getId()));
        delete(Remember.TABLE, Remember.USER_ID.val(user.getId()));
        delete(Key.TABLE, Key.USER_ID.val(user.getId()));
        delete(RefreshToken.TABLE, RefreshToken.USER_ID.val(user.getId()));
        delete(Invitation.TABLE, Invitation.USED_BY_USER_ID.val(user.getId()));
        delete(UserRoles.TABLE, UserRoles.USER_ID.val(user.getId()));
        delete(UserEula.TABLE, UserEula.USER_ID.val(user.getId()));

        delete(User.TABLE, User.ID.val(user.getId()));
    }

    /**
     * Returns the user for the given recover code.
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public User getUserRecoverCode(String code) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " IN "
                + "(SELECT " + RecoverCode.USER_ID.access() + " FROM " + RecoverCode.TABLE.from()
                + "WHERE " + RecoverCode.CODE.access() + " = ? AND " + RecoverCode.EXPIRATION_DATE.access() + " > now())", code);
    }

    /**
     * Returns the user with the given activation code.
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public User getUserActivationCode(String code) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " IN "
                + "(SELECT " + ActivationCode.USER_ID.access() + " FROM " + ActivationCode.TABLE.from()
                + "WHERE " + ActivationCode.CODE.access() + " = ? AND " + ActivationCode.EXPIRATION_DATE.access() + " > now())", code);
    }

    /**
     * Updated the user password.
     *
     * @param id a int.
     * @param oldHashedPassword a {@link java.lang.String} object.
     * @param newHashedPassword a {@link java.lang.String} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void updatePassword(int id, String oldHashedPassword, String newHashedPassword, int rounds) throws DAOException {
        update(User.TABLE, User.ID.val(id),
                User.HASHED_PASSWORD.val(newHashedPassword),
                User.ROUNDS.val(rounds));
    }

    /**
     * Updated the user password (by admin - without old password)
     *
     * @param id a int.
     * @param hashedPassword a {@link java.lang.String} object.
     * @param salt a {@link java.lang.String} object.
     * @param rounds int
     * @throws de.samply.sdao.DAOException if any.
     */
    public void updatePasswordByAdmin(int id, String hashedPassword, String salt, int rounds) throws DAOException {
        update(User.TABLE, User.ID.val(id),
                User.HASHED_PASSWORD.val(hashedPassword),
                User.SALT.val(salt),
                User.ROUNDS.val(rounds));
    }


    /**
     * Updates the user email address.
     * @param user
     * @throws DAOException
     */
    public void updateEmail(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()), User.EMAIL.val(user.getEmail()));
    }

    /**
     * Acceps the EULA for the given user.
     * @param user
     * @throws DAOException
     */
    public void acceptEula(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()), User.EULA_ACCEPTED.val(true));
    }

    /**
     * Verifies the given users email address.
     *
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public void verifyUser(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.VERIFIED.val(true));

        delete(ActivationCode.TABLE, ActivationCode.USER_ID.val(user.getId()));
    }

    /**
     * Resets the password
     *
     * @param code a {@link java.lang.String} object.
     * @param user a {@link de.samply.auth.dao.dto.User} object.
     * @param hashedPassword a {@link java.lang.String} object.
     * @param salt a {@link java.lang.String} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public void resetPassword(String code, User user, String hashedPassword, String salt, int rounds) throws DAOException {
        executeUpdate("UPDATE " + User.TABLE.table() + " SET " + User.SALT.column() + " = ?, "
                + User.HASHED_PASSWORD.column() + " = ?, "
                + User.ROUNDS.column() + " = ? WHERE " + User.ID.column() + " = (SELECT " + RecoverCode.USER_ID.access() +
                " FROM " + RecoverCode.TABLE.from() + "WHERE " + RecoverCode.CODE.access() + " = ? AND "
                + RecoverCode.EXPIRATION_DATE.access() + " > now() AND " + RecoverCode.USER_ID.access() + " = ?)",
                salt, hashedPassword, rounds, code, user.getId());

        delete(RecoverCode.TABLE, RecoverCode.USER_ID.val(user.getId()));
    }

    /**
     * Searches the database for users using the given query.
     *
     * @param query The name or email to search for
     * @return
     * @throws DAOException
     */
    public List<User> searchUser(String query) throws DAOException {
        String input = "%" + query + "%";
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE (" + User.EMAIL.access() + " ILIKE ? "
                + "OR " + User.DATA.access() + "#>>'{name}' ILIKE ?) AND (" + User.DATA.access() + "#>>'{searchable}')::boolean = true "
                + "AND " + User.ACTIVE.access() + " = true", input, input);
    }

    /**
     * Searches the database for users using the given query, ignoring the "searchable" flag.
     *
     * @param query The name or email to search for
     * @return
     * @throws DAOException
     */
    public List<User> findUsers(String query) throws DAOException {
        String input = "%" + query + "%";
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE (" + User.EMAIL.access() + " ILIKE ? "
                + "OR " + User.DATA.access() + "#>>'{name}' ILIKE ?) "
                + "AND " + User.ACTIVE.access() + " = true", input, input);
    }

    /**
     * Searches the database for users of a certain type using the given query, ignoring the "searchable" flag.
     *
     * @param query The name or email to search for
     * @return
     * @throws DAOException
     */
    public List<User> findUsers(String query, Usertype userType) throws DAOException {
        String input = "%" + query + "%";
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE (" + User.EMAIL.access() + " ILIKE ? "
                + "OR " + User.DATA.access() + "#>>'{name}' ILIKE ?) "
                + "AND " + User.ACTIVE.access() + " = true AND " + User.USERTYPE.access() + " = ?::usertype", input, input, userType);
    }

    /** {@inheritDoc} */
    @Override
    public User getObject(ResultSet set) throws SQLException {
        User user = new User();
        user.setId(set.getInt(User.ID.alias));
        user.setEmail(set.getString(User.EMAIL.alias));
        user.setHashedPassword(set.getString(User.HASHED_PASSWORD.alias));
        user.setSalt(set.getString(User.SALT.alias));
        user.setData(asJson(set.getString(User.DATA.alias)));
        user.setActive(set.getBoolean(User.ACTIVE.alias));
        user.setContactData(set.getString(User.CONTACT_DATA.alias));
        user.setEmailVerified(set.getBoolean(User.VERIFIED.alias));
        user.setRounds(set.getInt(User.ROUNDS.alias));
        user.setProviderId(set.getInt(User.PROVIDER_ID.alias));
        user.setExternalId(set.getString(User.EXTERNAL_ID.alias));
        user.setUsertype(Usertype.valueOf(set.getString(User.USERTYPE.alias)));
        user.setShibbolethIdentity(set.getString(User.SHIBBOLETH_IDENTITY.alias));
        user.setEulaAccepted(set.getBoolean(User.EULA_ACCEPTED.alias));
        user.setClientCertificate(set.getBoolean(User.CLIENT_CERTIFICATE.alias));
        return user;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return User.TABLE.table();
    }

    public static final String SQL_TABLE = User.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(User.class);

    private final ObjectMapper<RoleAdministration> roleAdminMapper = new ObjectMapper<RoleAdministration>() {
        @Override
        public RoleAdministration getObject(ResultSet set) throws SQLException {
            RoleAdministration admin = new RoleAdministration();
            admin.setUser(UserDAO.this.getObject(set));
            admin.setMode(AdministrationMode.valueOf(set.getString(RoleAdministration.MODE.alias)));
            admin.setRole(UserDAO.this.provider.get(RoleDAO.class).getObject(set));
            return admin;
        }
    };

    private final ObjectMapper<LocationAdministration> locationAdminMapper = new ObjectMapper<LocationAdministration>() {
        @Override
        public LocationAdministration getObject(ResultSet set) throws SQLException {
            LocationAdministration admin = new LocationAdministration();
            admin.setUser(UserDAO.this.getObject(set));
            admin.setMode(AdministrationMode.valueOf(set.getString(LocationAdministration.MODE.alias)));
            admin.setLocation(UserDAO.this.provider.get(LocationDAO.class).getObject(set));
            return admin;
        }
    };

    /**
     * Splits the DFN-AAI identity in array of single persisten ids
     * and represent this array as string for SQL query
     *
     * @param identity identity string received from DFN AAI
     * @return
     */
    private String splitIdentity(String identity) {
        String[] parts = identity.split(";");
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String part : parts) {
            builder.append("'%");
            builder.append(part);
            builder.append("%'");
            if (i!=parts.length-1) {
                builder.append(", ");
            }
            i++;
        }
        return builder.toString();
    };
}
