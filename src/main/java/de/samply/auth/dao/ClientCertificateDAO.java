/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.ClientCertificate;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * Handles the client certificates in the database.
 */
public class ClientCertificateDAO extends AbstractDAO<ClientCertificate> {

    protected ClientCertificateDAO(ConnectionProvider provider) {
        super(provider);
    }

    public void saveClientCertificate(ClientCertificate cc) throws DAOException {
        cc.setId(insert(ClientCertificate.TABLE,
                ClientCertificate.USER_ID.val(cc.getUserId()),
                ClientCertificate.CERTIFICATE_HASH.val(cc.getCertificateHash()),
                ClientCertificate.SUBJECT_DN.val(cc.getSubjectDN()),
                ClientCertificate.SERIAL_NUMBER.val(cc.getSerialNumber())));
    }

    public List<ClientCertificate> getClientCertificates(int userId) throws DAOException {
        return executeSelect(SQL_SELECT + " WHERE " + ClientCertificate.USER_ID.access() + " = ?", userId);
    }

    public ClientCertificate getClientCertificate(String hash) throws DAOException {
        return executeSingleSelect(SQL_SELECT + " WHERE " + ClientCertificate.CERTIFICATE_HASH.access() + " = ?", hash);
    }

    public void removeClientCertificate(ClientCertificate cc) throws DAOException {
        delete(ClientCertificate.TABLE, ClientCertificate.ID.val(cc.getId()));
    }

    @Override
    public ClientCertificate getObject(ResultSet resultSet) throws SQLException {
        ClientCertificate cc = new ClientCertificate();
        cc.setId(resultSet.getInt(ClientCertificate.ID.alias));
        cc.setUserId(resultSet.getInt(ClientCertificate.USER_ID.alias));
        cc.setCertificateHash(resultSet.getString(ClientCertificate.CERTIFICATE_HASH.alias));
        cc.setSubjectDN(resultSet.getString(ClientCertificate.SUBJECT_DN.alias));
        cc.setSerialNumber(resultSet.getString(ClientCertificate.SERIAL_NUMBER.alias));
        return cc;
    }

    public static final String SQL_SELECT = "SELECT " + getSelectFields(ClientCertificate.class) + " FROM " + ClientCertificate.TABLE.from();

}
