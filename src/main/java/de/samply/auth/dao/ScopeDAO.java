/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.Scope;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for OAuth2 scopes.
 *
 */
public class ScopeDAO extends AbstractDAO<Scope> {

    ScopeDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Returns all scopes.
     *
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<Scope> getAllScopes() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Returns a scope by its unique name.
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.Scope} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Scope getScope(String name) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Scope.NAME.access() + " = ?", name);
    }

    /**
     * Deletes the specified scope from the database.
     *
     * @param scope a {@link de.samply.auth.dao.dto.Scope} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void deleteScope(Scope scope) throws DAOException {
        delete(Scope.TABLE, Scope.ID.val(scope.getId()));
    }

    /**
     * Saves the database in the database.
     *
     * @param scope a {@link de.samply.auth.dao.dto.Scope} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveScope(Scope scope) throws DAOException {
        scope.setId(insert(
                Scope.TABLE,
                Scope.NAME.val(scope.getName()),
                Scope.TITLE_EN.val(scope.getTitleEn()),
                Scope.TITLE_DE.val(scope.getTitleDe()),
                Scope.DESCRIPTION_EN.val(scope.getDescriptionEn()),
                Scope.DESCRIPTION_DE.val(scope.getDescriptionDe()),
                Scope.ICON.val(scope.getIcon())));
    }

    /** {@inheritDoc} */
    @Override
    public Scope getObject(ResultSet set) throws SQLException {
        Scope scope = new Scope();
        scope.setDescriptionDe(set.getString(Scope.DESCRIPTION_DE.alias));
        scope.setDescriptionEn(set.getString(Scope.DESCRIPTION_EN.alias));
        scope.setTitleDe(set.getString(Scope.TITLE_DE.alias));
        scope.setTitleEn(set.getString(Scope.TITLE_EN.alias));
        scope.setIcon(set.getString(Scope.ICON.alias));
        scope.setName(set.getString(Scope.NAME.alias));
        scope.setId(set.getInt(Scope.ID.alias));
        return scope;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return Scope.TABLE.table();
    }

    public static final String SQL_TABLE = Scope.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Scope.class);

}
