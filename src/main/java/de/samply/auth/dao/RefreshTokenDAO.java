/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.RefreshToken;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for refresh tokens.
 *
 */
public class RefreshTokenDAO extends AbstractDAO<RefreshToken> {

    /**
     * Returns the refresh token with the specified UUID for the
     * specified client ID and client Secret.
     *
     * @param uuid a {@link java.lang.String} object.
     * @param clientId a {@link java.lang.String} object.
     * @param clientSecret a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.RefreshToken} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public RefreshToken getRefreshToken(String uuid, String clientId, String clientSecret) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " LEFT JOIN "
                + Client.TABLE.from() + " ON " + Client.ID.access() + " = " + RefreshToken.CLIENT_ID.access() + " WHERE "
                + Client.CLIENT_ID.access() + " = ? AND " + Client.ACTIVE.access() + " = true AND "
                + Client.CLIENT_SECRET.access() + " = ? AND " + RefreshToken.UUID.access() + " = ?", clientId, clientSecret, uuid);
    }

    /**
     * Saves the specified refresh token.
     *
     * @param token a {@link de.samply.auth.dao.dto.RefreshToken} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveRefreshToken(RefreshToken token) throws DAOException {
        executeUpdate("DELETE FROM " + RefreshToken.TABLE.table() + " WHERE " + RefreshToken.USER_ID.column() + " = ? AND "
                + RefreshToken.CLIENT_ID.column() + " = ?", token.getUserId(), token.getClientId());

        token.setId(insert(
                RefreshToken.TABLE,
                RefreshToken.CLIENT_ID.val(token.getClientId()),
                RefreshToken.USER_ID.val(token.getUserId()),
                RefreshToken.TOKEN.val(token.getToken()),
                RefreshToken.UUID.val(token.getUuid())));
    }

    RefreshTokenDAO(ConnectionProvider connection) {
        super(connection);
    }

    /** {@inheritDoc} */
    @Override
    public RefreshToken getObject(ResultSet set) throws SQLException {
        RefreshToken token = new RefreshToken();
        token.setId(set.getInt(RefreshToken.ID.alias));
        token.setToken(set.getString(RefreshToken.TOKEN.alias));
        token.setClientId(set.getInt(RefreshToken.CLIENT_ID.alias));
        token.setUserId(set.getInt(RefreshToken.USER_ID.alias));
        token.setUuid(set.getString(RefreshToken.UUID.alias));
        return token;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return RefreshToken.TABLE.table();
    }

    public static final String SQL_TABLE = RefreshToken.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(RefreshToken.class);

}
