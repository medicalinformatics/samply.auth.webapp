/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao;

import de.samply.auth.dao.dto.ShibbolethVerification;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Manages the access to shibboleth verification links.
 */
public class ShibbolethVerificationDAO extends AbstractDAO<ShibbolethVerification> {

    protected ShibbolethVerificationDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Returns the shibboleth verification object with the given uuid. If the verification is expired or the uuid is
     * not valid, this method will return null.
     * @param uuid
     * @return
     * @throws DAOException
     */
    public ShibbolethVerification get(String uuid, String identity) throws DAOException {
        if(uuid == null) {
            return null;
        }

        try {
            /**
             * Check if the UUID is valid. If we don't and the UUID is not valid, PostgreSQL will throw
             * an error.
             */
            UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return executeSingleSelect(SQL_SELECT + "WHERE " + ShibbolethVerification.UUID.access() + " = ?::uuid AND "
                + ShibbolethVerification.EXPIRATION_DATE.access() + " > now() AND "
                + ShibbolethVerification.SHIBBOLETH_IDENTITY.access() + " = ? ", uuid, identity);
    }

    /**
     * Saves the given shibboleth verification
     * @param verification
     * @throws DAOException
     */
    public void save(ShibbolethVerification verification) throws DAOException {
        verification.setId(
                insert(ShibbolethVerification.TABLE,
                        ShibbolethVerification.EMAIL.val(verification.getEmail()),
                        ShibbolethVerification.EXPIRATION_DATE.val(verification.getExpirationDate()),
                        ShibbolethVerification.IDENTITY_DATA.val(verification.getIdentityData()),
                        ShibbolethVerification.UUID.val(verification.getUuid()),
                        ShibbolethVerification.SHIBBOLETH_IDENTITY.val(verification.getShibbolethIdentity())));
    }

    /**
     * Deletes one specific shibboleth verification link.
     * @param verification
     * @throws DAOException
     */
    public void delete(ShibbolethVerification verification) throws DAOException {
        delete(ShibbolethVerification.TABLE,
                ShibbolethVerification.ID.val(verification.getId()));
    }

    /**
     * Cleans up the database from shibboleth verification links.
     * @throws DAOException
     */
    public void cleanup() throws DAOException {
        executeUpdate("DELETE FROM " + ShibbolethVerification.TABLE.table() + " WHERE " + ShibbolethVerification.EXPIRATION_DATE.column() + " < now()");
    }

    @Override
    public ShibbolethVerification getObject(ResultSet set) throws SQLException {
        ShibbolethVerification verification = new ShibbolethVerification();
        verification.setEmail(set.getString(ShibbolethVerification.EMAIL.alias));
        verification.setIdentityData(asJson(set.getString(ShibbolethVerification.IDENTITY_DATA.alias)));
        verification.setUuid(UUID.fromString(set.getString(ShibbolethVerification.UUID.alias)));
        verification.setExpirationDate(set.getTimestamp(ShibbolethVerification.EXPIRATION_DATE.alias));
        verification.setId(set.getInt(ShibbolethVerification.ID.alias));
        return verification;
    }


    private static final String SQL_SELECT = "SELECT " + getSelectFields(ShibbolethVerification.class) + " FROM "
            + ShibbolethVerification.TABLE.from() + " ";

}
