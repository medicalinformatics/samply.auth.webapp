/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public class RoleMapping implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("roleMapping", "map");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The incoming (meaning from the external identity provider) role / group name.
     */
    private String roleName;
    public static final Column ROLE_NAME = new Column(TABLE, "roleName");

    /**
     * The internal role
     */
    private Role role;
    public static final Column ROLE_ID = new Column(TABLE, "roleId");

    /**
     * The providers ID.
     */
    private IdentityProvider provider;
    public static final Column PROVIDER_ID = new Column(TABLE, "identityProviderId");

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the provider
     */
    public IdentityProvider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(IdentityProvider provider) {
        this.provider = provider;
    }

}
