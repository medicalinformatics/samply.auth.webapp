/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;
import java.util.Locale;
import java.util.Set;

import java.io.Serializable;

/**
 * An Eula in the Samply Auth application.
 */
public class Eula implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("eula", "eu");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * A label for a EULA, e.g. samply.Auth or CCP-IT
     */
    private String eulaType;
    public static final Column EULA_TYPE = new Column(TABLE, "eulaType");

    /**
     * EULA Version
     */
    private int version;
    public static final Column VERSION = new Column(TABLE, "version");

    /**
     * EULA Title
     * Can be specified for different languages in JSON format (e.g. {EN: 'samply.Auth EULA'})
     */
    private JSONResource title = new JSONResource();
    public static final Column TITLE = new Column(TABLE, "title");

    /**
     * EULA in html text (or text) format
     */
    private JSONResource htmlText = new JSONResource();
    public static final Column HTML_TEXT = new Column(TABLE, "htmlText");

    /**
     * EULA check box label
     */
    private JSONResource checkBoxLabel = new JSONResource();
    public static final Column CHECKBOX_LABEL = new Column(TABLE, "checkBoxLabel");

    /**
     * @return the eula type
     */
    public String getEulaType() {
        return eulaType;
    }

    /**
     * @param eulaType the eula type to set
     */
    public void setEulaType(String eulaType) {
        this.eulaType = eulaType;
    }

    /**
     * @return the eula version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version the eula version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the eula id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the eula id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for the field title
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getTitle() {
        return title;
    }

    /**
     * Getter for title in a language
     *
     * @return String
     */
    public String getTitleForLang(String lang) {
        if (title.getDefinedProperties().contains(lang)) {
            return title.getProperty(lang).getValue();
        }
        return null;
    }

    /**
     * Getter for html text (eula main text) in a language
     *
     * @return String
     */
    public String getHtmlTextForLang(String lang) {
        if (htmlText.getDefinedProperties().contains(lang)) {
            return htmlText.getProperty(lang).getValue();
        }
        return null;
    }

    /**
     * Getter for check box label (text to be accepted) in a language
     *
     * @return String
     */
    public String getCheckBoxLabelForLang(String lang) {
        if (checkBoxLabel.getDefinedProperties().contains(lang)) {
            return checkBoxLabel.getProperty(lang).getValue();
        }
        return null;
    }

    /**
     * Setter for title in a language
     */
    public void setTitleForLang(String lang, String value) {
        title.setProperty(lang, value);
    }

    /**
     * Setter for html text (eula main text) in a language
     */
    public void setHtmlTextForLang(String lang, String value) {
        htmlText.setProperty(lang, value);
    }

    /**
     * Setter for check box label (text to be accepted) in a language
     */
    public void setCheckBoxLabelForLang(String lang, String value) {
        checkBoxLabel.setProperty(lang, value);
    }

    /**
     * Setter for the field title
     *
     * @param title a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setTitle(JSONResource title) {
        this.title = title;
    }

    /**
     * Getter for the field htmlText
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getHtmlText() {
        return htmlText;
    }

    /**
     * Setter for the field htmlText
     *
     * @param htmlText a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setHtmlText(JSONResource htmlText) {
        this.htmlText = htmlText;
    }

    /**
     * Getter for the field checkBoxLabel
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getCheckBoxLabel() {
        return checkBoxLabel;
    }

    /**
     * Setter for the field checkBoxLabel
     *
     * @param checkBoxLabel a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setCheckBoxLabel(JSONResource checkBoxLabel) {
        this.checkBoxLabel = checkBoxLabel;
    }

    /**
     * Determines the language to display Eula
     * Check if there is a variant of Eula in environment language (default language)
     * If not, the first variant of Eula will be taken
     * @return
     */
    public String getLanguage() {
        Locale locale = Locale.getDefault();
        String lang = locale.getLanguage();

        if (checkBoxLabel.getDefinedProperties().contains(lang)) {
            return lang;
        } else {
            if (!checkBoxLabel.getDefinedProperties().isEmpty()) {
                lang = checkBoxLabel.getDefinedProperties().iterator().next();
                return lang;
            }
        }
        return null;
    }

    /**
     * Gets all available languages for Eula
     */
    public Set<String> getLanguages() {
        return checkBoxLabel.getDefinedProperties();
    }
}
