/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;
import java.net.URL;
import java.net.MalformedURLException;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * The client application used in the OAuth2 workflow.
 * Usually a client is an application.
 *
 */
public class Client implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("client", "c");

    /**
     * A unique ID.
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The name of the application.
     */
    private String name;
    public static final Column NAME = new Column(TABLE, "name");

    /**
     * A description of the application.
     */
    private String description;
    public static final Column DESCRIPTION = new Column(TABLE, "description");

    /**
     * A random unique string (public).
     */
    private String clientId;
    public static final Column CLIENT_ID = new Column(TABLE, "clientId");

    /**
     * A random unique string (secret).
     */
    private String clientSecret;
    public static final Column CLIENT_SECRET = new Column(TABLE, "clientSecret");

    /**
     * One or more redirect URLs, separated by a comma ','.
     */
    private String redirectUrl;
    public static final Column REDIRECT_URL = new Column(TABLE, "redirectUrl");

    /**
     * If false, the application is disabled and it can not get an access token.
     */
    private Boolean active;
    public static final Column ACTIVE = new Column(TABLE, "active");

    /**
     * If true, the user will not see the scopes that the client requests.
     */
    private Boolean trusted;
    public static final Column TRUSTED = new Column(TABLE, "trusted");

    /**
     * The client type, currently only "MDR", "FORMREPOSITORY" and "UNKOWN" are specified as client types.
     */
    private ClientType type;
    public static final Column TYPE = new Column(TABLE, "type");

    /**
     * If true, only users with the "login" permission for this client may login.
     */
    private Boolean whitelist;
    public static final Column WHITELIST = new Column(TABLE, "whitelist");

    /**
     * If true, the client can use the Samply.Auth REST interface without a token but with basic authentication using
     * the client id and client secret as username and password.
     */
    private Boolean basicAuthentication;
    public static final Column BASIC_AUTHENTICATION = new Column(TABLE, "basicAuthentication");

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>clientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * <p>Setter for the field <code>clientId</code>.</p>
     *
     * @param clientId a {@link java.lang.String} object.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * <p>Getter for the field <code>clientSecret</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * <p>Setter for the field <code>clientSecret</code>.</p>
     *
     * @param clientSecret a {@link java.lang.String} object.
     */
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    /**
     * <p>Getter for the field <code>active</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * <p>Setter for the field <code>active</code>.</p>
     *
     * @param active a {@link java.lang.Boolean} object.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>redirectUrl</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * <p>Setter for the field <code>redirectUrl</code>.</p>
     *
     * @param redirectUrl a {@link java.lang.String} object.
     */
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * <p>Getter for the field <code>trusted</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getTrusted() {
        return trusted;
    }

    /**
     * <p>Setter for the field <code>trusted</code>.</p>
     *
     * @param trusted a {@link java.lang.Boolean} object.
     */
    public void setTrusted(Boolean trusted) {
        this.trusted = trusted;
    }

    /**
     * <p>redirectUrlValid.</p>
     *
     * @param url a {@link java.lang.String} object.
     * @return a boolean.
     * @since 1.4.0
     */
    public boolean redirectUrlValid(String url) {
        URL urlToBeValidated, urlToValidate;

        try {
            urlToBeValidated = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }

        // url is compared to the list of redirect urls
        for(String host : redirectUrl.split(",")) {
            try {
                urlToValidate = new URL(host);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                continue;
            }

            // url is valid if it is equal (with or without trailing backslash)
            // or it starts with one of the redirect urls
            if(urlToBeValidated.getProtocol().trim().toLowerCase().equals(urlToValidate.getProtocol().trim().toLowerCase()) &&
                    urlToBeValidated.getHost().trim().toLowerCase().equals(urlToValidate.getHost().trim().toLowerCase()) &&
                    urlToBeValidated.getPort() == urlToValidate.getPort() &&
                    urlToBeValidated.getPath().trim().toLowerCase().startsWith(urlToValidate.getPath().trim().toLowerCase().replaceFirst("/$",""))) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Getter for the field <code>redirectUrl</code>.</p>
     *
     * @param domain a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public String getRedirectUrl(String domain) {
        String[] urls = redirectUrl.split(",");
        for(String url : urls) {
            if(url.trim().toLowerCase().contains(domain.trim().toLowerCase())) {
                return url.trim().toLowerCase();
            }
        }

        return urls[0];
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return the type
     * @since 1.4.RC4
     */
    public ClientType getType() {
        return type;
    }

    /**
     * <p>Setter for the field <code>type</code>.</p>
     *
     * @param type the type to set
     * @since 1.4.RC4
     */
    public void setType(ClientType type) {
        this.type = type;
    }

    /**
     * @return the whitelist
     */
    public Boolean getWhitelist() {
        return whitelist;
    }

    /**
     * @param whitelist the whitelist to set
     */
    public void setWhitelist(Boolean whitelist) {
        this.whitelist = whitelist;
    }

    public Boolean getBasicAuthentication() {
        return basicAuthentication;
    }

    public void setBasicAuthentication(Boolean basicAuthentication) {
        this.basicAuthentication = basicAuthentication;
    }
}
