/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.auth.NameUtils;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;
import de.samply.string.util.StringUtil;

/**
 * The samply user that can use Samply Auth.
 *
 */
public class User implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("samplyUser", "u");

    /**
     * The user ID
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The users email
     */
    private String email;
    public static final Column EMAIL = new Column(TABLE, "email");

    /**
     * The users hashed password. The hash is currently PBKDF2WithHmacSHA1
     */
    private String hashedPassword;
    public static final Column HASHED_PASSWORD = new Column(TABLE, "hashedPassword");

    /**
     * The salt used to "hash" the password
     */
    private String salt;
    public static final Column SALT = new Column(TABLE, "salt");

    /**
     * The data
     */
    private JSONResource data = new JSONResource();
    public static final Column DATA = new Column(TABLE, "data");

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String DISPLAY_NAME = "name";
    public static final String LANGUAGE = "lang";
    public static final String SEARCHABLE = "searchable";

    public static final String DEFAULT_LANGUAGE = "en";

    /**
     * If true the user can login. Otherwise he can not.
     */
    private Boolean active;
    public static final Column ACTIVE = new Column(TABLE, "active");

    /**
     * If true the users email has been verified by an activation email.
     */
    private Boolean emailVerified;
    public static final Column VERIFIED = new Column(TABLE, "verified");

    /**
     * The users contact data.
     */
    private String contactData;
    public static final Column CONTACT_DATA = new Column(TABLE, "contactData");

    /**
     * The number of rounds that is used to hash the password.
     */
    private int rounds;
    public static final Column ROUNDS = new Column(TABLE, "rounds");

    /**
     * The ID of the identity provider for this user. 0 if it is a local user.
     */
    private int providerId;
    public static final Column PROVIDER_ID = new Column(TABLE, "providerId");

    /**
     * The *external* ID for this user.
     */
    private String externalId;
    public static final Column EXTERNAL_ID = new Column(TABLE, "externalId");

    /**
     * The usertype, currently one of the following values:
     * NORMAL, OSSE_REGISTRY, BRIDGEHEAD
     */
    private Usertype usertype;
    public static final Column USERTYPE = new Column(TABLE, "usertype");

    /**
     * The users shibboleth identity. It is unique across one shibboleth federation.
     */
    private String shibbolethIdentity;
    public static final Column SHIBBOLETH_IDENTITY = new Column(TABLE, "shibbolethIdentity");

    /**
     * If true, the user may use client certificate authentication.
     */
    private Boolean clientCertificate;
    public static final Column CLIENT_CERTIFICATE = new Column(TABLE, "clientCertificate");

    /**
     * If true, the user has accepted the EULA.
     */
    private Boolean eulaAccepted;
    public static final Column EULA_ACCEPTED = new Column(TABLE, "eulaAccepted");

    /**
     * Returns true, if this user comes from an external identity provider.
     */
    public boolean isExternal() {
        return providerId != 0 || isShibbolethUser();
    }

    /**
     * Returns true, if the user is a user from shibboleth.
     * @return
     */
    public boolean isShibbolethUser() {
        return !StringUtil.isEmpty(shibbolethIdentity);
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Getter for the field <code>hashedPassword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * <p>Setter for the field <code>hashedPassword</code>.</p>
     *
     * @param hashedPassword a {@link java.lang.String} object.
     */
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    /**
     * <p>Getter for the field <code>salt</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSalt() {
        return salt;
    }

    /**
     * <p>Setter for the field <code>salt</code>.</p>
     *
     * @param salt a {@link java.lang.String} object.
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getData() {
        return data;
    }

    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param data a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setData(JSONResource data) {
        this.data = data;
    }

    /**
     * <p>Getter for the field <code>active</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * <p>Setter for the field <code>active</code>.</p>
     *
     * @param active a {@link java.lang.Boolean} object.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * <p>getName.</p>
     * Returns diplay name (name for registries or full name for normal users)
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        if(data.getDefinedProperties().contains(DISPLAY_NAME)) {
            return data.getProperty(DISPLAY_NAME).getValue();
        } else {
            return NameUtils.getDisplayName(getFirstName(), getLastName());
        }
    }

    /**
     * <p>getFirstName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        if(data.getDefinedProperties().contains(FIRST_NAME)) {
            return data.getProperty(FIRST_NAME).getValue();
        } else {
            return "";
        }
    }

    /**
     * <p>getLastName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        if(data.getDefinedProperties().contains(LAST_NAME)) {
            return data.getProperty(LAST_NAME).getValue();
        } else {
            return "";
        }
    }

    /**
     * <p>getLanguage.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLanguage() {
        if(data.getDefinedProperties().contains(LANGUAGE)) {
            return data.getProperty(LANGUAGE).getValue();
        } else {
            return DEFAULT_LANGUAGE;
        }
    }

    /**
     * <p>setFirstname.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        data.setProperty(FIRST_NAME, firstName);
    }

    /**
     * <p>setLastName.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        data.setProperty(LAST_NAME, lastName);
    }

    /**
     * <p>setName.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        data.setProperty(DISPLAY_NAME, name);
    }

    /**
     * <p>setLanguage.</p>
     *
     * @param language a {@link java.lang.String} object.
     */
    public void setLanguage(String language) {
        data.setProperty(LANGUAGE, language);
    }

    /**
     * <p>Getter for the field <code>contactData</code>.</p>
     *
     * @return the contactData
     */
    public String getContactData() {
        return contactData;
    }

    /**
     * <p>Setter for the field <code>contactData</code>.</p>
     *
     * @param contactData the contactData to set
     */
    public void setContactData(String contactData) {
        this.contactData = contactData;
    }

    /**
     * <p>Getter for the field <code>emailVerified</code>.</p>
     *
     * @return the emailVerified
     * @since 1.4.0
     */
    public Boolean getEmailVerified() {
        return emailVerified;
    }

    /**
     * <p>Setter for the field <code>emailVerified</code>.</p>
     *
     * @param emailVerified the emailVerified to set
     * @since 1.4.0
     */
    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    /**
     * If true, the user can be searched with the REST interface.
     * Default: false.
     *
     * @return
     */
    public Boolean getSearchable() {
        if(data.getDefinedProperties().contains(SEARCHABLE)) {
            return data.getProperty(SEARCHABLE).asBoolean();
        } else {
            return false;
        }
    }

    public void setSearchable(Boolean value) {
        data.setProperty(SEARCHABLE, value);
    }

    /**
     * @return the rounds
     */
    public int getRounds() {
        return rounds;
    }

    /**
     * @param rounds the rounds to set
     */
    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    /**
     * @return the providerId
     */
    public int getProviderId() {
        return providerId;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    /**
     * @return the externalId
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * @return the usertype
     */
    public Usertype getUsertype() {
        return usertype;
    }

    /**
     * @param usertype the usertype to set
     */
    public void setUsertype(Usertype usertype) {
        this.usertype = usertype;
    }

    /**
     * @return the shibbolethIdentity
     */
    public String getShibbolethIdentity() {
        return shibbolethIdentity;
    }

    /**
     * @param shibbolethIdentity the shibbolethIdentity to set
     */
    public void setShibbolethIdentity(String shibbolethIdentity) {
        this.shibbolethIdentity = shibbolethIdentity;
    }

    public Boolean getEulaAccepted() {
        return eulaAccepted;
    }

    public void setEulaAccepted(Boolean eulaAccepted) {
        this.eulaAccepted = eulaAccepted;
    }

    public Boolean getClientCertificate() {
        return clientCertificate;
    }

    public void setClientCertificate(Boolean clientCertificate) {
        this.clientCertificate = clientCertificate;
    }
}
