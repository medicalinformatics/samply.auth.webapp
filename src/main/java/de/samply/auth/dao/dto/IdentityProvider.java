/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

import java.io.Serializable;

/**
 * An identity provider authenticates a user. External users do not have their password
 * stored in Samply.Auth.
 */
public class IdentityProvider implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("identityProvider", "ip");

    public static final Column TYPE = new Column(TABLE, "type");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * A unique identifier, e.g. "DKFZ"
     */
    private String identifier;
    public static final Column IDENTIFIER = new Column(TABLE, "identifier");

    /**
     * A label for this identity provider.
     */
    private String label;
    public static final Column LABEL = new Column(TABLE, "label");

    /**
     * The url for this identity provider, e.g. "ldaps://example.org:636" or "https://example.org/adfs/oauth2"
     * or "mysql://example.org/example_db"
     */
    private String url;
    public static final Column URL = new Column(TABLE, "url");

    /**
     * The attribute for the email.
     */
    private String emailAttribute;
    public static final Column EMAIL_ATTRIBUTE = new Column(TABLE, "emailAttribute");

    /**
     * The attribute for the real name, e.g. "displayName".
     */
    private String nameAttribute;
    public static final Column NAME_ATTRIBUTE = new Column(TABLE, "nameAttribute");

    /**
     * The attribute for the first name. If this value is empty, it will be ignored.
     */
    private String firstNameAttribute;
    public static final Column FIRST_NAME_ATTRIBUTE = new Column(TABLE, "firstNameAttribute");

    /**
     * The attribute for the last name. If this value is empty, it will be ignored.
     */
    private String lastNameAttribute;
    public static final Column LAST_NAME_ATTRIBUTE = new Column(TABLE, "lastNameAttribute");

    /**
     * The attribute that is unique for all LDAP users. E.g.
     * "sAMAccountName".
     */
    private String idAttribute;
    public static final Column ID_ATTRIBUTE = new Column(TABLE, "idAttribute");

    /**
     * The member attribute (e.g. "memberOf" for LDAP or "group" for ADFS)
     */
    private String memberAttribute;
    public static final Column MEMBER_ATTRIBUTE = new Column(TABLE, "memberAttribute");

    /**
     * The link to the password recovery function of this identity provider.
     */
    private String passwordRecoveryLink;
    public static final Column PW_LINK = new Column(TABLE, "passwordRecoveryLink");

    /**
     * If true, new users will be activated.
     */
    private boolean defaultUserEnabled;
    public static final Column DEFAULT_USER_ENABLED = new Column(TABLE, "defaultUserEnabled");

    /**
     * If true, the email address of new users will be set to verified once they log in.
     */
    private boolean defaultUserVerified;
    public static final Column DEFAULT_USER_VERIFIED = new Column(TABLE, "defaultUserVerified");

    /**
     * If true, this identity provider is active, otherwise it is not possible to login using this identity provider.
     */
    private boolean active;
    public static final Column ACTIVE = new Column(TABLE, "active");

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public ProviderType getType() {
        if (id==0) {
            return null;
        }
        if(this instanceof LdapProvider) {
            return ProviderType.LDAP;
        } else if(this instanceof AdfsProvider) {
            return ProviderType.ADFS;
        } else if(this instanceof SqlProvider) {
            return ProviderType.SQL;
        } else {
            return null;
        }
    }

    /**
     * @return the emailAttribute
     */
    public String getEmailAttribute() {
        return emailAttribute;
    }

    /**
     * @param emailAttribute the emailAttribute to set
     */
    public void setEmailAttribute(String emailAttribute) {
        this.emailAttribute = emailAttribute;
    }

    /**
     * @return the nameAttribute
     */
    public String getNameAttribute() {
        return nameAttribute;
    }

    /**
     * @param nameAttribute the nameAttribute to set
     */
    public void setNameAttribute(String nameAttribute) {
        this.nameAttribute = nameAttribute;
    }

    /**
     * @return the idAttribute
     */
    public String getIdAttribute() {
        return idAttribute;
    }

    /**
     * @param idAttribute the idAttribute to set
     */
    public void setIdAttribute(String idAttribute) {
        this.idAttribute = idAttribute;
    }

    /**
     * @return the memberAttribute
     */
    public String getMemberAttribute() {
        return memberAttribute;
    }

    /**
     * @param memberAttribute the memberAttribute to set
     */
    public void setMemberAttribute(String memberAttribute) {
        this.memberAttribute = memberAttribute;
    }

    /**
     * @return the firstNameAttribute
     */
    public String getFirstNameAttribute() {
        return firstNameAttribute;
    }

    /**
     * @return the lastNameAttribute
     */
    public String getLastNameAttribute() {
        return lastNameAttribute;
    }

    /**
     * @param firstNameAttribute the firstNameAttribute to set
     */
    public void setFirstNameAttribute(String firstNameAttribute) {
        this.firstNameAttribute = firstNameAttribute;
    }

    /**
     * @param lastNameAttribute the lastNameAttribute to set
     */
    public void setLastNameAttribute(String lastNameAttribute) {
        this.lastNameAttribute = lastNameAttribute;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the passwordRecoveryLink
     */
    public String getPasswordRecoveryLink() {
        return passwordRecoveryLink;
    }

    /**
     * @param passwordRecoveryLink the passwordRecoveryLink to set
     */
    public void setPasswordRecoveryLink(String passwordRecoveryLink) {
        this.passwordRecoveryLink = passwordRecoveryLink;
    }

    /**
     * @return the defaultActive
     */
    public boolean isDefaultUserEnabled() {
        return defaultUserEnabled;
    }

    /**
     * @param defaultActive the defaultActive to set
     */
    public void setDefaultUserEnabled(boolean defaultActive) {
        this.defaultUserEnabled = defaultActive;
    }

    public boolean isDefaultUserVerified() {
        return defaultUserVerified;
    }

    public void setDefaultUserVerified(boolean defaultUserVerified) {
        this.defaultUserVerified = defaultUserVerified;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
