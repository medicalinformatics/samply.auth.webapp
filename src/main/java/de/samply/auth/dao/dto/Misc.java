/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public class Misc {

    public static class RecoverCode {
        public static final Table TABLE = new Table("recoverCode", "rvc");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");
        public static final Column CODE = new Column(TABLE, "code");

    }

    public static class ActivationCode {
        public static final Table TABLE = new Table("activationCode", "acc");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");
        public static final Column CODE = new Column(TABLE, "code");

    }

    public static class Remember {
        public static final Table TABLE = new Table("remember", "rem");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column CLIENT_ID = new Column(TABLE, "clientId");
    }

    public static class UserRoles {
        public static final Table TABLE = new Table("userRoles", "usrl");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column ROLE_ID = new Column(TABLE, "roleId");
        public static final Column DERIVED = new Column(TABLE, "derived");
    }

    public static class UserLocations {
        public static final Table TABLE = new Table("userLocations", "usrloc");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column LOCATION_ID = new Column(TABLE, "locationId");
    }

    public static class UserEula {
        public static final Table TABLE = new Table("userEula", "usreu");

        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column EULA_ID = new Column(TABLE, "eulaId");
        public static final Column DATE = new Column(TABLE, "date");
    }

    public static class ClientEula {
        public static final Table TABLE = new Table("clientEula", "cleu");

        public static final Column CLIENT_ID = new Column(TABLE, "clientId");
        public static final Column EULA_ID = new Column(TABLE, "eulaId");
    }

}
