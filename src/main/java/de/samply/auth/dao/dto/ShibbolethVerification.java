/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao.dto;

import de.samply.auth.NameUtils;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by paul on 6/28/16.
 */
public class ShibbolethVerification implements Serializable {

    public static final Table TABLE = new Table("shibbolethVerification", "sbv");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The UUID of this shibboleth verification
     */
    private UUID uuid;
    public static final Column UUID = new Column(TABLE, "uuid");

    /**
     * The users identity data (first and last name)
     */
    private JSONResource identityData = new JSONResource();
    public final static Column IDENTITY_DATA = new Column(TABLE, "identityData");

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";

    /**
     * The email address  the user entered when he generated this shibboleth verification
     */
    private String email;
    public static final Column EMAIL = new Column(TABLE, "email");

    /**
     * The expiration date for this shibboleth verification.
     */
    private Timestamp expirationDate;
    public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");

    /**
     * The shibboleth persistent id for whom this verification object has been created.
     */
    private String shibbolethIdentity;
    public static final Column SHIBBOLETH_IDENTITY = new Column(TABLE, "shibbolethIdentity");

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public java.util.UUID getUuid() {
        return uuid;
    }

    public void setUuid(java.util.UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return NameUtils.getDisplayName(getFirstName(), getLastName());
    }

    public JSONResource getIdentityData() {
        return identityData;
    }

    public void setIdentityData(JSONResource identityData) {
        this.identityData = identityData;
    }

    public String getFirstName() {
        if(identityData.getDefinedProperties().contains(FIRST_NAME)) {
            return identityData.getProperty(FIRST_NAME).getValue();
        } else {
            return "";
        }
    }

    public String getLastName() {
        if(identityData.getDefinedProperties().contains(LAST_NAME)) {
            return identityData.getProperty(LAST_NAME).getValue();
        } else {
            return "";
        }
    }

    public void setFirstName(String firstName) {
        identityData.setProperty(FIRST_NAME, firstName);
    }

    public void setLastName(String lastName) {
        identityData.setProperty(LAST_NAME, lastName);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getShibbolethIdentity() {
        return shibbolethIdentity;
    }

    public void setShibbolethIdentity(String shibbolethIdentity) {
        this.shibbolethIdentity = shibbolethIdentity;
    }
}
