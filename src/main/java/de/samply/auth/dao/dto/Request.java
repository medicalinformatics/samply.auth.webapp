/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import de.samply.auth.SecurityUtils;
import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;

/**
 * The request from the OAuth2 workflow.
 *
 */
public class Request implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("request", "req");

    /**
     * The id for this request.
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The client Id that made the request.
     */
    private int clientId;
    public static final Column CLIENT_ID = new Column(TABLE, "clientId");

    /**
     * The user ID for this request
     */
    private int userId;
    public static final Column USER_ID = new Column(TABLE, "userId");

    /**
     * The random string
     */
    private String code;
    public static final Column CODE = new Column(TABLE, "code");

    /**
     * The additional data like scopes
     */
    private JSONResource data;
    public static final Column DATA = new Column(TABLE, "data");

    /**
     * The expiration date.
     */
    private Timestamp expirationDate;
    public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");

    /**
     * <p>Constructor for Request.</p>
     */
    public Request() {
        setExpirationDate(new Timestamp(new Date().getTime() + 1000 * 60*2));
        setCode(SecurityUtils.newRandomString());
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getData() {
        return data;
    }

    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param data a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setData(JSONResource data) {
        this.data = data;
    }

    /**
     * <p>Getter for the field <code>userId</code>.</p>
     *
     * @return a int.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * <p>Setter for the field <code>userId</code>.</p>
     *
     * @param userId a int.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * <p>Getter for the field <code>expirationDate</code>.</p>
     *
     * @return a {@link java.sql.Timestamp} object.
     */
    public Timestamp getExpirationDate() {
        return new Timestamp(expirationDate.getTime());
    }

    /**
     * <p>Setter for the field <code>expirationDate</code>.</p>
     *
     * @param expirationDate a {@link java.sql.Timestamp} object.
     */
    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = new Timestamp(expirationDate.getTime());
    }

    /**
     * <p>Getter for the field <code>clientId</code>.</p>
     *
     * @return a int.
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * <p>Setter for the field <code>clientId</code>.</p>
     *
     * @param clientId a int.
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * <p>getDataResourceWithString.</p>
     *
     * @param scopes a {@link java.util.Collection} object.
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    static public JSONResource getDataResourceWithString(Collection<String> scopes) {
        JSONResource resource = new JSONResource();
        for(String s : scopes) {
            resource.addProperty(Vocabulary.SCOPE, s);
        }
        return resource;
    }

    /**
     * <p>getDataResourceWithScopes.</p>
     *
     * @param scopes a {@link java.util.Collection} object.
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    static public JSONResource getDataResourceWithScopes(Collection<Scope> scopes) {
        JSONResource resource = new JSONResource();
        for(Scope s : scopes) {
            resource.addProperty(Vocabulary.SCOPE, s.getName());
        }
        return resource;
    }

}
