/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public class LdapProvider extends IdentityProvider {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = IdentityProvider.TABLE;

    /**
     * The search base for this directory, e.g. "ou=User,dc=example,dc=com".
     */
    private String searchBase;
    public static final Column SEARCH_BASE = new Column(TABLE, "searchBase");

    /**
     * The base DN relative to the search base, e.g. "ou=My_Unit".
     */
    private String baseDN;
    public static final Column BASE_DN = new Column(TABLE, "baseDN");

    /**
     * If true, the application uses an anonymous bind. Otherwise it uses the
     * bindDN and bindPassword to bind to the LDAP server.
     */
    private boolean anonymous;
    public static final Column ANONYMOUS = new Column(TABLE, "anonymous");

    /**
     * The search filter used to find the users. E.g. "(sAMAccountName={0})",
     * where "{0}" will be replaced with the entered email.
     */
    private String searchFilter;
    public static final Column SEARCH_FILTER = new Column(TABLE, "searchFilter");

    /**
     * The DN that will be used for the bind.
     */
    private String bindDN;
    public static final Column BIND_DN = new Column(TABLE, "bindDN");

    /**
     * The password that will be used for the bind.
     */
    private String bindPassword;
    public static final Column BIND_PASSWORD = new Column(TABLE, "bindPassword");

    /**
     * @return the searchBase
     */
    public String getSearchBase() {
        return searchBase;
    }

    /**
     * @param searchBase the searchBase to set
     */
    public void setSearchBase(String searchBase) {
        this.searchBase = searchBase;
    }

    /**
     * @return the baseDN
     */
    public String getBaseDN() {
        return baseDN;
    }

    /**
     * @param baseDN the baseDN to set
     */
    public void setBaseDN(String baseDN) {
        this.baseDN = baseDN;
    }

    /**
     * @return the anonymous
     */
    public boolean isAnonymous() {
        return anonymous;
    }

    /**
     * @param anonymous the anonymous to set
     */
    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    /**
     * @return the searchFilter
     */
    public String getSearchFilter() {
        return searchFilter;
    }

    /**
     * @param searchFilter the searchFilter to set
     */
    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }

    /**
     * @return the bindDN
     */
    public String getBindDN() {
        return bindDN;
    }

    /**
     * @param bindDN the bindDN to set
     */
    public void setBindDN(String bindDN) {
        this.bindDN = bindDN;
    }

    /**
     * @return the bindPassword
     */
    public String getBindPassword() {
        return bindPassword;
    }

    /**
     * @param bindPassword the bindPassword to set
     */
    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

}
