/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * An ADFS providers uses the OAuth2 interface of ADFS (Active Directory Federation Services). An ADFS provider must return the following attributes:
 *
 * "sub", "name", "email", "group" (an array of group names, used for mapping between AD groups and local roles).
 */
public class AdfsProvider extends IdentityProvider {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = IdentityProvider.TABLE;

    /**
     * The base64 encoded public key in DER format, that is used to verify the access tokens coming from ADFS.
     */
    private String publicKey;
    public static final Column PUBLIC_KEY = new Column(TABLE, "publicKey");

    /**
     * The resource that will be accessed when using this provider.
     */
    private String resource;
    public static final Column RESOURCE = new Column(TABLE, "resource");

    /**
     * The ADFS clientID for this provider.
     */
    private String clientId;
    public static final Column CLIENT_ID = new Column(TABLE, "clientId");

    /**
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(String resource) {
        this.resource = resource;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


}
