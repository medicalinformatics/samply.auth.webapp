/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A scope in the OAuth2 workflow. A scope describes a set of permissions
 * used by a client and verified by interfaces.
 *
 */
public class Scope implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("scope", "sc");

    /**
     * The ID.
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The "icon", e.g. "&lt;i class='fa fa-check'/&gt;"
     */
    private String icon;
    public static final Column ICON = new Column(TABLE, "icon");

    /**
     * The english title
     */
    private String titleEn;
    public static final Column TITLE_EN = new Column(TABLE, "title_en");

    /**
     * The english description
     */
    private String descriptionEn;
    public static final Column DESCRIPTION_EN = new Column(TABLE, "description_en");

    /**
     * The german title
     */
    private String titleDe;
    public static final Column TITLE_DE = new Column(TABLE, "title_de");

    /**
     * The german description
     */
    private String descriptionDe;
    public static final Column DESCRIPTION_DE = new Column(TABLE, "description_de");

    /**
     * The unique name for this scope, e.g. "mdr".
     */
    private String name;
    public static final Column NAME = new Column(TABLE, "name");

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>icon</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIcon() {
        return icon;
    }

    /**
     * <p>Setter for the field <code>icon</code>.</p>
     *
     * @param icon a {@link java.lang.String} object.
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * <p>Getter for the field <code>descriptionEn</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * <p>Setter for the field <code>descriptionEn</code>.</p>
     *
     * @param descriptionEn a {@link java.lang.String} object.
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * <p>Getter for the field <code>descriptionDe</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescriptionDe() {
        return descriptionDe;
    }

    /**
     * <p>Setter for the field <code>descriptionDe</code>.</p>
     *
     * @param descriptionDe a {@link java.lang.String} object.
     */
    public void setDescriptionDe(String descriptionDe) {
        this.descriptionDe = descriptionDe;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>titleEn</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitleEn() {
        return titleEn;
    }

    /**
     * <p>Setter for the field <code>titleEn</code>.</p>
     *
     * @param titleEn a {@link java.lang.String} object.
     */
    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    /**
     * <p>Getter for the field <code>titleDe</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitleDe() {
        return titleDe;
    }

    /**
     * <p>Setter for the field <code>titleDe</code>.</p>
     *
     * @param titleDe a {@link java.lang.String} object.
     */
    public void setTitleDe(String titleDe) {
        this.titleDe = titleDe;
    }

}
