/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * The sign request in the extended OAuth2 workflow. A sign request can
 * be used to sign the code with the specified algorithm.
 * After the server has verified the signature it returns a valid access token.
 *
 */
public class SignRequest implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("signRequest", "sreq");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The keys ID. The key with this ID must be used to sign the code in this Sign Request.
     */
    private int keyId;
    public static final Column KEY_ID = new Column(TABLE, "keyId");

    /**
     * The random string that will be signed.
     */
    private String code;
    public static final Column CODE = new Column(TABLE, "code");

    /**
     * The expiration date for this sign request.
     */
    private Timestamp expirationDate;
    public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");

    /**
     * The sign algorithm that must be used for this sign request.
     */
    private String algorithm;
    public static final Column ALGORITHM = new Column(TABLE, "algorithm");

    /**
     * <p>Getter for the field <code>keyId</code>.</p>
     *
     * @return a int.
     */
    public int getKeyId() {
        return keyId;
    }

    /**
     * <p>Setter for the field <code>keyId</code>.</p>
     *
     * @param keyId a int.
     */
    public void setKeyId(int keyId) {
        this.keyId = keyId;
    }

    /**
     * <p>Getter for the field <code>expirationDate</code>.</p>
     *
     * @return a {@link java.sql.Timestamp} object.
     */
    public Timestamp getExpirationDate() {
        return new Timestamp(expirationDate.getTime());
    }

    /**
     * <p>Setter for the field <code>expirationDate</code>.</p>
     *
     * @param expirationDate a {@link java.sql.Timestamp} object.
     */
    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = new Timestamp(expirationDate.getTime());
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>algorithm</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * <p>Setter for the field <code>algorithm</code>.</p>
     *
     * @param algorithm a {@link java.lang.String} object.
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

}
