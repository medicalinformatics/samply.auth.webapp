/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Represents an authentication uuid, which can be used to authenticate a user.
 */
public class AuthenticationCookie implements Serializable {

    public static final Table TABLE = new Table("authenticationCookie", "authCook");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    private UUID uuid;
    public static final Column UUID = new Column(TABLE, "uuid");

    private String hash;
    public static final Column HASH = new Column(TABLE, "hash");

    private int userId;
    public static final Column USER_ID = new Column(TABLE, "userId");

    private Timestamp expirationDate;
    public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
