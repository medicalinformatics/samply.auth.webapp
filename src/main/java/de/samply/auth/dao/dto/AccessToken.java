/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.sql.Timestamp;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * The access token for the OAuth2 workflow. Expires two hours
 * after generation.
 *
 */
public class AccessToken {

    public static final Table TABLE = new Table("accessToken", "ac");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The user ID for whom this access token was issued.
     */
    private int userId;
    public static final Column USER_ID = new Column(TABLE, "userId");

    /**
     * The serialized access token.
     */
    private String accessToken;
    public static final Column ACCESS_TOKEN = new Column(TABLE, "token");

    /**
     * The timestamp after which the access token is no longer valid.
     */
    private Timestamp expirationDate;
    public static final Column EXPIRATION_DATE = new Column(TABLE, "expirationDate");

    /**
     * <p>Getter for the field <code>accessToken</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * <p>Setter for the field <code>accessToken</code>.</p>
     *
     * @param accessToken a {@link java.lang.String} object.
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * <p>Getter for the field <code>userId</code>.</p>
     *
     * @return a int.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * <p>Setter for the field <code>userId</code>.</p>
     *
     * @param userId a int.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>expirationDate</code>.</p>
     *
     * @return a {@link java.sql.Timestamp} object.
     */
    public Timestamp getExpirationDate() {
        return new Timestamp(expirationDate.getTime());
    }

    /**
     * <p>Setter for the field <code>expirationDate</code>.</p>
     *
     * @param expirationDate a {@link java.sql.Timestamp} object.
     */
    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = new Timestamp(expirationDate.getTime());
    }

}
