/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A public key that is used to verify a signature in the OAuth2 user-client flow
 *
 */
public class Key implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("key", "k");

    /**
     * The ID
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The user ID to whom this key belongs.
     */
    private int userId;
    public static final Column USER_ID = new Column(TABLE, "userId");

    /**
     * The base64 encoded key in DER format.
     */
    private String base64EncodedKey;
    public static final Column BASE64 = new Column(TABLE, "base64EncodedKey");

    /**
     * A simple description what this key is used for.
     */
    private String description;
    public static final Column DESCRIPTION = new Column(TABLE, "description");

    /**
     * The sha512 hash of the public key.
     */
    private String sha512Hash;
    public static final Column SHA512 = new Column(TABLE, "sha512Hash");

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>userId</code>.</p>
     *
     * @return a int.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * <p>Setter for the field <code>userId</code>.</p>
     *
     * @param userId a int.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * <p>Getter for the field <code>base64EncodedKey</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBase64EncodedKey() {
        return base64EncodedKey;
    }

    /**
     * <p>Setter for the field <code>base64EncodedKey</code>.</p>
     *
     * @param base64EncodedKey a {@link java.lang.String} object.
     */
    public void setBase64EncodedKey(String base64EncodedKey) {
        this.base64EncodedKey = base64EncodedKey;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>sha512Hash</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSha512Hash() {
        return sha512Hash;
    }

    /**
     * <p>Setter for the field <code>sha512Hash</code>.</p>
     *
     * @param sha512Hash a {@link java.lang.String} object.
     */
    public void setSha512Hash(String sha512Hash) {
        this.sha512Hash = sha512Hash;
    }

}
