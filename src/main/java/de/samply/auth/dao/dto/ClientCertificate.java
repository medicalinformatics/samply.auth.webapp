/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * Created by paul on 19.09.16.
 */
public class ClientCertificate implements Serializable {

    private static final long serialVersionUID = -6132523135741033816L;

    public static final Table TABLE = new Table("clientCertificate", "cCert");

    public static final Column ID = new Column(TABLE, "id");
    private int id;

    /**
     * The user Id who owns this client certificate
     */
    public static final Column USER_ID = new Column(TABLE, "userId");
    private int userId;

    /**
     * The SHA512 hash of the certificate
     */
    public static final Column CERTIFICATE_HASH = new Column(TABLE, "certificateHash");
    private String certificateHash;

    /**
     * The subject distinguished name
     */
    public static final Column SUBJECT_DN = new Column(TABLE, "subjectDN");
    private String subjectDN;

    /**
     * the serial number as string
     */
    public static final Column SERIAL_NUMBER = new Column(TABLE, "serialNumber");
    private String serialNumber;

    public String getCertificateHash() {
        return certificateHash;
    }

    public void setCertificateHash(String certificateHash) {
        this.certificateHash = certificateHash;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectDN() {
        return subjectDN;
    }

    public void setSubjectDN(String subjectDN) {
        this.subjectDN = subjectDN;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
