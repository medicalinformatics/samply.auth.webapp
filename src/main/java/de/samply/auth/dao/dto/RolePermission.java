/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A role permission objects grants a specific role a specific permission for an application.
 * E.g. "createSession" in a specific Mainzelliste.
 */
public class RolePermission implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("rolePermission", "rlpm");

    public static final Column ID = new Column(TABLE, "id", true);

    /**
     * The role ID.
     */
    private int roleId;
    public static final Column ROLE_ID = new Column(TABLE, "roleId");

    /**
     * The permission.
     */
    private Permission permission;
    public static final Column PERMISSION_ID = new Column(TABLE, "permissionId");

    /**
     * The client that was issued for this permission.
     */
    private Client client;
    public static final Column CLIENT_ID = new Column(TABLE, "clientId");

    /**
     * If true, the permission will also be exported into the access token.
     * Otherwise only the ID token will contain this permission.
     */
    private boolean export;
    public static final Column EXPORT = new Column(TABLE, "export");

    /**
     * @return the roleId
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    /**
     * @return the clientId
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClient(Client clientId) {
        this.client = clientId;
    }

    /**
     * @return the export
     */
    public boolean isExport() {
        return export;
    }

    /**
     * @param export the export to set
     */
    public void setExport(boolean export) {
        this.export = export;
    }


}
