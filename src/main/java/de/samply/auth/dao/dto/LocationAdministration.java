/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * An admin user organises a role: assigning new members, assigning new administrators if he is allowed to do so,
 * assigning permissions to a role, and so forth.
 */
public class LocationAdministration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("userAdminLocations", "locadm");

    /**
     * The role that is administrated by the specified user.
     */
    private Location location;
    public static final Column LOCATION_ID = new Column(TABLE, "locationId");

    /**
     * The user
     */
    private User user;
    public static final Column USER_ID = new Column(TABLE, "userId");

    /**
     * The mode of administration.
     */
    private AdministrationMode mode;
    public static final Column MODE = new Column(TABLE, "administrationMode");

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the mode
     */
    public AdministrationMode getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(AdministrationMode mode) {
        this.mode = mode;
    }

    /**
     * @return the role
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param role the role to set
     */
    public void setLocation(Location role) {
        this.location = role;
    }

}
