/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public class SqlProvider extends IdentityProvider {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = IdentityProvider.TABLE;

    /**
     * Sql Driver for connection
     */
    private String sqlDriver;
    public static final Column SQL_DRIVER = new Column(TABLE, "sqlDriver");

    /**
     * The DN that will be used for the bind.
     */
    private String bindDN;
    public static final Column BIND_DN = new Column(TABLE, "bindDN");

    /**
     * The password that will be used for the bind.
     */
    private String bindPassword;
    public static final Column BIND_PASSWORD = new Column(TABLE, "bindPassword");

    /**
     * The search query for authentification
     */
    private String searchQuery;
    public static final Column SEARCH_QUERY = new Column(TABLE, "searchQuery");

    /**
     * @return the searchQuery
     */
    public String getSqlDriver() {
        return sqlDriver;
    }

    /**
     * @param sqlDriver the sqlDriver to set
     */
    public void setSqlDriver(String sqlDriver) {
        this.sqlDriver = sqlDriver;
    }

    /**
     * @return the bindDN
     */
    public String getBindDN() {
        return bindDN;
    }

    /**
     * @param bindDN the bindDN to set
     */
    public void setBindDN(String bindDN) {
        this.bindDN = bindDN;
    }

    /**
     * @return the bindPassword
     */
    public String getBindPassword() {
        return bindPassword;
    }

    /**
     * @param bindPassword
     */
    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

    /**
     * @return the searchQuery
     */
    public String getSearchQuery() {
        return searchQuery;
    }

    /**
     * @param searchQuery the searchQuery to set
     */
    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

}
