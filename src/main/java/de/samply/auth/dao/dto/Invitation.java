/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao.dto;

import de.samply.auth.NameUtils;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;
import de.samply.string.util.StringUtil;

import java.io.Serializable;
import java.util.UUID;

/**
 * Represents an invite link, can be used once, via registration or login. After the link has been used, the
 * registration will not work
 * Created by paul on 4/25/16.
 */
public class Invitation implements Serializable {

    public final static Table TABLE = new Table("invitation", "inv");

    private int id;
    public final static Column ID = new Column(TABLE, "id");

    /**
     * The UUID of this invitation.
     */
    private UUID uuid;
    public final static Column UUID = new Column(TABLE, "uuid");

    /**
     * The target URL.
     */
    private String target;
    public final static Column TARGET = new Column(TABLE, "target");

    /**
     * The users name.
     */
    private String name;
    public final static Column NAME = new Column(TABLE, "name");

    /**
     * The users identity data (first and last name)
     */

    private JSONResource identityData = new JSONResource();
    public final static Column IDENTITY_DATA = new Column(TABLE, "identityData");

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";

    /**
     * The users email address.
     */
    private String email;
    public final static Column EMAIL = new Column(TABLE, "email");

    /**
     * If true, the invitation link has already been used. The target URL still works, but
     * the user is not added to the given role.
     */
    private boolean used;
    public final static Column USED = new Column(TABLE, "used");

    /**
     * The optional role. After the user uses the invitation link, he will be added to this role as a member, as
     * long as used is still false.
     */
    private int roleId;
    public final static Column ROLE_ID = new Column(TABLE, "roleId");

    /**
     * The preselectMode for this invitation. This field lets the creator of the invitation decide which method
     * will be used for authentication
     */
    private PreselectMode preselectMode;
    public final static Column PRESELECT_MODE = new Column(TABLE, "preselectMode");

    /**
     * If the preselect mode for this invitation is EXTERNAL_IDP, the providerId attributes specifies
     * the external identity provider which will be used for authentication.
     */
    private int providerId;
    public final static Column PROVIDER_ID = new Column(TABLE, "providerId");

    /**
     * The user ID who used this invitation. May be null.
     */
    private int usedByUserId;
    public final static Column USED_BY_USER_ID = new Column(TABLE, "usedByUserId");


    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        if(!StringUtil.isEmpty(name)) {
            return name;
        } else {
            return NameUtils.getDisplayName(getFirstName(), getLastName());
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>identityData</code>.</p>
     *
     * @return a {@link de.samply.sdao.json.JSONResource} object.
     */
    public JSONResource getIdentityData() {
        return identityData;
    }

    /**
     * <p>Setter for the field <code>data</code>.</p>
     *
     * @param identityData a {@link de.samply.sdao.json.JSONResource} object.
     */
    public void setIdentityData(JSONResource identityData) {
        this.identityData = identityData;
    }

    /**
     * <p>getFirstName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        if(identityData.getDefinedProperties().contains(FIRST_NAME)) {
            return identityData.getProperty(FIRST_NAME).getValue();
        } else {
            return "";
        }
    }

    /**
     * <p>getLastName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        if(identityData.getDefinedProperties().contains(LAST_NAME)) {
            return identityData.getProperty(LAST_NAME).getValue();
        } else {
            return "";
        }
    }

    /**
     * <p>setFirstname.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        identityData.setProperty(FIRST_NAME, firstName);
    }

    /**
     * <p>setLastName.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        identityData.setProperty(LAST_NAME, lastName);
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PreselectMode getPreselectMode() {
        return preselectMode;
    }

    public void setPreselectMode(PreselectMode preselectMode) {
        this.preselectMode = preselectMode;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getUsedByUserId() {
        return usedByUserId;
    }

    public void setUsedByUserId(int usedByUserId) {
        this.usedByUserId = usedByUserId;
    }
}
