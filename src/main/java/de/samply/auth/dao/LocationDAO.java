/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.AdministrationMode;
import de.samply.auth.dao.dto.Location;
import de.samply.auth.dao.dto.LocationAdministration;
import de.samply.auth.dao.dto.Misc.UserLocations;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 *
 */
public class LocationDAO extends AbstractDAO<Location> {

    /**
     * @param provider
     */
    protected LocationDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * @param location
     * @throws DAOException
     */
    public void updateLocation(Location location) throws DAOException {
        update(Location.TABLE, Location.ID.val(location.getId()),
                Location.IDENTIFIER.val(location.getIdentifier()),
                Location.NAME.val(location.getName()),
                Location.DESCRIPTION.val(location.getDescription()),
                Location.CONTACT.val(location.getContact()));
    }

    /**
     * Returns all locations.
     * @return
     * @throws DAOException
     */
    public List<Location> getLocations() throws DAOException {
        return executeSelect(SQL_SELECT);
    }

    /**
     * Returns the location with the given id.
     * @param id
     * @return
     * @throws DAOException
     */
    public Location getLocation(int id) throws DAOException {
        return executeSingleSelect(SQL_SELECT + " WHERE " + Location.ID.access() + " = ?", id);
    }

    /**
     * Returns a list of locations where the given user is a member of.
     * @return
     * @throws DAOException
     */
    public List<Location> getLocations(int userId) throws DAOException {
        return executeSelect(SQL_SELECT + " WHERE " + Location.ID.access() + " IN (SELECT " +
                UserLocations.LOCATION_ID.access() + " FROM " + UserLocations.TABLE.from() + " WHERE " + UserLocations.USER_ID.access() + " = ?)",
                userId);
    }

    /**
     * Returns a list of roles that are editable for the given userId.
     * @param id
     * @return
     * @throws DAOException
     */
    public List<Location> getEditableLocations(int id) throws DAOException {
        return executeSelect(SQL_SELECT  + " WHERE "
                + Location.ID.access() + " IN (SELECT " + LocationAdministration.LOCATION_ID.access() + " FROM " + LocationAdministration.TABLE.from() + " WHERE "
                + LocationAdministration.USER_ID.access() + " = ?)",
                id);
    }

    /**
     * Updates the list of members for the given location.
     * @param location
     * @param members
     * @throws DAOException
     */
    public void updateMembers(Location location, List<User> members) throws DAOException {
        executeUpdate("DELETE FROM " + UserLocations.TABLE.table() + " WHERE "
                + UserLocations.LOCATION_ID.column() + " = ?", location.getId());

        for(User m : members) {
            insert(UserLocations.TABLE,
                    UserLocations.USER_ID.val(m.getId()),
                    UserLocations.LOCATION_ID.val(location.getId()));
        }
    }

    /**
     * Updates the list of admins for the given location.
     * @param location
     * @param admins
     * @throws DAOException
     */
    public void updateAdmins(Location location, List<LocationAdministration> admins) throws DAOException {
        int sizeAdmins = 0;

        for(LocationAdministration admin : admins) {
            if(admin.getMode() == AdministrationMode.ADMIN) {
                sizeAdmins++;
            }
        }

        if(sizeAdmins == 0) {
            throw new NoAdminsException();
        }

        delete(LocationAdministration.TABLE, LocationAdministration.LOCATION_ID.val(location.getId()));

        for(LocationAdministration m : admins) {
            insert(LocationAdministration.TABLE,
                    LocationAdministration.LOCATION_ID.val(location.getId()),
                    LocationAdministration.USER_ID.val(m.getUser().getId()),
                    LocationAdministration.MODE.val(m.getMode()));
        }
    }

    /**
     * Saves the given location in the database.
     * @param location
     * @throws DAOException
     */
    public void saveLocation(Location location) throws DAOException {
        location.setId(insert(Location.TABLE,
                Location.IDENTIFIER.val(location.getIdentifier()),
                Location.NAME.val(location.getName()),
                Location.DESCRIPTION.val(location.getDescription()),
                Location.CONTACT.val(location.getContact())));
    }

    /**
     * Deletes the given location.
     * @param location
     * @throws DAOException
     */
    public void deleteLocation(Location location) throws DAOException {
        delete(UserLocations.TABLE, UserLocations.LOCATION_ID.val(location.getId()));
        delete(LocationAdministration.TABLE, LocationAdministration.LOCATION_ID.val(location.getId()));
        delete(Location.TABLE, Location.ID.val(location.getId()));
    }

    /* (non-Javadoc)
     * @see de.samply.sdao.AbstractDAO#getObject(java.sql.ResultSet)
     */
    @Override
    public Location getObject(ResultSet set) throws SQLException {
        Location location = new Location();
        location.setId(set.getInt(Location.ID.alias));
        location.setIdentifier(set.getString(Location.IDENTIFIER.alias));
        location.setDescription(set.getString(Location.DESCRIPTION.alias));
        location.setName(set.getString(Location.NAME.alias));
        location.setContact(set.getString(Location.CONTACT.alias));
        return location;
    }

    private static final String SQL_SELECT = "SELECT " + getSelectFields(Location.class) + " FROM " + Location.TABLE.from();

}
