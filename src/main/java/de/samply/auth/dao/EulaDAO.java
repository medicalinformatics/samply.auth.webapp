/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.auth.dao.dto.*;
import de.samply.auth.dao.dto.Misc.UserEula;
import de.samply.auth.dao.dto.Misc.ClientEula;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * The DAO for EULA
 */
public class EulaDAO extends AbstractDAO<Eula> {

    protected EulaDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Adds user which accepted an EULA.
     * @param eula
     * @param user
     * @throws DAOException
     */
    public void userAcceptedEula(Eula eula, User user) throws DAOException {
        Calendar calendar = Calendar.getInstance();
        insert(UserEula.TABLE,
                UserEula.USER_ID.val(user.getId()),
                UserEula.EULA_ID.val(eula.getId()),
                UserEula.DATE.val(new Timestamp(calendar.getTime().getTime())));
    }

    /**
     * Adds an EULA to client.
     * @param eula
     * @param client
     * @throws DAOException
     */
    public void addEulaToClient(Eula eula, Client client) throws DAOException {
        insert(ClientEula.TABLE,
                ClientEula.EULA_ID.val(eula.getId()),
                ClientEula.CLIENT_ID.val(client.getId()));
    }

    /**
     * Deletes an EULA for client.
     * @param eula
     * @param client
     * @throws DAOException
     */
    public void deleteEulaForClient(Eula eula, Client client) throws DAOException {
        executeUpdate("DELETE FROM " + ClientEula.TABLE.table() + " WHERE "
                + ClientEula.EULA_ID.column() + " = ? AND " + ClientEula.CLIENT_ID.column() + " = ?", eula.getId(), client.getId());    }

    /**
     * Returns a list of EULA for the given userId.
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<Eula> getEulaForUser(int userId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Eula.ID.access() + " IN (SELECT " + UserEula.EULA_ID.access() + " FROM " + UserEula.TABLE.from() + " WHERE "
                + UserEula.USER_ID.access() + " = ?)",
                userId);
    }

    /**
     * Returns a list of EULA for the given clientId.
     * @param clientId
     * @return
     * @throws DAOException
     */
    public List<Eula> getEulaForClient(int clientId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                        + Eula.ID.access() + " IN (SELECT " + ClientEula.EULA_ID.access() + " FROM " + ClientEula.TABLE.from() + " WHERE "
                        + ClientEula.CLIENT_ID.access() + " = ?)",
                clientId);
    }

    /**
     * Returns a list of not accepted EULA for the given clientId and given userId.
     * @param clientId
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<Eula> getEulaToAccept(int clientId, int userId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                        + Eula.ID.access() + " IN (SELECT " + ClientEula.EULA_ID.access() + " FROM " + ClientEula.TABLE.from() + " WHERE "
                        + ClientEula.CLIENT_ID.access() + " = ?) AND "
                        + Eula.ID.access() + " NOT IN (SELECT " + UserEula.EULA_ID.access() + " FROM " + UserEula.TABLE.from() + " WHERE "
                        + UserEula.USER_ID.access() + " = ?)",
                clientId, userId);
    }

    /**
     * Returns the eula with the given eula id.
     *
     * @param eulaId a int.
     * @return a {@link de.samply.auth.dao.dto.Eula} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Eula getEula(int eulaId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Eula.ID.access() + " = ?", eulaId);
    }

    /**
     * Returns all Eula types (together with the last version).
     *
     * @return a {@link de.samply.auth.dao.dto.Eula} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<Eula> getAllEulaTypes() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE +
                " WHERE " + Eula.VERSION.access() + " = " + "(SELECT MAX(" + Eula.VERSION.column() + ") FROM " + Eula.TABLE.table() +
                " WHERE " + Eula.EULA_TYPE.column() + " = " + Eula.EULA_TYPE.access() + ")");
    }

    /**
     * Returns the last version of eula having a type
     *
     * @param eulaType
     * @return a {@link de.samply.auth.dao.dto.Eula} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Eula getEulaLastVersion(String eulaType) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE +
                " WHERE " + Eula.EULA_TYPE.access() + " = ? AND " +
                Eula.VERSION.access() + " = " + "(SELECT MAX(" + Eula.VERSION.column() + ") FROM " + Eula.TABLE.table() +
                " WHERE " + Eula.EULA_TYPE.column() + " = ?)", eulaType, eulaType);
    }

    /**
     * Saves the given eula.
     * @param eula
     * @throws DAOException
     */
    public void saveEula(Eula eula) throws DAOException {
        eula.setId(insert(
                Eula.TABLE,
                Eula.EULA_TYPE.val(eula.getEulaType()),
                Eula.VERSION.val(eula.getVersion()),
                Eula.TITLE.val(eula.getTitle()),
                Eula.HTML_TEXT.val(eula.getHtmlText()),
                Eula.CHECKBOX_LABEL.val(eula.getCheckBoxLabel())));
    }

    /**
     * Updates the given eula.
     * @param eula
     * @throws DAOException
     */
    public void updateEula(Eula eula) throws DAOException {
        update( Eula.TABLE, Eula.ID.val(eula.getId()),
                Eula.EULA_TYPE.val(eula.getEulaType()),
                Eula.VERSION.val(eula.getVersion()),
                Eula.TITLE.val(eula.getTitle()),
                Eula.HTML_TEXT.val(eula.getHtmlText()),
                Eula.CHECKBOX_LABEL.val(eula.getCheckBoxLabel()));
    }

    @Override
    public Eula getObject(ResultSet set) throws SQLException {
        Eula eula = new Eula();
        eula.setId(set.getInt(Eula.ID.alias));
        eula.setEulaType(set.getString(Eula.EULA_TYPE.alias));
        eula.setVersion(set.getInt(Eula.VERSION.alias));
        eula.setTitle(asJson(set.getString(Eula.TITLE.alias)));
        eula.setHtmlText(asJson(set.getString(Eula.HTML_TEXT.alias)));
        eula.setCheckBoxLabel(asJson(set.getString(Eula.CHECKBOX_LABEL.alias)));
        return eula;
    }

    @Override
    protected String getInsertTable() {
        return Eula.TABLE.table();
    }

    public static final String SQL_TABLE = Eula.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Eula.class);

}
