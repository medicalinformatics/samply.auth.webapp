/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.AdministrationMode;
import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Misc.UserRoles;
import de.samply.auth.dao.dto.Permission;
import de.samply.auth.dao.dto.Role;
import de.samply.auth.dao.dto.RoleAdministration;
import de.samply.auth.dao.dto.RoleMapping;
import de.samply.auth.dao.dto.RolePermission;
import de.samply.auth.dao.dto.User;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.ObjectMapper;

/**
 * The DAO for roles, members and permissions.
 */
public class RoleDAO extends AbstractDAO<Role> {

    /**
     * Adds the given user as a member to the given group. This method uses the derived flag.
     * @param role
     * @param user
     * @throws DAOException
     */
    public void addDerivedMember(Role role, User user) throws DAOException {
        insert(UserRoles.TABLE,
                UserRoles.USER_ID.val(user.getId()),
                UserRoles.ROLE_ID.val(role.getId()),
                UserRoles.DERIVED.val(true));
    }

    /**
     * Deletes all derived memberships for the given user.
     * @param user
     * @throws DAOException
     */
    public void deleteDerivedMembership(User user) throws DAOException {
        executeUpdate("DELETE FROM " + UserRoles.TABLE.table() + " WHERE "
                + UserRoles.USER_ID.column() + " = ? AND " + UserRoles.DERIVED.column() + " = true", user.getId());
    }

    /**
     * Updates the list of members for the given role.
     * @param role
     * @param members
     * @throws DAOException
     */
    public void updateMembers(Role role, List<User> members) throws DAOException {
        executeUpdate("DELETE FROM " + UserRoles.TABLE.table() + " WHERE "
                + UserRoles.ROLE_ID.column() + " = ? AND " + UserRoles.DERIVED.column() + " = false", role.getId());

        for(User m : members) {
            insert(UserRoles.TABLE,
                    UserRoles.USER_ID.val(m.getId()),
                    UserRoles.ROLE_ID.val(role.getId()));
        }
    }

    /**
     * Updates the permissions for the given role.
     * @param role the role that will be updated
     * @param permissions the new list of role permissions
     * @throws DAOException
     */
    public void updatePermissions(Role role, List<RolePermission> permissions) throws DAOException {
        delete(RolePermission.TABLE, RolePermission.ROLE_ID.val(role.getId()));

        for(RolePermission m : permissions) {
            insert(RolePermission.TABLE,
                    RolePermission.ROLE_ID.val(role.getId()),
                    RolePermission.PERMISSION_ID.val(m.getPermission().getId()),
                    RolePermission.CLIENT_ID.val(m.getClient().getId()),
                    RolePermission.EXPORT.val(m.isExport()));
        }
    }

    /**
     * Updates the list of admins for the given role.
     * @param role
     * @param admins
     * @throws DAOException
     */
    public void updateAdmins(Role role, List<RoleAdministration> admins) throws DAOException {
        int sizeAdmins = 0;

        for(RoleAdministration admin : admins) {
            if(admin.getMode() == AdministrationMode.ADMIN) {
                sizeAdmins++;
            }
        }

        if(sizeAdmins == 0) {
            throw new NoAdminsException();
        }

        delete(RoleAdministration.TABLE, RoleAdministration.ROLE_ID.val(role.getId()));

        for(RoleAdministration m : admins) {
            insert(RoleAdministration.TABLE,
                    RoleAdministration.ROLE_ID.val(role.getId()),
                    RoleAdministration.USER_ID.val(m.getUser().getId()),
                    RoleAdministration.MODE.val(m.getMode()));
        }
    }

    /**
     * Returns a list of roles that are editable for the given userId.
     * @param id
     * @return
     * @throws DAOException
     */
    public List<Role> getEditableRoles(int id) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Role.ID.access() + " IN (SELECT " + RoleAdministration.ROLE_ID.access() + " FROM " + RoleAdministration.TABLE.from() + " WHERE "
                + RoleAdministration.USER_ID.access() + " = ?)",
                id);
    }

    /**
     * Returns a list of role permissions for the given role.
     * @param role
     * @return
     * @throws DAOException
     */
    public List<RolePermission> getRolePermissions(Role role) throws DAOException {
        return executeSelect("SELECT " + SQL_ROLE_PERMISSION_SELECT_FIELDS + " FROM " + SQL_ROLE_PERMISSION_TABLES + " WHERE "
                + RolePermission.ROLE_ID.access() + " = ?", permissionMapper,
                role.getId());
    }

    /**
     * Returns the list of all role permissions for the given userId.
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<RolePermission> getRolePermissionsForUser(int userId) throws DAOException {
        return executeSelect("SELECT " + SQL_ROLE_PERMISSION_SELECT_FIELDS + " FROM " + SQL_USER_ROLE_PERMISSION_TABLES +
                " WHERE " + UserRoles.USER_ID.access() + " = ?", permissionMapper,
                userId);
    }

    /**
     * Returns the role with the given ID.
     * @param roleId
     * @return
     * @throws DAOException
     */
    public Role getRole(int roleId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Role.ID.access() + " = ?", roleId);
    }

    /**
     * Returns the role with the given identifier
     * @param roleIdentifier
     * @return
     * @throws DAOException
     */
    public Role getRole(String roleIdentifier) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE LOWER(" + Role.IDENTIFIER.access() + ") = ?", roleIdentifier.toLowerCase());
    }

    /**
     * Deletes a role.
     * @param role
     * @throws DAOException
     */
    public void deleteRole(Role role) throws DAOException {
        delete(RolePermission.TABLE, RolePermission.ROLE_ID.val(role.getId()));
        delete(UserRoles.TABLE, UserRoles.ROLE_ID.val(role.getId()));
        delete(RoleAdministration.TABLE, RoleAdministration.ROLE_ID.val(role.getId()));
        delete(RoleMapping.TABLE, RoleMapping.ROLE_ID.val(role.getId()));
        delete(Role.TABLE, Role.ID.val(role.getId()));
    }

    /**
     * Updates the given role.
     * @param role
     * @throws DAOException
     */
    public void updateRole(Role role) throws DAOException {
        update(Role.TABLE, Role.ID.val(role.getId()),
                Role.NAME.val(role.getName()),
                Role.DESCRIPTION.val(role.getDescription()),
                Role.IDENTIFIER.val(role.getIdentifier()));
    }

    /**
     * Returns a list of all roles.
     * @return
     * @throws DAOException
     */
    public List<Role> getRoles() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Returns a list of roles where the given user is a member of.
     * @return
     * @throws DAOException
     */
    public List<Role> getRoles(int userId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Role.ID.access() + " IN (SELECT " +
                UserRoles.ROLE_ID.access() + " FROM " + UserRoles.TABLE.from() + " WHERE " + UserRoles.USER_ID.access() + " = ?)",
                userId);
    }

    /**
     * Saves the given role.
     * @param role
     * @throws DAOException
     */
    public void saveRole(Role role) throws DAOException {
        role.setId(insert(
                Role.TABLE,
                Role.NAME.val(role.getName()),
                Role.DESCRIPTION.val(role.getDescription()),
                Role.IDENTIFIER.val(role.getIdentifier())));
    }

    protected RoleDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public Role getObject(ResultSet set) throws SQLException {
        Role role = new Role();
        role.setName(set.getString(Role.NAME.alias));
        role.setDescription(set.getString(Role.DESCRIPTION.alias));
        role.setId(set.getInt(Role.ID.alias));
        role.setIdentifier(set.getString(Role.IDENTIFIER.alias));
        return role;
    }

    @Override
    protected String getInsertTable() {
        return Role.TABLE.table();
    }

    public static final String SQL_TABLE = Role.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Role.class);

    public static final String SQL_ROLE_PERMISSION_SELECT_FIELDS = getSelectFields(RolePermission.class, Permission.class,
            Client.class);

    public static final String SQL_ROLE_PERMISSION_TABLES = RolePermission.TABLE.from() + " LEFT JOIN " + Client.TABLE.from() + " ON "
            + Client.ID.access() + " = " + RolePermission.CLIENT_ID.access()
            + "LEFT JOIN " + Permission.TABLE.from() + " ON " + RolePermission.PERMISSION_ID.access() + " = " + Permission.ID.access();

    public static final String SQL_USER_ROLE_PERMISSION_TABLES = SQL_ROLE_PERMISSION_TABLES + " LEFT JOIN "
            + UserRoles.TABLE.from() + " ON " + UserRoles.ROLE_ID.access() + " = " + RolePermission.ROLE_ID.access();

    private final ObjectMapper<RolePermission> permissionMapper = new ObjectMapper<RolePermission>() {
        @Override
        public RolePermission getObject(ResultSet set) throws SQLException {
            RolePermission perm = new RolePermission();
            perm.setClient(provider.get(ClientDAO.class).getObject(set));
            perm.setPermission(provider.get(PermissionDAO.class).getObject(set));
            perm.setRoleId(set.getInt(RolePermission.ROLE_ID.alias));
            perm.setExport(set.getBoolean(RolePermission.EXPORT.alias));
            return perm;
        }
    };

}
