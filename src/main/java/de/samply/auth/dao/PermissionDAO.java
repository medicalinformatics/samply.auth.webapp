/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.Permission;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

public class PermissionDAO extends AbstractDAO<Permission> {

    public void deletePermission(Permission permission) throws DAOException {
        executeUpdate("DELETE FROM " + Permission.TABLE.table() + " WHERE " + Permission.ID.column() + " = ?", permission.getId());
    }

    public void updatePermission(Permission p) throws DAOException {
        update(Permission.TABLE, Permission.ID.val(p.getId()), Permission.NAME.val(p.getName()));
    }

    public List<Permission> getPermissions() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    public void savePermission(Permission permission) throws DAOException {
        permission.setId(insert(
                Permission.TABLE,
                Permission.NAME.val(permission.getName())));
    }

    protected PermissionDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public Permission getObject(ResultSet set) throws SQLException {
        Permission permission = new Permission();
        permission.setId(set.getInt(Permission.ID.alias));
        permission.setName(set.getString(Permission.NAME.alias));
        return permission;
    }

    @Override
    protected String getInsertTable() {
        return Permission.TABLE.table();
    }

    public static final String SQL_TABLE = Permission.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Permission.class);

}
