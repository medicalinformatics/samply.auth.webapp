/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Request;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The DAO for the request (Exchange code or a signed code for an access token)
 *
 */
public class RequestDAO extends AbstractDAO<Request> {

    RequestDAO(ConnectionProvider connection) {
        super(connection);
    }

    public List<Request> getRequests() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Saves a Request in the database
     *
     * @param request a {@link de.samply.auth.dao.dto.Request} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveRequest(Request request) throws DAOException {
        request.setId(insert(
                Request.TABLE,
                Request.CLIENT_ID.val(request.getClientId()),
                Request.DATA.val(request.getData()),
                Request.CODE.val(request.getCode()),
                Request.EXPIRATION_DATE.val(request.getExpirationDate()),
                Request.USER_ID.val(request.getUserId())));
    }

    /**
     * Returns a specific Request for the given code, clientId and clientSecret.
     *
     * If the client has been disabled by the administrator, this will return null.
     *
     * @param code a {@link java.lang.String} object.
     * @param clientId a {@link java.lang.String} object.
     * @param clientSecret a {@link java.lang.String} object.
     * @throws de.samply.sdao.DAOException if any.
     * @return a {@link de.samply.auth.dao.dto.Request} object.
     */
    public Request getRequest(String code, String clientId, String clientSecret) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " LEFT JOIN "
                + Client.TABLE.from() + " ON " + Client.ID.access() + " = " + Request.CLIENT_ID.access() + " WHERE "
                + Client.ACTIVE.access() + " = true AND " + Request.CODE.access() + " = ? AND " + Client.CLIENT_ID.access() + " = ? AND "
                + Client.CLIENT_SECRET.access() +" = ? AND " + Request.EXPIRATION_DATE.access() + " > now()", code, clientId, clientSecret);
    }

    /**
     * Deletes a specific request. This should be called right after an access token has been obtained.
     *
     * @param req a {@link de.samply.auth.dao.dto.Request} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void deleteRequest(Request req) throws DAOException {
        executeUpdate("DELETE FROM " + Request.TABLE.table() + " WHERE " + Request.ID.column() + " = ?", req.getId());
    }

    /**
     * Deletes all old (expired) requests
     *
     * @throws de.samply.sdao.DAOException if any.
     */
    public void cleanUp() throws DAOException {
        executeUpdate("DELETE FROM " + Request.TABLE.table() + " WHERE " + Request.EXPIRATION_DATE.column() + " < now()");
    }

    /** {@inheritDoc} */
    @Override
    public Request getObject(ResultSet set) throws SQLException {
        Request request = new Request();
        request.setClientId(set.getInt(Request.CLIENT_ID.alias));
        request.setCode(set.getString(Request.CODE.alias));
        request.setExpirationDate(set.getTimestamp(Request.EXPIRATION_DATE.alias));
        request.setId(set.getInt(Request.ID.alias));
        request.setData(asJson(set.getString(Request.DATA.alias)));
        request.setUserId(set.getInt(Request.USER_ID.alias));
        return request;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return Request.TABLE.table();
    }

    public static final String SQL_TABLE = Request.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Request.class);

}
