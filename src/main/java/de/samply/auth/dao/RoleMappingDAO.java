/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.AdfsProvider;
import de.samply.auth.dao.dto.IdentityProvider;
import de.samply.auth.dao.dto.LdapProvider;
import de.samply.auth.dao.dto.Role;
import de.samply.auth.dao.dto.RoleMapping;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for role mapping for external identity providers.
 */
public class RoleMappingDAO extends AbstractDAO<RoleMapping>{

    /**
     * Deletes the given role mapping.
     * @param mapping
     * @throws DAOException
     */
    public void deleteRoleMapping(RoleMapping mapping) throws DAOException {
        delete(RoleMapping.TABLE, RoleMapping.ID.val(mapping.getId()));
    }

    /**
     * Saves the given role mapping.
     * @param mapping
     * @throws DAOException
     */
    public void saveRoleMapping(RoleMapping mapping) throws DAOException {
        mapping.setId(insert(
                RoleMapping.TABLE,
                RoleMapping.PROVIDER_ID.val(mapping.getProvider().getId()),
                RoleMapping.ROLE_ID.val(mapping.getRole().getId()),
                RoleMapping.ROLE_NAME.val(mapping.getRoleName())));
    }

    /**
     * Returns a list of all role mappings.
     * @return
     * @throws DAOException
     */
    public List<RoleMapping> getRoleMappings() throws DAOException {
        return executeSelect(SQL_SELECT);
    }

    /**
     * Returns a list of all role mappings for the given provider.
     * @param providerId
     * @return
     * @throws DAOException
     */
    public List<RoleMapping> getRoleMappingsForProvider(int providerId) throws DAOException {
        return executeSelect(SQL_SELECT + " WHERE " + RoleMapping.PROVIDER_ID.access() + " = ?", providerId);
    }

    protected RoleMappingDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public RoleMapping getObject(ResultSet set) throws SQLException {
        RoleMapping map = new RoleMapping();
        map.setId(set.getInt(RoleMapping.ID.alias));
        map.setRoleName(set.getString(RoleMapping.ROLE_NAME.alias));
        map.setProvider(provider.get(ProviderDAO.class).getObject(set));
        map.setRole(provider.get(RoleDAO.class).getObject(set));
        return map;
    }

    @Override
    protected String getInsertTable() {
        return RoleMapping.TABLE.table();
    }

    public static final String SQL_TABLE = RoleMapping.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(RoleMapping.class, Role.class, IdentityProvider.class, AdfsProvider.class, LdapProvider.class);

    public static final String SQL_SELECT = "SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " LEFT JOIN " + IdentityProvider.TABLE.from()
            + " ON " + IdentityProvider.ID.access() + " = " + RoleMapping.PROVIDER_ID.access() + " LEFT JOIN " + Role.TABLE.from()
            + " ON " + Role.ID.access() + " = " + RoleMapping.ROLE_ID.access();

}
