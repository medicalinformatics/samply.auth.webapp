/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.Client;
import de.samply.auth.dao.dto.Eula;
import de.samply.auth.dao.dto.ClientType;
import de.samply.auth.dao.dto.Misc.Remember;
import de.samply.auth.dao.dto.Misc.ClientEula;
import de.samply.auth.dao.dto.RefreshToken;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for clients.
 *
 */
public class ClientDAO extends AbstractDAO<Client> {

    ClientDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * returns all clients.
     *
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<Client> getClients() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " ORDER BY " + Client.NAME.access() + "");
    }

    /**
     * returns all clients which requires accepting a EULA
     *
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<Client> getClients(Eula eula) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Client.ID.access() + " IN "
                + "(SELECT " + ClientEula.CLIENT_ID.access() + " FROM " + ClientEula.TABLE.from() + " WHERE " + ClientEula.EULA_ID.access()
                + " = ?)", eula.getId());
    }

    /**
     * Updates the eula version needed to be accepted for the specified client in the database.
     *
     * @throws de.samply.sdao.DAOException if any.
     */
    public void updateEulaVersion(Eula oldValue, Eula newValue) throws DAOException {
        update(ClientEula.TABLE, ClientEula.EULA_ID.val(oldValue.getId()), ClientEula.EULA_ID.val(newValue.getId()));
    }

    /**
     * Saves the specified client in the database.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveClient(Client client) throws DAOException {
        client.setId(insert(
                Client.TABLE,
                Client.NAME.val(client.getName()),
                Client.DESCRIPTION.val(client.getDescription()),
                Client.CLIENT_ID.val(client.getClientId()),
                Client.CLIENT_SECRET.val(client.getClientSecret()),
                Client.REDIRECT_URL.val(client.getRedirectUrl()),
                Client.ACTIVE.val(client.getActive()),
                Client.TRUSTED.val(client.getTrusted()),
                Client.TYPE.val(client.getType()),
                Client.WHITELIST.val(client.getWhitelist()),
                Client.BASIC_AUTHENTICATION.val(client.getBasicAuthentication())));
    }

    /**
     * Returns the client by its client ID (OAuth2 Client ID)
     *
     * @param clientId a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Client getClient(String clientId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Client.CLIENT_ID.access() + " = ?", clientId);
    }

    /**
     * Returns the client by its database ID.
     *
     * @param clientId a int.
     * @return a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Client getClient(int clientId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Client.ID.access() + " = ?", clientId);
    }

    /**
     * Delete the specified client. This method will fail, if there are still valid
     * access tokens used for this application;
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void delete(Client client) throws DAOException {
        delete(Remember.TABLE, Remember.CLIENT_ID.val(client.getId()));
        delete(RefreshToken.TABLE, RefreshToken.CLIENT_ID.val(client.getId()));
        delete(ClientEula.TABLE, ClientEula.CLIENT_ID.val(client.getId()));
        delete(Client.TABLE, Client.ID.val(client.getId()));
    }

    /**
     * Disables the specified client by its database ID.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void disable(Client client) throws DAOException {
        update(Client.TABLE, Client.ID.val(client.getId()),
                Client.ACTIVE.val(false));
    }

    /**
     * Enables the specified client by its database ID.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void enable(Client client) throws DAOException {
        update(Client.TABLE, Client.ID.val(client.getId()),
                Client.ACTIVE.val(true));
    }

    /**
     * Trusts the specified application by its database ID.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public void trust(Client client) throws DAOException {
        update(Client.TABLE, Client.ID.val(client.getId()),
                Client.TRUSTED.val(true));
    }

    /**
     * Distrusts the specified application by its database ID.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.0
     */
    public void distrust(Client client) throws DAOException {
        update(Client.TABLE, Client.ID.val(client.getId()),
                Client.TRUSTED.val(false));
    }

    /**
     * Updated the specified client by its database ID.
     *
     * @param client a {@link de.samply.auth.dao.dto.Client} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.RC4
     */
    public void updateClient(Client client) throws DAOException {
        update(Client.TABLE, Client.ID.val(client.getId()),
                Client.TRUSTED.val(client.getTrusted()),
                Client.NAME.val(client.getName()),
                Client.DESCRIPTION.val(client.getDescription()),
                Client.CLIENT_ID.val(client.getClientId()),
                Client.CLIENT_SECRET.val(client.getClientSecret()),
                Client.TYPE.val(client.getType()),
                Client.WHITELIST.val(client.getWhitelist()),
                Client.REDIRECT_URL.val(client.getRedirectUrl()),
                Client.BASIC_AUTHENTICATION.val(client.getBasicAuthentication()));
    }

    /** {@inheritDoc} */
    @Override
    public Client getObject(ResultSet set) throws SQLException {
        Client client = new Client();
        client.setId(set.getInt(Client.ID.alias));
        client.setName(set.getString(Client.NAME.alias));
        client.setDescription(set.getString(Client.DESCRIPTION.alias));
        client.setClientId(set.getString(Client.CLIENT_ID.alias));
        client.setClientSecret(set.getString(Client.CLIENT_SECRET.alias));
        client.setRedirectUrl(set.getString(Client.REDIRECT_URL.alias));
        client.setActive(set.getBoolean(Client.ACTIVE.alias));
        client.setTrusted(set.getBoolean(Client.TRUSTED.alias));
        client.setType(ClientType.valueOf(set.getString(Client.TYPE.alias)));
        client.setWhitelist(set.getBoolean(Client.WHITELIST.alias));
        client.setBasicAuthentication(set.getBoolean(Client.BASIC_AUTHENTICATION.alias));
        return client;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return Client.TABLE.table();
    }

    public static final String SQL_TABLE = Client.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Client.class);

}
