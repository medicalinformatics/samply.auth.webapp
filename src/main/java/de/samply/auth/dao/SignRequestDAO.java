/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.samply.auth.dao.dto.SignRequest;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for sign requests.
 *
 */
public class SignRequestDAO extends AbstractDAO<SignRequest> {

    SignRequestDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Deletes expired sign requests.
     *
     * @throws de.samply.sdao.DAOException if any.
     */
    public void cleanUp() throws DAOException {
        executeUpdate("DELETE FROM " + SignRequest.TABLE.table() + " WHERE " + SignRequest.EXPIRATION_DATE.column() + " < now()");
    }

    /**
     * Returns the sign request with the specified code.
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.SignRequest} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public SignRequest getSignRequest(String code) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + SignRequest.CODE.access() + " = ? AND "
                + SignRequest.EXPIRATION_DATE.access() + " > now()", code);
    }

    /**
     * Deletes the specified sign request by its ID.
     *
     * @param id a int.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void deleteSignRequest(int id) throws DAOException {
        delete(SignRequest.TABLE, SignRequest.ID.val(id));
    }

    /**
     * Saves a sign request in the database.
     *
     * @param request a {@link de.samply.auth.dao.dto.SignRequest} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveSignRequest(SignRequest request) throws DAOException {
        request.setId(insert(
                SignRequest.TABLE,
                SignRequest.KEY_ID.val(request.getKeyId()),
                SignRequest.CODE.val(request.getCode()),
                SignRequest.ALGORITHM.val(request.getAlgorithm()),
                SignRequest.EXPIRATION_DATE.val(request.getExpirationDate())));
    }

    /** {@inheritDoc} */
    @Override
    public SignRequest getObject(ResultSet set) throws SQLException {
        SignRequest request = new SignRequest();
        request.setKeyId(set.getInt(SignRequest.KEY_ID.alias));
        request.setExpirationDate(set.getTimestamp(SignRequest.EXPIRATION_DATE.alias));
        request.setCode(set.getString(SignRequest.CODE.alias));
        request.setId(set.getInt(SignRequest.ID.alias));
        request.setAlgorithm(set.getString(SignRequest.ALGORITHM.alias));
        return request;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return SignRequest.TABLE.table();
    }

    public static final String SQL_TABLE = SignRequest.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(SignRequest.class);

}
