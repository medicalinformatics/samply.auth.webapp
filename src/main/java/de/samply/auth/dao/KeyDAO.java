/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.auth.dao.dto.Key;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The data access object for public keys
 *
 */
public class KeyDAO extends AbstractDAO<Key> {

    KeyDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Returns the public key using its database ID.
     *
     * @param id a int.
     * @return a {@link de.samply.auth.dao.dto.Key} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Key getKey(int id) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Key.ID.access() + " = ?", id);
    }

    /**
     * <p>getKey.</p>
     *
     * @param hash a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.Key} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public Key getKey(String hash) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Key.SHA512.access() + " = ?", hash);
    }

    /**
     * <p>getUserKeys.</p>
     *
     * @param userId a int.
     * @return a {@link java.util.List} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public List<Key> getUserKeys(int userId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Key.USER_ID.access() + " = ?", userId);
    }

    /**
     * Saves the specified public key in the database.
     *
     * @param key a {@link de.samply.auth.dao.dto.Key} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveKey(Key key) throws DAOException {
        key.setId(insert(
                Key.TABLE,
                Key.USER_ID.val(key.getUserId()),
                Key.BASE64.val(key.getBase64EncodedKey()),
                Key.DESCRIPTION.val(key.getDescription()),
                Key.SHA512.val(key.getSha512Hash())));
    }

    /**
     * Deletes the specified key from the database.
     *
     * @param key a {@link de.samply.auth.dao.dto.Key} object.
     * @throws de.samply.sdao.DAOException if any.
     * @since 1.4.RC4
     */
    public void deleteKey(Key key) throws DAOException {
        delete(Key.TABLE, Key.ID.val(key.getId()));
    }

    /** {@inheritDoc} */
    @Override
    public Key getObject(ResultSet set) throws SQLException {
        Key key = new Key();
        key.setId(set.getInt(Key.ID.alias));
        key.setUserId(set.getInt(Key.USER_ID.alias));
        key.setBase64EncodedKey(set.getString(Key.BASE64.alias));
        key.setDescription(set.getString(Key.DESCRIPTION.alias));
        key.setSha512Hash(set.getString(Key.SHA512.alias));
        return key;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return Key.TABLE.table();
    }

    public static final String SQL_TABLE = Key.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Key.class);

}
