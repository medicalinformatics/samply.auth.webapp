/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.auth.dao.dto.AdfsProvider;
import de.samply.auth.dao.dto.IdentityProvider;
import de.samply.auth.dao.dto.LdapProvider;
import de.samply.auth.dao.dto.SqlProvider;
import de.samply.auth.dao.dto.ProviderType;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The DAO for external identity providers (LdapProvider, AdfsProvider or SqlProvider).
 */
public class ProviderDAO extends AbstractDAO<IdentityProvider> {

    /**
     * Returns the provider with the given ID.
     * @param providerId
     * @return
     * @throws DAOException
     */
    public IdentityProvider getProvider(int providerId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + IdentityProvider.ID.access() + " = ?", providerId);
    }

    /**
     * @param ip
     * @throws DAOException
     */
    public IdentityProvider getProvider(String ip) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + IdentityProvider.IDENTIFIER.access() + " = ?", ip);
    }

    /**
     * Returns all providers
     * @return
     * @throws DAOException
     */
    public List<IdentityProvider> getAllProviders() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Deletes the given identity provider. This method will fail, if the identity provider is still in use (for mappings, role permissions).
     * @param provider
     * @throws DAOException
     */
    public void deleteProvider(IdentityProvider provider) throws DAOException {
        delete(IdentityProvider.TABLE, IdentityProvider.ID.val(provider.getId()));
    }

    /**
     * Updates the given provider.
     * @param provider
     * @throws DAOException
     */
    public void updateProvider(IdentityProvider provider) throws DAOException {
        if(provider instanceof LdapProvider) {
            LdapProvider ldap = (LdapProvider) provider;
            update(IdentityProvider.TABLE, IdentityProvider.ID.val(provider.getId()),
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.LDAP),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(provider.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(provider.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(provider.getMemberAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(provider.getIdAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    LdapProvider.ANONYMOUS.val(ldap.isAnonymous()),
                    LdapProvider.BASE_DN.val(ldap.getBaseDN()),
                    LdapProvider.BIND_DN.val(ldap.getBindDN()),
                    LdapProvider.BIND_PASSWORD.val(ldap.getBindPassword()),
                    LdapProvider.SEARCH_BASE.val(ldap.getSearchBase()),
                    LdapProvider.SEARCH_FILTER.val(ldap.getSearchFilter()));
        } else if(provider instanceof AdfsProvider) {
            AdfsProvider adfs = (AdfsProvider) provider;
            update(IdentityProvider.TABLE, IdentityProvider.ID.val(provider.getId()),
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.ADFS),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(provider.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(provider.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(provider.getMemberAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(provider.getIdAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    AdfsProvider.PUBLIC_KEY.val(adfs.getPublicKey()),
                    AdfsProvider.CLIENT_ID.val(adfs.getClientId()),
                    AdfsProvider.RESOURCE.val(adfs.getResource()));
        } else if(provider instanceof SqlProvider) {
            SqlProvider sqlP = (SqlProvider) provider;
            update(IdentityProvider.TABLE, IdentityProvider.ID.val(provider.getId()),
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.SQL),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(provider.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(provider.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(provider.getMemberAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(provider.getIdAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    SqlProvider.SQL_DRIVER.val(sqlP.getSqlDriver()),
                    SqlProvider.BIND_DN.val(sqlP.getBindDN()),
                    SqlProvider.BIND_PASSWORD.val(sqlP.getBindPassword()),
                    SqlProvider.SEARCH_QUERY.val(sqlP.getSearchQuery()));
        }
    }

    /**
     * Saves the given provider.
     * @param provider
     * @throws DAOException
     */
    public void saveProvider(IdentityProvider provider) throws DAOException {
        if(provider instanceof LdapProvider) {
            LdapProvider ldap = (LdapProvider) provider;
            provider.setId(insert(
                    IdentityProvider.TABLE,
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.LDAP),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(ldap.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(ldap.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(ldap.getIdAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(ldap.getMemberAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    LdapProvider.SEARCH_BASE.val(ldap.getSearchBase()),
                    LdapProvider.BASE_DN.val(ldap.getBaseDN()),
                    LdapProvider.ANONYMOUS.val(ldap.isAnonymous()),
                    LdapProvider.SEARCH_FILTER.val(ldap.getSearchFilter()),
                    LdapProvider.BIND_DN.val(ldap.getBindDN()),
                    LdapProvider.BIND_PASSWORD.val(ldap.getBindPassword())));
        } else if(provider instanceof AdfsProvider) {
            AdfsProvider adfs = (AdfsProvider) provider;
            provider.setId(insert(
                    IdentityProvider.TABLE,
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.ADFS),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(adfs.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(adfs.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(adfs.getIdAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(adfs.getMemberAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    AdfsProvider.PUBLIC_KEY.val(adfs.getPublicKey()),
                    AdfsProvider.RESOURCE.val(adfs.getResource()),
                    AdfsProvider.CLIENT_ID.val(adfs.getClientId())));
        } else if(provider instanceof SqlProvider) {
            SqlProvider sqlP = (SqlProvider) provider;
            provider.setId(insert(
                    IdentityProvider.TABLE,
                    IdentityProvider.LABEL.val(provider.getLabel()),
                    IdentityProvider.URL.val(provider.getUrl()),
                    IdentityProvider.TYPE.val(ProviderType.SQL),
                    IdentityProvider.EMAIL_ATTRIBUTE.val(provider.getEmailAttribute()),
                    IdentityProvider.NAME_ATTRIBUTE.val(provider.getNameAttribute()),
                    IdentityProvider.FIRST_NAME_ATTRIBUTE.val(provider.getFirstNameAttribute()),
                    IdentityProvider.LAST_NAME_ATTRIBUTE.val(provider.getLastNameAttribute()),
                    IdentityProvider.MEMBER_ATTRIBUTE.val(provider.getMemberAttribute()),
                    IdentityProvider.ID_ATTRIBUTE.val(provider.getIdAttribute()),
                    IdentityProvider.IDENTIFIER.val(provider.getIdentifier()),
                    IdentityProvider.PW_LINK.val(provider.getPasswordRecoveryLink()),
                    IdentityProvider.DEFAULT_USER_ENABLED.val(provider.isDefaultUserEnabled()),
                    IdentityProvider.DEFAULT_USER_VERIFIED.val(provider.isDefaultUserVerified()),
                    IdentityProvider.ACTIVE.val(provider.isActive()),
                    SqlProvider.SQL_DRIVER.val(sqlP.getSqlDriver()),
                    SqlProvider.BIND_DN.val(sqlP.getBindDN()),
                    SqlProvider.BIND_PASSWORD.val(sqlP.getBindPassword()),
                    SqlProvider.SEARCH_QUERY.val(sqlP.getSearchQuery())));
        } else {
            throw new UnsupportedOperationException("Trying to store a different kind of provider!");
        }
    }

    protected ProviderDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public IdentityProvider getObject(ResultSet set) throws SQLException {
        ProviderType type = ProviderType.valueOf(set.getString(IdentityProvider.TYPE.alias));

        IdentityProvider provider = null;

        if(type == ProviderType.ADFS) {
            AdfsProvider adfs = new AdfsProvider();
            adfs.setPublicKey(set.getString(AdfsProvider.PUBLIC_KEY.alias));
            adfs.setResource(set.getString(AdfsProvider.RESOURCE.alias));
            adfs.setClientId(set.getString(AdfsProvider.CLIENT_ID.alias));
            provider = adfs;
        } else if(type == ProviderType.LDAP) {
            LdapProvider ldap = new LdapProvider();
            ldap.setSearchBase(set.getString(LdapProvider.SEARCH_BASE.alias));
            ldap.setBaseDN(set.getString(LdapProvider.BASE_DN.alias));
            ldap.setAnonymous(set.getBoolean(LdapProvider.ANONYMOUS.alias));
            ldap.setSearchFilter(set.getString(LdapProvider.SEARCH_FILTER.alias));
            ldap.setBindDN(set.getString(LdapProvider.BIND_DN.alias));
            ldap.setBindPassword(set.getString(LdapProvider.BIND_PASSWORD.alias));
            provider = ldap;
        } else if(type == ProviderType.SQL) {
            SqlProvider sqlP = new SqlProvider();
            sqlP.setSqlDriver(set.getString(SqlProvider.SQL_DRIVER.alias));
            sqlP.setBindDN(set.getString(SqlProvider.BIND_DN.alias));
            sqlP.setBindPassword(set.getString(SqlProvider.BIND_PASSWORD.alias));
            sqlP.setSearchQuery(set.getString(SqlProvider.SEARCH_QUERY.alias));
            provider = sqlP;
        } else {
            throw new UnsupportedOperationException("Unknown provider type: " + type.toString());
        }

        provider.setMemberAttribute(set.getString(IdentityProvider.MEMBER_ATTRIBUTE.alias));
        provider.setNameAttribute(set.getString(IdentityProvider.NAME_ATTRIBUTE.alias));
        provider.setFirstNameAttribute(set.getString(IdentityProvider.FIRST_NAME_ATTRIBUTE.alias));
        provider.setLastNameAttribute(set.getString(IdentityProvider.LAST_NAME_ATTRIBUTE.alias));
        provider.setIdAttribute(set.getString(IdentityProvider.ID_ATTRIBUTE.alias));
        provider.setEmailAttribute(set.getString(IdentityProvider.EMAIL_ATTRIBUTE.alias));
        provider.setLabel(set.getString(IdentityProvider.LABEL.alias));
        provider.setUrl(set.getString(IdentityProvider.URL.alias));
        provider.setId(set.getInt(IdentityProvider.ID.alias));
        provider.setIdentifier(set.getString(IdentityProvider.IDENTIFIER.alias));
        provider.setPasswordRecoveryLink(set.getString(IdentityProvider.PW_LINK.alias));
        provider.setDefaultUserEnabled(set.getBoolean(IdentityProvider.DEFAULT_USER_ENABLED.alias));
        provider.setDefaultUserVerified(set.getBoolean(IdentityProvider.DEFAULT_USER_VERIFIED.alias));
        provider.setActive(set.getBoolean(IdentityProvider.ACTIVE.alias));

        return provider;
    }

    @Override
    protected String getInsertTable() {
        return IdentityProvider.TABLE.table();
    }

    public static final String SQL_TABLE = IdentityProvider.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(IdentityProvider.class, AdfsProvider.class,
            LdapProvider.class, SqlProvider.class);


}
