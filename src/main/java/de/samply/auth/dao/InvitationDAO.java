/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.PreselectMode;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.auth.dao.dto.*;
import de.samply.auth.dao.dto.Misc.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.List;
import java.util.ArrayList;

/**
 * DAO for invitations
 */
public class InvitationDAO extends AbstractDAO<Invitation> {

    /**
     * Returns all members for the given role.
     * @return
     * @throws DAOException
     */
    public List<Invitation> getInvitations() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Returns all members for the given role.
     * @param role
     * @return
     * @throws DAOException
     */
    public List<Invitation> getInvitedMembers(Role role) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Invitation.ROLE_ID.access() + " = ?", role.getId());
    }

    public Invitation getInvitation(String uuid) throws DAOException {
        if(uuid == null) {
            return null;
        }

        try {
            /**
             * Check if the UUID is valid. If we don't and the UUID is not valid, PostgreSQL will throw
             * an error.
             */
            UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return executeSingleSelect(SQL_SELECT + "WHERE " + Invitation.UUID.access() + " = ?::uuid", uuid);
    }

    public void saveInvite(Invitation invitation) throws DAOException {
        insert(Invitation.TABLE,
                Invitation.EMAIL.val(invitation.getEmail()),
                Invitation.NAME.val(invitation.getName()),
                Invitation.IDENTITY_DATA.val(invitation.getIdentityData()),
                Invitation.ROLE_ID.val(invitation.getRoleId()),
                Invitation.TARGET.val(invitation.getTarget()),
                Invitation.USED.val(invitation.isUsed()),
                Invitation.UUID.val(invitation.getUuid()),
                Invitation.PRESELECT_MODE.val(invitation.getPreselectMode()),
                Invitation.PROVIDER_ID.val(invitation.getProviderId() == 0 ? null : invitation.getProviderId()),
                Invitation.USED_BY_USER_ID.val(invitation.getUsedByUserId() == 0 ? null : invitation.getUsedByUserId()));
    }

    /**
     * Updates invitation list for the given role.
     * @param invitation
     * @return
     * @throws DAOException
     */
    public void updateInvitation(Invitation invitation) throws DAOException {
        update(Invitation.TABLE, Invitation.ID.val(invitation.getId()),
                Invitation.EMAIL.val(invitation.getEmail()),
                Invitation.NAME.val(invitation.getName()),
                Invitation.IDENTITY_DATA.val(invitation.getIdentityData()),
                Invitation.ROLE_ID.val(invitation.getRoleId()),
                Invitation.TARGET.val(invitation.getTarget()),
                Invitation.USED.val(invitation.isUsed()),
                Invitation.UUID.val(invitation.getUuid()),
                Invitation.PRESELECT_MODE.val(invitation.getPreselectMode()),
                Invitation.PROVIDER_ID.val(invitation.getProviderId() == 0 ? null : invitation.getProviderId()),
                Invitation.USED_BY_USER_ID.val(invitation.getUsedByUserId() == 0 ? null : invitation.getUsedByUserId()));
    }

    /**
     * Updates invitation list for the given role.
     * @param role
     * @param invitations
     * @return
     * @throws DAOException
     */
    public void updateInvitations(Role role, List<Invitation> invitations) throws DAOException {
        /**
         * Get list of valid invitations and delete the rest of invitations from DB
         */
        List<UUID> invitationIds = new ArrayList<>();
        for (Invitation invitation : invitations) {
            invitationIds.add(invitation.getUuid());
        }
        List<Invitation> dbInvitations = executeSelect(SQL_SELECT + "WHERE " + Invitation.ROLE_ID.access() + " = ?", role.getId());
        for (Invitation dbInvitation : dbInvitations) {
            if (!invitationIds.contains(dbInvitation.getUuid())) {
                delete(Invitation.TABLE, Invitation.UUID.val(dbInvitation.getUuid()));
            }
        }
    }

    public void markAsUsed(Invitation invitation) throws DAOException {
        update(Invitation.TABLE, Invitation.ID.val(invitation.getId()),
                Invitation.USED.val(true),
                Invitation.USED_BY_USER_ID.val(invitation.getUsedByUserId() == 0 ? null : invitation.getUsedByUserId()));
    }

    protected InvitationDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public Invitation getObject(ResultSet set) throws SQLException {
        Invitation inv = new Invitation();
        inv.setId(set.getInt(Invitation.ID.alias));
        inv.setName(set.getString(Invitation.NAME.alias));
        inv.setIdentityData(asJson(set.getString(Invitation.IDENTITY_DATA.alias)));
        inv.setEmail(set.getString(Invitation.EMAIL.alias));
        inv.setTarget(set.getString(Invitation.TARGET.alias));
        inv.setUsed(set.getBoolean(Invitation.USED.alias));
        inv.setUuid(UUID.fromString(set.getString(Invitation.UUID.alias)));
        inv.setRoleId(set.getInt(Invitation.ROLE_ID.alias));
        inv.setPreselectMode(PreselectMode.valueOf(set.getString(Invitation.PRESELECT_MODE.alias)));
        inv.setProviderId(set.getInt(Invitation.PROVIDER_ID.alias));
        inv.setUsedByUserId(set.getInt(Invitation.USED_BY_USER_ID.alias));
        return inv;
    }

    private static final String SQL_SELECT = "SELECT " + getSelectFields(Invitation.class) + " FROM " + Invitation.TABLE.from() + " ";

    public static final String SQL_TABLE = Invitation.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Invitation.class);
}
