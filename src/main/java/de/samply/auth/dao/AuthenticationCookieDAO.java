/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao;

import de.samply.auth.dao.dto.AuthenticationCookie;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Handles the access to authentication cookies.
 */
public class AuthenticationCookieDAO extends AbstractDAO<AuthenticationCookie> {

    protected AuthenticationCookieDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Saves the given authentication cookie in the database.
     * @param cookie
     * @return
     * @throws DAOException
     */
    public int save(AuthenticationCookie cookie) throws DAOException {
        cookie.setId(insert(AuthenticationCookie.TABLE,
                AuthenticationCookie.EXPIRATION_DATE.val(cookie.getExpirationDate()),
                AuthenticationCookie.HASH.val(cookie.getHash()),
                AuthenticationCookie.USER_ID.val(cookie.getUserId()),
                AuthenticationCookie.UUID.val(cookie.getUuid())));
        return cookie.getId();
    }

    /**
     * Deletes expired authentication cookies.
     *
     * @throws de.samply.sdao.DAOException if any.
     */
    public void cleanUp() throws DAOException {
        executeUpdate("DELETE FROM " + AuthenticationCookie.TABLE.table() + " WHERE " + AuthenticationCookie.EXPIRATION_DATE.column() + " < now()");
    }

    /**
     * Returns the authentication cookie with the given uuid and hash
     * @param uuid
     * @param hash
     * @return
     * @throws DAOException
     */
    public AuthenticationCookie get(String uuid, String hash) throws DAOException {
        return executeSingleSelect(SQL_SELECT + " WHERE " + AuthenticationCookie.UUID.access() + " = ?::uuid AND " +
                AuthenticationCookie.HASH.access() + " = ?", uuid, hash);
    }

    /**
     * Removes the given authentication cookie.
     * @param cookie
     * @throws DAOException
     */
    public void remove(AuthenticationCookie cookie) throws DAOException {
        delete(AuthenticationCookie.TABLE,
                AuthenticationCookie.ID.val(cookie.getId()));
    }

    @Override
    public AuthenticationCookie getObject(ResultSet set) throws SQLException {
        AuthenticationCookie cookie = new AuthenticationCookie();
        cookie.setId(set.getInt(AuthenticationCookie.ID.alias));
        cookie.setUuid(UUID.fromString(set.getString(AuthenticationCookie.UUID.alias)));
        cookie.setHash(set.getString(AuthenticationCookie.HASH.alias));
        cookie.setUserId(set.getInt(AuthenticationCookie.USER_ID.alias));
        cookie.setExpirationDate(set.getTimestamp(AuthenticationCookie.EXPIRATION_DATE.alias));
        return cookie;
    }

    private final String SQL_SELECT = "SELECT " + getSelectFields(AuthenticationCookie.class)
            + " FROM " + AuthenticationCookie.TABLE.from();

}
