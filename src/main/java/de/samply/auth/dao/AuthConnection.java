/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.sdao.ConnectionProvider;

/**
 * A connection to the SQL database. Manages the DAOs and the SQL
 * connection used by those DAOs.
 *
 */
public class AuthConnection extends ConnectionProvider implements AutoCloseable {

    /** Defines the required database version for this Connection. */
    public static final int requiredVersion = 43;

    /**
     * <p>Constructor for AuthConnection.</p>
     *
     * @param host a {@link java.lang.String} object.
     * @param database a {@link java.lang.String} object.
     * @param username a {@link java.lang.String} object.
     * @param password a {@link java.lang.String} object.
     */
    public AuthConnection(String host, String database, String username, String password) {
        super(host, database, username, password, 0);
    }

    /** {@inheritDoc} */
    @Override
    public int getRequiredVersion() {
        return requiredVersion;
    }

}
