/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.samply.auth.dao.dto.AccessToken;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO for the access token used in OAuth2.
 *
 */
public class AccessTokenDAO extends AbstractDAO<AccessToken> {

    AccessTokenDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Saves the access token in the database.
     *
     * @param token a {@link de.samply.auth.dao.dto.AccessToken} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void saveAccessToken(AccessToken token) throws DAOException {
        token.setId(insert(
                AccessToken.TABLE,
                AccessToken.ACCESS_TOKEN.val(token.getAccessToken()),
                AccessToken.USER_ID.val(token.getUserId()),
                AccessToken.EXPIRATION_DATE.val(token.getExpirationDate())));
    }

    /**
     * Returns the access token with the specified token.
     *
     * @param token a {@link java.lang.String} object.
     * @return a {@link de.samply.auth.dao.dto.AccessToken} object.
     * @throws de.samply.sdao.DAOException if any.
     */
    public AccessToken getAccessToken(String token) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + AccessToken.ACCESS_TOKEN.access() + " = ?", token);
    }

    /**
     * Deletes expired access tokens.
     *
     * @throws de.samply.sdao.DAOException if any.
     */
    public void cleanUp() throws DAOException {
        executeUpdate("DELETE FROM " + AccessToken.TABLE.from() + " WHERE " + AccessToken.EXPIRATION_DATE.access() + " < now()");
    }

    /** {@inheritDoc} */
    @Override
    public AccessToken getObject(ResultSet set) throws SQLException {
        AccessToken token = new AccessToken();
        token.setId(set.getInt(AccessToken.ID.alias));
        token.setAccessToken(set.getString(AccessToken.ACCESS_TOKEN.alias));
        token.setUserId(set.getInt(AccessToken.USER_ID.alias));
        return token;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return AccessToken.TABLE.table();
    }

    public static final String SQL_TABLE = AccessToken.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(AccessToken.class);

}
