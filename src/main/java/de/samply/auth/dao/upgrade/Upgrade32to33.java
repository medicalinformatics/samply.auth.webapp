/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao.upgrade;

import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.UpgradeExecutor;
import de.samply.sdao.json.JSONResource;


/**
 * Upgrades the database from version 32 to version 33.
 *
 * Adds a new table for authentication cookies.
 *
 */
public class Upgrade32to33 extends UpgradeExecutor {

    private Upgrade32to33(ConnectionProvider provider) {
        super(provider, "/sql/upgrade/upgrade32to33.sql");
    }

    /** {@inheritDoc}
     * @throws DAOException */
    @Override
    public boolean executeUpgrade(boolean dryRun) throws DAOException {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public JSONResource getDefaultConfig() {
        JSONResource config = new JSONResource();
        config.setProperty(Vocabulary.DB_VERSION, 33);
        config.setProperty(Vocabulary.REGISTRATION_ENABLED, true);
        config.setProperty(Vocabulary.REST_REGISTRATION_ENABLED, true);
        config.setProperty(Vocabulary.DEFAULT_USER_ENABLED, true);
        config.setProperty(Vocabulary.VERIFY_EMAIL_ENABLED, true);
        config.setProperty(Vocabulary.VERIFICATION_REQUIRED, true);

        JSONResource policy = new JSONResource();
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_LENGTH, 10);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_LOWER_CASE, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_NUMBERS, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_SPECIAL_CHARACTERS, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_UPPER_CASE, 0);

        config.setProperty(Vocabulary.PASSWORD_POLICY, policy);

        JSONResource shibboleth = new JSONResource();
        shibboleth.setProperty(Vocabulary.Shibboleth.ACTIVE, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.IDENTITY_ATTRIBUTE, "persistent-id");
        shibboleth.setProperty(Vocabulary.Shibboleth.MAIL_ATTRIBUTE, "mail");
        shibboleth.setProperty(Vocabulary.Shibboleth.NAME_ATTRIBUTE, "cn");
        shibboleth.setProperty(Vocabulary.Shibboleth.DEFAULT_ACTIVE, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.DEFAULT_VERIFIED, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.NAME, "Shibboleth");
        shibboleth.setProperty(Vocabulary.Shibboleth.USE_HTTP_HEADERS, true);

        config.setProperty(Vocabulary.CONTACT_INFO, "Not available");
        config.setProperty(Vocabulary.EULA_ENABLED, false);
        config.setProperty(Vocabulary.EXTERNAL_AUTHORIZATION, false);

        config.setProperty(Vocabulary.SHIBBOLETH, shibboleth);

        return config;
    }

}
