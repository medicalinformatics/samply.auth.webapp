/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.auth.dao.upgrade;

import de.samply.auth.AuthConfig;
import de.samply.auth.NameUtils;
import de.samply.auth.dao.InvitationDAO;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.Vocabulary;
import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.User;
import de.samply.auth.dao.dto.Usertype;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.UpgradeExecutor;
import de.samply.sdao.json.JSONResource;

import java.sql.SQLException;


/**
 * Upgrades the database from version 42 to version 43.
 *
 * Adds two fields to data in samplyUser: firstname and lastname (only normal users)
 * Adds identityData to invitation and shibbolethVerification
 * Migration name -> first name/ last name for samplyUser and invitation
 * Migration for shibbolethVerification won't be done automatically,
 * because of short time of shibboleth verification link validity (2 hours)
 * Update Config for Shibboleth IdP
  */
public class Upgrade42to43 extends UpgradeExecutor {

    private Upgrade42to43(ConnectionProvider provider) {
        super(provider, "/sql/upgrade/upgrade42to43.sql");
    }

    /** {@inheritDoc}
     * @throws DAOException */
    @Override
    public boolean executeUpgrade(boolean dryRun) throws DAOException {
        // Split user name into firstname / lastname (table samplyUser)
        for(User user : provider.get(UserDAO.class).getUsers()) {
            if (user.getUsertype() == Usertype.NORMAL) {
                String name = user.getName().trim();
                if (!name.equals("")) {
                    user.setFirstName(NameUtils.getFirstName(name));
                    user.setLastName(NameUtils.getLastName(name));
                    provider.get(UserDAO.class).updateData(user);
                }
            }
        }

        // Split user name into firstname / lastname (table invitation)
        for(Invitation invitedMember : provider.get(InvitationDAO.class).getInvitations()) {
            String name = invitedMember.getName().trim();
            if (!name.equals("")) {
                invitedMember.setFirstName(NameUtils.getFirstName(name));
                invitedMember.setLastName(NameUtils.getLastName(name));
                provider.get(InvitationDAO.class).updateInvitation(invitedMember);
            }
        }

        // Update Config for Shibboleth IdP
        JSONResource config = provider.getConfig();
        config.getProperty(Vocabulary.SHIBBOLETH).asJSONResource().setProperty(Vocabulary.Shibboleth.FIRST_NAME_ATTRIBUTE, "givenName");
        config.getProperty(Vocabulary.SHIBBOLETH).asJSONResource().setProperty(Vocabulary.Shibboleth.LAST_NAME_ATTRIBUTE, "sn");
        provider.saveConfig(config);

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public JSONResource getDefaultConfig() {
        JSONResource config = new JSONResource();
        config.setProperty(Vocabulary.DB_VERSION, 43);
        config.setProperty(Vocabulary.REGISTRATION_ENABLED, true);
        config.setProperty(Vocabulary.REST_REGISTRATION_ENABLED, true);
        config.setProperty(Vocabulary.DEFAULT_USER_ENABLED, true);
        config.setProperty(Vocabulary.VERIFY_EMAIL_ENABLED, true);
        config.setProperty(Vocabulary.VERIFICATION_REQUIRED, true);
        config.setProperty(Vocabulary.LOCAL_ACCOUNTS_ENABLED, true);
        config.setProperty(Vocabulary.CLIENT_CERTIFICATE_ENABLED, false);
        config.setProperty(Vocabulary.HIDE_CLIENT_CERTIFICATE_BUTTON, true);

        JSONResource policy = new JSONResource();
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_LENGTH, 10);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_LOWER_CASE, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_NUMBERS, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_SPECIAL_CHARACTERS, 0);
        policy.setProperty(Vocabulary.PasswordPolicy.MIN_UPPER_CASE, 0);

        config.setProperty(Vocabulary.PASSWORD_POLICY, policy);

        JSONResource shibboleth = new JSONResource();
        shibboleth.setProperty(Vocabulary.Shibboleth.ACTIVE, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.IDENTITY_ATTRIBUTE, "persistent-id");
        shibboleth.setProperty(Vocabulary.Shibboleth.MAIL_ATTRIBUTE, "mail");
        shibboleth.setProperty(Vocabulary.Shibboleth.NAME_ATTRIBUTE, "cn");
        shibboleth.setProperty(Vocabulary.Shibboleth.FIRST_NAME_ATTRIBUTE, "givenName");
        shibboleth.setProperty(Vocabulary.Shibboleth.LAST_NAME_ATTRIBUTE, "sn");
        shibboleth.setProperty(Vocabulary.Shibboleth.DEFAULT_ACTIVE, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.DEFAULT_VERIFIED, false);
        shibboleth.setProperty(Vocabulary.Shibboleth.NAME, "Shibboleth");
        shibboleth.setProperty(Vocabulary.Shibboleth.USE_HTTP_HEADERS, true);

        config.setProperty(Vocabulary.CONTACT_INFO, "Not available");
        config.setProperty(Vocabulary.EULA_ENABLED, false);
        config.setProperty(Vocabulary.EXTERNAL_AUTHORIZATION, false);

        config.setProperty(Vocabulary.SHIBBOLETH, shibboleth);

        return config;
    }

}
