/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

import de.samply.auth.dao.dto.Misc.Remember;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The DAO used to check if the user trusts an OAuth2 client.
 *
 */
public class RememberDAO extends AbstractDAO<Boolean> {

    /**
     * Checks if the specified user trusts the specified client.
     *
     * @param userId a int.
     * @param clientId a int.
     * @return a boolean.
     * @throws de.samply.sdao.DAOException if any.
     */
    public boolean remembers(int userId, int clientId) throws DAOException {
        return executeSingleSelect("SELECT true FROM " + Remember.TABLE.from() + " WHERE "
                + Remember.USER_ID.access() + " = ? AND " + Remember.CLIENT_ID.access() + " = ?", userId, clientId) == null ? false : true;
    }

    /**
     * Saves the information, that the specified client trusts the specified client.
     *
     * @param userId a int.
     * @param clientId a int.
     * @throws de.samply.sdao.DAOException if any.
     */
    public void remember(int userId, int clientId) throws DAOException {
        insert(
                Remember.TABLE,
                Remember.USER_ID.val(userId),
                Remember.CLIENT_ID.val(clientId));
    }

    RememberDAO(ConnectionProvider connection) {
        super(connection);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean getObject(ResultSet set) throws SQLException {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    protected String getInsertTable() {
        return Remember.TABLE.table();
    }

}
