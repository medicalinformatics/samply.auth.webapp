/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.dao;

/**
 * Properties for the configuration object.
 *
 * @since 1.4.0
 */
public class Vocabulary {

    /** Constant <code>DB_VERSION="dbVersion"</code> */
    public static final String DB_VERSION = "dbVersion";

    /**
     * Properties for the Configuration
     */
    public static final String REGISTRATION_ENABLED = "registration";
    public static final String REST_REGISTRATION_ENABLED = "restRegistration";
    public static final String DEFAULT_USER_ENABLED = "defaultUser";
    public static final String VERIFY_EMAIL_ENABLED = "verifyEmail";
    public static final String VERIFICATION_REQUIRED = "verificationRequired";
    public static final String PASSWORD_POLICY = "passwordPolicy";
    public static final String LOCAL_ACCOUNTS_ENABLED = "localAccountsEnabled";

    public static final String SHIBBOLETH = "shibboleth";

    public static class Shibboleth {
        public static final String ACTIVE  = "active";
        public static final String IDENTITY_ATTRIBUTE = "identityAttribute";
        public static final String NAME_ATTRIBUTE = "nameAttribute";
        public static final String FIRST_NAME_ATTRIBUTE = "firstNameAttribute";
        public static final String LAST_NAME_ATTRIBUTE = "lastNameAttribute";
        public static final String MAIL_ATTRIBUTE = "mailAttribute";
        public static final String DEFAULT_ACTIVE = "defaultActive";
        public static final String DEFAULT_VERIFIED = "defaultVerified";
        public static final String USE_HTTP_HEADERS = "useHttpHeaders";

        public static final String NAME = "name";
    }

    public static final String SCOPE = "scope";
    public static final String STATE = "state";
    public static final String NONCE = "nonce";
    public static final String CONTACT_INFO = "contactInformation";

    public static final String EXTERNAL_AUTHORIZATION = "externalAuthorization";
    public static final String EULA_ENABLED = "eulaEnabled";
    public static final String CLIENT_CERTIFICATE_ENABLED = "clientCertificate";
    public static final String HIDE_CLIENT_CERTIFICATE_BUTTON = "hideClientCertificateButton";

    public static final String IMAGE_DATA = "imageData";

    public static final String LOGO = "logo";

    public static final String EULA = "eula";

    public static class PasswordPolicy {
        public static final String MIN_LENGTH = "minLength";
        public static final String MIN_SPECIAL_CHARACTERS = "minSpecialCharacters";
        public static final String MIN_NUMBERS = "minNumbers";
        public static final String MIN_LOWER_CASE = "minLowerCase";
        public static final String MIN_UPPER_CASE = "minUpperCase";
    }

    public static final String ADMIN = "admin";
    public static final String EMAIL = "email";

    /**
     * Legacy Fields, important for old upgrades, but shouldn't be used beyond DB version 26.
     */
    public static final String MTA_SERVER = "mtaServer";
    public static final String EMAIL_FROM = "emailFrom";

}
