/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import java.io.File;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.samply.auth.dao.*;
import de.samply.config.util.FileFinderUtil;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * The Servlet listener that reads the config files and starts the clean up task,
 * that deletes expired data from the database.
 *
 */
@WebListener
public class ServletListener implements ServletContextListener {

    private static final Logger logger = LoggerFactory.getLogger(ServletListener.class);

    private Timer timer;

    /**
     * <p>Constructor for ServletListener.</p>
     */
    public ServletListener() {
    }

    /** {@inheritDoc} */
    @Override
    public void contextInitialized(ServletContextEvent event)  {
        /**
         * This is the first method that is called. It should
         * read the configuration files, load drivers, and start
         * the cleanup thread.
         */

        try {
            logger.info("Context initialized");

            ServletContext context = event.getServletContext();

            String projectName = context.getInitParameter("projectName");

            /**
             * If the project name is not set, get the default and use that.
             */
            if(StringUtil.isEmpty(projectName)) {
                projectName = AuthConfig.getProjectName();
            }

            String fallback = context.getRealPath("/WEB-INF");

            File logConfigFile = FileFinderUtil.findFile("log4j2.xml", projectName, fallback);
            Configurator.initialize(null, logConfigFile.getAbsolutePath());

            logger.info("Log4j2 configuration file: " + logConfigFile.getAbsolutePath());

            logger.info("Registering PostgreSQL driver");
            Class.forName("org.postgresql.Driver").newInstance();

            AuthConfig.initialize(projectName, fallback);

            logger.info("Starting cleanup task");
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try(AuthConnection auth = ConnectionFactory.get()) {
                        auth.get(RequestDAO.class).cleanUp();
                        auth.get(AccessTokenDAO.class).cleanUp();
                        auth.get(SignRequestDAO.class).cleanUp();
                        auth.get(AuthenticationCookieDAO.class).cleanUp();
                        auth.get(ShibbolethVerificationDAO.class).cleanup();
                        auth.commit();
                    } catch (DAOException e) {
                        e.printStackTrace();
                    }
                }
            };

            timer = new Timer();

            /**
             * Start the timer to execute every hour.
             */
            timer.scheduleAtFixedRate(task, 5000, 1000 * 60 * 60);

            logger.info("Cleanup task started. Listener finished.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     *
     * Cancel the task and unload the jdbc drivers
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent event)  {
        logger.debug("Undeploying application");
        if(timer != null) {
            timer.cancel();
            logger.debug("Canceling cleanup timer");
        }

        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logger.info("Unregistering driver " + driver.toString());
            } catch (SQLException e) {
                logger.debug("Exception deregistering driver", e);
            }
        }

        logger.info("Context destroyed.");
    }

}
