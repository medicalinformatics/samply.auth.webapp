/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.User;
import de.samply.auth.dao.dto.Usertype;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

import java.util.List;

/**
 * Simple utility class to get "user suggestions" from the database.
 */
public class AuthUtil {

    /**
     * Returns a ';|;' separated list of users for the given input text
     * @param input
     * @return
     */
    public static String getSuggestions(String input) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            return getSuggestions(input, auth, null);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Returns a ';|;' separated list of users for the given input text
     * @param input
     * @return
     */
    public static String getSuggestions(String input, Usertype userType) {
        try(AuthConnection auth = ConnectionFactory.get()) {
            return getSuggestions(input, auth, userType);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Returns a ';|;' separated list of users for the given input text
     * @param input
     * @return
     */
    public static String getSuggestions(String input, AuthConnection auth, Usertype userType) throws DAOException {
        List<User> users;
        if (userType == null) {
            users = auth.get(UserDAO.class).findUsers(input);
        } else {
            users = auth.get(UserDAO.class).findUsers(input, userType);
        }

        return StringUtil.join(users, ";|;", new StringUtil.Builder<User>() {
            @Override
            public String build(User o) {
                return o.getName() + " <" + o.getEmail() + "> [" + o.getId() + "]" +
                        " (" + (AuthConfig.getProviderLabel(o)) + ")";
            }
        });
    }

}
