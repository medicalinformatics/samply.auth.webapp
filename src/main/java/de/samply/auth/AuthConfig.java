/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.ProviderDAO;
import de.samply.auth.dao.Vocabulary;
import de.samply.auth.dao.dto.AdfsProvider;
import de.samply.auth.dao.dto.IdentityProvider;
import de.samply.auth.dao.dto.LdapProvider;
import de.samply.auth.dao.dto.SqlProvider;
import de.samply.auth.dao.dto.User;
import de.samply.common.config.OAuth2Provider;
import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.common.mailing.MailSending;
import de.samply.config.util.JAXBUtil;
import de.samply.sdao.ConfigDAO;
import de.samply.sdao.DAOException;
import de.samply.sdao.Upgrader;
import de.samply.sdao.json.JSONResource;

/**
 * The static configuration singleton.
 *
 */
public class AuthConfig {

    /**
     * The default number of rounds used to hash passwords.
     */
    public static final int DEFAULT_ROUNDS = 100000;

    /**
     * The OAuth2 configuration file name
     */
    public static final String OAUTH2_FILE = "auth.oauth2.xml";

    /**
     * The Postgres configuration file name
     */
    public static final String POSTGRES_FILE = "auth.postgres.xml";

    /**
     * The Mail configuration file name
     */
    public static final String MAIL_FILE = "auth.mail.xml";

    /**
     * The project name. Used for loading the configuration files. Can be changed through context paramters loaded in the
     * servlet listener at startup.
     */
    private String projectName = "samply";

    /**
     * Logger for debug output.
     */
    private static final Logger logger = LoggerFactory.getLogger(AuthConfig.class);

    /**
     * The singleton instance.
     */
    private final static AuthConfig instance = new AuthConfig();

    /**
     * private constructor
     */
    private AuthConfig() {
    }

    /**
     * The fallback folder for the FileFinderUtil.
     */
    private String fallbackFolder = null;

    /**
     * The OAuth2 public key, used to verify OAuth2 REST requests
     */
    private PublicKey publicKey;

    /**
     * The key used to sign the access tokens. Corresponding private key for the public key above.
     */
    private PrivateKey privateKey;

    /**
     * The base issuer URL, without trailing slash.
     */
    private String issuer;

    /**
     * The database configuration
     */
    private String host, database, username, password;

    /**
     * If true the registration process in the web interface is enabled
     */
    private Boolean registrationEnabled = true;

    /**
     * If true the registration process in the REST interface is enabled
     */
    private Boolean restRegistrationEnabled = true;

    /**
     * If true the newly registered user is "enabled" (= can login)
     */
    private Boolean defaultUserEnabled = false;

    /**
     * If true the user gets a verification email right after his registration
     */
    private Boolean verifyEmailEnabled = false;

    /**
     * If true the user *must* verify his email after the registration process
     */
    private Boolean verificationRequired = false;

    /**
     * The current database version
     */
    private Integer dbVersion = 1;

    /**
     * If true the server is in maintenance mode:
     * REST interface returns 503, web interface redirects to /maintenance.xhtml or /admin/maintenance.xhtml
     */
    private Boolean maintenanceMode = false;

    /**
     * The static JAXBContext used for JAXB deserialization.
     */
    private JAXBContext jaxbContext;

    /**
     * A list of active LDAP Providers.
     */
    private Map<Integer, LdapProvider> ldapProviders;

    /**
     * A list of active ADFS Providers.
     */
    private Map<Integer, AdfsProvider> adfsProviders;

    /**
     * A list of active SQL Providers.
     */
    private Map<Integer, SqlProvider> sqlProviders;

    /**
     * A list of *all* external identity providers.
     */
    private List<IdentityProvider> identityProviders;

    /**
     * A list of all active external identity providers, as well as the local identity provider, is enabled.
     */
    private List<IdentityProvider> activeIdentityProviders;

    /**
     * The current password policy.
     */
    private PasswordPolicy policy = null;

    /**
     * The shibboleth configuration
     */
    private Shibboleth shibboleth = null;

    /**
     * The local identity provider. This application itself.
     */
    private LdapProvider localIdentityProvider = null;

    /**
     * The contact information that is show on the login page and the index page.
     */
    private String contactInformation = "";

    /**
     * The MailSending confguration. Is loaded from the file "auth.mail.xml".
     */
    private MailSending mailSending;

    /**
     * If true, all users must accept the EULA
     */
    private boolean eulaEnabled;

    /**
     * The EULA HTML text
     */
    private String eula;

    /**
     * If true, this application does not enforce the security constraints on "/admin/". The administrator must do so
     * himself, e.g. through an apache web server acting as a reverse proxy.
     */
    private boolean externalAuthorization;

    /**
     * If false, the login for local accounts has been disabled.
     */
    private boolean localAccountsEnabled;

    /**
     * If true, the authentication via client certificates is enabled.
     */
    private boolean clientCertificatesEnabled;

    /**
     * If true, the login via client certificate button on the main page is not shown. The reverse proxy needs to be configured properly.
     */
    private boolean hideClientCertificateButton;

    /**
     * Admin email (to send debug information)
     */
    private String adminEmail;

    /**
     * Returns the JAXBContext. Creates one if necessary.
     *
     * @return The JAXBContext
     * @throws JAXBException
     */
    private synchronized static JAXBContext getJAXBContext() throws JAXBException {
        if(instance.jaxbContext == null) {
            instance.jaxbContext = JAXBContext.newInstance(ObjectFactory.class, de.samply.common.mailing.ObjectFactory.class);
        }
        return instance.jaxbContext;
    }

    /**
     * Initializes the configuration. Sets maintenance to true, if the database version are not compatible.
     *
     * @param fallback the fallback folder
     * @throws java.io.FileNotFoundException if any.
     * @throws javax.xml.bind.JAXBException if any.
     * @throws org.xml.sax.SAXException if any.
     * @throws javax.xml.parsers.ParserConfigurationException if any.
     * @throws de.samply.sdao.DAOException if any.
     */
    public static void initialize(String projectName, String fallback) throws FileNotFoundException, JAXBException,
            SAXException, ParserConfigurationException, DAOException {
        Postgresql psql = JAXBUtil.findUnmarshall(POSTGRES_FILE, getJAXBContext(), Postgresql.class, projectName, fallback);
        OAuth2Provider providerConfig = JAXBUtil.findUnmarshall(OAUTH2_FILE, getJAXBContext(), OAuth2Provider.class, projectName, fallback);
        MailSending mailSending = JAXBUtil.findUnmarshall(MAIL_FILE, getJAXBContext(), MailSending.class, projectName, fallback);

        synchronized (instance) {
            instance.projectName = projectName;
            instance.fallbackFolder = fallback;
            instance.mailSending = mailSending;

            instance.database = psql.getDatabase();
            instance.host = psql.getHost();
            instance.password = psql.getPassword();
            instance.username = psql.getUsername();

            instance.publicKey = KeyLoader.loadKey(providerConfig.getPublicKey());
            instance.privateKey = KeyLoader.loadPrivateKey(providerConfig.getPrivateKey());
            instance.issuer = providerConfig.getIssuer();

            try (AuthConnection auth = ConnectionFactory.get()) {
                if (!auth.isInstalled()) {
                    logger.info("Database is not installed correctly or at all. Attempting to create the tables");

                    InputStreamReader reader = new InputStreamReader(AuthConfig.class.getResourceAsStream("/sql/auth.sql"), StandardCharsets.UTF_8);
                    auth.executeStream(reader);

                    Upgrader upgrade = new Upgrader(auth);
                    upgrade.upgrade(false);

                    auth.commit();
                }

                reloadConfig(auth);
            }
        }
    }

    /**
     * Reloads the configuration from the database. Checks if the database version
     * is equal to the version required to run this application properly.
     *
     * @param auth an auth database connection
     * @throws de.samply.sdao.DAOException if any.
     */
    public static void reloadConfig(AuthConnection auth) throws DAOException {
        synchronized (instance) {
            JSONResource config = auth.getConfig();
            instance.dbVersion = config.getProperty(Vocabulary.DB_VERSION).asInteger();

            /**
             * Load the external authorization configuration, if possible, and only after that check the database version.
             * If you remove this line, it is not possible to update the database with enabled external authorization.
             */
            if(config.getDefinedProperties().contains(Vocabulary.EXTERNAL_AUTHORIZATION)) {
                instance.externalAuthorization = config.getProperty(Vocabulary.EXTERNAL_AUTHORIZATION).asBoolean();
            }

            if (instance.dbVersion != AuthConnection.requiredVersion) {
                logger.error("The database versions are not compatible. Starting in maintenance mode. Required Version: "
                        + AuthConnection.requiredVersion + ", current version: " + instance.dbVersion);
                instance.maintenanceMode = true;
                return;
            } else {
                instance.maintenanceMode = false;
            }

            /**
             * Get all booleans and values from the configuration database.
             */
            instance.registrationEnabled = config.getProperty(Vocabulary.REGISTRATION_ENABLED).asBoolean();
            instance.restRegistrationEnabled = config.getProperty(Vocabulary.REST_REGISTRATION_ENABLED).asBoolean();
            instance.defaultUserEnabled = config.getProperty(Vocabulary.DEFAULT_USER_ENABLED).asBoolean();
            instance.verifyEmailEnabled = config.getProperty(Vocabulary.VERIFY_EMAIL_ENABLED).asBoolean();
            instance.verificationRequired = config.getProperty(Vocabulary.VERIFICATION_REQUIRED).asBoolean();
            instance.contactInformation = config.getProperty(Vocabulary.CONTACT_INFO).getValue();

            instance.externalAuthorization = config.getProperty(Vocabulary.EXTERNAL_AUTHORIZATION).asBoolean();
            instance.eulaEnabled = config.getProperty(Vocabulary.EULA_ENABLED).asBoolean();

            instance.policy = PasswordPolicy.fromJSON(config.getProperty(Vocabulary.PASSWORD_POLICY).asJSONResource());
            instance.shibboleth = Shibboleth.fromJSON(config.getProperty(Vocabulary.SHIBBOLETH).asJSONResource());

            instance.localAccountsEnabled = config.getProperty(Vocabulary.LOCAL_ACCOUNTS_ENABLED).asBoolean();
            instance.clientCertificatesEnabled = config.getProperty(Vocabulary.CLIENT_CERTIFICATE_ENABLED).asBoolean();
            instance.hideClientCertificateButton = config.getProperty(Vocabulary.HIDE_CLIENT_CERTIFICATE_BUTTON).asBoolean();

            instance.ldapProviders = new HashMap<>();
            instance.adfsProviders = new HashMap<>();
            instance.sqlProviders = new HashMap<>();
            instance.activeIdentityProviders = new ArrayList<>();

            instance.identityProviders = auth.get(ProviderDAO.class).getAllProviders();

            for (IdentityProvider ip : instance.identityProviders) {
                if(ip.isActive()) {
                    instance.activeIdentityProviders.add(ip);
                }

                if (ip instanceof LdapProvider) {
                    instance.ldapProviders.put(ip.getId(), (LdapProvider) ip);
                } else if (ip instanceof AdfsProvider) {
                    instance.adfsProviders.put(ip.getId(), (AdfsProvider) ip);
                } else if (ip instanceof SqlProvider) {
                    instance.sqlProviders.put(ip.getId(), (SqlProvider) ip);
                }
            }

            instance.localIdentityProvider = new LdapProvider();
            instance.localIdentityProvider.setId(0);
            instance.localIdentityProvider.setLabel("Local");
            instance.localIdentityProvider.setDefaultUserEnabled(instance.defaultUserEnabled);
            instance.localIdentityProvider.setActive(instance.localAccountsEnabled);

            instance.ldapProviders.put(0, instance.localIdentityProvider);

            if(instance.localIdentityProvider.isActive()) {
                instance.activeIdentityProviders.add(instance.localIdentityProvider);
            }

            String logoBase64 = auth.get(ConfigDAO.class).get(Vocabulary.LOGO).getProperty(Vocabulary.IMAGE_DATA).getValue();
            ImageServlet.setLogo(Base64.decodeBase64(logoBase64));

            instance.eula = auth.get(ConfigDAO.class).get(Vocabulary.EULA).getProperty(Vocabulary.EULA).getValue();
            instance.adminEmail = auth.get(ConfigDAO.class).get(Vocabulary.ADMIN).getProperty(Vocabulary.EMAIL).getValue();
        }
    }

    /**
     * Returns a human readable string of the identity provider for the given user.
     * @param user the user
     * @return
     */
    public static String getProviderLabel(User user) {
        if(user.isShibbolethUser()) {
            return getShibboleth().getName();
        } else {
            return getIdentityProvider(user.getProviderId()).getLabel();
        }
    }

    /**
     * Returns the identity provider with the given ID.
     * @param id
     * @return
     */
    public static IdentityProvider getIdentityProvider(int id) {
        if(instance.ldapProviders.containsKey(id)) {
            return instance.ldapProviders.get(id);
        } else if(instance.adfsProviders.containsKey(id)) {
            return instance.adfsProviders.get(id);
        } else if(instance.sqlProviders.containsKey(id)) {
            return instance.sqlProviders.get(id);
        } else {
            return null;
        }
    }

    /**
     * Returns the LDAP provider with the given ID, null if it does not exist.
     * @param id
     * @return
     */
    public static LdapProvider getLdapProvider(int id) {
        return instance.ldapProviders.get(id);
    }

    /**
     * Returns the adfs provider with the given ID, null if it does not exist.
     * @param id
     * @return
     */
    public static AdfsProvider getAdfsProvider(int id) {
        return instance.adfsProviders.get(id);
    }

    /**
     * Returns the sql provider with the given ID, null if it does not exist.
     * @param id
     * @return
     */
    public static SqlProvider getSqlProvider(int id) {
        return instance.sqlProviders.get(id);
    }

    /**
     * <p>Getter for the field <code>registrationEnabled</code>.</p>
     *
     * @return the registrationEnabled
     */
    public static Boolean getRegistrationEnabled() {
        return instance.registrationEnabled;
    }

    /**
     * <p>Getter for the field <code>defaultUserEnabled</code>.</p>
     *
     * @return the defaultUserEnabled
     */
    public static Boolean getDefaultUserEnabled() {
        return instance.defaultUserEnabled;
    }

    /**
     * <p>Getter for the field <code>restRegistrationEnabled</code>.</p>
     *
     * @return the restRegistrationEnabled
     */
    public static Boolean getRestRegistrationEnabled() {
        return instance.restRegistrationEnabled;
    }

    /**
     * <p>Getter for the field <code>dbVersion</code>.</p>
     *
     * @return the dbVersion
     * @since 1.4.0
     */
    public static Integer getDbVersion() {
        return instance.dbVersion;
    }

    /**
     * <p>Getter for the field <code>verifyEmailEnabled</code>.</p>
     *
     * @return the verifyEmail
     * @since 1.4.0
     */
    public static Boolean getVerifyEmailEnabled() {
        return instance.verifyEmailEnabled;
    }

    /**
     * <p>Getter for the field <code>verificationRequired</code>.</p>
     *
     * @return the verifyRequiredEnabled
     * @since 1.4.0
     */
    public static Boolean getVerificationRequired() {
        return instance.verificationRequired;
    }

    /**
     * <p>Getter for the field <code>maintenanceMode</code>.</p>
     *
     * @return the maintenanceMode
     * @since 1.4.0
     */
    public static Boolean getMaintenanceMode() {
        return instance.maintenanceMode;
    }

    /**
     * <p>Getter for the field <code>publicKey</code>.</p>
     *
     * @return the publicKey
     */
    public static PublicKey getPublicKey() {
        return instance.publicKey;
    }

    /**
     * <p>Getter for the field <code>privateKey</code>.</p>
     *
     * @return the privateKey
     */
    public static PrivateKey getPrivateKey() {
        return instance.privateKey;
    }

    /**
     * <p>Getter for the field <code>host</code>.</p>
     *
     * @return the host
     * @since 1.4.0
     */
    public static String getHost() {
        return instance.host;
    }

    /**
     * <p>Getter for the field <code>database</code>.</p>
     *
     * @return the database
     * @since 1.4.0
     */
    public static String getDatabase() {
        return instance.database;
    }

    /**
     * <p>Getter for the field <code>username</code>.</p>
     *
     * @return the username
     * @since 1.4.0
     */
    public static String getUsername() {
        return instance.username;
    }

    /**
     * <p>Getter for the field <code>password</code>.</p>
     *
     * @return the password
     * @since 1.4.0
     */
    public static String getPassword() {
        return instance.password;
    }

    /**
     * @return the issuer
     */
    public static String getIssuer() {
        return instance.issuer;
    }

    /**
     * Returns the prefix for the subject, e.g. "https://auth.samply.de/users/"
     * @return
     */
    public static String getSubjectPrefix() {
        if(instance.issuer.endsWith("/")) {
            return instance.issuer + "users/";
        } else {
            return instance.issuer + "/users/";
        }
    }

    /**
     * @return the ldapProviders
     */
    public static Collection<LdapProvider> getLdapProviders() {
        return instance.ldapProviders.values();
    }

    /**
     * @return the adfsProviders
     */
    public static Collection<AdfsProvider> getAdfsProviders() {
        return instance.adfsProviders.values();
    }

    /**
     * @return the SqlProviders
     */
    public static Collection<SqlProvider> getSqlProviders() {
        return instance.sqlProviders.values();
    }

    /**
     * @return the identityProviders
     */
    public static List<IdentityProvider> getIdentityProviders() {
        return instance.identityProviders;
    }

    /**
     * @return the policy
     */
    public static PasswordPolicy getPolicy() {
        return instance.policy;
    }

    /**
     * @return the shibboleth
     */
    public static Shibboleth getShibboleth() {
        return instance.shibboleth;
    }

    /**
     * @return
     */
    public static IdentityProvider getLocalIdentityProvider() {
        return instance.localIdentityProvider;
    }

    /**
     * @return the contactInformation
     */
    public static String getContactInformation() {
        return instance.contactInformation;
    }

    public static MailSending getMailSending() {
        return instance.mailSending;
    }

    public static String getFallbackFolder() {
        return instance.fallbackFolder;
    }

    public static boolean isExternalAuthorization() {
        return instance.externalAuthorization;
    }

    public static boolean isEulaEnabled() {
        return instance.eulaEnabled;
    }

    public static String getEula() {
        return instance.eula;
    }

    public static boolean getLocalAccountsEnabled() {
        return instance.localAccountsEnabled;
    }

    public static List<IdentityProvider> getActiveIdentityProviders() {
        return instance.activeIdentityProviders;
    }

    public static String getProjectName() {
        return instance.projectName;
    }

    public static boolean isClientCertificatesEnabled() {
        return instance.clientCertificatesEnabled;
    }

    public static boolean isHideClientCertificateButton() {
        return instance.hideClientCertificateButton;
    }

    /**
     * @return the admin email
     */
    public static String getAdminEmail() {
        return instance.adminEmail;
    }
}
