/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.json.JSONResource;

/**
 * This class describes some basic requirements for passwords like
 * a minimal length.
 */
public class PasswordPolicy {

    public static PasswordPolicy fromJSON(JSONResource json) {
        PasswordPolicy compl = new PasswordPolicy();
        compl.setMinLength(json.getProperty(Vocabulary.PasswordPolicy.MIN_LENGTH).asInteger());
        compl.setMinLowerCase(json.getProperty(Vocabulary.PasswordPolicy.MIN_LOWER_CASE).asInteger());
        compl.setMinNumber(json.getProperty(Vocabulary.PasswordPolicy.MIN_NUMBERS).asInteger());
        compl.setMinSpecial(json.getProperty(Vocabulary.PasswordPolicy.MIN_SPECIAL_CHARACTERS).asInteger());
        compl.setMinUpperCase(json.getProperty(Vocabulary.PasswordPolicy.MIN_UPPER_CASE).asInteger());
        return compl;
    }

    /**
     * The minimum length of the password
     */
    private int minLength = 10;

    /**
     * The minimum number of special characters
     */
    private int minSpecial = 0;

    /**
     * The minimum number of numbers
     */
    private int minNumber = 0;

    /**
     * The minimum number of lower case letters
     */
    private int minLowerCase = 0;

    /**
     * The minimum number of upper case letters
     */
    private int minUpperCase = 0;

    /**
     * @return the minLength
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * @param minLength the minLength to set
     */
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    /**
     * @return the minSpecialCharacters
     */
    public int getMinSpecial() {
        return minSpecial;
    }

    /**
     * @param minSpecialCharacters the minSpecialCharacters to set
     */
    public void setMinSpecial(int minSpecialCharacters) {
        this.minSpecial = minSpecialCharacters;
    }

    /**
     * @return the minNumbers
     */
    public int getMinNumber() {
        return minNumber;
    }

    /**
     * @param minNumbers the minNumbers to set
     */
    public void setMinNumber(int minNumbers) {
        this.minNumber = minNumbers;
    }

    /**
     * @return the minLowerCase
     */
    public int getMinLowerCase() {
        return minLowerCase;
    }

    /**
     * @param minLowerCase the minLowerCase to set
     */
    public void setMinLowerCase(int minLowerCase) {
        this.minLowerCase = minLowerCase;
    }

    /**
     * @return the minUpperCase
     */
    public int getMinUpperCase() {
        return minUpperCase;
    }

    /**
     * @param minUpperCase the minUpperCase to set
     */
    public void setMinUpperCase(int minUpperCase) {
        this.minUpperCase = minUpperCase;
    }

}
