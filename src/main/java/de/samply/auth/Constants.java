/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

/**
 *
 */
public class Constants {

    /**
     * The Tomcat Role that gives the user access to the admin interface
     */
    public static final String EXTERNAL_ADMIN_ROLE = "auth-admin";

    /**
     * The internal client ID
     */
    public static final String AUTH_CLIENT_ID = "auth";

    /**
     * The permission identifier used to give access to clients that use a whitelist.
     */
    public static final String PERMISSION_USE = "use";

    /**
     * The permission identifier used to give a regular user access to the admin interface.
     */
    public static final String PERMISSION_ADMIN = "admin";

    /**
     * The OpenID scope constant. This scope is required.
     */
    public static final String OPENID_SCOPE = "openid";

    /**
     * The name of the authentication cookie.
     */
    public static final String AUTH_COOKIE = "authCookie";

    /**
     * The "Shib-Identity-Provider" HTTP Header used to get the shibboleth identity provider identifier.
     */
    public static final String SHIB_IDENTITY_PROVIDER = "Shib-Identity-Provider";

    /**
     * The header which contains the client certificate
     */
    public static final String HEADER_SSL_CLIENT_CERT = "SSL_CLIENT_CERT";

    /**
     * The header which contains the client certificate verification
     */
    public static final String HEADER_SSL_CLIENT_VERIFY = "SSL_CLIENT_VERIFY";

    /**
     * The value for a successful authentication via client certificate
     */
    public static final String SSL_CLIENT_VERIFY_SUCCESS = "SUCCESS";


}
