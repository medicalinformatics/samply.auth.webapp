/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import de.samply.auth.dao.Vocabulary;
import de.samply.sdao.json.JSONResource;

import java.io.Serializable;

/**
 * The shibboleth configuration object. Contains all necessary data for Shibboleth
 */
public class Shibboleth implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * If true, the external Shibboleth Identity Provider is enabled.
     */
    private boolean active;

    /**
     * The attribute that is used to identify a user
     */
    private String identityAttribute;

    /**
     * The attributes that describes the display name of the user
     */
    private String nameAttribute;

    /**
     * The attributes that describes the first name of the user
     */
    private String firstNameAttribute;

    /**
     * The attributes that describes the last name of the user
     */
    private String lastNameAttribute;

    /**
     * The attribute that describes the email of the user
     */
    private String mailAttribute;

    /**
     * If true, new users are enabled by default
     */
    private boolean defaultActive;

    /**
     * If true, the email of new users are assumed as verified
     */
    private boolean defaultVerified;

    /**
     * The name of this Shibboleth network, e.g. "DFN-AAI"
     */
    private String name;

    /**
     * If true, this application will try to use HTTP-headers instead of AJP variables
     */
    private boolean useHttpHeaders;

    public static Shibboleth fromJSON(JSONResource json) {
        Shibboleth shib = new Shibboleth();
        shib.setActive(json.getProperty(Vocabulary.Shibboleth.ACTIVE).asBoolean());
        shib.setDefaultActive(json.getProperty(Vocabulary.Shibboleth.DEFAULT_ACTIVE).asBoolean());
        shib.setDefaultVerified(json.getProperty(Vocabulary.Shibboleth.DEFAULT_VERIFIED).asBoolean());
        shib.setIdentityAttribute(json.getProperty(Vocabulary.Shibboleth.IDENTITY_ATTRIBUTE).getValue());
        shib.setNameAttribute(json.getProperty(Vocabulary.Shibboleth.NAME_ATTRIBUTE).getValue());
        shib.setFirstNameAttribute(json.getProperty(Vocabulary.Shibboleth.FIRST_NAME_ATTRIBUTE).getValue());
        shib.setLastNameAttribute(json.getProperty(Vocabulary.Shibboleth.LAST_NAME_ATTRIBUTE).getValue());
        shib.setMailAttribute(json.getProperty(Vocabulary.Shibboleth.MAIL_ATTRIBUTE).getValue());
        shib.setName(json.getProperty(Vocabulary.Shibboleth.NAME).getValue());
        shib.setUseHttpHeaders(json.getProperty(Vocabulary.Shibboleth.USE_HTTP_HEADERS).asBoolean());
        return shib;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the identityAttribute
     */
    public String getIdentityAttribute() {
        return identityAttribute;
    }

    /**
     * @param identityAttribute the identityAttribute to set
     */
    public void setIdentityAttribute(String identityAttribute) {
        this.identityAttribute = identityAttribute;
    }

    /**
     * @return the nameAttribute
     */
    public String getNameAttribute() {
        return nameAttribute;
    }

    /**
     * @return the firstNameAttribute
     */
    public String getFirstNameAttribute() {
        return firstNameAttribute;
    }

    /**
     * @return the lastNameAttribute
     */
    public String getLastNameAttribute() {
        return lastNameAttribute;
    }

    /**
     * @param nameAttribute the nameAttribute to set
     */
    public void setNameAttribute(String nameAttribute) {
        this.nameAttribute = nameAttribute;
    }

    /**
     * @param firstNameAttribute the firstNameAttribute to set
     */
    public void setFirstNameAttribute(String firstNameAttribute) {
        this.firstNameAttribute = firstNameAttribute;
    }

    /**
     * @param lastNameAttribute the lastNameAttribute to set
     */
    public void setLastNameAttribute(String lastNameAttribute) {
        this.lastNameAttribute = lastNameAttribute;
    }

    /**
     * @return the mailAttribute
     */
    public String getMailAttribute() {
        return mailAttribute;
    }

    /**
     * @param mailAttribute the mailAttribute to set
     */
    public void setMailAttribute(String mailAttribute) {
        this.mailAttribute = mailAttribute;
    }

    /**
     * @return the defaultActive
     */
    public boolean isDefaultActive() {
        return defaultActive;
    }

    /**
     * @param defaultActive the defaultActive to set
     */
    public void setDefaultActive(boolean defaultActive) {
        this.defaultActive = defaultActive;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the useHttpHeaders
     */
    public boolean isUseHttpHeaders() {
        return useHttpHeaders;
    }

    /**
     * @param useHttpHeaders the useHttpHeaders to set
     */
    public void setUseHttpHeaders(boolean useHttpHeaders) {
        this.useHttpHeaders = useHttpHeaders;
    }

    public boolean isDefaultVerified() {
        return defaultVerified;
    }

    public void setDefaultVerified(boolean defaultVerified) {
        this.defaultVerified = defaultVerified;
    }
}
