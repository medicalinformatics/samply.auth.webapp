/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 * Helper class to create JSF messages from bundles
 *
 */
public class Message {

    /**
     * <p>Adds an error. If keep is true, the message will survive a redirect.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param keep a boolean.
     * @since 1.4.0
     */
    public static void addError(String name, boolean keep) {
        add(name, keep, FacesMessage.SEVERITY_ERROR);
    }

    /**
     * <p>Adds an error.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public static void addError(String name) {
        add(name, false, FacesMessage.SEVERITY_ERROR);
    }

    /**
     * <p>Adds an info. If keep is true, the message will survive a redirect.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param keep a boolean.
     * @since 1.4.0
     */
    public static void addInfo(String name, boolean keep) {
        add(name, keep, FacesMessage.SEVERITY_INFO);
    }

    /**
     * <p>Adds an error.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public static void addInfo(String name) {
        add(name, false, FacesMessage.SEVERITY_INFO);
    }

    /**
     * Adds an info for the given component
     * @param componentId
     * @param name
     */
    public static void addInfo(String componentId, String name) {
        add(componentId, name, false, FacesMessage.SEVERITY_INFO);
    }

    /**
     * <p>Adds an message with the specified severity.</p>
     *
     * @param name a {@link String} object.
     * @param severity a {@link Severity} object.
     */
    public static void add(String name, Severity severity) {
        add(name, false, severity);
    }

    /**
     * <p>Adds an message with the specified severity. If keep is true, the message will survive a redirect.</p>
     *  @param name a {@link String} object.
     * @param keep a boolean.
     * @param severity a {@link Severity} object.
     */
    public static void add(String name, boolean keep, Severity severity, Object... params) {
        add(null, name, keep, severity, params);
    }

    /**
     * Adds the message with the given namen for the given component.
     * @param componentId
     * @param name
     * @param keep
     * @param severity
     * @param params
     */
    public static void add(String componentId, String name, boolean keep, Severity severity, Object... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");

        String msg = bundle.getString(name);
        String detail;
        try {
            detail = MessageFormat.format(bundle.getString(name + "Detail"), params);
        } catch(MissingResourceException e) {
            detail = msg;
        }

        addMessage(componentId, msg, detail, keep, severity);
    }

    /**
     * <p>Sets the keepMessages flag to true.</p>
     *
     * @since 1.4.0
     */
    public static void keepMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    /**
     * <p>Returns the translated string.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @since 1.4.0
     */
    public static String getString(String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
        return bundle.getString(name);
    }

    /**
     * Adds the given title and message with the given parameters to the JSF message list.
     * @param title
     * @param message
     * @param keep
     * @param severity
     */
    public static void addMessage(String title, String message, boolean keep, Severity severity) {
        addMessage(null, title, message, keep, severity);
    }

    /**
     * Adds the given title and message with the given parameters to the JSF message list for the given component ID.
     * @param componentId
     * @param title
     * @param message
     * @param keep
     * @param severity
     */
    public static void addMessage(String componentId, String title, String message, boolean keep, Severity severity) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(componentId, new FacesMessage(severity, title, message));

        if(keep) {
            keepMessages();
        }
    }
}
