/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.mail;

import java.io.FileNotFoundException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.samply.auth.AuthConfig;
import de.samply.auth.dao.AuthConnection;
import de.samply.auth.dao.ConnectionFactory;
import de.samply.auth.dao.UserDAO;
import de.samply.auth.dao.dto.Invitation;
import de.samply.auth.dao.dto.ShibbolethVerification;
import de.samply.auth.dao.dto.User;
import de.samply.auth.rest.ServletUtil;
import de.samply.common.mailing.EmailBuilder;
import de.samply.common.mailing.MailSender;
import de.samply.common.mailing.MailSending;
import de.samply.common.mailing.OutgoingEmail;
import de.samply.config.util.FileFinderUtil;
import de.samply.sdao.DAOException;
import java.util.*;
import java.text.*;

/**
 * Some methods for mails
 *
 * @since 1.4.0
 */
public class MailUtils {

    private static Logger logger = LoggerFactory.getLogger(MailUtils.class);

    /**
     * Sends the shibboleth verification email in case the identity provider does not send all attributes.
     * @param verification
     */
    public static void sendShibbolethVerification(ShibbolethVerification verification) throws FileNotFoundException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        String url = ServletUtil.getLocalRedirectUrl(context.getRequestScheme(), context.getRequestServerName(),
                context.getRequestServerPort(), context.getRequestContextPath(), "/shibbolethVerification.xhtml?verificationUuid=" + verification.getUuid().toString());

        MailSending mailSending = AuthConfig.getMailSending();
        EmailBuilder builder = initializeBuilder(mailSending);

        builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "shibbolethVerification.soy",
                AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "Verification");

        OutgoingEmail email = new OutgoingEmail();
        email.setSubject(de.samply.auth.Message.getString("emailVerificationSubject"));
        email.setLocale("en");
        email.setBuilder(builder);
        email.putParameter("url", url);
        email.addAddressee(verification.getEmail());

        sendEmail(email);
    }

    /**
     * Sends the reactivation email for the given user.
     * @param user
     * @return
     * @throws DAOException
     */
    public static boolean resendActivationMail(User user) throws DAOException {
        try(AuthConnection auth = ConnectionFactory.get()) {
            return resendActivationMail(auth, user);
        }
    }

    /**
     * Sends the reactivation email for the given user using the given connection.
     * @param auth
     * @param user
     * @return
     * @throws DAOException
     */
    public static boolean resendActivationMail(AuthConnection auth, User user) throws DAOException {
        UserDAO userDAO = auth.get(UserDAO.class);

        try {
            if (user != null) {
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

                String url = ServletUtil.getLocalRedirectUrl(context.getRequestScheme(), context.getRequestServerName(),
                        context.getRequestServerPort(), context.getRequestContextPath(), "/activate.xhtml?code=" + userDAO.getActivationCode(user));

                MailSending mailSending = AuthConfig.getMailSending();
                EmailBuilder builder = initializeBuilder(mailSending);

                builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "verification.soy",
                        AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "Verification");

                OutgoingEmail email = new OutgoingEmail();
                email.setSubject(de.samply.auth.Message.getString("emailVerificationSubject"));
                email.setLocale("en");
                email.setBuilder(builder);
                email.putParameter("url", url);
                email.addAddressee(user.getEmail());

                sendEmail(email);

                auth.commit();
                return true;
            }
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Sends the given invitation as email.
     * @param invitation
     */
    public static void sendInviteLink(Invitation invitation) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        String url = ServletUtil.getLocalRedirectUrl(context.getRequestScheme(), context.getRequestServerName(),
                context.getRequestServerPort(), context.getRequestContextPath(), "/invitation.xhtml?invitation=" + invitation.getUuid());

        try {
            MailSending mailSending = AuthConfig.getMailSending();
            EmailBuilder builder = initializeBuilder(mailSending);

            builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "invitation.soy",
                    AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "Invitation");

            OutgoingEmail email = new OutgoingEmail();
            email.setSubject(de.samply.auth.Message.getString("emailInvitationSubject"));
            email.setLocale("en");
            email.setBuilder(builder);
            email.putParameter("url", url);
            email.putParameter("name", invitation.getFirstName()+" "+invitation.getLastName());
            email.addAddressee(invitation.getEmail());

            sendEmail(email);
        } catch(FileNotFoundException e) {
            logger.error("Exception: " + e.getMessage(), e);
        }
    }

    /**
     * Sends a passwort recovery mail to the given user.
     * @param user
     * @throws DAOException
     */
    public static void sendRecoveryMail(User user) throws DAOException {
        try (AuthConnection auth = ConnectionFactory.get()) {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

            String url = ServletUtil.getLocalRedirectUrl(context.getRequestScheme(), context.getRequestServerName(),
                    context.getRequestServerPort(), context.getRequestContextPath(),
                    "/reset.xhtml?code=" + auth.get(UserDAO.class).getNewRecoverCode(user));

            MailSending mailSending = AuthConfig.getMailSending();
            EmailBuilder builder = initializeBuilder(mailSending);

            builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "recovery.soy",
                    AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "Recovery");

            OutgoingEmail email = new OutgoingEmail();
            email.setSubject(de.samply.auth.Message.getString("emailRecoverySubject"));
            email.setLocale("en");
            email.setBuilder(builder);
            email.putParameter("url", url);
            email.addAddressee(user.getEmail());

            sendEmail(email);
            auth.commit();
        } catch(FileNotFoundException e) {
            logger.error("Exception: " + e.getMessage(), e);
        }
    }

    /**
     * Send an email
     *
     * @param email The outgoing email.
     */
    public static void sendEmail(OutgoingEmail email) {
        MailSender mailSender = new MailSender(AuthConfig.getMailSending());
        mailSender.send(email);

        logger.info("Email successfully sent to " + email.getAddressees().get(0));
    }

    /**
     * Initializes an EmailBuilder with the two default Soy-Templates: main.soy and footer.soy.
     * @param mailSending
     * @return
     * @throws FileNotFoundException
     */
    private static EmailBuilder initializeBuilder(MailSending mailSending) throws FileNotFoundException {
        EmailBuilder builder = new EmailBuilder(mailSending.getTemplateFolder(), false);

        builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "main.soy",
                AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), null);
        builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "footer.soy",
                AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "Footer");
        return builder;
    }

    /**
     * Sends a passwort recovery mail to the given user.
     * @param adminEmail
     * @param headerValues
     * @throws DAOException
     */
    public static void sendShibbolethTestMail(String adminEmail, Map headerValues) {
        StringBuilder debugInfo = new StringBuilder();
        try {
            MailSending mailSending = AuthConfig.getMailSending();
            EmailBuilder builder = initializeBuilder(mailSending);

            builder.addTemplateFile(FileFinderUtil.findFile(mailSending.getTemplateFolder() + "shibbolethTest.soy",
                    AuthConfig.getProjectName(), AuthConfig.getFallbackFolder()), "ShibbolethTest");

            // Timestamp for email subject
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());

            // Timestamp for login trial time (extracted from HTTP headers)
            String loginTime;
            try {
                loginTime = headerValues.get("shib-authentication-instant").toString();
            } catch(NullPointerException e) {
                loginTime = "";
            }

            // Debug information (all attributes) for email content
            List<String> headerNames = new ArrayList<String> (headerValues.keySet());
            for (String headerName : headerNames) {

                debugInfo.append('{');
                debugInfo.append(headerName);
                debugInfo.append("}: ");
                debugInfo.append(headerValues.get(headerName));
                debugInfo.append("\n\n");

            }

            OutgoingEmail email = new OutgoingEmail();
            email.setSubject(de.samply.auth.Message.getString("emailShibbolethTestSubject")+ " from " + timeStamp);
            email.setLocale("en");
            email.setBuilder(builder);
            email.putParameter("debugInfo", debugInfo.toString());
            email.putParameter("timestamp", loginTime);
            email.putParameter("admin", "admin");
            email.addAddressee(adminEmail);

            sendEmail(email);
        } catch(FileNotFoundException e) {
            logger.error("Exception: " + e.getMessage(), e);
        }
    }

}
